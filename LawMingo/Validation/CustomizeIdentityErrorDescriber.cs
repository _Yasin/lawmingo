﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Validation
{
    public class CustomizeIdentityErrorDescriber : IdentityErrorDescriber
    {
        //public override IdentityError PasswordRequiresUpper()
        //{
        //    return new IdentityError
        //    {
        //        Code = nameof(PasswordRequiresUpper),
        //        Description = "رمز عبور حداقل باید شامل یک حرف انگلیسی بزرگ باشد ."
        //    };
        //}
        public override IdentityError DuplicateEmail(string email)           
        {
            return new IdentityError
            {
                Code = nameof(DuplicateEmail),
                Description = "ایمیل وارد شده تکراری است ."
            };
        }
        public override IdentityError DuplicateUserName(string userName)
        {
            return new IdentityError
            {
                Code = nameof(DuplicateUserName),
                Description = "شماره موبایل وارد شده قبلا ثبت شده است ."
            };
        }
        public override IdentityError InvalidRoleName(string role)
      
        {
            return new IdentityError
            {
                Code = nameof(InvalidRoleName),
                Description = "نقش وارد شده نامعتبر است ."
            };
        }
        //public override IdentityError PasswordRequiresDigit()     
        //{
        //    return new IdentityError
        //    {
        //        Code = nameof(PasswordRequiresDigit),
        //        Description = "رمز عبور حداقل باید شامل یک عدد باشد ."
        //    };
        //}
        //public override IdentityError PasswordRequiresLower()      
        //{
        //    return new IdentityError
        //    {
        //        Code = nameof(PasswordRequiresLower),
        //        Description = "رمز عبور حداقل باید شامل یک حرف انگلیسی کوچک باشد ."
        //    };
        //}

        //public override IdentityError PasswordRequiresNonAlphanumeric()     
        //{
        //    return new IdentityError
        //    {
        //        Code = nameof(PasswordRequiresNonAlphanumeric),
        //        Description = "رمز عبور باید شامل اعداد، حروف و کاراکتر های خاص باشد ."
        //    };
        //}

        public override IdentityError PasswordTooShort(int length)
        {
            return new IdentityError
            {
                Code = nameof(PasswordRequiresNonAlphanumeric),
                Description = "رمز عبور حداقل باید دارای 6 کاراکتر باشد . "
            };
          
        }
       

        //public override IdentityError PasswordRequiresUniqueChars(int uniqueChars)   
        //{
        //    return new IdentityError
        //    {
        //        Code = nameof(PasswordRequiresUniqueChars),
        //        Description = "رمز عبور حداقل باید شامل یک کاراکتر خاص مانند  @,#,$,%,...  باشد ."
        //    };
        //}
        public override IdentityError InvalidToken()       
        {
            return new IdentityError
            {
                Code = nameof(InvalidToken),
                Description = "توکن امنیتی نامعتبر است !"
            };
        }
        public override IdentityError DuplicateRoleName(string role)       
        {
            return new IdentityError
            {
                Code = nameof(DuplicateRoleName),
                Description = "نقش وارد شده تکراری است ."
            };
        }

        //public override IdentityError InvalidUserName(string userName)     
        //{
        //    return new IdentityError
        //    {
        //        Code = nameof(InvalidUserName),
        //        Description = "در نام کاربری نباید از کاراکتر‌های فارسی استفاده شود . "
        //    };
        //}


    }
}

