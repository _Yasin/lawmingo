﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    
        public bool HasDota2Account(string id)
        {
              return false;
        }

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            foreach (var relationship in modelbuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            //modelbuilder.Entity<Contract>(entity => {
            //    entity.HasIndex(e => e.Name).IsUnique();
            //    entity.HasIndex(e => e.EnglishName).IsUnique();
            //});

            modelbuilder.Entity<ApplicationUser>(entity => {
                entity.HasIndex(e => e.UserName).IsUnique();               
            });


            modelbuilder.Entity<ArticleProperty>(entity => {
                entity.HasIndex(e => e.EnglishName).IsUnique();
                entity.HasIndex(e => e.PropertyName).IsUnique();
            });

            modelbuilder.Entity<Category>(entity => {
                entity.HasIndex(e => e.EnglishName).IsUnique();
                entity.HasIndex(e => e.Name).IsUnique();
            });


            modelbuilder.Entity<Counselor>().HasMany(c => c.Schedules);



            base.OnModelCreating(modelbuilder);
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ArticleDetail> ArticleDetails { get; set; }
        public DbSet<ArticleProperty> ArticleProperties { get; set; }
        public DbSet<Contract> Contract { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }    
        public DbSet<RelatedArticle> RelatedArticles { get; set; }
        public DbSet<UserContract> UserContracts { get; set; }
        public DbSet<UserContractProperty> UserContractProperties { get; set; }
        public DbSet<PaymentLog> PaymentLogs { get; set; }
        public DbSet<LikeDislike> LikeDislikes { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<ContractScore> ContractScores { get; set; }
        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<Help> Helps { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<UserSubscription> UserSubscriptions { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<RelatedPost> RelatedPosts { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<RelatedContract> RelatedContracts { get; set; }
        public DbSet<Complaint> Complaints { get; set; }
        public DbSet<Counselor> Counselors { get; set; }
        public DbSet<ConsultationRequest> ConsultationRequests { get; set; }
        public DbSet<SmsLog> SmsLogs { get; set; }
        public DbSet<ContactUs> Contacts { get; set; }
        public DbSet<ContractRequest> ContractRequests { get; set; }
        public DbSet<Affiliate> Affiliates { get; set; }       
        public DbSet<AffiliateDetail> AffiliateDetails { get; set; }       
        public DbSet<AffiliateGroup> AffiliateGroups { get; set; }       
        public DbSet<AffiliateLog> AffiliateLogs { get; set; }       
        public DbSet<AffiliateTransaction> AffiliateTransactions { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<CounselorCategory> CounselorCategories { get; set; }
        public DbSet<CategoryAttach> CategoryAttachment { get; set; }
        public DbSet<ArtsDetail> ArtsDetails { get; set; }
        public DbSet<signatureShare> signatureShares { get; set; }
        public DbSet<Signature> Signatures { get; set; }

    }
}
