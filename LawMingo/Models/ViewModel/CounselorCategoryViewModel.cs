﻿using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LawMingo.Models.ViewModel
{

    public class CounselorViewModel
    {
        public IQueryable<Counselor> Counselors { get; set; }
        public IEnumerable<Category> CounselorCategories { get; set; }
    }

}