﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.ViewModel
{

    public class ContractSelectedViewModel
    {    
        public int ContractID { get; set; }    
        public string Name { get; set; }     
        public string EnglishName { get; set; }
        public decimal Price { get; set; }
    }

    public class ContractViewModel
    {
        public string CategoryName { get; set; }
        public IEnumerable<ContractSelectedViewModel> contract { get; set; }
    }


    public class ArticleDetailsViewModel
    {     
        public string Content { get; set; }      
    }

    public class ArticledetailViewModel
    {
        public int Order { get; set; }
        public string Article { get; set; }
        public IEnumerable<ArticleDetailsViewModel> ArticleDetail { get; set; }
    }


}
