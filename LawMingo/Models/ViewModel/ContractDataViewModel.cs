﻿using LawMingo.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.ViewModel
{
    public class ContractDataViewModel
    {
        public DomainModel.Contract Contract { get; set; }
        public int CommentCount { get; set; }
        public bool GoToInfo{ get; set; }
        public string[] userAnswers { get; set; }

        public string UserId { get; set; }
        public List<Comment> FAQ { get; set; }
    }
}
