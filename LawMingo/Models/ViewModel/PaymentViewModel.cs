﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static LawMingo.Models.DomainModel.Contract;

namespace LawMingo.Models.ViewModel
{
    public class PaymentViewModel
    {
        public string TrackingCode { get; set; }
        public int? UserContractID { get; set; }
        public int? UserSubscriptionID { get; set; }
        public string ConsultationOrderID { get; set; }
        public bool IsSuccessful { get; set; }
        public string Time { get; set; }
        public decimal Amount { get; set; }
        public string PaymentID { get; set; }
        public string Subscription { get; set; }
        public string Bank { get; set; }
        public ContractType Type { get; set; }
        public string Title { get; set; }
    }
}