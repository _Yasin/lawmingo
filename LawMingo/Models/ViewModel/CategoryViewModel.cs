﻿using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LawMingo.Models.ViewModel
{
    public class CategoryViewModel
    {
      

        [ScaffoldColumn(false)]
        public int CategoryID { get; set; }

        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]
        [MaxLength(100, ErrorMessage = "حداکثر تعداد کاراکتر 100 می باشد .")]
        [Display(Name = "گروه ")]
        public string Name { get; set; }

        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]
        [MaxLength(100, ErrorMessage = "حداکثر تعداد کاراکتر 100 می باشد .")]
        [Display(Name = "نام انگلیسی گروه ")]
        public string EnglishName { get; set; }

        public int? ParentID { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        public DateTime Time { get; set; }

    
        [Key, ForeignKey("ParentID")]

        public virtual Category Parrent { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }


        public ICollection<Category> Children { get; set; }

      
    }
}