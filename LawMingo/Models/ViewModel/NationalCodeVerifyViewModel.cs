﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.ViewModel
{
    public class NationalCodeVerifyViewModel
    {
        public class JsonResponse
        {
            public result result { get; set; }
            public string status { get; set; }
        }
        public class JsonRes
        {
            public Result result { get; set; }
            public string status { get; set; }
            public string trackId { get; set; }
        }
        public class result
        {
            public string value { get; set; }
            public string[] scopes { get; set; }
            public int lifeTime { get; set; }
            public string creationDate { get; set; }
            public string refreshToken { get; set; }
        }
        public class Result
        {
            public bool isValid { get; set; }
        }
    }
}
