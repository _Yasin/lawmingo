﻿using LawMingo.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.ViewModel
{
    public class ContractsViewModel
    {
        public IEnumerable<LawMingo.Models.DomainModel.Contract> Contracts { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public requestContract Request { get; set; }

    }

    public class requestContract
    {

        [Display(Name = "نام و نام خانوادگی")]   
        public string FullName { get; set; }

        [Display(Name = "موبایل")]
        [Required(ErrorMessage = "وارد کردن موبایل الزامی است .")]
        public string Mobile { get; set; }

        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [Display(Name = "نام قرارداد")]
        public string ContractName { get; set; }

        [Display(Name = "توضیحات")]
        public string Description { get; set; }
       
    }
}
