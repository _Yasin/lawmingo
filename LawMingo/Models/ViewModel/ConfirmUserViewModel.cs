﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.ViewModel
{
    public class ConfirmUserViewModel
    {

        [Display(Name = "موبایل")]
        [Required(ErrorMessage = "وارد کردن موبایل الزامی است.")]
        [RegularExpression(pattern: @"^0*?[9]\d{9}$", ErrorMessage = "شماره موبایل وارد شده صحیح نیست .")]
        public string MobilResever { get; set; }


        [Display(Name = "کد ملی")]
        [Required(ErrorMessage = "وارد کردن کد ملی الزامی است.")]
        [StringLength(10, ErrorMessage = "کد ملی وارد شده نامعتبر است. ", MinimumLength = 10)]
        public string NationalCode { get; set; }

    }
}
