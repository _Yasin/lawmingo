﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.ViewModel
{
    public class SmsViewModel
    {
        public class Return
        {
            public int status { get; set; }
            public string message { get; set; }
        }

        public class Entries
        {
            public int messageid { get; set; }
            public string message { get; set; }
            public int status { get; set; }
            public string statustext { get; set; }
            public string sender { get; set; }
            public string receptor { get; set; }
            public int date { get; set; }
            public int cost { get; set; }
        }

        public class RootObject
        {
            public Return @return { get; set; }
            public IList<Entries> entries { get; set; }
        }
    }
}
