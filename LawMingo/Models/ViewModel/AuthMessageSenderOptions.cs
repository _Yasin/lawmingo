﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.ViewModel
{
    public class AuthMessageSenderOptions
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}
