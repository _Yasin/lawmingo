﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.ViewModel
{
    public class ArticleDetailViewModel
    {
        public int ArticleId { get; set; }
        public string content { get; set; }
        public string ArticleName { get; set; }

    }

    public class ArticleDetail_DataViewModel
    {
        public int ArticleId { get; set; }
        public string content { get; set; }

    }


    public class ArticleDetailDataViewModel
    {
        public int Order { get; set; }
        public string Article { get; set; }
        public IEnumerable<string> Content { get; set; }
        public IEnumerable<dynamic> Properties { get; set; }
    }
}
