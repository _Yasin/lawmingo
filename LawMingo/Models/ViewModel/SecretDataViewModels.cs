﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.ViewModel
{
    public class SecretDataViewModels
    {
        public string AuthenticationGoogleClientId { get; set; }
        public string AuthenticationGoogleClientSecret { get; set; }
        public string AuthenticationFacebookAppId { get; set; }
        public string AuthenticationFacebookAppSecret { get; set; }
        public string AuthenticationMicrosoftApplicationId { get; set; }
        public string AuthenticationMicrosoftPassword { get; set; }
    }
}
