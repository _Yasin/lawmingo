﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.ViewModel
{
    public class SubscribViewModel
    {
        public string Title { get; set; }
        public int? usegeCount { get; set; }
        public int? RemainingCount { get; set; }
        public string Time { get; set; }
        public string Expire { get; set; }    
        public string Message { get; set; }
        public int? SubScriptionId { get; set; }
        public int UserSubscriptionId { get; set; }
    }
}
