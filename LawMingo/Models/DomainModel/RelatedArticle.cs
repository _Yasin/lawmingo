﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace LawMingo.Models.DomainModel
{
    public class RelatedArticle
    {

        public RelatedArticle() { }
        public enum Actions
        {
            [Display(Name ="افزودن")]
            Add,
            [Display(Name = "ویرایش")]
            Update,
            [Display(Name = "حذف")]
            Delete
        }
      
        public int RelatedArticleID { get; set; }

        public int AnswerID { get; set; }

        [UIHint("Req_Article")]
        [Required(ErrorMessage ="انتخاب ماده الزامی است .")]
        [Display(Name = "ماده")]
        public int ArticleID { get; set; }


        //[UIHint("foreignKey_Article")]
        [Display(Name = "بند ")]
        [Required(ErrorMessage = "انتخاب بند الزامی است .")]
        public int ArticleDetailID { get; set; }


        [Display(Name = "عملیات")]
        [UIHint("actionTemplate")]
        public Actions Act { get; set; }

    
      
        [Display(Name = "بند جایگزین")]
        [UIHint("TextEditor_Article_Detail")]
        [DataType(DataType.MultilineText)]
        public string NewContent { get; set; }
      
        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }   
        
         
        public DateTime Time { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }   
        public virtual Answer Answer { get; set; }
        public virtual Article Article { get; set; }
        public virtual ArticleDetail ArticleDetail { get; set; }

    }
}