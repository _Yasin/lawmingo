﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LawMingo.Models.DomainModel
{
    public class CounselorCategory
    {
        public CounselorCategory() { }

        [ScaffoldColumn(false)]
        public int CounselorCategoryID { get; set; }

        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]
        [Display(Name = "گروه")]
        public string Name { get; set; }

        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]
        [Display(Name = "نام انگلیسی")]
        public string EnglishName { get; set; }

        public int? ParentID { get; set; }

        [Key, ForeignKey("ParentID")]
        public virtual CounselorCategory Parent { get; set; }
        public virtual ICollection<CounselorCategory> Children { get; set; }
    }
}