﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class ArtsDetail
    {
        public ArtsDetail() { }

        public int ArtsDetailID { get; set; }
        public int AnswerID { get; set; }

        [UIHint("TextEditorArticleDetail")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "ویژگی ها")]
        [Required(ErrorMessage = "وارد کردن بند الزامی است .")]
        public string Content { get; set; }

        public DateTime Time { get; set; }
        public string ApplicationUserId { get; set; }

        [JsonIgnore]
        public virtual Answer Answer { get; set; }


    }
}
