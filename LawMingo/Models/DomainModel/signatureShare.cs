﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class signatureShare
    {
        public int signatureShareId { get; set; }


        [Display(Name = "نام و نام خانوادگی فرستنده")]
        [Required(ErrorMessage = "نام و نام خانوادگی الزامی است.")]
        [StringLength(100, ErrorMessage = "نام و نام خانوادگی فرستنده حداقل باید دارای 6 کاراکتر باشد .", MinimumLength = 6)]
        [RegularExpression(pattern: @"(?!^\d+$)^.+$", ErrorMessage = "برای درج نام و نام خانوادگی امکان استفاده از اعداد وجود ندارد.")]

      
        public string FullNameSender { get; set; }


        [Display(Name = "موبایل فرستنده")]
        [Required(ErrorMessage = "وارد کردن موبایل ارسال‌کننده الزامی است.")]
        [RegularExpression(pattern: @"^0*?[9]\d{9}$", ErrorMessage = "شماره موبایل وارد شده صحیح نیست .")]
        public string MobilSender { get; set; }


        [Display(Name = "کد ملی")]
        [Required(ErrorMessage = "وارد کردن کد ملی ارسال‌کننده الزامی است.")]
        [StringLength(10, ErrorMessage = "کد ملی وارد شده نامعتبر است. ", MinimumLength = 10)]
        public string NationalCode { get; set; }


        [Display(Name = "تائید موبایل ارسال‌کننده")]      
        public bool PhoneNumberConfirmed { get; set; }


        [Display(Name = "تائید کد ملی")]
        public bool NationalCodeConfirmed { get; set; }


        [Display(Name = "نام و نام خانوادگی گیرنده")]
        [Required(ErrorMessage = "نام و نام خانوادگی گیرنده الزامی است.")]
        [StringLength(100, ErrorMessage = "نام و نام خانوادگی فرستنده حداقل باید دارای 6 کاراکتر باشد .", MinimumLength = 6)]
        [RegularExpression(pattern: @"(?!^\d+$)^.+$", ErrorMessage = "برای درج نام و نام خانوادگی امکان استفاده از اعداد وجود ندارد.")]
        public string FullNameRecever { get; set; }


        [Display(Name = "موبایل گیرنده")]
        [Required(ErrorMessage = "وارد کردن موبایل گیرنده الزامی است.")]
        [RegularExpression(pattern: @"^0*?[9]\d{9}$", ErrorMessage = "شماره موبایل وارد شده صحیح نیست .")]
        public string MobilResever { get; set; }


        [Required(ErrorMessage = "ایمیل گیرنده الزامی است.")]
        [EmailAddress(ErrorMessage = "ایمیل وارد شده نامعتبر است.")]          
        [Display(Name = "ایمیل گیرنده")]   
        public string EmailResever { get; set; }


        [Required(ErrorMessage = "ایمیل فرستنده الزامی است.")]
        [EmailAddress(ErrorMessage = "ایمیل وارد شده نامعتبر است.")]
        [Display(Name = "ایمیل فرستنده")]
        public string EmailSender { get; set; }


        [Display(Name = "تائید ارسال لینک")]
        [Required(ErrorMessage = "تائید ارسال لینک الزامی است.")]
        public bool ConfirmSendContractLink { get; set; }

        public DateTime RegisterTime { get; set; }

        public DateTime EditTime { get; set; }

        public string ApplicationUserId { get; set; }
    
        public string SenderIPAddress { get; set; }
        public string ReseverIPAddress { get; set; }

    }
}
