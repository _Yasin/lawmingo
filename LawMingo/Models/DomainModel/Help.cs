﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class Help
    {
        public Help() { }

        public int HelpID { get; set; }
        public int QuestionID { get; set; }

        [Display(Name = "متن")]
        public string Content { get; set; }
        public int Like { get; set; } = 0;

        public int DisLike { get; set; } = 0;

        [Display(Name = "قابل مشاهده")]
        public bool Visible { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime Time { get; set; }

        [ScaffoldColumn(false)]
        public string UserAppId { get; set; }

        [JsonIgnore]
        public virtual Question Question { get; set; }

    }
}
