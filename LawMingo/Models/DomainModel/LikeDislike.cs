﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class LikeDislike
    {

        public LikeDislike() {}

        public int LikeDislikeID { get; set; }

        public int CommentID { get; set; }

        public string ApplicationUserId { get; set; }

        public bool Type { get; set; }

        public DateTime Time { get; set; }

        public string IP { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Comment Comment { get; set; }
    }
}
