﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{

    public class ContractScore
    {
        public ContractScore() { }

        public int ContractScoreID { get; set; }

        public int ContractID { get; set; }

        public int Score { get; set; }

        public string IP { get; set; }

        public DateTime Time { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Contract Contract { get; set; }

    }
}
