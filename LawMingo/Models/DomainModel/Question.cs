﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;



namespace LawMingo.Models.DomainModel
{
    public class Question
    {

        public Question() { }
        public int QuestionID { get; set; }

        public int ArticleID { get; set; }

        public int? AnsID { get; set; }

        [Display(Name = "سوال")]
        [Required(ErrorMessage = "وارد کردن سوال الزامی می باشد .")]
        public string MyQuestion { get; set; }
     
        public string ApplicationUserId { get; set; }
  
        public DateTime Time { get; set; }

        [Display(Name = "چک باکس")]
        public bool CheckBox { get; set; }

        [Display(Name = "آخرین سوال ")]
        public bool End { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Article Article { get; set; }
        public ICollection<Answer> Answers { get; set; }      
        public ICollection<Help> Helps { get; set; }
    }
}