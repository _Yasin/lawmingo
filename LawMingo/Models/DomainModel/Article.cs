﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class Article
    {

        public Article() { }
        public int ArticleID { get; set; }

        public int ContractID { get; set; }

        [Display(Name = "ترتیب")]
        public int Order { get; set; }

        [Display(Name = "ماده")]
        [Required(ErrorMessage = "وارد کردن نام ماده الزامی می باشد .")]
        public string Name { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime Time { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Contract Contract { get; set; }      
        [JsonIgnore]   
        public ICollection<Question> Questions { get; set; }
    }
}
