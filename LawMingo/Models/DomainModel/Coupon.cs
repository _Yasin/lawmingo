﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LawMingo.Models.DomainModel
{
    public class Coupon
    {

        public Coupon() { }
        public enum DiscountType
        {
            [Display(Name = "درصدی")]
            Percent,
            [Display(Name = "ثابت")]
            Static
        }
 
        public int CouponID { get; set; }

        [Required(ErrorMessage ="وارد کردن نام کوپن الزامی است .")]
        [Display(Name = "نام کوپن")]
        public string Name { get; set; }

        [Display(Name = "نوع کوپن")]
        [Required(ErrorMessage = "وارد کردن نوع کوپن الزامی است .")]
        [UIHint("CouponType")]
        public DiscountType  discountType { get; set; }

        [Display(Name = "مقدار تخفیف")]      
        public decimal? Amount { get; set; }
     
        [Display(Name = "تعداد")]
        [Required(ErrorMessage = "وارد کردن تعداد دفعات استفاده از کوپن الزامی است .")]
        public int? Count { get; set; }

        [Display(Name = "تاریخ شروع")]
        [Required(ErrorMessage = "وارد کردن تاریخ شروع الزامی است .")]
        [UIHint("CustomDate")]
        public DateTime Start { get; set; }

        [Required(ErrorMessage = "وارد کردن تاریخ انقضا الزامی است .")]
        [Display(Name = "تاریخ انقضا")]
        [UIHint("CustomDate")]
        public DateTime End { get; set; }

        [Display(Name = "گروه")]
        [UIHint("NullableCat")]
        public int? CategoryID { get; set; }
      
        [Display(Name = "تاریخ ایجاد کوپن")]
        public DateTime Time { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [Display(Name = "وضعیت")]
        public bool IsActive { get; set; }

        public int? ParentID { get; set; }
        public int? UserContractID { get; set; }
        public int? ConsultationRequestID { get; set; }
        public int? UserSubscriptionID { get; set; }

        [Key, ForeignKey("ParentID")]
        [JsonIgnore]
        public virtual Coupon Parrent { get; set; }
        public virtual ICollection<Coupon> Children { get; set; }
        public virtual Category Category { get; set; }
        public virtual UserSubscription UserSubscription { get; set; }
        public virtual ConsultationRequest ConsultationRequest { get; set; }
        public virtual UserContract UserContract { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}