﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LawMingo.Models.DomainModel
{
    public class PaymentLog
    {

        public PaymentLog() { }

        [Display(Name = "شناسه ")]
        public int PaymentLogID { get; set; }

        [Display(Name = "شناسه پرداخت کاربر")]
        public string PaymentID { get; set; }

        [Display(Name = "شناسه قرارداد کاربر")]
        public int? UserContractID { get; set; }

        [Display(Name = "شناسه اشتراک")]
        public int? UserSubscriptionID { get; set; }

        [Display(Name = "شناسه درخواست وقت مشاور")]
        public string ConsultationOrderID { get; set; }

        [Display(Name = "کد تراکنش")]
        public string TrackingCode { get; set; }

        [Display(Name = "کد پاسخ پرداخت")]
        public string ResponseCode { get; set; }

        [Display(Name = "پیام پاسخ پرداخت")]
        public string ResponseMessage { get; set; }

        [Display(Name = "وضعیت")]
        public bool IsSuccessful { get; set; }

        [Display(Name = "تاریخ پرداخت")]
        public DateTime Time { get; set; }

        [Display(Name = "بانک")]
        public string ReferenceBank { get; set; }

        [Display(Name = "وضعیت درخواست")]
        public string RequestStatus { get; set; }

        [Display(Name = "مبلغ")]
        public decimal Amount { get; set; }
        public string ApplicationUserId { get; set; }
        public UserContract UserContract { get; set; }
        public UserSubscription UserSubscription { get; set; }
        public ConsultationRequest consultationRequest { get; set; }
    }
}