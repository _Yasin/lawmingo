﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class Contract
    {

        public enum ContractType
        {
            [Display(Name = "هوشمند")]
            Smart,
            [Display(Name = "تمپلیت")]
            Template
        }
        public Contract() { }
     
        public int ContractID { get; set; }
   
        [Display(Name = "نوع قرارداد")]
        [UIHint("_ContractType")]
        public ContractType Type { get; set; }

        [Display(Name = "ترتیب")]
        public int? Order { get; set; }

        [Required(ErrorMessage = "وارد کردن نام قرارداد الزامی می باشد .")]      
        [MaxLength(150, ErrorMessage = "حداکثر تعداد کاراکتر 150 می باشد .")]
        [Display(Name = "نام قرارداد")]
        public string Name { get; set; }

        [Required(ErrorMessage = "وارد کردن نام انگلیسی الزامی می باشد .")]
        [MaxLength(150, ErrorMessage = "حداکثر تعداد کاراکتر 150 می باشد .")]    
        [Display(Name = "نام انگلیسی قرارداد")]
        public string EnglishName { get; set; }

     
        [Display(Name = "فایل قرارداد")]     
        [UIHint("contractFile")]
        public string File { get; set; }

        [Display(Name = "تصویر اصلی")]
        [UIHint("ContractBanner")]
        public string Banner { get; set; }

        [Display(Name = "تصویر ")]
        [UIHint("ContractImage")]
        public string Image { get; set; }


        [Display(Name = "ویدئو")]
        [UIHint("FileUpload")]
        [AdditionalMetadata("action", "ContractVideo")]
        public string Video { get; set; }

        [Display(Name = "کاور ویدئو")]
        [UIHint("FileUpload")]
        [AdditionalMetadata("action", "ContractVideoCover")]
        public string VideoCover { get; set; }


        [Display(Name = "قیمت")]
        public decimal? Price { get; set; }

        [Display(Name = "تخفیف")]
        public decimal? Discount  { get; set; }

        [Display(Name = "امتیاز")]
        public float Score { get; set; }

        [Display(Name = "تعداد بازدید")]
        public int Visits { get; set; } = 0;

     

        [UIHint("TextEditorContract")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "توضیحات")]
        public string Description { get; set; }

        [Display(Name = "عنوان صفحه")]
        public string Title { get; set; }


        [Display(Name = "توضیحات متا تگ")]
        public string MetaDescription { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "خلاصه توضیحات")]
        public string Summery { get; set; }

        [Display(Name = "قابل مشاهده")]
        public bool IsVisible { get; set; }

        [NotMapped]
        public int QuestionCount { get; set; }



        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }
   
        public DateTime Time { get; set; }
    
        public virtual ApplicationUser ApplicationUser { get; set; }    
        public ICollection<Article> Articles { get; set; }
        public ICollection<CategoryAttach> CategoryAttachs { get; set; }
        public ICollection<ContractScore> ContractScores { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
