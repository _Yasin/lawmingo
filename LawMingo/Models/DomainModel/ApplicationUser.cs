﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class ApplicationUser : IdentityUser
    {
        public enum Status
        {
            [Display(Name = "فعال")]
            Active,
            [Display(Name = "بلاک")]
            Blocked,
            [Display(Name = "معلق")]
            suspended
        }

        [Display(Name = "شناسه کاربر")]
        public override string Id { get; set; }

        [Display(Name = "شماره موبایل")]     
        public override string UserName { get; set; }

        [Display(Name = "نام کاربری تائید شده")]
        public override string NormalizedUserName { get; set; }

        [Display(Name = "ایمیل")]
        public override string Email { get; set; }

        [Display(Name = "تائید ایمیل")]
        public override bool EmailConfirmed { get; set; }

        [Display(Name = "ایمیل با حروف بزرگ")]
        public override string NormalizedEmail { get; set; }
        
        [Display(Name = "کلمه عبور")]     
        public override string PasswordHash { get; set; }

        [Display(Name = "شناسه امنیتی")]
        public override string SecurityStamp { get; set; }

        [Display(Name = "شناسه هم زمانی")]
        public override string ConcurrencyStamp { get; set; }

        [Display(Name = "شماره موبایل")]      

        public override string PhoneNumber { get; set; }

        [Display(Name = "تائید شماره موبایل")]
        public override bool PhoneNumberConfirmed { get; set; }

        [Display(Name = "فعال سازی 2 فاکتور امنیتی")]
        public override bool TwoFactorEnabled { get; set; }

        [Display(Name = "پایان قفل حساب کاربری")]
        public override DateTimeOffset? LockoutEnd { get; set; }

        [Display(Name = "فعال سازی قفل حساب کاربری")]
        public override bool LockoutEnabled { get; set; }

        [Display(Name = "تعداد دسترسی ناموفق")]
        public override int AccessFailedCount { get; set; }

        [Display(Name = "تصویر کاربر")]
        [UIHint("Avatar")]
        public string Avatar { get; set; }

        [Display(Name = "نام")]
        public string Name { get; set; }

        [Display(Name = "نام خانوادگی")]
        public string Family { get; set; }

        [Display(Name = "تاریخ تولد")]
        public DateTime? BirthDate { get; set; }

        [Display(Name = "آخرین تاریخ ورود به سایت")]
        public DateTime? LastLogin { get; set; }

        [Display(Name = "تاریخ ثبت نام")]
        public DateTime? Register { get; set; }

        [Display(Name = "وضعیت کاربر")]
        [UIHint("UserStatus")]
        public Status UserStatus { get; set; }

        [Display(Name = "جنسیت کاربر")]
        public bool Gender { get; set; }

        [Display(Name = "ورود به لیست سیاه")]
        public bool BlackList { get; set; }

        [Display(Name = "شماره کارت بانکی")]
        public string BanckCartNumber { get; set; }

        public string Refrence { get; set; }

        public string AffiliateID { get; set; }

        public string Company { get; set; }
        public string City { get; set; }
    }
}
