﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class ContactUs
    {
        public int ContactUsID { get; set; }
    
        [Required(ErrorMessage = "وارد کردن ایمیل الزامی می باشد .")]
        [EmailAddress(ErrorMessage = "ایمیل نامعتبر !")]
        public string Email { get; set; }

        [Required(ErrorMessage = "وارد کردن موضوع الزامی می باشد .")]
        public string Subject { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "وارد کردن متن الزامی می باشد .")]
        public string Content { get; set; }

        public DateTime Time { get; set; }
    }
}
