﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class Comment
    {
        public Comment() { }

        [ScaffoldColumn(false)]
        public int CommentID { get; set; }

        [ScaffoldColumn(false)]
        public int? ContractID { get; set; }

        [ScaffoldColumn(false)]
        public int? PostID { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [ScaffoldColumn(false)]
        public int? ParentID { get; set; }

        [Display(Name = "نام نویسنده")]
        public string Author { get; set; }

        [Display(Name = "ایمیل")]
        public string AuthorEmail { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Ip آدرس")]
        public string AuthorIPAddress { get; set; }

        [Display(Name = "تاریخ ثبت")]
        public DateTime Time { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "قابل مشاهده")]
        public bool IsActive { get; set; }

        [Display(Name = "خوانده شده")]
        public bool IsRead { get; set; }

        [Display(Name = "عمومی")]
        public bool IsPublic { get; set; }
        
        [Display(Name = "عنوان")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "وارد کردن محتوا الزامی است .")]
        [UIHint("TextArea")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "محتوا")]

        public string Content { get; set; }

        public string Code { get; set; }

        [AdditionalMetadata("action", "GetCounselorTags")]
        [AdditionalMetadata("type", 1)]
        [AdditionalMetadata("dataTextfield", "title")]
        [AdditionalMetadata("dataValuefield", "id")]
        [UIHint("MultiSelect")]
        [Display(Name = "برچسب ها")]
        public string Tags { get; set; }

        [UIHint("CategorySelect")]
        [Display(Name = "گروه")]
        public int? CategoryID { get; set; }

        public int AnswerCount { get; set; }
        public int CommentCount { get; set; }

        [AdditionalMetadata("action", "GetCounselors")]
        [AdditionalMetadata("dataTextfield", "title")]
        [AdditionalMetadata("dataValuefield", "id")]
        [UIHint("GridForeignKey_Ajax")]
        [Display(Name = "مشاور")]
        public int? CounselorID { get; set; }

        public int Likes { get; set; }
        public int DiskLikes { get; set; }
        public int Visits { get; set; }

        [Display(Name = "نام کمپین")]
        public string CampaignName { get; set; }

        [NotMapped]
        public List<string> BreadCrump { get; set; }

        [ForeignKey("ParentID")]
        public virtual Comment Parent { get; set; }
        [ForeignKey("CategoryID")]
        public virtual CounselorCategory Category { get; set; }
        public virtual ICollection<Comment> Answers { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Counselor Counselor { get; set; }
        public virtual Post Post { get; set; }
        public virtual Contract Contract { get; set; }
        public virtual ICollection<LikeDislike> LikeDislikes { get; set; }

    }
}
