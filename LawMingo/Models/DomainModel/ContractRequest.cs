﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class ContractRequest
    {

        public ContractRequest() { }
        public int ContractRequestID { get; set; }

        //[Display(Name = "نام و نام خانوادگی")]
        //[Required(ErrorMessage = "وارد کردن نام و نام خانوادگی الزامی است .")]
        public string FullName { get; set; }

        [Display(Name = "موبایل")]
        [Required(ErrorMessage = "وارد کردن موبایل الزامی است .")]
        public string Mobile { get; set; }

        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        //[Display(Name = "نام قرارداد")]
        //[Required(ErrorMessage = "وارد کردن نام قرارداد الزامی است .")]
        public string ContractName { get; set; }

        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public bool Read { get; set; }
        public bool Confirm { get; set; }   
    }
}
