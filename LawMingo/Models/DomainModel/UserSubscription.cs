﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class UserSubscription
    {
        public UserSubscription() { }

        public int UserSubscriptionID { get; set; }
        public string OrderId { get; set; }
        public int SubscriptionID { get; set; }
        public string ApplicationUserId { get; set; }
        public DateTime Time { get; set; }
        public int? UsageCount { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Subscription Subscription { get; set; }
        public ICollection<PaymentLog> PaymentLogs { get; set; }
    }
}
