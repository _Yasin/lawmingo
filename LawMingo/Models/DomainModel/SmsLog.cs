﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class SmsLog
    {
        public SmsLog() { }

        public int SmsLogID { get; set; }
        public int  ReturnStatus { get; set; }
        public int  EntriyStatus { get; set; }
        public string ReturnMessage { get; set; }
        public string EntriyMessage { get; set; }
        public string Receptor { get; set; }
        public int messageid { get; set; }
        public string statustext { get; set; }
        public string sender { get; set; }      
        public string Template { get; set; }
        public string Usage { get; set; }
        public int date { get; set; }
        public int cost { get; set; }
        public DateTime Time { get; set; }
      
    }
}
