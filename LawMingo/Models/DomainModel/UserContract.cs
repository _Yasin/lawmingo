﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace LawMingo.Models.DomainModel
{
    public class UserContract
    {
        public enum Status
        {
            [Display(Name = "ناتمام")]
            Pending,
            [Display(Name = "بسته شده")]
            Complete
        }

        public UserContract() { }

        public int UserContractID { get; set; }

        public string OrderId { get; set; }
    

        [Required]
        public int ContractID { get; set; }

        [Required]
        [ForeignKey("ApplicationUser")]     
        public string ApplicationUserId { get; set; }
      
        [DataType(DataType.MultilineText)]
        [Display(Name = "محتوای قرارداد")]
        public string Content { get; set; }

        [Required]
        [Display(Name = "جواب‌های انتخاب‌ شده")]
        public string SelectedAnswer { get; set; }
     
        [Display(Name = "شناسه بند")]
        public string ArtsID { get; set; }

        [Display(Name = "تعداد ویرایش")]
        public int? EditCount { get; set; } = 0;

        [Required]
        public DateTime Time { get; set; }

        public DateTime EditTime { get; set; }

        public string  Word { get; set; }

        public int? ParentID { get; set; }

        public Status status { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual LawMingo.Models.DomainModel.Contract Contract { get; set; }
        public ICollection<UserContractProperty> UserContractProperties { get; set; }
        public ICollection<PaymentLog> PaymentLogs { get; set; }
        public ICollection<Coupon> Coupons { get; set; }

        public int? AffiliateDetailID { get; set; }

        [Key, ForeignKey("ParentID")]
        [JsonIgnore]
        public virtual UserContract Parrent { get; set; }

        public virtual AffiliateDetail AffiliateDetail { get; set; }
        public virtual ICollection<UserContract> Children { get; set; }   

    }  
}