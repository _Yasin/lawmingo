﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class CategoryAttach
    {
        public CategoryAttach() { }
         
        public int CategoryAttachID { get; set; }

        [UIHint("CategoryDropDown")]
        [Display(Name = "گروه")]
        [Required(ErrorMessage ="انتخاب گروه الزامی است.")]
        public int CategoryID { get; set; }
        public int? ContractID { get; set; }
        public int? CounselorID { get; set; }

        public virtual Category Category { get; set; }
        public virtual Contract Contract { get; set; }
        public virtual Counselor Counselor { get; set; }

    }
}