﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class Subscription
    {
        public Subscription() { }

        public int SubscriptionID { get; set; }

        [Display(Name = "عنوان اشتراک")]
        public string Title { get; set; }

        [Display(Name = "تعداد ماه")]
        public int Month { get; set; }

        [Display(Name = "تعداد قرارداد")]
        public int Count { get; set; }

        [Display(Name = "مدت زمان مشاوره")]
        public int AdviserTime { get; set; }

        [Display(Name = "تعداد قراردادهای قابل ارجاع")]
        public int? ReferencedContract { get; set; }

        [Display(Name = "مبلغ")]
        public decimal Price { get; set; }
     
        [Display(Name = "میزان تخفیف اعمال شده")]
        public int? Discount { get; set; }

        [Display(Name = "زمان ثبت")]
        public DateTime Time { get; set; }

        [Display(Name = "وضعیت")]
        public bool IsActive { get; set; }
   
    }
}
