﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace LawMingo.Models.DomainModel
{
    public class ArticleProperty
    {

        public ArticleProperty() { }
        public int ArticlePropertyID { get; set; }
   
        [Display(Name = "ویژگی")]    
        [MaxLength(100, ErrorMessage = "تعداد کاراکتر به کار رفته بیشتر از 100 می باشد !")]
        [Required(ErrorMessage ="وارد کردن ویژگی الزامی است .")]
        public string PropertyName { get; set; }
    
        [Display(Name = "نام انگلیسی")]
        [MaxLength(100,ErrorMessage ="تعداد کاراکتر به کار رفته بیشتر از 100 می باشد !")]
        [Required(ErrorMessage = "وارد کردن نام انگلیسی الزامی است .")]
        public string EnglishName { get; set; }
   
    }
}