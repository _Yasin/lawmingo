﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class ConsultationRequest
    {

        public enum Status
        {
            [Display(Name = "در انتظار")]
            Waiting,
            [Display(Name = "تائید")]
            Confirm,
            [Display(Name = "لغو")]
            Cancel,
            [Display(Name = "اتمام")]
            Finish,           
        }

        public int ConsultationRequestID { get; set; }

        [Display(Name = "ایمیل")]
        public string ApplicationUserId { get; set; }

    
        [Display(Name = "تاریخ ثبت")]
        public DateTime Time { get; set; }

        [Display(Name = "مشاور")]
        [UIHint("GridForeignKey")]
        public int CounselorID { get; set; }

        [Required(ErrorMessage = "وارد کردن تاریخ و زمان مشاوره الزامی است .")]
        [Display(Name = "تاریخ و زمان شروع مشاوره")]
        public DateTime AdviceTime { get; set; }

        [Required(ErrorMessage = "وارد کردن مدت زمان جهت مشاوره الزامی است .")]      
        [Display(Name = "مدت زمان مشاوره")]
        public string TimeRange { get; set; }

        [UIHint("ConsultationRequest_Status")]
        [Display(Name = "وضعیت درخواست")]
        public Status status { get; set; }

        [Display(Name = "شناسه درخواست")]
        public string OrderID { get; set; }

        [Display(Name = "موبایل")]
        [RegularExpression(pattern: @"^0*?[9]\d{9}$", ErrorMessage = "شماره موبایل وارد شده نامعتبر است ")]
        [Required(ErrorMessage = "وارد کردن شماره موبایل الزامی است .")]     
        public string Mobile { get; set; }

        [Display(Name = "نام و نام خانوادگی")]
        [Required(ErrorMessage = "وارد کردن نام و نام خانوادگی الزامی است .")]
        public string FullName { get; set; }

        [Display(Name = "توضیحات تکمیلی")]
        [Required(ErrorMessage = "خلاصه‌ای از مشکل خود را بنویسید. ")]
        public string Description { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Counselor Counselor { get; set; }
        public ICollection<PaymentLog> PaymentLogs { get; set; }

    }
}
