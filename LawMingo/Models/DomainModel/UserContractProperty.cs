﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LawMingo.Models.DomainModel
{
    public class UserContractProperty
    {
        public UserContractProperty() { }

        public int UserContractPropertyID { get; set; }
        public int UserContractID { get; set; }
        public int ArticlePropertyID { get; set; }
        public string Value { get; set; }

        public virtual ArticleProperty ArticleProperty { get; set; }
        public virtual UserContract UserContract { get; set; }
    }
}