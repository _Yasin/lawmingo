﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LawMingo.Models.DomainModel
{
    public class Answer
    {
 
        public Answer() { }
        public int AnswerID { get; set; }

        [Display(Name = "پاسخ فارسی")]
        public string MyAnswer { get; set; }

        [Display(Name = "پاسخ انگلیسی")]
        public string EnglishAnswer { get; set; }

        public int QuestionID { get; set; }
      
        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime Time { get; set; }
      
        public virtual ApplicationUser ApplicationUser { get; set; }
        [JsonIgnore]
        public virtual Question Question { get; set; }

        [JsonIgnore]
        public ICollection<ArticleDetail> ArticleDetails { get; set; }
        public ICollection<RelatedArticle> RelatedArticles { get; set; }

    }
}