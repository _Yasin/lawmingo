﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class Schedule
    {
        public Schedule() { }
        public enum Days
        {
            [Display(Name = "شنبه")]
            Saturday,
            [Display(Name = "یکشنبه")]
            Sunday,
            [Display(Name = "دوشنبه")]
            Monday,
            [Display(Name = "سه شنبه")]
            Tuesday,
            [Display(Name = "چهارشنبه")]
            Wednesday,
            [Display(Name = "پنج شنبه")]
            Thursday,
            [Display(Name = "جمعه")]
            Friday,
        }

        public int ScheduleID { get; set; }

        [Required]
        public int CounselorID { get; set; }


        [Display(Name = "روز")]
        [Required(ErrorMessage = "انتخاب روز الزامی است.")]
        [UIHint("DaysWeek")]
        public Days day { get; set; }

        [Display(Name = "شروع بازه زمانی اول")]
        [DataType(DataType.Time)]
        public DateTime? StartTimeRange1 { get; set; }

        [Display(Name = "اتمام بازه زمانی اول")]
        [DataType(DataType.Time)]
        public DateTime? EndTimeRange1 { get; set; }

        [Display(Name = "شروع بازه زمانی دوم")]
        [DataType(DataType.Time)]
        public DateTime? StartTimeRange2 { get; set; }

        [Display(Name = "اتمام بازه زمانی دوم")]
        [DataType(DataType.Time)]
        public DateTime? EndTimeRange2 { get; set; }

        [Display(Name = "شروع بازه زمانی سوم")]
        [DataType(DataType.Time)]
        public DateTime? StartTimeRange3 { get; set; }

        [Display(Name = "اتمام بازه زمانی سوم")]
        [DataType(DataType.Time)]
        public DateTime? EndTimeRange3 { get; set; }

        [Display(Name = "شروع بازه زمانی چهار")]
        [DataType(DataType.Time)]
        public DateTime? StartTimeRange4 { get; set; }

        [Display(Name = "اتمام بازه زمانی چهار")]
        [DataType(DataType.Time)]
        public DateTime? EndTimeRange4 { get; set; }

        [Display(Name = "شروع بازه زمانی پنجم")]
        [DataType(DataType.Time)]
        public DateTime? StartTimeRange5 { get; set; }

        [Display(Name = "اتمام بازه زمانی پنجم")]
        [DataType(DataType.Time)]
        public DateTime? EndTimeRange5 { get; set; }

        [Display(Name = "شروع بازه زمانی ششم")]
        [DataType(DataType.Time)]
        public DateTime? StartTimeRange6 { get; set; }

        [Display(Name = "اتمام بازه زمانی ششم")]
        [DataType(DataType.Time)]
        public DateTime? EndTimeRange6 { get; set; }

        [Display(Name = "شروع بازه زمانی هفتم")]
        [DataType(DataType.Time)]
        public DateTime? StartTimeRange7 { get; set; }

        [Display(Name = "اتمام بازه زمانی هفتم")]
        [DataType(DataType.Time)]
        public DateTime? EndTimeRange7 { get; set; }

        public virtual Counselor Counselor { get; set; }

    }
}
