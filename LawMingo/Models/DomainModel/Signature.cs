﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class Signature
    {
        public int SignatureID { get; set; }
        public string PDF { get; set; }
        public string Sign { get; set; }
        public string FinalFile { get; set; }
        public string ApplicationUserId { get; set; }
        public DateTime RegisterTime { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}
