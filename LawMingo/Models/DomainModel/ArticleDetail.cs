﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;



namespace LawMingo.Models.DomainModel
{
    public class ArticleDetail
    {

        public ArticleDetail() { }
        public int ArticleDetailID { get; set; }
        public int AnswerID { get; set; }
         
        [UIHint("TextEditorArticleDetail")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "ویژگی ها")]
        [Required(ErrorMessage = "وارد کردن محتوا الزامی می باشد .")]
        public string ArticleContent { get; set; }

        public DateTime Time { get; set; }

        public string ApplicationUserId { get; set; }

        [JsonIgnore]
        public virtual Answer Answer { get; set; }
      
    }
}