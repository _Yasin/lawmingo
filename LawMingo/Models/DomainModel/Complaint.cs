﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace LawMingo.Models.DomainModel
{
    public class Complaint
    {
        public int ComplaintID { get; set; }

        [Required(ErrorMessage = "وارد کردن ایمیل الزامی می باشد .")]     
        [EmailAddress(ErrorMessage = "ایمیل نامعتبر !")]
        public string Email { get; set; }

        [Required(ErrorMessage = "وارد کردن موضوع الزامی می باشد .")]
        public string Subject { get; set; }

        [DataType(DataType.MultilineText)]       
        [Required(ErrorMessage = "وارد کردن متن الزامی می باشد .")]
        public string  Content { get; set; }

        public DateTime Time { get; set; }
    }
}
