﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Models.DomainModel
{
    public class Counselor
    {
   
        public int CounselorID { get; set; }
        

        [Display(Name = "نام مشاور")]
        [Required(ErrorMessage ="وارد کردن نام مشاور الزامی است.")]  
        public string Name { get; set; }


        [Required(ErrorMessage = "وارد کردن سمت الزامی است.")]
        [Display(Name = "سمت")]
        public string Post { get; set; }


        [Display(Name = "تحصیلات")]
        [Required(ErrorMessage = "وارد کردن تحصیلات الزامی است.")]
        public string Education { get; set; }
    
        [Display(Name = "مشاوره تلفنی")]
        public bool Phone { get; set; }

        [Display(Name = "مشاوره حضوری")]
        public bool Direct { get; set; }


        [Display(Name = "مبلغ 1 ساعت مشاوره حضوری")]
        //[Range(100000, 1000000, ErrorMessage = "مقدار {0} باید بین {1} و {2}")]
        public decimal Price { get; set; }


        [Display(Name = "مبلغ 15 دقیقه مشاوره تلفنی")]
        //[Range(10000, 100000, ErrorMessage = "مقدار {0} باید بین {1} و {2}")]
        public decimal CounselationPrice15 { get; set; }


        [Display(Name = "مبلغ 30 دقیقه مشاوره تلفنی")]
       // [Range(10000, 100000, ErrorMessage = "مقدار {0} باید بین {1} و {2}")]
        public decimal CounselationPrice30 { get; set; }


        [Display(Name = "مبلغ 60 دقیقه مشاوره تلفنی")]
        //[Range(10000, 100000, ErrorMessage = "مقدار {0} باید بین {1} و {2}")]
        public decimal CounselationPrice60 { get; set; }


        [Display(Name = "تصویر ")]
        [UIHint("CustomUploadFile")]
        public string Image { get; set; }

        [Display(Name = "تصویر کوچک")]
        [UIHint("UploadThumbnail")]
        public string Thumbnail { get; set; }


        [Display(Name = "رزومه")]
        [Required(ErrorMessage = "وارد کردن رزومه الزامی است.")]
        [UIHint("TextEditorContract")]
        [DataType(DataType.MultilineText)]
        public string Summery { get; set; }

        [Display(Name = "وضعیت فعالیت")]
        public bool IsActive { get; set; }


        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime Time { get; set; }


        [Display(Name = "مشاوره آنلاین")]
        public bool Online { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }     
        public virtual ICollection<Schedule> Schedules { get; set; }
   
        public virtual ICollection<CategoryAttach> CategoryAttachs { get; set; }

    }
}
