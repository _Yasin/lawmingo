﻿using LawMingo;
using Microsoft.AspNetCore.WebSockets.Internal;
using Newtonsoft.Json;
using POD_Base_Service;
using POD_Base_Service.Base;
using POD_Base_Service.Exception;
using POD_Base_Service.Model.ValueObject;
using POD_Base_Service.Result;
using POD_Billing;
using POD_Billing.Model.ServiceOutput;
using POD_Billing.Model.ValueObject;
using POD_SSO;
using POD_SSO.Base;
using POD_SSO.Base.Enum;
using POD_SSO.Model.OAuth2.ServiceOutput;
using POD_SSO.Model.OAuth2.ValueObject;
using POD_UserOperation;
using POD_UserOperation.Model.ServiceOutput;
using POD_UserOperation.Model.ValueObject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Podium
{
    public static class SSO
    {
        //private static string ClientId = "17425749be95245e987a0d9ad3e4cd24d";
        //private static string ClientSecret = "6d6d13de";

        private static string SSO_Redirect_Url = "https://localhost:5001/home/POD_SSO_Verify";

        public static string GetLink()
        {
            try
            {
                var loginUrlVo = LoginUrlVo.ConcreteBuilder
                    .SetClientId(Startup.StaticConfig["Podium:ClientId"])
                    .SetResponseType(ResponseType.code)
                    .SetScope(new ScopeType[] { ScopeType.phone, ScopeType.profile, ScopeType.email })
                    .SetRedirectUri(SSO_Redirect_Url)
                    .Build();
                var output = loginUrlVo.GetLink();
                return output;
            }
            catch (PodException podException)
            {
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }
        public static OAuthResponseSrv GetToken(string code)
        {
            try
            {
                var clientInfo = new ClientInfo
                {
                    ClientId = Startup.StaticConfig["Podium:ClientId"],
                    ClientSecret = Startup.StaticConfig["Podium:ClientSecret"]
                };
                var ssoService = new SsoService(clientInfo);

                var output = new OAuthResponseSrv();
                var accessTokenVo = AccessTokenVo.ConcreteBuilder
                        .SetCode(code)
                        .SetGrantType("authorization_code")
                        .SetRedirectUri(SSO_Redirect_Url)
                        .Build();
                ssoService.GetAccessToken(accessTokenVo, response => Listener.GetResult(response, out output));
                return output;
            }
            catch (PodException podException)
            {
                Console.WriteLine(podException);
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        public static TokenInfoSrv GetInfo(string token)
        {
            try
            {
                var clientInfo = new ClientInfo
                {
                    ClientId = Startup.StaticConfig["Podium:ClientId"],
                    ClientSecret = Startup.StaticConfig["Podium:ClientSecret"]
                };
                var ssoService = new SsoService(clientInfo);
                var output = new TokenInfoSrv();
                var tokenInfoVo = TokenInfoVo.ConcreteBuilder
                    .SetToken(token)
                    .SetTokenTypeHint(TokenType.id_token)
                    .Build();
                ssoService.GetTokenInfo(tokenInfoVo, response => Listener.GetResult(response, out output));
                return output;
            }
            catch (PodException podException)
            {
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }

        }
        public static ResultSrv<CustomerProfileSrv> GetProfile()
        {
            try
            {
                var internalServiceCallVo = InternalServiceCallVo.ConcreteBuilder
                    .SetToken(Startup.StaticConfig["Podium:PublicToken"])
                    //.SetScVoucherHash({Put your VoucherHash})
                    //.SetScApiKey("{Put your ApiKey}")
                    .Build();
                var output = new ResultSrv<CustomerProfileSrv>();
                var userProfileVo = UserProfileVo.ConcreteBuilder
                                   .SetServiceCallParameters(internalServiceCallVo)
                                   .SetClientId(Startup.StaticConfig["Podium:ClientId"])
                                   .SetClientSecret(Startup.StaticConfig["Podium:ClientSecret"])
                                   .Build();
                
                UserOperationService.GetUserProfile(userProfileVo, response => Listener.GetResult(response, out output));
                return output;
            }
            catch (PodException podException)
            {
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

    }
    public static class Payment
    {
        public static long GenerateInvoice(long pod_uid , decimal amount , string desc)
        {
            try
            {
                var internalServiceCallVo = InternalServiceCallVo.ConcreteBuilder
                    .SetToken(Startup.StaticConfig["Podium:PublicToken"])
                    //.SetScVoucherHash({Put your VoucherHash})
                    //.SetScApiKey(Startup.StaticConfig["Podium:PublicToken"])
                    .Build();
                var output = new ResultSrv<InvoiceSrv>();
                var profile_response = Podium.SSO.GetProfile();

                var invoiceSrv = IssueInvoiceVo.ConcreteBuilder
                                    .SetServiceCallParameters(internalServiceCallVo)
                                    .SetGuildCode("CONSULTATION_GUILD")
                                    .SetProductsInfo(new List<ProductInfo>
                                        {
                                            ProductInfo.ConcreteBuilder.SetProductId(0).SetPrice(amount * 10).SetQuantity(1).SetProductDescription(desc).Build(),
                                        }
                                    )
                                    .SetVerificationNeeded(true)
                                    .SetPreferredTaxRate(0)
                                    .SetOtt(profile_response.Ott)
                                    //.SetPreview({put your preview})
                                    //.SetMetadata("{put your metadata}")
                                    .SetUserId(pod_uid)
                                    //.SetAddressId("{put your addressId}")
                                    .Build(); // Create new instance 
                BillingService.IssueInvoice(invoiceSrv, response => Listener.GetResult(response, out output));
                return output.Result.Id;
            }
            catch (PodException podException)
            {
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }
        public static string GenerateURL(long InvoiceId , string callback)
        {
            try
            {
                var payInvoiceByWalletVo = PayInvoiceByWalletVo.ConcreteBuilder
                                .SetInvoiceId(InvoiceId)
                                //.SetCallUri("{Put your CallUri}")
                                .SetRedirectUri(callback)
                                .Build();
                var output = payInvoiceByWalletVo.GetLink();
                return output;
            }
            catch (PodException podException)
            {
                throw;
            }
        catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }
        public static ResultSrv<List<InvoiceSrv>> Inquiry(long InvoiceId)
        {
            try
            {
                var internalServiceCallVo = InternalServiceCallVo.ConcreteBuilder
                    .SetToken(Startup.StaticConfig["Podium:PublicToken"])
                    .Build();

                var output = new ResultSrv<List<InvoiceSrv>>();
                var getInvoiceListVo = GetInvoiceListVo.ConcreteBuilder
                        .SetServiceCallParameters(internalServiceCallVo)
                        .SetId(InvoiceId)
                        .SetOffset(0)
                        .SetSize(1)
                        .Build();
                BillingService.GetInvoiceList(getInvoiceListVo, response => Listener.GetResult(response, out output));
                return output;
            }
            catch (PodException podException)
            {
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }
        public static ResultSrv<InvoiceSrv> VerifyAndCloseInvoice(long InvoiceId)
        {
            try
            {
                var internalServiceCallVo = InternalServiceCallVo.ConcreteBuilder
                    .SetToken(Startup.StaticConfig["Podium:PublicToken"])
                    .Build();

                var output = new ResultSrv<InvoiceSrv>();
                var verifyAndCloseInvoiceVo = VerifyAndCloseInvoiceVo.ConcreteBuilder
                        .SetServiceCallParameters(internalServiceCallVo)
                        .SetId(InvoiceId)
                        .Build();
                BillingService.VerifyAndCloseInvoice(verifyAndCloseInvoiceVo, response => Listener.GetResult(response, out output));
                return output;
            }
            catch (PodException podException)
            {
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }
        public static ResultSrv<bool> CloseInvoce(long InvoiceId)
        {
            try
            {
                var internalServiceCallVo = InternalServiceCallVo.ConcreteBuilder
                    .SetToken(Startup.StaticConfig["Podium:PublicToken"])
                    //.SetScVoucherHash({Put your VoucherHash})
                    //.SetScApiKey("{Put your ApiKey}")
                    .Build();
                var output = new ResultSrv<bool>();
                var closeInvoiceVo = CloseInvoiceVo.ConcreteBuilder
                                    .SetServiceCallParameters(internalServiceCallVo)
                                    .SetId(InvoiceId)
                                .Build();
                BillingService.CloseInvoice(closeInvoiceVo, response => Listener.GetResult(response, out output));
                return output;
            }
            catch (PodException podException)
            {
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }

    }
    public static class Tools{
        public static string GetOTT() {

            try
            {
                var internalServiceCallVo = InternalServiceCallVo.ConcreteBuilder
                                            .SetToken(Startup.StaticConfig["Podium:PublicToken"])
                                            //.SetScVoucherHash({Put your VoucherHash})
                                            //.SetScApiKey("{Put your ApiKey}")
                                            .Build();
                var output = new ResultSrv<string>();
                BaseService.GetOtt(internalServiceCallVo, response => Listener.GetResult(response, out output));
                return output.Ott;
            }
            catch (PodException podException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}
