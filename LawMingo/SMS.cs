﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LawMingo
{
    public static class SMS
    {
        private static string Token = "525043434C35686441787436486F596E71467A56452B62776E6B504D453844716C34725469676A335758733D";
        private static string BaseURL = "https://api.kavenegar.com/v1/"+Token;
        private static string verifyURL = BaseURL+"/verify/lookup.json";
        
        public static string  Verify(string receptor, string token , string template)
        {
            string responseContent = null;

            if (receptor != null)
            {

                var request = WebRequest.Create(verifyURL+ String.Format("?receptor={0}&token={1}&template={2}" , receptor , token , template) ) as HttpWebRequest;
                request.KeepAlive = true;
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                                       
                try
                {
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            responseContent = reader.ReadToEnd();                          
                        }

                    }
                }
                catch (WebException ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
                }
                finally
                {
                   
                }
             
            }
            if (responseContent != null)
            {
                //JavaScriptSerializer js = new JavaScriptSerializer();
                //var c = js.Deserialize<PushResult>(responseContent);
                //responseContent = c.id;
            }

            return responseContent;
        }
        //public static async Task<string> DeliveryReceived(int oid)
        //{
        //    HttpClient httpClient = new HttpClient();
        //    var content = new StringContent("{\"guid\":\"" + db.OrderHeaders.Find(oid).ApplicationUserID + "\",\"oid\":\"" + oid + "\"}", Encoding.UTF8, "application/json");
        //    httpClient.DefaultRequestHeaders.Add("token", Token);
        //    try
        //    {
        //        var res = (await httpClient.PostAsync(Bot + "delivery/confirm", content));
        //        return res.StatusCode.ToString();
        //    }
        //    catch (Exception)
        //    {
        //        return "Error";
        //    }
        //}

    }

}
