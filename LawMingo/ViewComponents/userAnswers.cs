﻿using LawMingo.Data;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.ViewComponents
{
    public class userAnswersViewComponent : ViewComponent
    {               
            private ApplicationDbContext _db { get; set; }
            public userAnswersViewComponent(ApplicationDbContext db)
            {
                _db = db;
            }

            public async Task<IViewComponentResult> InvokeAsync(ContractDataViewModel model)
            {              
                return  View(model);
            }
          
    }
}
