﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Zarin
{
    public static class Gateway
    {
        private static string MerchantID = "a8df972a-e18f-11e8-9276-005056a205be";
        private static string BaseURL = "https://www.zarinpal.com/pg/rest/WebGate";
        private static string RequestURL = BaseURL+ "/PaymentRequest.json";
        private static string VerifyURL = BaseURL+ "/PaymentVerification.json";
        
        public static async Task<Dto.Response.Payment.Request> Request(int amount , string desc , string callback)
        {
            var _Res = new Dto.Response.Payment.Request() { };
            var obj  = new
            {
                MerchantID,
                Amount = amount,
                Description = desc,
                CallbackURL = callback
            };

            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync(RequestURL, content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    _Res = JsonConvert.DeserializeObject<Dto.Response.Payment.Request>(apiResponse);
                }
            }

            return _Res;
        }
        public static async Task<Dto.Response.Payment.Verification> Verify(int amount, string authority)
        {
            var _Res = new Dto.Response.Payment.Verification() { };
            var obj = new
            {
                MerchantID,
                Amount = amount,
                Authority = authority
            };

            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync(VerifyURL, content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();

                    dynamic __Res = JsonConvert.DeserializeObject<object>(apiResponse);

                    _Res.Status = __Res.Status;
                }
            }
            return _Res;
        }

    }

}
