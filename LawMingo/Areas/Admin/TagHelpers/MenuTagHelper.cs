﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Areas.Admin.TagHelpers
{

    //[HtmlTargetElement("Menu-Item")]
    //[RestrictChildren("Menu-Item-Content")]
    public class MenuItemTagHelper : TagHelper
    {      
        public string Controller { get; set; }
        public ViewContext destination { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
           
            output.TagName = "li";
            var currentConroller = destination.RouteData.Values.Values.First().ToString();

            if(string.Equals(currentConroller, Controller, StringComparison.OrdinalIgnoreCase))           
                output.Attributes.SetAttribute("class", "active");                     
        }
    }
}
