﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawMingo.Areas.Admin.TagHelpers
{

    [HtmlTargetElement("Menu-Item-Content", ParentTag = "Menu-Item")]
    public class MenuItemContentTagHelper : TagHelper
    {
        public string Controller { get; set; }
        public string Icon { get; set; }
        public string title { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {

            var outerTag = new TagBuilder("a");
            outerTag.Attributes.Add("class", "nav-link");
            if(Controller == "Home") outerTag.Attributes.Add("href", $@"/Admin/{Controller}/Index");
            else outerTag.Attributes.Add("href", $@"/Admin/{Controller}/List");
            output.MergeAttributes(outerTag);
            output.TagName = outerTag.TagName;

         
            var font = new TagBuilder("i");
            font.Attributes.Add("class", "material-icons");
            font.InnerHtml.Append(this.Icon);
            output.PreContent.SetHtmlContent(font);

         
            var paragraph = new TagBuilder("p");
            paragraph.InnerHtml.Append(this.title);
            var originalContents = await output.GetChildContentAsync();
            paragraph.InnerHtml.Append(originalContents.GetContent());
            output.Content.SetHtmlContent(paragraph);
        }
    }
}
