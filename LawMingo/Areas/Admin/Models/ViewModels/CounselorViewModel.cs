﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Areas.Admin.Models.ViewModels
{
    public class CounselorViewModel
    {
        public int CounselorID { get; set; }
        public string Name { get; set; }
    }
}
