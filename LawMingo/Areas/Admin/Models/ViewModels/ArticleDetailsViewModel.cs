﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LawMingo.Areas.Admin.Models.ViewModels
{
    public class ArticleDetailsViewModel
    {
        public int ArticleDetailID { get; set; }
        public string Content { get; set; }
    }
}

