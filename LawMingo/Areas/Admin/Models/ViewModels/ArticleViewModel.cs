﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LawMingo.Areas.Admin.Models.ViewModels
{
    public class ArticleViewModel
    {
        public int ArticleID { get; set; }
        public string Name { get; set; }
    }
}

