﻿using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Areas.Admin.Models.ViewModels
{
    public class UserContractViewModel
    {
      
        public int UserContractID { get; set; }

        public string OrderId { get; set; }

        [Display(Name = "نام قرارداد")]
        public string ContractName { get; set; }

        [Display(Name = "شناسه کاربر")]
        public string ApplicationUserId { get; set; }

        [Display(Name = "ایمیل کاربر")]
        public string Email { get; set; }

        [Display(Name = "PDF")]
        public string Content { get; set; }
      
        [Display(Name = "شناسه پاسخ ها")]
        public string SelectedAnswer { get; set; }

        [Display(Name = "شناسه بندها")]
        public string ArtsID { get; set; }

        [Display(Name = "ویرایش")]
        public int EditCount { get; set; } = 0;
     
        [Display(Name = "تاریخ")]
        public DateTime Time { get; set; }

        [Display(Name = "تاریخ ویرایش ")]
        public DateTime EditTime { get; set; }

        [Display(Name = "Word")]
        public string Word { get; set; }

        [Display(Name = "قیمت")]
        public decimal? Price { get; set; }

        [Display(Name = "موبایل")]
        public string   Mobile { get; set; }

        [Display(Name = "وضعیت پرداخت")]
        public bool Payment { get; set; }

        [Display(Name = "مبلغ پرداختی")]
        public decimal? Amount { get; set; }

        [Display(Name = "کوپن")]
        public string Coupon { get; set; }

        public int? ParentID { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual LawMingo.Models.DomainModel.Contract Contract { get; set; }
        public ICollection<UserContractProperty> UserContractProperties { get; set; }
        public ICollection<PaymentLog> PaymentLogs { get; set; }
    }
}
