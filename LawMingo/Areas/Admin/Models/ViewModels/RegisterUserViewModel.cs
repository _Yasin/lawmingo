﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LawMingo.Areas.Admin.Models.ViewModels
{
    public class RegisterUserViewModel
    {

        [Required(ErrorMessage = "پست الکترونیکی الزامی است .")]
        [EmailAddress(ErrorMessage = "پست الکترونیکی نامعتبر !")]
        [Display(Name = "پست الکترونیکی")]
        public string Email { get; set; }

        [Required(ErrorMessage = "کلمه عبور الزامی است .")]
        [StringLength(100, ErrorMessage = "کلمه عبور حداقل باید دارای 6 کاراکتر باشد .", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تکرار کلمه عبور")]
        [Compare("Password", ErrorMessage = "مغایرت کلمه عبور  و تکرار کلمه عبور")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "نقش کاربر")]
        public string Role { get; set; }
    }
}