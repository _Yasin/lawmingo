﻿using LawMingo.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Areas.Admin.Models.ViewModels
{
    public class UserSubscriptionViewModel
    {
        public int UserSubscriptionID { get; set; }
        public string OrderId { get; set; }
        public int SubscriptionID { get; set; }
        public string ApplicationUserId { get; set; }
        public DateTime Time { get; set; }
        public int? UsageCount { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public decimal PaymentPrice { get; set; }
        public bool Payment { get; set; }
       
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Subscription Subscription { get; set; }
        public ICollection<PaymentLog> PaymentLogs { get; set; }
    }
}
