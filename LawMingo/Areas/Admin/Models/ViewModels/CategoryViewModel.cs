﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LawMingo.Areas.Admin.Models.ViewModels
{
    public class CategoryViewModel
    {
        public int CategoryID { get; set; }
        public string Name { get; set; }
    }
}

