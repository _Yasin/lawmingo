﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LawMingo.Areas.Admin.Controllers
{

    [Authorize(Roles = "Admin")]
    [Area("Admin")]

    public class PaymentLogController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public PaymentLogController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
              db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult PaymentLogs_Read([DataSourceRequest]DataSourceRequest request , int? UserContractId , string ConsultationRequestID)
        {

            IQueryable<PaymentLog> PaymentLogs = db.PaymentLogs;

            if (UserContractId != null)
            {
                PaymentLogs = PaymentLogs.Where(q => q.UserContractID == UserContractId);
            }
            if (ConsultationRequestID != null)
            {
                PaymentLogs = PaymentLogs.Where(q => q.ConsultationOrderID == ConsultationRequestID);
            }
          
            DataSourceResult result = PaymentLogs.ToDataSourceResult(request, paymentLog => new 
            {
                paymentLog.PaymentLogID,
                paymentLog.Amount,
                paymentLog.IsSuccessful,
                paymentLog.ReferenceBank,
                paymentLog.ResponseCode,
                paymentLog.ResponseMessage,              
                paymentLog.TrackingCode,
                paymentLog.RequestStatus,
                paymentLog.Time,
                paymentLog.UserContractID,
                paymentLog.ConsultationOrderID,
                paymentLog.UserSubscriptionID,

            });

            return Json(result);
        }

        public ActionResult PaymentLogs_Destroy([DataSourceRequest]DataSourceRequest request, PaymentLog paymentLog)
        {

            var entity = db.PaymentLogs.Find(paymentLog.PaymentLogID);
            db.PaymentLogs.Remove(entity);
            db.SaveChanges();

            return Json(new[] { paymentLog }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}