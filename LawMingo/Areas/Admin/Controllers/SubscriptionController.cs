﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{

    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class SubscriptionController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public SubscriptionController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Subscription_Read([DataSourceRequest]DataSourceRequest request)
        {

            IQueryable<Subscription> subscriptions = _db.Subscriptions;
            DataSourceResult result = subscriptions.ToDataSourceResult(request, subscrib => new
            {
                subscrib.SubscriptionID,
                subscrib.Title,             
                subscrib.Count,
                subscrib.AdviserTime,
                subscrib.IsActive,
                subscrib.Price,
                subscrib.Month,
                subscrib.Discount,
                subscrib.ReferencedContract,
                subscrib.Time
            });

            return Json(result);

        }

        public ActionResult Subscription_Create([DataSourceRequest]DataSourceRequest request, Subscription subscription)
        {
            if (ModelState.IsValid)
            {
                var entity = new Subscription
                {
                    Title = subscription.Title,                
                    Price = subscription.Price,
                    IsActive = subscription.IsActive,
                    Count = subscription.Count,
                    Month = subscription.Month,
                    AdviserTime = subscription.AdviserTime,
                    Discount = subscription.Discount,
                    ReferencedContract = subscription.ReferencedContract,    
                    Time = subscription.Time
                };

                _db.Subscriptions.Add(entity);
                _db.SaveChanges();
              
            }

            return Json(new[] { subscription }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Subscription_Update([DataSourceRequest]DataSourceRequest request, Subscription subscription)
        {
            if (ModelState.IsValid)
            {
                var entity = new Subscription
                {
                     SubscriptionID = subscription.SubscriptionID,
                     Count = subscription.Count,
                     Month = subscription.Month,
                     Title = subscription.Title,
                     Price = subscription.Price,                   
                     IsActive = subscription.IsActive,
                     AdviserTime = subscription.AdviserTime,
                     Discount = subscription.Discount,
                     Time = subscription.Time
                };

                _db.Subscriptions.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { subscription }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Subscription_Destroy([DataSourceRequest]DataSourceRequest request, Subscription subscription)
        {

             var entity = _db.Subscriptions.Find(subscription.SubscriptionID);
            _db.Subscriptions.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { subscription }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}