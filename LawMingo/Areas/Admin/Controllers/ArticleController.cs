﻿
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class ArticleController : Controller
    {

        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public ArticleController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
            db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }
   
        public ActionResult Articles_Read([DataSourceRequest]DataSourceRequest request, int CI)
        {
            IQueryable<Article> articles = db.Articles.Where(q => q.ContractID == CI);
            DataSourceResult result = articles.ToDataSourceResult(request, article => new {
                ArticleID = article.ArticleID,
                Name = article.Name,
                Time = article.Time,
                ApplicationUserId = article.ApplicationUserId,
                ContractID = article.ContractID,
                Order = article.Order,
            });

            return Json(result);
        }

   
        public ActionResult Articles_Create([DataSourceRequest]DataSourceRequest request, Article article, int CI)
        {
            if (ModelState.IsValid)
            {
                var entity = new Article
                {
                    Name = article.Name,
                    ContractID = CI,
                    Time = article.Time,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                    Order = article.Order,
                };

                db.Articles.Add(entity);
                db.SaveChanges();
                article.ArticleID = entity.ArticleID;
            }

            return Json(new[] { article }.ToDataSourceResult(request, ModelState));
        }

       
        public ActionResult Articles_Update([DataSourceRequest]DataSourceRequest request, Article article, int CI)
        {
            if (ModelState.IsValid)
            {
                var entity = new Article
                {
                    ArticleID = article.ArticleID,
                    ContractID = CI,
                    Name = article.Name,
                    Time = article.Time,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                    Order = article.Order,
                };

                db.Articles.Attach(entity);
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(new[] { article }.ToDataSourceResult(request, ModelState));
        }

        
        public ActionResult Articles_Destroy([DataSourceRequest]DataSourceRequest request, Article article)
        {

            var entity = db.Articles.Find(article.ArticleID);
            db.Articles.Remove(entity);
            db.SaveChanges();
            return Json(new[] { article }.ToDataSourceResult(request, ModelState));
        }

         
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
