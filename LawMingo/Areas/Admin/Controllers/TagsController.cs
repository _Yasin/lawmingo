﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]

    public class TagsController : Controller
    {
        private readonly ApplicationDbContext db;

        public TagsController(ApplicationDbContext _db)
        {
            db = _db;
        }

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Tags_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Tag> tags = db.Tags;
            DataSourceResult result = tags.ToDataSourceResult(request, tag => new {
                TagID = tag.TagID,
                EnglishTitle = tag.EnglishTitle,
                Title = tag.Title,
                Description = tag.Description,
                Image = tag.Image,
                Icon = tag.Icon,
                Color = tag.Color,
                FontColor = tag.FontColor,
                Type = tag.Type
            });

            return Json(result);
        }

        [HttpPost]
        public ActionResult Tags_Create([DataSourceRequest]DataSourceRequest request, Tag tag , TagType _Type , string value = null)
        {
            tag.Title = tag.Title ?? value;
            tag.EnglishTitle = tag.EnglishTitle ?? tag.Title;
            
            var entity = new Tag
            {
                TagID = tag.TagID,
                EnglishTitle = tag.EnglishTitle,
                Title = tag.Title,
                Description = tag.Description,
                Image = tag.Image,
                Icon = tag.Icon,
                Color = tag.Color,
                FontColor = tag.FontColor,
                Type = _Type
            };

            db.Tags.Add(entity);
            db.SaveChanges();
            tag.TagID = entity.TagID;

            return Json(new[] { tag }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Tags_Update([DataSourceRequest]DataSourceRequest request, Tag tag)
        {
            var entity = db.Tags.Find(tag.TagID);

            entity.Title = tag.Title;
            entity.EnglishTitle = tag.EnglishTitle;
            entity.Description = tag.Description;
            entity.Icon = tag.Icon;
            entity.Image = tag.Image;
            entity.Color = tag.Color;
            entity.FontColor = tag.FontColor;
            entity.Type = tag.Type;

            db.SaveChanges();

            return Json(new[] { tag }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Tags_Destroy([DataSourceRequest]DataSourceRequest request, Tag tag)
        {
            if (ModelState.IsValid)
            {
                var entity = new Tag
                {
                    TagID = tag.TagID,
                    Title = tag.Title,
                    Description = tag.Description,
                    Color = tag.Color,
                    FontColor = tag.FontColor,
                    Type = tag.Type
                };

                db.Tags.Attach(entity);
                db.Tags.Remove(entity);
                db.SaveChanges();
            }

            return Json(new[] { tag }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
