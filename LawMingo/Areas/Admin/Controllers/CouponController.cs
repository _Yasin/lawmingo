﻿
using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class CouponController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public CouponController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }



        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public ActionResult List()
        {


            //for (int i = 0; i < 200; i++)
            //{

            //  var name =   RandomString(8);

            //     var entity = new Coupon
            //    {
            //        Amount = 25,
            //        CategoryID = null,
            //        Count = 2,
            //        discountType =  Coupon.DiscountType.Percent,
            //        End = DateTime.Now.AddYears(1),
            //        Name = name,
            //        Start = DateTime.Now,
            //        Time = DateTime.Now,
            //        IsActive = true,
            //    };

            //    _db.Coupons.Add(entity);
            //    _db.SaveChanges();
            //}



            PopulateCategories();
            return View();
        }

        public ActionResult Coupons_Read([DataSourceRequest]DataSourceRequest request , int? CI)
        {

            var coupons = _db.Coupons.Include("ApplicationUser").Include("Category").Include("UserContract.Contract").Include("UserContract.PaymentLogs").ToList();

            if(CI != null)
            {
                coupons = coupons.Where(w => w.ParentID == CI).ToList();
            }
            else
            {
                coupons = coupons.Where(w => w.ParentID == null).ToList();
            }


            DataSourceResult result = coupons.ToDataSourceResult(request, coupon => new CouponViewModel
            {
                Amount = coupon.Amount,
                ApplicationUserId = coupon.ApplicationUserId,
                CategoryID = coupon.CategoryID,
                Count = coupon.Count,
                CouponID = coupon.CouponID,
                discountType = coupon.discountType,
                Name = coupon.Name,
                Start = coupon.Start,
                End = coupon.End,
                Time = coupon.Time,
                IsActive = coupon.IsActive,
                UserContractID = coupon.UserContractID,
                ConsultationRequestID = coupon.ConsultationRequestID,
                UserSubscriptionID = coupon.UserSubscriptionID,
                ParentID = coupon.ParentID,
                Mobile = coupon.ApplicationUser != null ? coupon.ApplicationUser.PhoneNumber : "",
                Email = coupon.ApplicationUser != null ? coupon.ApplicationUser.Email : "",
                ContractName = coupon.UserContract != null ? coupon.UserContract.Contract.Name : "",
                Payment = coupon.UserContract != null ? coupon.UserContract.PaymentLogs.Any(e => e.IsSuccessful) : false,
                PaymentAmount = coupon.UserContract != null ? _db.PaymentLogs.Where(e => e.UserContractID == coupon.UserContractID).FirstOrDefault().Amount : 0,
                ContractPrice = coupon.UserContract != null ? coupon.UserContract.Contract.Price.GetValueOrDefault() : 0,
                usageCount = coupon.Children != null && coupon.Children.Count() > 0 ? coupon.Children.Count() : 0,


            }); 

            return Json(result);

        }

        public ActionResult Coupons_Create([DataSourceRequest]DataSourceRequest request, Coupon coupon)
        {
            if (ModelState.IsValid)
            {
                var entity = new Coupon
                {
                     Amount = coupon.Amount,                  
                     CategoryID = coupon.CategoryID,
                     Count = coupon.Count,
                     discountType = coupon.discountType,
                     End = coupon.End,
                     Name = coupon.Name,
                     Start = coupon.Start,
                     Time = DateTime.Now,
                     IsActive = coupon.IsActive,
                };

                _db.Coupons.Add(entity);
                _db.SaveChanges();
                coupon.CouponID = entity.CouponID;
            }

            return Json(new[] { coupon }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Coupons_Update([DataSourceRequest]DataSourceRequest request, Coupon coupon)
        {
            if (ModelState.IsValid)
            {
                var entity = new Coupon
                {
                     CouponID = coupon.CouponID,
                     Amount = coupon.Amount,
                     CategoryID = coupon.CategoryID,
                     Name = coupon.Name,
                     Start = coupon.Start,
                     End = coupon.End,
                     discountType = coupon.discountType,
                     Time = coupon.Time,
                     Count = coupon.Count,
                     IsActive = coupon.IsActive,                    
                   
                };

                _db.Coupons.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { coupon }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Coupons_Destroy([DataSourceRequest]DataSourceRequest request, Coupon coupon)
        {

             var entity = _db.Coupons.Find(coupon.CouponID);
            _db.Coupons.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { coupon }.ToDataSourceResult(request, ModelState));
        }

        private void PopulateCategories()
        {

            var Categories = _db.Categories
                        .Select(c => new CategoryViewModel
                        {
                            CategoryID = c.CategoryID,
                            Name = c.Name
                        })
                        .OrderBy(e => e.Name);

            ViewData["Categories"] = Categories;

            //var Contracts = _db.Contract
            //          .Select(c => new Contract_ViewModel
            //          {
            //               ContractID = c.ContractID,
            //               Name = c.Name
            //          })
            //          .OrderBy(e => e.Name);

            //ViewData["Contracts"] = Contracts;
        }


        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}
