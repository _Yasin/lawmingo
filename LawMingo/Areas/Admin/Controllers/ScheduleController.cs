﻿
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class ScheduleController : Controller
    {

        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public ScheduleController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
            db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Schedules_Read([DataSourceRequest]DataSourceRequest request, int CI)
        {
            IQueryable<Schedule> schedules = db.Schedules.Where(q => q.CounselorID == CI);
            DataSourceResult result = schedules.ToDataSourceResult(request, schedule => new
            {
                schedule.ScheduleID,
                schedule.CounselorID,
                schedule.day,
                schedule.StartTimeRange1,
                schedule.StartTimeRange2,
                schedule.StartTimeRange3,
                schedule.StartTimeRange4,
                schedule.StartTimeRange5,
                schedule.StartTimeRange6,
                schedule.StartTimeRange7,
                schedule.EndTimeRange1,
                schedule.EndTimeRange2,
                schedule.EndTimeRange3,
                schedule.EndTimeRange4,
                schedule.EndTimeRange5,
                schedule.EndTimeRange6,
                schedule.EndTimeRange7,

            });

            return Json(result);
        }


        public ActionResult Schedules_Create([DataSourceRequest]DataSourceRequest request, Schedule schedule, int CI)
        {
            if (ModelState.IsValid)
            {
                var entity = new Schedule
                {
                    CounselorID = CI,
                    day = schedule.day,
                    StartTimeRange1 = schedule.StartTimeRange1,
                    StartTimeRange2 = schedule.StartTimeRange2,
                    StartTimeRange3 = schedule.StartTimeRange3,
                    StartTimeRange4 = schedule.StartTimeRange4,
                    StartTimeRange5 = schedule.StartTimeRange5,
                    StartTimeRange6 = schedule.StartTimeRange6,
                    StartTimeRange7 = schedule.StartTimeRange7,
                    EndTimeRange1 = schedule.EndTimeRange1,
                    EndTimeRange2 = schedule.EndTimeRange2,
                    EndTimeRange3 = schedule.EndTimeRange3,
                    EndTimeRange4 = schedule.EndTimeRange4,
                    EndTimeRange5 = schedule.EndTimeRange5,
                    EndTimeRange6 = schedule.EndTimeRange6,
                    EndTimeRange7 = schedule.EndTimeRange7,
                };

                db.Schedules.Add(entity);
                db.SaveChanges();
            }

            return Json(new[] { schedule }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Schedules_Update([DataSourceRequest]DataSourceRequest request, Schedule schedule)
        {
            if (ModelState.IsValid)
            {
                var entity = new Schedule
                {
                    ScheduleID = schedule.ScheduleID,
                    CounselorID = schedule.CounselorID,
                    day = schedule.day,
                    StartTimeRange1 = schedule.StartTimeRange1,
                    StartTimeRange2 = schedule.StartTimeRange2,
                    StartTimeRange3 = schedule.StartTimeRange3,
                    StartTimeRange4 = schedule.StartTimeRange4,
                    StartTimeRange5 = schedule.StartTimeRange5,
                    StartTimeRange6 = schedule.StartTimeRange6,
                    StartTimeRange7 = schedule.StartTimeRange7,
                    EndTimeRange1 = schedule.EndTimeRange1,
                    EndTimeRange2 = schedule.EndTimeRange2,
                    EndTimeRange3 = schedule.EndTimeRange3,
                    EndTimeRange4 = schedule.EndTimeRange4,
                    EndTimeRange5 = schedule.EndTimeRange5,
                    EndTimeRange6 = schedule.EndTimeRange6,
                    EndTimeRange7 = schedule.EndTimeRange7,
                };

                db.Schedules.Attach(entity);
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(new[] { schedule }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Schedules_Destroy([DataSourceRequest]DataSourceRequest request, Schedule schedule)
        {

            var entity = db.Schedules.Find(schedule.ScheduleID);
            db.Schedules.Remove(entity);
            db.SaveChanges();
            return Json(new[] { schedule }.ToDataSourceResult(request, ModelState));
        }

    }
}
