﻿
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class AnswerController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

      public AnswerController(ApplicationDbContext db , UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }
   
        public ActionResult List()
        {
            return View();
        }

        public ActionResult Answers_Read([DataSourceRequest]DataSourceRequest request, int QU)
        {

            IQueryable<Answer> answers = _db.Answers.Where(q => q.QuestionID == QU);
            DataSourceResult result = answers.ToDataSourceResult(request, answer => new
            {
                 answer.AnswerID,
                 answer.ApplicationUserId,
                 answer.QuestionID,
                 answer.Time,
                 answer.MyAnswer,
                 answer.EnglishAnswer
            });

            return Json(result);

        }
 
        public ActionResult Answers_Create([DataSourceRequest]DataSourceRequest request, Answer answer, int QU)
        {
            if (ModelState.IsValid)
            {
                var entity = new Answer
                {
                    QuestionID = QU,
                    MyAnswer = answer.MyAnswer,
                    EnglishAnswer = answer.EnglishAnswer,
                    Time = answer.Time,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),

                };

                _db.Answers.Add(entity);
                _db.SaveChanges();
                answer.AnswerID = entity.AnswerID;
            }

            return Json(new[] { answer }.ToDataSourceResult(request, ModelState));
        }
    
        public ActionResult Answers_Update([DataSourceRequest]DataSourceRequest request, Answer answer)
        {
            if (ModelState.IsValid)
            {
                var entity = new Answer
                {
                    AnswerID = answer.AnswerID,
                    MyAnswer = answer.MyAnswer,
                    EnglishAnswer = answer.EnglishAnswer,
                    Time = answer.Time,
                    QuestionID = answer.QuestionID,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),

                };

                _db.Answers.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { answer }.ToDataSourceResult(request, ModelState));
        }

      
        public ActionResult Answers_Destroy([DataSourceRequest]DataSourceRequest request, Answer answer)
        {

            var entity = _db.Answers.Find(answer.AnswerID);
            _db.Answers.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { answer }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}
