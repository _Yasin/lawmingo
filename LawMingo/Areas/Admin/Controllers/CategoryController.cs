﻿
using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using LawMingo.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using LawMingo.Models.DomainModel;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{

    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class CategoryController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public CategoryController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }


        public ActionResult Category_Read([DataSourceRequest]DataSourceRequest request)
        {

            IQueryable<Category> Categories = _db.Categories;
            DataSourceResult result = Categories.ToDataSourceResult(request, category => new
            {
                category.CategoryID,
                category.Name,
                category.EnglishName,             
                category.Time,
            });

            return Json(result);
        }

        public ActionResult Category_Create([DataSourceRequest]DataSourceRequest request, Category Category)
        {
            if (ModelState.IsValid)
            {
                var entity = new Category
                {
                    EnglishName = Category.EnglishName,
                    Name = Category.Name,                
                    Time = DateTime.Now,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                };

                _db.Categories.Add(entity);
                _db.SaveChanges();
            }

            return Json(new[] { Category }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Category_Update([DataSourceRequest]DataSourceRequest request, Category Category)
        {
            if (ModelState.IsValid)
            {
                var entity = new Category
                {
                    CategoryID = Category.CategoryID,
                    EnglishName = Category.EnglishName,
                    Name = Category.Name,              
                    Time = DateTime.Now,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                };

                _db.Categories.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { Category }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Category_Destroy([DataSourceRequest]DataSourceRequest request, Category Category)
        {

            var entity = _db.Categories.Find(Category.CategoryID);
            _db.Categories.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { Category }.ToDataSourceResult(request, ModelState));
        }


        //public JsonResult TreeView(int? id)
        //{
        //    var Categories = from c in db.Categories
        //                     where (id.HasValue ? c.ParentID == id : c.ParentID == null)
        //                     select new
        //                     {
        //                         id = c.CategoryID,
        //                         Name = c.Name,
        //                         hasChildren = c.Children.Any()
        //                     };
        //    return Json(Categories);
        //}


        //public JsonResult AddNode(Category category)
        //{
        //    try
        //    {
        //        category.EnglishName = category.EnglishName.Trim().Replace(' ', '-');
        //        category.ApplicationUserId = _userManager.GetUserId(HttpContext.User);
        //        category.Time = DateTime.Now;
        //        db.Categories.Add(category);
        //        db.SaveChanges();
        //    }
        //    catch(Exception)
        //    {
        //        return Json(new { errorMessage = "گره وارد شده تکراری می باشد ." });
        //    }
        //    return Json("");
        //}


        //public ActionResult DeleteNode(int id)
        //{
        //    var cat =  db.Categories.Find(id);
        //    db.Categories.Remove(cat);
        //    db.SaveChanges();
        //    return Json("گره با موفقیت حذف شد .");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    _db.Dispose();
        //    base.Dispose(disposing);
        //}
    }
}