﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class QuestionsController : Controller
    {
        private readonly ApplicationDbContext db;

        public QuestionsController(ApplicationDbContext _db)
        {
            db = _db;
        }

        public ActionResult List()
        {

            var Counselors = db.Counselors
                        .Select(c => new Counselor
                        {
                            CounselorID = c.CounselorID,
                            Name = c.Name
                        })
                        .OrderBy(e => e.Name);

            ViewData["Counselors"] = Counselors;

            return View();
        }

        public ActionResult Questions_Read([DataSourceRequest]DataSourceRequest request, int? _ContractID =  null , int? _CommentID = null)
        {
            IQueryable<Comment> comments = db.Comments.Include(c => c.Category).Include(c => c.Counselor)
                .Where(t=> t.ParentID == _CommentID && (_CommentID == null ? t.Code != null : true));
            DataSourceResult result = comments.ToDataSourceResult(request, comment => new Comment
            {
                CommentID = comment.CommentID,
                Time = comment.Time,
                IsActive = comment.IsActive,
                IsRead = comment.IsRead,
                Subject = comment.Subject,
                Content = comment.Content,
                Author = comment.Author,
                Code = comment.Code,
                CategoryID = comment.CategoryID,
                BreadCrump = Categories.GetParents(db, comment.CategoryID).Select(s => s.Name).ToList(),
                Tags = comment.Tags,
                CounselorID = comment.CounselorID,
                Counselor = comment.Counselor,
                AuthorEmail = comment.AuthorEmail
            });

            db.SaveChanges();

            return Json(result);
        }

        [HttpPost]
        public ActionResult Questions_Create([DataSourceRequest]DataSourceRequest request, Comment comment, int _ContractID, int? _CommentID = null)
        {
            var entity = new Comment
            {
                ContractID = _ContractID == 0 ? (int?)null : _ContractID,
                Content = comment.Content,
                Author = comment.Author,
                CategoryID = comment.CategoryID,
                CounselorID = comment.CounselorID,
                Tags = comment.Tags,
                AuthorEmail = "admin",
                IsActive = true,
                ParentID = _CommentID,
                IsPublic = comment.IsPublic,
                Time = DateTime.Now,
                Visits = comment.Visits,
                Likes = comment.Likes,
                DiskLikes = comment.DiskLikes,
                CampaignName = comment.CampaignName
            };
            if (_CommentID != null) { 
                var parentcm = db.Comments.Where(c => c.CommentID == _CommentID).FirstOrDefault();

                var mobile = parentcm.AuthorEmail;
                var Code = parentcm.Code;
                parentcm.Tags = comment.Tags;
                parentcm.CategoryID = comment.CategoryID;

                try
                {
                    SMS.Verify(mobile, Code.ToString(), "CommentAnswer");
                }
                catch (Exception e)
                {
                }
            }
            db.Comments.Add(entity);
            db.SaveChanges();
            comment.CommentID = entity.CommentID;

            return Json(new[] { comment }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Questions_Update([DataSourceRequest]DataSourceRequest request, Comment comment)
        {
            var entity = db.Comments.Find(comment.CommentID);
            
            entity.Subject = comment.Subject;
            entity.Content = comment.Content;

            entity.CategoryID = comment.CategoryID;
            entity.CounselorID = comment.CounselorID;
            entity.Tags = comment.Tags;
            entity.Visits = comment.Visits;
            entity.Likes = comment.Likes;
            entity.DiskLikes = comment.DiskLikes;
            entity.CampaignName = comment.CampaignName;

            db.SaveChanges();

            comment.CommentID = entity.CommentID;
            return Json(new[] { comment }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult active(int id)
        {
            var entity = db.Comments.Find(id);
            entity.IsActive = !entity.IsActive;
            db.SaveChanges();
            return Json(entity.IsActive);
        }
        [HttpPost]
        public ActionResult read(int id)
        {
            var entity = db.Comments.Find(id);
            entity.IsRead = true;
            db.SaveChanges();
            return Json(true);
        }
        [HttpPost]
        public ActionResult remove(int id)
        {
            var entity = db.Comments.Find(id);
            db.Comments.Remove(entity);
            db.SaveChanges();
            return Json(true);
        }

        [HttpPost]
        public ActionResult Questions_Destroy([DataSourceRequest]DataSourceRequest request, Comment comment)
        {
            var entity = db.Comments.Find(comment.CommentID);
            db.Comments.Remove(entity);
            db.SaveChanges();

            return Json(new[] { comment }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
