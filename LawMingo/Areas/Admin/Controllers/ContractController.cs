﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using LawMingo.Areas.Admin.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.IO;
using LawMingo.Helpers;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
using LawMingo.Models.DomainModel;

namespace LawMingo.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ContractController : Controller
    {

        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ContractController(ApplicationDbContext db, UserManager<ApplicationUser> userManager, IHostingEnvironment hostingEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult List()
        {
            PopulateCategories();
            return View();
        }

        public ActionResult Contracts_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<LawMingo.Models.DomainModel.Contract> contract = _db.Contract.Include(c=>c.Comments);
            DataSourceResult result = contract.ToDataSourceResult(request, cont => new {

                cont.ContractID,
                cont.EnglishName,
                cont.Time,
                cont.ApplicationUserId,
                cont.Price,
                cont.IsVisible,
                cont.Discount,
                Description = System.Net.WebUtility.HtmlDecode(cont.Description),
                cont.Name,
                cont.Score,
                cont.Visits,
                cont.Banner,
                cont.Image,
                cont.Summery,
                cont.Type,
                cont.File,
                cont.Video,
                cont.VideoCover,
                cont.Order,
                cont.Title,
                cont.MetaDescription,
                QuestionCount = cont.Comments.Where(c => !c.IsRead && !c.IsPublic).Count()

            });

            return Json(result);
        }

        public ActionResult Contracts_Create([DataSourceRequest]DataSourceRequest request, LawMingo.Models.DomainModel.Contract contract, string Desc)
        {
            if (ModelState.IsValid)
            {
                var entity = new LawMingo.Models.DomainModel.Contract
                {
                    Name = contract.Name,
                    EnglishName = contract.EnglishName,
                    Time = DateTime.Now,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                    Price = contract.Price,
                    IsVisible = false,
                    Discount = contract.Discount,
                    Banner = contract.Banner,
                    Image = contract.Image,
                    Description = Desc,
                    Summery = contract.Summery,
                    Video = contract.Video,
                    VideoCover = contract.VideoCover,
                    Score = 0,
                    Visits = 0,
                    Type = contract.Type,
                    File = contract.File,
                    Order = contract.Order,
                    Title = contract.Title,
                    MetaDescription = contract.MetaDescription
                };

                _db.Contract.Add(entity);
                _db.SaveChanges();
                contract.ContractID = entity.ContractID;
            }

            return Json(new[] { contract }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Contracts_Update([DataSourceRequest]DataSourceRequest request, LawMingo.Models.DomainModel.Contract contract, string Desc)
        {
            if (ModelState.IsValid)
            {
                var entity = new LawMingo.Models.DomainModel.Contract
                {
                    ContractID = contract.ContractID,
                    Name = contract.Name,
                    EnglishName = contract.EnglishName,
                    Time = DateTime.Now,
                    ApplicationUserId = contract.ApplicationUserId,
                    Price = contract.Price,
                    IsVisible = contract.IsVisible,
                    Discount = contract.Discount,
                    Banner = contract.Banner,
                    Image = contract.Image,
                    Description = Desc,
                    Score = contract.Score,
                    Visits = contract.Visits,
                    Summery = contract.Summery,
                    Video = contract.Video,
                    VideoCover = contract.VideoCover,
                    Type = contract.Type,
                    File = contract.File,
                    Order = contract.Order,
                    Title = contract.Title,
                    MetaDescription = contract.MetaDescription
                };

                _db.Contract.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { contract }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Contract_Destroy([DataSourceRequest]DataSourceRequest request, LawMingo.Models.DomainModel.Contract contract)
        {

            var entity = _db.Contract.Find(contract.ContractID);
            _db.Contract.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { contract }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public async Task<JsonResult> SaveContractImage([Bind(Prefix = "Image.file")] IFormFile file)
        {

            string fileName = "";
            if (CheckIfImageFile(file) && file.Length > 1)
            {
                try
                {
                    string folderName = "Uploads";
                    string SubFolder = "ContractImage";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName, SubFolder);

                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }

                    string Extention = Path.GetExtension(file.FileName);
                    fileName = file.FileName.Split(".")[0] + "__" + _userManager.GetUserId(HttpContext.User) + Extention;

                    string fullPath = Path.Combine(newPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
                catch (Exception)
                {

                }
            }
            return Json(new { ContractImageName = fileName });
        }

        [HttpPost]
        public async Task<JsonResult> SaveContractBanner([Bind(Prefix = "Banner.file")] IFormFile file)
        {

            string fileName = "";
            if (CheckIfImageFile(file) && file.Length > 1)
            {
                try
                {
                    string folderName = "Uploads";
                    string SubFolder = "ContractBanner";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName, SubFolder);

                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }

                    string Extention = Path.GetExtension(file.FileName);
                    fileName = file.FileName.Split(".")[0] + "__" + _userManager.GetUserId(HttpContext.User) + Extention;

                    string fullPath = Path.Combine(newPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
                catch (Exception)
                {

                }
            }

            return Json(new { ContractBannerName = fileName });
        }

        [HttpPost]
        public async Task<JsonResult> SaveContractFile([Bind(Prefix = "File.Contractfile")]  IFormFile file)
        {

            string fileName = "";
            if (file.Length > 1)
            {
                try
                {
                    string folderName = "Uploads";
                    string SubFolder = "ContractTemplate";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName, SubFolder);

                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }


                    //Guid id = Guid.NewGuid();

                    //string Extention = Path.GetExtension(file.FileName);
                    //fileName = file.FileName.Split(".")[0]  + Extention;

                    string fullPath = Path.Combine(newPath, file.FileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
                catch (Exception)
                {

                }
            }

            return Json(new { ContractFileName = file.FileName });
        }




        public ActionResult RemoveContractImage(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {

                    string Name = fullName.Split('.')[0].Trim() + "__" + _userManager.GetUserId(HttpContext.User) + "." + fullName.Split('.')[1].Trim();
                    var fileName = Path.GetFileName(Name);
                    var physicalPath = Path.Combine(_hostingEnvironment.WebRootPath, "Uploads\\ContractImage", fileName);
                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }

            return Content("");
        }

        public ActionResult RemoveContractBanner(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {

                    string Name = fullName.Split('.')[0].Trim() + "__" + _userManager.GetUserId(HttpContext.User) + "." + fullName.Split('.')[1].Trim();
                    var fileName = Path.GetFileName(Name);
                    var physicalPath = Path.Combine(_hostingEnvironment.WebRootPath, "Uploads\\ContractBanner", fileName);
                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }

            return Content("");
        }



        private bool CheckIfImageFile(IFormFile file)
        {
            byte[] fileBytes;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                fileBytes = ms.ToArray();
            }

            return WriterHelper.GetImageFormat(fileBytes) != WriterHelper.ImageFormat.unknown;
        }

        public JsonResult GetProperties()
        {
            return Json(_db.ArticleProperties);
        }

        private void PopulateCategories()
        {

            var Categories = _db.Categories
                        .Select(c => new CategoryViewModel
                        {
                            CategoryID = c.CategoryID,
                            Name = c.Name
                        })
                        .OrderBy(e => e.Name);

            ViewData["Categories"] = Categories;

            var Properties = _db.ArticleProperties
                     .Select(c => new PropertyViewModel
                     {
                         ArticlePropertyID = c.ArticlePropertyID,
                         PropertyName = c.PropertyName
                     })
                     .OrderBy(e => e.PropertyName);

            ViewData["Properties"] = Properties;

            var Articles = _db.Articles

                   .Select(c => new ArticleViewModel
                   {
                       ArticleID = c.ArticleID,
                       Name = c.Name
                   })
                   .OrderBy(e => e.ArticleID);

            ViewData["Articles"] = Articles;

            var ArticleDetails = _db.ArticleDetails

                  .Select(c => new ArticleDetailsViewModel
                  {
                      ArticleDetailID = c.ArticleDetailID,
                      Content = c.ArticleContent
                  })
                  .OrderBy(e => e.ArticleDetailID);

            ViewData["ArticleDetails"] = ArticleDetails;

        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

    }
}