﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{


    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class CounselorCategoryController : Controller
    {

        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public CounselorCategoryController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult CounselorCategory_Read([DataSourceRequest]DataSourceRequest request , int? _ParentID = null)
        {

            IQueryable<CounselorCategory> CounselorCategories = _db.CounselorCategories.Where(c => c.ParentID == _ParentID);
            DataSourceResult result = CounselorCategories.ToDataSourceResult(request, CounselorCategory => new
            {
                CounselorCategory.CounselorCategoryID,
                CounselorCategory.Name,
                CounselorCategory.EnglishName,
                CounselorCategory.ParentID
            });

            return Json(result);
        }

        public ActionResult CounselorCategory_Create([DataSourceRequest]DataSourceRequest request, CounselorCategory CounselorCategory, int? _ParentID = null)
        {
            if (ModelState.IsValid)
            {
                var entity = new CounselorCategory
                {
                    EnglishName = CounselorCategory.EnglishName,
                    Name = CounselorCategory.Name,
                    ParentID = _ParentID
                };

                _db.CounselorCategories.Add(entity);
                _db.SaveChanges();
            }

            return Json(new[] { CounselorCategory }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult CounselorCategory_Update([DataSourceRequest]DataSourceRequest request, CounselorCategory CounselorCategory)
        {
            if (ModelState.IsValid)
            {
                var entity = _db.CounselorCategories.Find(CounselorCategory.CounselorCategoryID);

                entity.EnglishName = CounselorCategory.EnglishName;
                entity.Name = CounselorCategory.Name;

                _db.SaveChanges();
            }

            return Json(new[] { CounselorCategory }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult CounselorCategory_Destroy([DataSourceRequest]DataSourceRequest request, CounselorCategory CounselorCategory)
        {

            var entity = _db.CounselorCategories.Find(CounselorCategory.CounselorCategoryID);
            _db.CounselorCategories.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { CounselorCategory }.ToDataSourceResult(request, ModelState));
        }

    }
}