﻿
using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using LawMingo.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using LawMingo.Models.DomainModel;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{

    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class CategoryAttachController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public CategoryAttachController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult CategoryAttach_Read([DataSourceRequest]DataSourceRequest request, int? CI , int? Co)
        {

            IQueryable<CategoryAttach> Categories = _db.CategoryAttachment;

            if(CI != null)
            {
                Categories = Categories.Where(q => q.ContractID == CI);
            }
            if (Co != null)
            {
                Categories = Categories.Where(q => q.CounselorID == Co);
            }
            DataSourceResult result = Categories.ToDataSourceResult(request, category => new
            {
                category.CategoryAttachID,
                category.CategoryID,            
                category.ContractID,             
                category.CounselorID,
            });

            return Json(result);
        }

        public ActionResult CategoryAttach_Create([DataSourceRequest]DataSourceRequest request, CategoryAttach categoryAttach , int? CI , int? Co)
        {
            if (ModelState.IsValid)
            {
                var entity = new CategoryAttach
                {
                     CategoryID = categoryAttach.CategoryID,                   
                     ContractID = CI, 
                     CounselorID = Co
                };

                _db.CategoryAttachment.Add(entity);
                _db.SaveChanges();
            }

            return Json(new[] { categoryAttach }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult CategoryAttach_Update([DataSourceRequest]DataSourceRequest request, CategoryAttach categoryAttach)
        {
            if (ModelState.IsValid)
            {
                var entity = new CategoryAttach
                {
                    CategoryAttachID = categoryAttach.CategoryAttachID,
                    CategoryID = categoryAttach.CategoryID,                 
                    ContractID = categoryAttach.ContractID,
                    CounselorID = categoryAttach.CounselorID,
                };

                _db.CategoryAttachment.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { categoryAttach }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult CategoryAttach_Destroy([DataSourceRequest]DataSourceRequest request, CategoryAttach categoryAttach)
        {

             var entity = _db.CategoryAttachment.Find(categoryAttach.CategoryAttachID);
            _db.CategoryAttachment.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { categoryAttach }.ToDataSourceResult(request, ModelState));
        }


     
    }
}