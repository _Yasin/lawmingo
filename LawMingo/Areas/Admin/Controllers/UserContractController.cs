﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{

    [Authorize(Roles = "Admin")]
    [Area("Admin")]

    public class UserContractController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserContractController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
             db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult UserContracts_Read([DataSourceRequest]DataSourceRequest request , int? UCI , int? contractPrice , int? PaymentStatus)
        {
            IQueryable<UserContract> UserContracts = db.UserContracts.Include("Contract").Include("ApplicationUser").Include("PaymentLogs");

            if(UCI != null)
            {
                UserContracts = UserContracts.Where(e => e.ParentID == UCI);
            }
            else
            {
                UserContracts = UserContracts.Where(e => e.ParentID == null);
            }

            if(contractPrice != null)
            {
                switch (contractPrice)
                {
                    case 0:
                        UserContracts = UserContracts.Where(e => e.Contract.Price > 0  && e.Contract.Price != null);
                        break;
                    case 1:
                        UserContracts = UserContracts.Where(e => e.Contract.Price == 0 || e.Contract.Price == null);
                        break;                    
                }               
            }

            if (PaymentStatus != null)
            {
                switch (PaymentStatus)
                {
                    case 0:
                        UserContracts = UserContracts.Where(e => !e.PaymentLogs.Any(t => t.IsSuccessful));
                        break;
                    case 1:
                        UserContracts = UserContracts.Where(e => e.PaymentLogs.Any(t => t.IsSuccessful));
                        break;
                }
            }

            DataSourceResult result = UserContracts.ToDataSourceResult(request, UserContract => new UserContractViewModel
            {
                  UserContractID = UserContract.UserContractID,
                  OrderId = UserContract.OrderId,
                  ApplicationUserId = UserContract.ApplicationUserId,
                  ArtsID = UserContract.ArtsID,
                  Content = UserContract.Content,
                  ContractName = UserContract.Contract.Name,
                  EditCount = UserContract.EditCount.GetValueOrDefault(),
                  EditTime = UserContract.EditTime,
                  Email = UserContract.ApplicationUser.Email,
                  Time = UserContract.Time,
                  Word = UserContract.Word,
                  SelectedAnswer = UserContract.SelectedAnswer,                  
                  Price =  UserContract.Contract.Price,
                  Mobile = UserContract.ApplicationUser.PhoneNumber,
                  Payment = UserContract.PaymentLogs.Count > 0 ?  UserContract.PaymentLogs.Any(e => e.IsSuccessful) : false,
                  Amount =  UserContract.PaymentLogs.Any(r => r.IsSuccessful) ? UserContract.PaymentLogs.Where(e => e.IsSuccessful).FirstOrDefault().Amount : 0,   
                  Coupon = db.Coupons.Where(w => w.UserContractID == UserContract.UserContractID).FirstOrDefault() != null ? db.Coupons.Where(w => w.UserContractID == UserContract.UserContractID).FirstOrDefault().Name : ""
            });

            return Json(result);
        }

         
        public ActionResult UserContracts_Destroy([DataSourceRequest]DataSourceRequest request, UserContract userContract)
        {

            var entity = db.UserContracts.Find(userContract.UserContractID);
            db.UserContracts.Remove(entity);
            db.SaveChanges();

            return Json(new[] { userContract }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}