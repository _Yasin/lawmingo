﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Controllers
{

    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class ConsultationRequestController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public ConsultationRequestController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            PopulateCategories();
            return View();
        }

        public ActionResult ConsultationRequest_Read([DataSourceRequest]DataSourceRequest request)
        {

            IQueryable<ConsultationRequest> CounselorsRequests = _db.ConsultationRequests.Include("ApplicationUser");
            DataSourceResult result = CounselorsRequests.ToDataSourceResult(request, counselorsRequests => new
            {

                counselorsRequests.ConsultationRequestID,
                counselorsRequests.AdviceTime,
                ApplicationUserId = counselorsRequests.ApplicationUser.Email,
                counselorsRequests.CounselorID,
                counselorsRequests.Time,
                counselorsRequests.status,
                counselorsRequests.TimeRange,
                counselorsRequests.OrderID,
                counselorsRequests.Mobile,
                counselorsRequests.Description,
                counselorsRequests.FullName,

            });

            return Json(result);

        }
    
        public ActionResult ConsultationRequest_Update([DataSourceRequest]DataSourceRequest request, ConsultationRequest consultationRequest)
        {
            if (ModelState.IsValid)
            {
                var entity = new ConsultationRequest
                {

                   ConsultationRequestID = consultationRequest.ConsultationRequestID,
                   AdviceTime = consultationRequest.AdviceTime,
                   OrderID = consultationRequest.OrderID,
                   TimeRange = consultationRequest.TimeRange,
                   status = consultationRequest.status,
                   Time = consultationRequest.Time,
                   Mobile = consultationRequest.Mobile,
                   CounselorID = consultationRequest.CounselorID,
                   Description = consultationRequest.Description,

                };

                _db.ConsultationRequests.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { consultationRequest }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult ConsultationRequest_Destroy([DataSourceRequest]DataSourceRequest request, ConsultationRequest consultationRequest)
        {

             var entity = _db.ConsultationRequests.Find(consultationRequest.ConsultationRequestID);
            _db.ConsultationRequests.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { consultationRequest }.ToDataSourceResult(request, ModelState));
        }

        private void PopulateCategories()
        {

            var Counselors = _db.Counselors
                        .Select(c => new CounselorViewModel
                        {
                           CounselorID = c.CounselorID,
                           Name = c.Name 
                        })
                        .OrderBy(e => e.Name);

            ViewData["Counselors"] = Counselors;
       
        }


        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}