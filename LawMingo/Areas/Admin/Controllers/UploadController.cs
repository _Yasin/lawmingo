﻿using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;
using Microsoft.AspNetCore.Authorization;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class UploadController : Controller
    {

        private readonly IHostingEnvironment _hostingEnvironment;

        public UploadController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ContractVideo([Bind(Prefix = "Video.ContractVideo")]IFormFile image)
        {
            return UploadFile(image, 0, "Videos");
        }
        public ActionResult ContractVideoCover([Bind(Prefix = "VideoCover.ContractVideoCover")]IFormFile image)
        {
            return UploadFile(image, 0, "Videocovers");
        }

        public JsonResult UploadFile(IFormFile file , int Width, string SubFolder = "", string folderName = "Uploads")
        {
            var fileName = "";
            Guid GUID = Guid.NewGuid();
            try
            {
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName, SubFolder);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }


                string Extention = Path.GetExtension(file.FileName);
                fileName = file.FileName.Split(".")[0] + "__" + GUID + Extention;

                string fullPath = Path.Combine(newPath, fileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

            }
            catch (Exception e)
            {

            }
            return Json(new { Image = fileName });
        }
    }
}
