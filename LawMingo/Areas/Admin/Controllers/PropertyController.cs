﻿
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using LawMingo.Models.DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using LawMingo.Data;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class PropertyController : Controller
    {

        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public PropertyController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
            db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult ArticleProperties_Read([DataSourceRequest]DataSourceRequest request )
        {
            IQueryable<ArticleProperty> articleproperties = db.ArticleProperties;
            DataSourceResult result = articleproperties.ToDataSourceResult(request, articleProperty => new {
                ArticlePropertyID = articleProperty.ArticlePropertyID,
                PropertyName = articleProperty.PropertyName,
                EnglishName = articleProperty.EnglishName,

            });

            return Json(result);
        }

     
        public ActionResult ArticleProperties_Create([DataSourceRequest]DataSourceRequest request, ArticleProperty articleProperty)
        {

           
          if (ModelState.IsValid)
             {
                try
                {
                    var entity = new ArticleProperty
                    {
                        PropertyName = articleProperty.PropertyName,
                        EnglishName = articleProperty.EnglishName,
                    };

                    db.ArticleProperties.Add(entity);
                    db.SaveChanges();
                   
                }
                catch
                {
                    ModelState.AddModelError("", "ویژگی وارد شده تکراری می باشد .");
                }
                    
            }
          
            
            return Json(new[] { articleProperty }.ToDataSourceResult(request, ModelState));
        }
 
        public ActionResult ArticleProperties_Update([DataSourceRequest]DataSourceRequest request, ArticleProperty articleProperty)
        {
            if (ModelState.IsValid)
            {
                var entity = new ArticleProperty
                {
                    ArticlePropertyID = articleProperty.ArticlePropertyID,
                    PropertyName = articleProperty.PropertyName,
                    EnglishName = articleProperty.EnglishName,
                };

                db.ArticleProperties.Attach(entity);
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(new[] { articleProperty }.ToDataSourceResult(request, ModelState));
        }


       
        public ActionResult ArticleProperties_Destroy([DataSourceRequest]DataSourceRequest request, ArticleProperty articleProperty)
        {

               var entity = db.ArticleProperties.Find(articleProperty.ArticlePropertyID);
                db.ArticleProperties.Remove(entity);
                db.SaveChanges();  

            return Json(new[] { articleProperty }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
