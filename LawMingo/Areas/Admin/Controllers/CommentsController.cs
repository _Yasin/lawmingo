﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class CommentsController : Controller
    {
        private readonly ApplicationDbContext db;

        public CommentsController(ApplicationDbContext _db)
        {
            db = _db;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Comments_Read([DataSourceRequest]DataSourceRequest request, int? _ContractID =  null , int? _CommentID = null)
        {
            IQueryable<Comment> comments = db.Comments.Where(t=> t.ContractID == _ContractID && t.ParentID == _CommentID);
            DataSourceResult result = comments.ToDataSourceResult(request, comment => new {
                CommentID = comment.CommentID,
                Time = comment.Time.ToString("yyyy-MM-dd HH:mm:ss"),
                IsActive = comment.IsActive,
                IsRead = comment.IsRead,
                Content = comment.Content,
                Author = comment.Author,
                AuthorEmail = comment.AuthorEmail,
                AuthorIPAddress = comment.AuthorIPAddress
            });

            db.SaveChanges();

            return Json(result);
        }

        [HttpPost]
        public ActionResult Comments_Create([DataSourceRequest]DataSourceRequest request, Comment comment, int _ContractID, int? _CommentID = null)
        {
            var entity = new Comment
            {
                ContractID = _ContractID == 0 ? (int?)null : _ContractID,
                Content = comment.Content,
                Author = comment.Author,
                AuthorEmail = "admin",
                IsActive = true ,
                ParentID = _CommentID,
                Time = DateTime.Now
            };
            if (_CommentID != null)
            {
                var mobile = db.Comments.Find(_CommentID).AuthorEmail;
                var Code = 63836 + _CommentID;

                try
                {
                    SMS.Verify(mobile, Code.ToString(), "CommentAnswer");
                }
                catch (Exception e)
                {
                }
            }
            db.Comments.Add(entity);
            db.SaveChanges();
            comment.CommentID = entity.CommentID;

            return Json(new[] { comment }.ToDataSourceResult(request, ModelState));
        }


        [HttpPost]
        public ActionResult active(int id)
        {
            var entity = db.Comments.Find(id);
            entity.IsActive = !entity.IsActive;
            db.SaveChanges();
            return Json(entity.IsActive);
        }
        [HttpPost]
        public ActionResult read(int id)
        {
            var entity = db.Comments.Find(id);
            entity.IsRead = true;
            db.SaveChanges();
            return Json(true);
        }
        [HttpPost]
        public ActionResult remove(int id)
        {
            var entity = db.Comments.Find(id);
            db.Comments.Remove(entity);
            db.SaveChanges();
            return Json(true);
        }

        [HttpPost]
        public ActionResult Comments_Destroy([DataSourceRequest]DataSourceRequest request, Comment comment)
        {
            var entity = db.Comments.Find(comment.CommentID);
            db.Comments.Remove(entity);
            db.SaveChanges();

            return Json(new[] { comment }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
