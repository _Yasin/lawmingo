﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using LawMingo.Models.DomainModel;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class QuestionController : Controller
    {

        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public QuestionController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
             db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Questions_Read([DataSourceRequest]DataSourceRequest request , int AI , int? AnI)
        {
            IQueryable<Question> questions = AnI == null ?  db.Questions.Where(q => q.ArticleID == AI): db.Questions.Where(q => q.ArticleID == AI && q.AnsID == AnI);
            DataSourceResult result = questions.ToDataSourceResult(request, question => new {
                QuestionID = question.QuestionID,
                MyQuestion = question.MyQuestion,
                Time = question.Time,
                ApplicationUserId = question.ApplicationUserId,
                ArticleID = question.ArticleID,
                AnsID = question.AnsID,
                CheckBox = question.CheckBox,
                End = question.End,
            });

            return Json(result);
        }

     
        public ActionResult Questions_Create([DataSourceRequest]DataSourceRequest request, Question question , int AI , int? AnI)
        {
            if (ModelState.IsValid)
            {
                var entity = new Question
                {
                    MyQuestion = question.MyQuestion,
                    Time = question.Time,
                    ArticleID = AI,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                    AnsID = AnI,
                    CheckBox = question.CheckBox,
                    End = question.End,
                };

                db.Questions.Add(entity);
                db.SaveChanges();
                question.QuestionID = entity.QuestionID;
            }

            return Json(new[] { question }.ToDataSourceResult(request, ModelState));
        }

       
        public ActionResult Questions_Update([DataSourceRequest]DataSourceRequest request, Question question , int AI, int? AnI)
        {
            if (ModelState.IsValid)
            {
                var entity = new Question
                {
                    QuestionID = question.QuestionID,
                    MyQuestion = question.MyQuestion,
                    Time = DateTime.Now,
                    ArticleID = AI,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                    AnsID = question.AnsID,
                    CheckBox = question.CheckBox,
                    End = question.End,
                };

                db.Questions.Attach(entity);
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(new[] { question }.ToDataSourceResult(request, ModelState));
        }

      
        public ActionResult Questions_Destroy([DataSourceRequest]DataSourceRequest request, Question question)
        {

                var entity = db.Questions.Find(question.QuestionID); 
                db.Questions.Remove(entity);
                db.SaveChanges();           

            return Json(new[] { question }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
