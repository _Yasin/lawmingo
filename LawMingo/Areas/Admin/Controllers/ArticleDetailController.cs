﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using LawMingo.Models.DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using LawMingo.Data;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class ArticleDetailController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public ArticleDetailController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
             db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult ArticleDetails_Read([DataSourceRequest]DataSourceRequest request , int AI)
        {
            IQueryable<ArticleDetail> articledetails = db.ArticleDetails.Where(q => q.AnswerID== AI);
            DataSourceResult result = articledetails.ToDataSourceResult(request, articleDetail => new {
                 articleDetail.ArticleDetailID,
                 ArticleContent = System.Net.WebUtility.HtmlDecode(articleDetail.ArticleContent),
                 articleDetail.Time,
                 articleDetail.ApplicationUserId,
                 articleDetail.AnswerID,
            });

            return Json(result);
        }


        public ActionResult ArticleDetails_Create([DataSourceRequest]DataSourceRequest request, ArticleDetail articleDetail, int AI , string Content)
        {
           
                var entity = new ArticleDetail
                {
                    ArticleContent = System.Net.WebUtility.HtmlDecode(Content),
                    AnswerID = AI,
                    Time = DateTime.Now,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                };

                db.ArticleDetails.Add(entity);
                db.SaveChanges();
                articleDetail.ArticleDetailID = entity.ArticleDetailID;
            

            return Json(new[] { articleDetail }.ToDataSourceResult(request, ModelState));
        }

      
        public ActionResult ArticleDetails_Update([DataSourceRequest]DataSourceRequest request, ArticleDetail articleDetail , string Content)
        {
            if (ModelState.IsValid)
            {
                var entity = new ArticleDetail
                {
                    ArticleDetailID = articleDetail.ArticleDetailID,
                    ArticleContent = Content,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                    AnswerID = articleDetail.AnswerID,
                    Time = DateTime.Now,
                };

                db.ArticleDetails.Attach(entity);
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(new[] { articleDetail }.ToDataSourceResult(request, ModelState));
        }

       
        public ActionResult ArticleDetails_Destroy([DataSourceRequest]DataSourceRequest request, ArticleDetail articleDetail)
        {

                var entity = db.ArticleDetails.Find(articleDetail.ArticleDetailID);                          
                db.ArticleDetails.Remove(entity);
                db.SaveChanges();
           
            return Json(new[] { articleDetail }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
