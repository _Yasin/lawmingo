﻿using LawMingo.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class RemoteDataController : Controller
    {
        private readonly ApplicationDbContext db;

        public RemoteDataController(ApplicationDbContext _db)
        {
            db = _db;
        }

        public async Task<ActionResult> GetCounselorTags()
        {
            var res = await db.Tags.Select(t => new {
                title = t.Title,
                id = t.Title,
            }).ToListAsync();
            return Json(res);
        }

        public async Task<ActionResult> GetCounselorCats(int? id = null)
        {
            var res = await db.CounselorCategories.Where(c => c.ParentID == id).Select(p => new
            {
                id = p.CounselorCategoryID,
                name = p.Name,
                hasChildren = p.Children.Any()
            }).ToListAsync();
            return Json(res);
        }

        public async Task<ActionResult> GetCounselors()
        {
            var res = await db.Counselors.Select(p => new
            {
                id = p.CounselorID,
                title = p.Name
            }).ToListAsync();
            return Json(res);
        }

        
    }
}