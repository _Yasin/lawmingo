﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{


    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class HelpController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public HelpController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Helps_Read([DataSourceRequest]DataSourceRequest request, int QI)
        {

            IQueryable<Help> Helps  = _db.Helps.Where(q => q.QuestionID == QI);
            DataSourceResult result = Helps.ToDataSourceResult(request, Help => new
            {
                Help.HelpID,
                Help.QuestionID,
                Help.Like,
                Help.DisLike,
                Help.Content,
                Help.ApplicationUserId,
                Help.Time,
                Help.Visible,
            });

            return Json(result);
        }

        public ActionResult Helps_Create([DataSourceRequest]DataSourceRequest request, Help help, int QI)
        {
            if (ModelState.IsValid)
            {
                var entity = new Help
                {
                    QuestionID = QI,
                    Content = help.Content,
                    DisLike = help.DisLike,
                    Like = help.Like,                  
                    Time = help.Time,
                    Visible = help.Visible,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                };

                _db.Helps.Add(entity);
                _db.SaveChanges();           
            }

            return Json(new[] { help }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Helps_Update([DataSourceRequest]DataSourceRequest request, Help help)
        {
            if (ModelState.IsValid)
            {
                var entity = new Help
                {
                     HelpID  =  help.HelpID,
                     Content = help.Content,
                     DisLike = help.DisLike,
                     Like = help.Like,
                     Time = help.Time,
                     QuestionID = help.QuestionID,
                     Visible = help.Visible,
                     ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                };

                _db.Helps.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { help }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Helps_Destroy([DataSourceRequest]DataSourceRequest request, Help help)
        {

             var entity = _db.Helps.Find(help.HelpID);
            _db.Helps.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { help }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}