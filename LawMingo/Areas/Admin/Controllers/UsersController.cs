﻿
using System;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{

    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public UsersController(ApplicationDbContext db, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _db = db;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Users_Read([DataSourceRequest]DataSourceRequest request)
        {

            var Users = _db.Users;                       
            DataSourceResult result = Users.ToDataSourceResult(request, user => new
            {
                user.Email,
                user.EmailConfirmed,
                user.Id,
                user.LockoutEnabled,
                user.LockoutEnd,
                user.NormalizedEmail,
                user.NormalizedUserName,
                user.PasswordHash,
                user.PhoneNumber,
                user.PhoneNumberConfirmed,
                user.SecurityStamp,
                user.TwoFactorEnabled,
                user.UserName,
                user.AccessFailedCount,
                user.ConcurrencyStamp,
                user.Register,
                user.BlackList,
                user.BirthDate,
                user.Avatar,
                user.Gender,
                user.LastLogin,
                user.BanckCartNumber,
                user.UserStatus,
                user.Name,
                user.Family,
            });

           
            return Json(result);

        }

        public async Task<ActionResult> User_Create([DataSourceRequest]DataSourceRequest request, ApplicationUser User)
        {

           if(!string.IsNullOrEmpty(User.UserName) && !string.IsNullOrEmpty(User.PasswordHash))
            {
                var user = new ApplicationUser
                {
                    Name = User.Name,
                    Family = User.Family,
                    BirthDate = User.BirthDate,
                    Avatar = User.Avatar,
                    Gender = User.Gender,
                    UserName = User.UserName,
                    PhoneNumber = User.UserName,
                    PhoneNumberConfirmed = true,
                    Register = DateTime.Now,
                    Email = User.Email,
                    EmailConfirmed = !string.IsNullOrEmpty(User.Email) ? true : false
                };
              
                try
                {
                    var result = await _userManager.CreateAsync(user, User.PasswordHash);
                    if (result.Succeeded)
                    {
                        return Json(new[] { User }.ToDataSourceResult(request, ModelState));
                    }
                    else
                    {
                        //var errMsg = ModelState.Values
                        //  .Where(x => x.Errors.Count >= 1)
                        //    .Aggregate("خطاهای رغ داده: ", (current, err) => current + err.Errors.Select(x => x.ErrorMessage));
                        //        ModelState.AddModelError(string.Empty, errMsg);

                        ModelState.AddModelError(string.Empty, "اطلاعات وارد شده جهت ایجاد کاربر صحیح نمی باشد .");
                        ModelState.AddModelError(string.Empty, "شماره موبایل تکراری ، رمز عبور کمتر از 6 کاراکتر .");

                        return Json(new[] { User }.ToDataSourceResult(request, ModelState));
                    }
                                             
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);
                }                         
            }
            else
            {

                ModelState.AddModelError(string.Empty, "شماره موبایل و کلمه عبور الزامی است !");
                return Json(new[] { User }.ToDataSourceResult(request, ModelState));
            }
                    
            return Json("");

        }

        public ActionResult User_Update([DataSourceRequest]DataSourceRequest request, ApplicationUser User)
        {
            if (ModelState.IsValid)
            {
                var entity = new ApplicationUser
                {
                     AccessFailedCount = User.AccessFailedCount,
                     Email = User.Email,
                     Id = User.Id,
                     LockoutEnd = User.LockoutEnd,
                     NormalizedEmail = User.NormalizedEmail,
                     PasswordHash = User.PasswordHash,
                     PhoneNumberConfirmed = User.PhoneNumberConfirmed,
                     PhoneNumber = User.PhoneNumber,
                     NormalizedUserName = User.NormalizedUserName,
                     UserName = User.UserName,
                     TwoFactorEnabled = User.TwoFactorEnabled,
                     EmailConfirmed = User.EmailConfirmed,
                     ConcurrencyStamp = User.ConcurrencyStamp,
                     LockoutEnabled = User.LockoutEnabled,
                     SecurityStamp = User.SecurityStamp,
                     Avatar = User.Avatar,
                     BanckCartNumber = User.BanckCartNumber,
                     BirthDate = User.BirthDate,
                     BlackList = User.BlackList,
                     Family = User.Family,
                     Gender = User.Gender,
                     LastLogin = User.LastLogin,
                     Name = User.Name,
                     Register = User.Register,
                     UserStatus = User.UserStatus,
                };

                _db.Users.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { User }.ToDataSourceResult(request, ModelState));
        }


        public async Task<ActionResult> User_DestroyAsync([DataSourceRequest]DataSourceRequest request, ApplicationUser User)
        {

            var logins = await _userManager.GetLoginsAsync(User);
            //var rolesForUser = await _userManager.GetRolesAsync(User);
            var rolesForUser = _db.UserRoles.Where(e => e.UserId == User.Id).ToList();
            //using (var transaction = _db.Database.BeginTransaction())
            //{

            //}

            try
            {
                IdentityResult result = IdentityResult.Success;

                 await  _db.SaveChangesAsync();
                foreach (var login in logins)
                {
                    result = await _userManager.RemoveLoginAsync(User, login.LoginProvider, login.ProviderKey);
                    if (result != IdentityResult.Success)
                        break;
                }
                if (result == IdentityResult.Success)
                {
                    foreach (var item in rolesForUser)
                    {

                        //result = await _userManager.RemoveFromRoleAsync(User, item);
                        _db.UserRoles.Remove(item);
                        _db.SaveChanges();
                           
                            if (result != IdentityResult.Success)
                                break;                        
                    }
                }
                if (result == IdentityResult.Success)
                {                   
                        result = await _userManager.DeleteAsync(User);
                        if (result == IdentityResult.Success)
                    {

                    }
                                              
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(new[] { User }.ToDataSourceResult(request, ModelState));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddRoleAsync(string RoleName , string Id)
        {

            if (!string.IsNullOrEmpty(RoleName))
            {
                var RoleExist = await _roleManager.RoleExistsAsync(RoleName);
                if (!RoleExist)
                {
                    await _roleManager.CreateAsync(new IdentityRole(RoleName));
                }

                var User = _db.Users.Where(q => q.Id == Id).FirstOrDefault();
                if(User != null)
                {
                    var result = await _userManager.AddToRoleAsync(User, RoleName);
                    if (result.Succeeded)
                    {
                        return Json(true);
                    }
                    else
                    {
                        return Json(false);
                    }
                }  
              
            }

            return Json("نقش مورد نظر اضافه گردید .");
        }

     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PasswordChange(string Id, string Password)
        {
            
            var user = await _userManager.FindByNameAsync(Id);
            if (user == null) { /**/ }
                   
            var newPassword =  _userManager.PasswordHasher.HashPassword(user, Password);
            user.PasswordHash = newPassword;
            var res = await _userManager.UpdateAsync(user);

            if (res.Succeeded)
            {
                return Json(true);
            }
            else
            {
                return Json(false);
            }
        }


        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}