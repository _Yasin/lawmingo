﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LawMingo.Helpers;
using LawMingo.Areas.Admin.Models.ViewModels;

namespace LawMingo.Areas.Admin.Controllers
{

    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class CounselorController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;
        public CounselorController(ApplicationDbContext db, UserManager<ApplicationUser> userManager, IHostingEnvironment hostingEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult List()
        {
            PopulateCategories();
            return View();
        }

        public ActionResult Counselors_Read([DataSourceRequest]DataSourceRequest request)
        {

            IQueryable<Counselor> Counselors = _db.Counselors;
            DataSourceResult result = Counselors.ToDataSourceResult(request, counselor => new
            {
                counselor.CounselorID,
                counselor.Name,
                counselor.Post,
                counselor.Education,
                counselor.Time,
                counselor.ApplicationUserId,
                counselor.Image,
                counselor.Summery,            
                counselor.Direct,
                counselor.Phone,
                counselor.Price,
                counselor.CounselationPrice30,
                counselor.CounselationPrice15,
                counselor.CounselationPrice60,
                counselor.IsActive,
                counselor.Thumbnail

            });

            return Json(result);

        }

        public ActionResult Counselors_Create([DataSourceRequest]DataSourceRequest request, Counselor counselor, string description)
        {
            if (counselor.Name != null && counselor.Education != null && counselor.Post != null && description != null)
            {
                var entity = new Counselor
                {
                    Name = counselor.Name,
                    Education = counselor.Education,
                    Post = counselor.Post,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                    Time = counselor.Time,
                    Image = counselor.Image,
                    Summery = description,
                    Phone = counselor.Phone,
                    Direct = counselor.Direct,
                    Price = counselor.Price,
                    CounselationPrice15 = counselor.CounselationPrice15,
                    CounselationPrice30 = counselor.CounselationPrice30,
                    CounselationPrice60 = counselor.CounselationPrice60,                 
                    IsActive = counselor.IsActive,
                    Thumbnail = counselor.Thumbnail,
                };

                _db.Counselors.Add(entity);
                _db.SaveChanges();

            }

            return Json(new[] { counselor }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Counselors_Update([DataSourceRequest]DataSourceRequest request, Counselor counselor, string description)
        {
            if (counselor.Name != null && counselor.Education != null && counselor.Post != null && description != null)
            {
                var entity = new Counselor
                {
                    CounselorID = counselor.CounselorID,
                    Education = counselor.Education,
                    Post = counselor.Post,
                    Name = counselor.Name,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                    Time = counselor.Time,
                    Image = counselor.Image,
                    Summery = description,                 
                    Phone = counselor.Phone,
                    Direct = counselor.Direct,
                    Price = counselor.Price,
                    CounselationPrice15 = counselor.CounselationPrice15,
                    CounselationPrice30 = counselor.CounselationPrice30,
                    CounselationPrice60 = counselor.CounselationPrice60,
                    IsActive = counselor.IsActive,
                    Thumbnail = counselor.Thumbnail,
                };

                _db.Counselors.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { counselor }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult Counselors_Destroy([DataSourceRequest]DataSourceRequest request, Counselor counselor)
        {

            var entity = _db.Counselors.Find(counselor.CounselorID);
            _db.Counselors.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { counselor }.ToDataSourceResult(request, ModelState));
        }


        public async Task<ActionResult> SaveImageAsync([Bind(Prefix = "Image.file")] IFormFile file)
        {

            if (CheckIfImageFile(file) && file.Length > 1)
            {
                try
                {
                    string folderName = "Uploads";
                    string SubFolder = "CounselorImage";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName, SubFolder);

                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }

                    //string Extention = Path.GetExtension(file.FileName);
                    //fileName = file.FileName.Split(".")[0] + "__" + _userManager.GetUserId(HttpContext.User) + Extention;

                    string fullPath = Path.Combine(newPath, file.FileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
                catch (Exception)
                {

                }
            }
            return Json(new { ImageName = file.FileName });
        }

        public async Task<ActionResult> SaveThumbnailAsync([Bind(Prefix = "Thumbnail.thumb")] IFormFile file)
        {

            if (CheckIfImageFile(file) && file.Length > 1)
            {
                try
                {
                    string folderName = "Uploads";
                    string SubFolder = "CounselorThumbnail";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName, SubFolder);

                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }

                    //string Extention = Path.GetExtension(file.FileName);
                    //fileName = file.FileName.Split(".")[0] + "__" + _userManager.GetUserId(HttpContext.User) + Extention;

                    string fullPath = Path.Combine(newPath, file.FileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
                catch (Exception)
                {

                }
            }
            return Json(new { ImageName = file.FileName });
        }

        


        public async Task<ActionResult> RemoveImageAsync(IFormFile file)
        {

            try
            {

            }
            catch (Exception)
            {

            }

            //return Json(new { CounselorImageName = fileName });

            return Json(new { });
        }




        private bool CheckIfImageFile(IFormFile file)
        {
            byte[] fileBytes;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                fileBytes = ms.ToArray();
            }

            return WriterHelper.GetImageFormat(fileBytes) != WriterHelper.ImageFormat.unknown;
        }


        private void PopulateCategories()
        {

            var Categories = _db.Categories
                        .Select(c => new CategoryViewModel
                        {
                            CategoryID = c.CategoryID,
                            Name = c.Name
                        })
                        .OrderBy(e => e.Name);

            ViewData["Categories"] = Categories;

        }


        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}