﻿
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class UserController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;


        public UserController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
             db = _db;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(RegisterUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                                  
                string name = model.Role;
                string password = model.Password;
           
                if (!string.IsNullOrEmpty(name))
                {
                    var RoleExist =  _roleManager.RoleExistsAsync(name);
                    if(!RoleExist.Result)
                    {
                        var roleresult = _roleManager.CreateAsync(new IdentityRole(name));
                    }                   
                }

                var user = new ApplicationUser();
                user.UserName = model.Email;
                user.Email = model.Email;
                user.PasswordHash = model.Password;
                user.EmailConfirmed = true;
          
                var adminresult = _userManager.CreateAsync(user , password);
                if (adminresult.IsCompletedSuccessfully)
                {
                    if (!string.IsNullOrEmpty(name))
                    {
                        var result = _userManager.AddToRoleAsync(user,name);
                    }
                  
                    return Json("<div class='alert alert-success'><strong>کاربر با موفقیت ثبت شد .</strong></div>");
                }
             
                return Json("<div class='alert alert-danger'><strong>ایمیل وارد شده تکراری است .</strong></div>");
            }
            return Json("");
        }

        public ActionResult Password()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PasswordChange(PasswordChangeViewModel model)
        {
        
            var user = _userManager.FindByEmailAsync(model.Email);
            if (user.IsCompletedSuccessfully)
            {
                _userManager.AddPasswordAsync(user.Result, model.Password);
                return Json("<div class='alert alert-success'><strong>کلمه عبور با موفقیت تغییر کرد .</strong></div>");
            }
            else
            {
                return Json("<div class='alert alert-danger'><strong>کاربر مورد نظر یافت نشد .</strong></div>");
            }
          
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}