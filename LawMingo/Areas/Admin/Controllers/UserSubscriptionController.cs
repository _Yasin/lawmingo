﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class UserSubscriptionController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserSubscriptionController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult UserSubscription_Read([DataSourceRequest]DataSourceRequest request)
        {

            IQueryable<UserSubscription> userSubscriptions = _db.UserSubscriptions.Include("ApplicationUser").Include("Subscription").Include("PaymentLogs");
            DataSourceResult result = userSubscriptions.ToDataSourceResult(request, Usersubscrib => new UserSubscriptionViewModel
            {
                 ApplicationUserId = Usersubscrib.ApplicationUserId,
                 Email = Usersubscrib.ApplicationUser != null ? Usersubscrib.ApplicationUser.Email : "",
                 OrderId = Usersubscrib.OrderId,
                 PhoneNumber = Usersubscrib.ApplicationUser != null ? Usersubscrib.ApplicationUser.PhoneNumber : "",
                 Price = Usersubscrib.Subscription.Price,
                 Time = Usersubscrib.Time,
                 UsageCount = Usersubscrib.UsageCount,
                 UserSubscriptionID = Usersubscrib.UserSubscriptionID,
                 Title = Usersubscrib.Subscription.Title,
                 ApplicationUser = Usersubscrib.ApplicationUser,
                 PaymentLogs = Usersubscrib.PaymentLogs,
                 SubscriptionID = Usersubscrib.SubscriptionID,
                 Subscription = Usersubscrib.Subscription,
                 Payment = Usersubscrib.PaymentLogs.Any(e => e.IsSuccessful),
                 PaymentPrice = Usersubscrib.PaymentLogs.Where(e => e.IsSuccessful).FirstOrDefault() != null ? Usersubscrib.PaymentLogs.Where(e => e.IsSuccessful).FirstOrDefault().Amount : 0
            });

            return Json(result);

        }
     
        public ActionResult UserSubscription_Update([DataSourceRequest]DataSourceRequest request, UserSubscription userSubscription)
        {
            if (ModelState.IsValid)
            {
                var entity = new UserSubscription
                {
                     UserSubscriptionID = userSubscription.UserSubscriptionID,
                     ApplicationUserId = userSubscription.ApplicationUserId,
                     SubscriptionID = userSubscription.SubscriptionID,
                     OrderId = userSubscription.OrderId,
                     PaymentLogs = userSubscription.PaymentLogs,
                     Time = DateTime.Now,
                     UsageCount = userSubscription.UsageCount                    
                };

                _db.UserSubscriptions.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }

            return Json(new[] { userSubscription }.ToDataSourceResult(request, ModelState));
        }


        public ActionResult UserSubscription_Destroy([DataSourceRequest]DataSourceRequest request, UserSubscription userSubscription)
        {

             var entity = _db.Subscriptions.Find(userSubscription.SubscriptionID);
            _db.Subscriptions.Remove(entity);
            _db.SaveChanges();

            return Json(new[] { userSubscription }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}