﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using LawMingo.Data;
using Microsoft.AspNetCore.Identity;
using LawMingo.Models.DomainModel;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [Area("Admin")]
    public class RelatedArticleController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public RelatedArticleController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
            db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public ActionResult RelatedArticles_Read([DataSourceRequest]DataSourceRequest request , int AnsId)
        {
            IQueryable<RelatedArticle> relatedarticles = db.RelatedArticles.Where(q => q.AnswerID == AnsId);
            DataSourceResult result = relatedarticles.ToDataSourceResult(request, relatedArticle => new {
                RelatedArticleID = relatedArticle.RelatedArticleID,
                AnswerID = relatedArticle.AnswerID,
                ArticleID = relatedArticle.ArticleID,
                ArticleDetailID = relatedArticle.ArticleDetailID,
                Act = relatedArticle.Act,
                NewContent = relatedArticle.NewContent,
                Time = relatedArticle.Time,
                ApplicationUserId = relatedArticle.ApplicationUserId,
            });

            return Json(result);
        }
  
        public ActionResult RelatedArticles_Create([DataSourceRequest]DataSourceRequest request, RelatedArticle relatedArticle , int AnsId)
        {
            if (ModelState.IsValid)
            {
                var entity = new RelatedArticle
                {
                    Act = relatedArticle.Act,
                    NewContent = relatedArticle.NewContent,
                    AnswerID = AnsId,
                    ArticleID = relatedArticle.ArticleID,
                    ArticleDetailID = relatedArticle.ArticleDetailID,
                    Time = DateTime.Now,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                };

                db.RelatedArticles.Add(entity);
                db.SaveChanges();
                relatedArticle.RelatedArticleID = entity.RelatedArticleID;
            }

            return Json(new[] { relatedArticle }.ToDataSourceResult(request, ModelState));
        }

    
        public ActionResult RelatedArticles_Update([DataSourceRequest]DataSourceRequest request, RelatedArticle relatedArticle)
        {
            if (ModelState.IsValid)
            {
                var entity = new RelatedArticle
                {
                    RelatedArticleID = relatedArticle.RelatedArticleID,
                    Act = relatedArticle.Act,
                    NewContent = relatedArticle.NewContent,
                    AnswerID = relatedArticle.AnswerID,
                    ArticleID = relatedArticle.ArticleID,
                    ArticleDetailID = relatedArticle.ArticleDetailID,
                    Time = DateTime.Now,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                };

                db.RelatedArticles.Attach(entity);
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(new[] { relatedArticle }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult RelatedArticles_Destroy([DataSourceRequest]DataSourceRequest request, RelatedArticle relatedArticle)
        {

                var entity = db.RelatedArticles.Find(relatedArticle.RelatedArticleID);            
                db.RelatedArticles.Remove(entity);
                db.SaveChanges();       

            return Json(new[] { relatedArticle }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
