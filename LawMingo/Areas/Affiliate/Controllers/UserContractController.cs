﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Affiliate.Controllers
{

    [Authorize(Roles = "Affiliate")]
    [Area("Affiliate")]

    public class UserContractController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserContractController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
             db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }
        
        public async Task<ActionResult> UserContracts_Read([DataSourceRequest]DataSourceRequest request)
        {
            //var userid = _userManager.GetUserId(HttpContext.User);
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var groups = (await db.Affiliates.Where(a => a.ApplicationUserId == userId)
                .Include("Groups.Details").FirstOrDefaultAsync()).Groups.Select(g => g.Details).ToList();
            var details = new List<int>();
            groups.ForEach(g => details.AddRange(g.Select(s => s.AffiliateDetailID)));

            IQueryable<UserContract> UserContracts = db.UserContracts
                .Where(u => u.AffiliateDetailID != null && details.Contains(u.AffiliateDetailID ?? 0))
                .Include("Contract").Include("AffiliateDetail.AffiliateGroup").Include("ApplicationUser");

            DataSourceResult result = UserContracts.ToDataSourceResult(request, UserContract => new UserContractViewModel
            {
                  UserContractID = UserContract.UserContractID,
                  ApplicationUserId = UserContract.ApplicationUserId,
                  ArtsID = UserContract.ArtsID,
                  Content = UserContract.Content,
                  ContractName = UserContract.Contract.Name,
                  EditCount = UserContract.EditCount.GetValueOrDefault(),
                  EditTime = UserContract.EditTime,
                  Email = UserContract.ApplicationUser.UserName,
                  Time = UserContract.Time,
                  Word = UserContract.Word,
                  SelectedAnswer = UserContract.SelectedAnswer,
                  Price = UserContract.Contract.Price
            });

            return Json(result);
        }

         
        public ActionResult UserContracts_Destroy([DataSourceRequest]DataSourceRequest request, UserContract userContract)
        {

            var entity = db.UserContracts.Find(userContract.UserContractID);
            db.UserContracts.Remove(entity);
            db.SaveChanges();

            return Json(new[] { userContract }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}