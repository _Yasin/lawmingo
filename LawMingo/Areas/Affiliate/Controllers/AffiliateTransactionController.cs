﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Affiliate.Controllers
{

    [Authorize(Roles = "Affiliate")]
    [Area("Affiliate")]

    public class AffiliateTransactionController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public AffiliateTransactionController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
             db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }
        
        public async Task<ActionResult> AffiliateTransactions_Read([DataSourceRequest]DataSourceRequest request)
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var groups = (await db.Affiliates.Where(a => a.ApplicationUserId == userId)
                .Include("Groups.Details").FirstOrDefaultAsync()).Groups.Select(g => g.Details).ToList();
            var details = new List<int>();
            groups.ForEach(g => details.AddRange(g.Select(s => s.AffiliateDetailID)));

            IQueryable<AffiliateTransaction> AffiliateTransactions = db.AffiliateTransactions
                .Where(u => details.Contains(u.AffiliateDetailID ?? 0)).Include("UserContract");

            DataSourceResult result = AffiliateTransactions.ToDataSourceResult(request, l => new 
            {
                Time = l.Time,
                Ip = l.Amount
            });

            return Json(result);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}