﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Affiliate.Controllers
{

    [Authorize(Roles = "Affiliate")]
    [Area("Affiliate")]

    public class LogController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public LogController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
             db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }
        
        public async Task<ActionResult> AffiliateLogs_Read([DataSourceRequest]DataSourceRequest request)
        {
            //var userid = _userManager.GetUserId(HttpContext.User);
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var affiliateid = (await db.Affiliates.Where(a => a.ApplicationUserId == userId).FirstOrDefaultAsync()).AffiliateID;
            IQueryable<AffiliateLog> AffiliateLogs = db.AffiliateLogs
                .Where(u => u.AffiliateID == affiliateid).Include("ApplicationUser");

            DataSourceResult result = AffiliateLogs.ToDataSourceResult(request, l => new 
            {
                Time = l.Time,
                Ip = l.Ip,
                Request = l.Request,
                IsValid = l.Success,
                ForbiddenAccess = l.ForbiddenAccess
            });

            return Json(result);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}