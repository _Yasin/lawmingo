﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Affiliate.Controllers
{

    [Authorize(Roles = "Affiliate")]
    [Area("Affiliate")]

    public class AffiliateDetailController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public AffiliateDetailController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
             db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            return View();
        }

        public async Task<ActionResult> AffiliateDetail_Read([DataSourceRequest]DataSourceRequest request, int _AffiliateGroupID, bool single = false)
        {
            //var userid = _userManager.GetUserId(HttpContext.User);
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var affiliateid = (await db.Affiliates.Where(a => a.ApplicationUserId == userId).FirstOrDefaultAsync()).AffiliateID;
            IQueryable<AffiliateDetail> AffiliateDetails = db.AffiliateDetails
                .Where(u => u.AffiliateGroupID == _AffiliateGroupID).Include("Category");
            DataSourceResult result = new DataSourceResult();

            if (single)
            {
                result = AffiliateDetails.ToDataSourceResult(request, l => new AffiliateDetail
                {
                    ContractID = l.ContractID,
                    CommissionPercent = l.CommissionPercent,
                    Price = l.Price,
                    Discount = l.Discount,
                });
            }
            else
            {
                result = AffiliateDetails.ToDataSourceResult(request, l => new AffiliateDetail
                {
                    CategoryID = l.CategoryID,
                    CommissionPercent = l.CommissionPercent,
                    ExpandView = l.ExpandView,
                    Price = l.Price,
                    Discount = l.Discount,
                });
            }


            return Json(result);
        }
        public async Task<ActionResult> AffiliateDetail_Create([DataSourceRequest]DataSourceRequest request , AffiliateDetail affiliatedetail, int _AffiliateGroupID)
        {
            //var userid = _userManager.GetUserId(HttpContext.User);
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var affiliateid = (await db.Affiliates.Where(a => a.ApplicationUserId == userId).FirstOrDefaultAsync()).AffiliateID;

            var entity = new AffiliateDetail
            {
                AffiliateGroupID = _AffiliateGroupID,
                ContractID = affiliatedetail.ContractID,
                Price = affiliatedetail.Price,
                Discount = affiliatedetail.Discount,
                CategoryID = affiliatedetail.CategoryID,
                CommissionPercent = affiliatedetail.CommissionPercent,
                CommissionStatic = affiliatedetail.CommissionStatic,
                MaxCommission = affiliatedetail.MaxCommission,
            };

            db.AffiliateDetails.Add(entity);
            await db.SaveChangesAsync();

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}