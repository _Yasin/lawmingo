﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Areas.Admin.Models.ViewModels;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Affiliate.Controllers
{

    [Authorize(Roles = "Affiliate")]
    [Area("Affiliate")]

    public class AffiliateGroupController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public AffiliateGroupController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
             db = _db;
            _userManager = userManager;
        }

        public ActionResult List()
        {
            PopulateCategories();

            return View();
        }
        
        public async Task<ActionResult> AffiliateGroup_Read([DataSourceRequest]DataSourceRequest request)
        {
            //var userid = _userManager.GetUserId(HttpContext.User);
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var affiliateid = (await db.Affiliates.Where(a => a.ApplicationUserId == userId).FirstOrDefaultAsync()).AffiliateID;
            IQueryable<AffiliateGroup> AffiliateGroups = db.AffiliateGroups.Where(a => a.AffiliateID == affiliateid);

            DataSourceResult result = AffiliateGroups.ToDataSourceResult(request, l => new AffiliateGroup
            {
                AffiliateGroupID = l.AffiliateGroupID,
                Title = l.Title,
                CreateTime = l.CreateTime,
                Wallet = l.Wallet,
                Disabled = l.Disabled,
                Token = l.Token,
            });

            return Json(result);
        }

        public async Task<ActionResult> AffiliateGroup_Create([DataSourceRequest]DataSourceRequest request, AffiliateGroup affiliategroup)
        {
            //var userid = _userManager.GetUserId(HttpContext.User);
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var affiliateid = (await db.Affiliates.Where(a => a.ApplicationUserId == userId).FirstOrDefaultAsync()).AffiliateID;
            
            var user = db.Users.Find(userId);

            var entity = new AffiliateGroup
            {
                AffiliateID = affiliateid,
                Title = affiliategroup.Title,
                Wallet = affiliategroup.Wallet,
                CreateTime = DateTime.Now,
                Token = "asd",
                Disabled = true
            };

            db.AffiliateGroups.Add(entity);
            await db.SaveChangesAsync();

            var token = Token.Generate(entity.AffiliateGroupID.ToString() , user);
            entity.Token = token;
            await db.SaveChangesAsync();

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        private void PopulateCategories()
        {

            var Categories = db.Categories
                        .Select(c => new CategoryViewModel
                        {
                            CategoryID = c.CategoryID,
                            Name = c.Name
                        })
                        .OrderBy(e => e.Name);

            ViewData["Categories"] = Categories;

            var Contracts = db.Contract
                        .Select(c => new
                        {
                            ContractID = c.ContractID,
                            Name = c.Name
                        })
                        .OrderBy(e => e.Name);

            ViewData["Contracts"] = Contracts;

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}