﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LawMingo.Models.DomainModel
{
    public enum PostViewType
    {
        LargestCardWithBackground,
        LargeCardWithBackground,
        LargeCardWithImage,
        DefaultCardWithImage,
        DefaultCardWithBackground,
        DefaultCardNoImage,
        SmallCardNoImage,
        SmallCardWithBackground,
        SmallCardWithImage,
    }

    public class Post
    {
        public Post() { }
      
        [ScaffoldColumn(false)]
        public int PostID { get; set; }

        [AdditionalMetadata("action", "GetSubjects")]
        [AdditionalMetadata("dataTextfield", "title")]
        [AdditionalMetadata("dataValuefield", "id")]
        [UIHint("GridForeignKey_Ajax")]
        [Display(Name = "گروه")]
        public int? SubjectID { get; set; }
        
        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]    
        [MaxLength(100, ErrorMessage = "حداکثر تعداد کاراکتر 100 می باشد .")]
        [Display(Name = "عنوان ")]
        public string Title { get; set; }

        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]
        [MaxLength(100, ErrorMessage = "حداکثر تعداد کاراکتر 100 می باشد .")]     
        [Display(Name = "عنوان انگلیسی")]
        public string EnglishTitle { get; set; }

        [Display(Name = "عنوان SEO")]
        [Required]
        public string SEO_Title { get; set; }

        [Display(Name = "توضیحات SEO")]
        [Required]
        public string SEO_Description { get; set; }

        [Display(Name = "کیدواژه SEO")]
        [Required]
        public string SEO_Keywords { get; set; }
        [AdditionalMetadata("action", "GetPosts")]
        [AdditionalMetadata("dataTextfield", "title")]
        [AdditionalMetadata("dataValuefield", "id")]
        [UIHint("GridForeignKey_Ajax")]
        [Display(Name = "رشته پست")]
        public int? ParentID { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [UIHint("CustomDate")]
        [ScaffoldColumn(false)]
        public DateTime CreateTime { get; set; }

        [UIHint("CustomDate")]
        [ScaffoldColumn(false)]
        public DateTime LastModified { get; set; }

        [UIHint("CustomDate")]
        [Display(Name = "تاریخ انتشار")]
        public DateTime PublishTime { get; set; }

        [Display(Name = "رنگ")]
        [UIHint("Colorbox")]
        public string Color { get; set; }

        [Display(Name = "رنگ فونت")]
        [UIHint("Colorbox")]
        public string FontColor { get; set; }

        [Display(Name = "تصویر")]
        [UIHint("FileUpload")]
        [AdditionalMetadata("action", "PostImage")]
        public string Image { get; set; }
        public string ImageAlt { get; set; }

        [Display(Name = "کاور")]
        [UIHint("FileUpload")]
        [AdditionalMetadata("action", "PostCover")]
        public string Cover { get; set; }

        [UIHint("TextEditorFull")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "متن پست")]
        public string Content { get; set; }

        [UIHint("TextEditor")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "خلاصه")]
        public string Summary { get; set; }

        [AdditionalMetadata("action", "GetTags")]
        [AdditionalMetadata("type", 1)]
        [AdditionalMetadata("dataTextfield", "title")]
        [AdditionalMetadata("dataValuefield", "id")]
        [UIHint("MultiSelect")]
        [Display(Name = "برچسب ها")]
        public string Tags { get; set; }

        [Display(Name = "عدم نمایش")]
        public bool Disable { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "تعداد بازدید")]
        public int Visits { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "تعداد لایک")]
        public int Likes { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "تعداد دیسلایک")]
        public int DisLikes { get; set; }

        [Display(Name = "نمایش در اسلایدر")]
        public bool Slider { get; set; }

        [UIHint("Enum_PostView")]
        [Display(Name = "تحوه نمایش")]
        public PostViewType ViewType { get; set; }

        [AdditionalMetadata("action", "GetAuthors")]
        [AdditionalMetadata("dataTextfield", "title")]
        [AdditionalMetadata("dataValuefield", "id")]
        [UIHint("GridForeignKey_Ajax")]
        [Display(Name = "نویسنده")]
        public int? AuthorID { get; set; }

        //[ScaffoldColumn(false)]
        //[Display(Name = "تعداد کامنت")]
        //public int Comments { get; set; }

        [Display(Name = "کامنت غیر فعال شود")]
        public bool NoComment { get; set; }

        [ScaffoldColumn(false)]
        public int Rate {
            get {
                var total = Likes + DisLikes;
                return total > 0 ? 5 * (Likes / (Likes + DisLikes)) : 0;
            }
        }

        [Key, ForeignKey("ParentID")]
        public virtual Post Parent { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual Author Author { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }    
        public ICollection<Post> Children { get; set; }    
        public ICollection<Comment> Comments { get; set; } 

    }
}