﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LawMingo.Models.DomainModel
{
    public class Subject
    {

        public Subject() { }

        [ScaffoldColumn(false)]
        public int SubjectID { get; set; }

        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]
        [MaxLength(100, ErrorMessage = "حداکثر تعداد کاراکتر 100 می باشد .")]
        [Display(Name = "عنوان ")]
        public string Title { get; set; }

        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]
        [MaxLength(100, ErrorMessage = "حداکثر تعداد کاراکتر 100 می باشد .")]
        [Display(Name = "عنوان انگلیسی  ")]
        public string EnglishTitle { get; set; }

        [Display(Name = "عنوان SEO")]
        [Required]
        public string SEO_Title { get; set; }

        [Display(Name = "توضیحات SEO")]
        [Required]
        public string SEO_Description { get; set; }

        [Display(Name = "کیدواژه SEO")]
        [Required]
        public string SEO_Keywords { get; set; }
        [ScaffoldColumn(false)]
        public int? ParentID { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime CreateTime { get; set; }

        [Display(Name = "رنگ")]
        [UIHint("Colorbox")]
        [Required]
        public string Color { get; set; }

        [Display(Name = "رنگ فونت")]
        [UIHint("Colorbox")]
        [Required]
        public string FontColor { get; set; }

        [Display(Name = "تصویر")]
        [UIHint("FileUpload")]
        [AdditionalMetadata("action", "SubjectImage")]
        public string Image { get; set; }

        [Display(Name = "آیکون")]
        [UIHint("FileUpload")]
        [AdditionalMetadata("action", "SubjectIcon")]
        public string Icon { get; set; }

        [UIHint("TextEditor")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "توضیحات")]
        public string Description { get; set; }

        [UIHint("Enum_PostView")]
        [Display(Name = "تحوه نمایش")]
        public PostViewType? ViewType { get; set; }

        [Display(Name = "نمایش ستون کناری")]
        public bool SideBar { get; set; }

        [Display(Name = "نمایش پست های مرتبط")]
        public bool SimmilarPosts { get; set; }

        [Display(Name = "کامنت غیر فعال شود")]
        public bool NoComment { get; set; }


        [Key, ForeignKey("ParentID")]
        public virtual Subject Parent { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public ICollection<Subject> Children { get; set; }

    }
}