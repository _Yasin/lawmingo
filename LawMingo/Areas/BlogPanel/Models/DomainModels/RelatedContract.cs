﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LawMingo.Models.DomainModel
{
    public class RelatedContract
    {
        public RelatedContract() { }

        [ScaffoldColumn(false)]
        public int RelatedContractID { get; set; }
        public int PostID { get; set; }

        [AdditionalMetadata("action", "GetRelatedContracts")]
        [AdditionalMetadata("dataTextfield", "title")]
        [AdditionalMetadata("dataValuefield", "id")]
        [AdditionalMetadata("cascadefrom", "PostID")]
        [UIHint("vahidtest")]
        [Display(Name = "قرارداد مرتبط")]
        public int ContractID { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime CreateTime { get; set; }

        public string Title { get;}

        [Key, ForeignKey("ContractID")]    
        public Contract Contract { get; set; }
        public Post Post { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }    

    }
}