﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LawMingo.Models.DomainModel
{
    public class Author
    {
        public Author() { }
      
        [ScaffoldColumn(false)]
        public int AuthorID { get; set; }
        
        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]    
        [MaxLength(100, ErrorMessage = "حداکثر تعداد کاراکتر 100 می باشد .")]
        [Display(Name = "عنوان ")]
        public string Title { get; set; }

        [Display(Name = "شعار")]
        public string Summery { get; set; }

        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]
        [MaxLength(100, ErrorMessage = "حداکثر تعداد کاراکتر 100 می باشد .")]     
        [Display(Name = "عنوان انگلیسی  ")]
        public string EnglishTitle { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime CreateTime { get; set; }

        [Display(Name = "رنگ")]
        [UIHint("Colorbox")]
        public string Color { get; set; }

        [Display(Name = "رنگ فونت")]
        [UIHint("Colorbox")]
        public string FontColor { get; set; }

        [Display(Name = "تصویر کاور")]
        [UIHint("FileUpload")]
        [AdditionalMetadata("action", "AuthorImage")]
        public string Image { get; set; }

        [Display(Name = "آواتار")]
        [UIHint("FileUpload")]
        [AdditionalMetadata("action", "AuthorIcon")]
        public string Icon { get; set; }

        [UIHint("TextEditor")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "درباره نویسنده")]
        public string Description { get; set; }

    }
}