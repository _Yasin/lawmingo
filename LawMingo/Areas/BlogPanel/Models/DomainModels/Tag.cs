﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LawMingo.Models.DomainModel
{
    public enum TagType
    {
        Tag,
        Glossary,
    }

    public class Tag
    {
        public Tag() { }
      
        
        [ScaffoldColumn(false)]
        public int TagID { get; set; }

        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]    
        [MaxLength(100, ErrorMessage = "حداکثر تعداد کاراکتر 100 می باشد .")]
        [Display(Name = "عنوان ")]
        public string Title { get; set; }

        [Required(ErrorMessage = "وارد کردن این فیلد الزامی می باشد .")]
        [MaxLength(100, ErrorMessage = "حداکثر تعداد کاراکتر 100 می باشد .")]     
        [Display(Name = "عنوان انگلیسی  ")]
        public string EnglishTitle { get; set; }

        [ScaffoldColumn(false)]
        public string ApplicationUserId { get; set; }

        [ScaffoldColumn(false)]
        public DateTime CreateTime { get; set; }

        [Display(Name = "رنگ")]
        [UIHint("Colorbox")]
        public string Color { get; set; }

        [Display(Name = "رنگ فونت")]
        [UIHint("Colorbox")]
        public string FontColor { get; set; }

        [Display(Name = "تصویر")]
        [UIHint("FileUpload")]
        [AdditionalMetadata("action", "SubjectImage")]
        public string Image { get; set; }

        [Display(Name = "آیکون")]
        [UIHint("FileUpload")]
        [AdditionalMetadata("action", "SubjectIcon")]
        public string Icon { get; set; }

        [UIHint("TextEditor")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "عنوان SEO")]
        public string SEO_Title { get; set; }

        [Display(Name = "توضیحات SEO")]
        public string SEO_Description { get; set; }

        [Display(Name = "کیدواژه SEO")]
        public string SEO_Keywords { get; set; }

        [Display(Name = "نوع")]
        [UIHint("Enum_TagType")]
        [ScaffoldColumn(false)]
        public TagType Type { get; set; }


    }
}