﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.BlogPanel.Controllers
{
    [Area("BlogPanel")]

    public class RelatedPostsController : Controller
    {
        private readonly ApplicationDbContext db;

        public RelatedPostsController(ApplicationDbContext _db)
        {
            db = _db;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RelatedPosts_Read([DataSourceRequest]DataSourceRequest request, int _PostID)
        {
            IQueryable<RelatedPost> Posts = db.RelatedPosts.Where(p => p.PostID == _PostID).Include(p => p.Related);
            DataSourceResult result = Posts.ToDataSourceResult(request, post => new {
                RelatedID = post.RelatedID,
                Title = post.Related.Title,
            });

            return Json(result);
        }

        [HttpPost]
        public ActionResult RelatedPosts_Create([DataSourceRequest]DataSourceRequest request, RelatedPost post, int _PostID)
        {
            if (ModelState.IsValid)
            {
                var entity = new RelatedPost
                {
                    CreateTime = DateTime.Now,
                    PostID = _PostID,
                    RelatedID = post.RelatedID
                };

                db.RelatedPosts.Add(entity);
                db.SaveChanges();
                post.PostID = entity.PostID;
            }

            return Json(new[] { post }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult RelatedPosts_Destroy([DataSourceRequest]DataSourceRequest request, RelatedPost post)
        {
            var entity = db.RelatedPosts.Find(post.RelatedPostID);
            db.RelatedPosts.Attach(entity);
            db.RelatedPosts.Remove(entity);
            db.SaveChanges();

            return Json(new[] { post }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
