﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.BlogPanel.Controllers
{
    [Area("BlogPanel")]
    [Authorize(Roles = "Blogadmin")]

    public class PostsController : Controller
    {
        private readonly ApplicationDbContext db;

        public PostsController(ApplicationDbContext _db)
        {
            db = _db;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Posts_Read([DataSourceRequest]DataSourceRequest request, int? _ParentID)
        {
            IQueryable<Post> _Posts = db.Posts.Where(p => p.ParentID == _ParentID).Include(e => e.Comments).OrderByDescending(p => p.PostID);
            DataSourceResult result = _Posts.ToDataSourceResult(request, post => new {
                Title = post.Title,
                EnglishTitle = post.EnglishTitle,
                SubjectID = post.SubjectID,
                AuthorID = post.AuthorID,
                Summary = post.Summary,
                Content = post.Content,
                Disable = post.Disable,
                Color = post.Color,
                Tags = post.Tags,
                FontColor = post.FontColor,
                Image = post.Image,
                Cover = post.Cover,
                Url = Posts.URL(post.SEO_Title , post.Subject != null ? post.Subject.SEO_Title : null),
                commentCount = post.Comments.Count(c => !c.IsRead),
                CreateTime = post.CreateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                PublishTime = post.PublishTime.ToString("yyyy-MM-ddTHH:mm:ss"),
                LastModified = post.LastModified.ToString("yyyy-MM-dd HH:mm:ss"),
                SEO_Description = post.SEO_Description,
                SEO_Title = post.SEO_Title,
                SEO_Keywords = post.SEO_Keywords,
                PostID = post.PostID,
                ParentID = post.ParentID,
                Slider = post.Slider,
                NoComment = post.NoComment,
                ViewType = post.ViewType
            });

            return Json(result);
        }

        [HttpPost]
        public ActionResult Posts_Create([DataSourceRequest]DataSourceRequest request, Post post, int? _ParentID)
        {
            if (ModelState.IsValid)
            {
                var entity = new Post
                {
                    Title = post.Title,
                    EnglishTitle = post.EnglishTitle,
                    SubjectID = post.SubjectID,
                    AuthorID = post.AuthorID,
                    Summary = post.Summary,
                    Content = post.Content,
                    Disable = post.Disable,
                    Color = post.Color,
                    Tags = post.Tags,
                    FontColor = post.FontColor,
                    Image = post.Image,
                    Cover = post.Cover,
                    CreateTime = post.CreateTime,
                    PublishTime = post.PublishTime,
                    LastModified = post.LastModified,
                    SEO_Description = post.SEO_Description,
                    SEO_Title = post.SEO_Title,
                    SEO_Keywords = post.SEO_Keywords,
                    PostID = post.PostID,
                    ParentID = post.ParentID,
                    Slider = post.Slider,
                    ViewType = post.ViewType,
                    NoComment = post.NoComment
                };

                db.Posts.Add(entity);
                db.SaveChanges();
                post.PostID = entity.PostID;
            }

            return Json(new[] { post }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Posts_Update([DataSourceRequest]DataSourceRequest request, Post post)
        {
            var entity = db.Posts.Find(post.PostID);

            entity.Title = post.Title;

            entity.EnglishTitle = post.EnglishTitle;
            entity.SubjectID = post.SubjectID;
            entity.AuthorID = post.AuthorID;
            entity.Summary = post.Summary;
            entity.Content = post.Content;
            entity.Color = post.Color;
            entity.Tags = post.Tags;
            entity.FontColor = post.FontColor;
            entity.Image = post.Image;
            entity.Cover = post.Cover;
            entity.Disable = post.Disable;
            entity.Slider = post.Slider;
            entity.ViewType = post.ViewType;

            entity.PublishTime = post.PublishTime;
            entity.LastModified = DateTime.Now;

            entity.SEO_Description = post.SEO_Description;
            entity.SEO_Title = post.SEO_Title;
            entity.SEO_Keywords = post.SEO_Keywords;
            entity.ParentID = post.ParentID;

            entity.NoComment = post.NoComment;

            db.SaveChanges();

            return Json(new[] { post }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Posts_Destroy([DataSourceRequest]DataSourceRequest request, Post post)
        {
            var entity = db.Posts.Find(post.PostID);
            db.Posts.Attach(entity);
            db.Posts.Remove(entity);
            db.SaveChanges();

            return Json(new[] { post }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
