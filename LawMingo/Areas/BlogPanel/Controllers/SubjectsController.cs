﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LawMingo.Areas.BlogPanel.Controllers
{
    [Area("BlogPanel")]
    [Authorize(Roles = "Blogadmin")]

    public class SubjectsController : Controller
    {
        private readonly ApplicationDbContext db;

        public SubjectsController(ApplicationDbContext _db)
        {
            db = _db;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult subjects_Read([DataSourceRequest]DataSourceRequest request, int? _ParentID)
        {
            IQueryable<Subject> subjects = db.Subjects.Where(c => c.ParentID == _ParentID);
            DataSourceResult result = subjects.ToDataSourceResult(request, subject => new {
                SubjectID = subject.SubjectID,
                Title = subject.Title,
                EnglishTitle = subject.EnglishTitle,
                Color = subject.Color,
                FontColor = subject.FontColor,
                Image = subject.Image,
                Icon = subject.Icon,
                Description = subject.Description
            });

            return Json(result);
        }

        [HttpPost]
        public ActionResult subjects_Create([DataSourceRequest]DataSourceRequest request, Subject subject, int? _ParentID)
        {
            if (ModelState.IsValid)
            {
                var entity = new Subject
                {
                    Title = subject.Title,
                    EnglishTitle = subject.EnglishTitle,
                    Color = subject.Color,
                    ParentID = _ParentID,
                    FontColor = subject.FontColor,
                    Image = subject.Image,
                    Icon = subject.Icon,
                    Description = subject.Description
                };

                db.Subjects.Add(entity);
                db.SaveChanges();
                subject.SubjectID = entity.SubjectID;
            }

            return Json(new[] { subject }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult subjects_Update([DataSourceRequest]DataSourceRequest request, Subject subject)
        {
            var entity = db.Subjects.Find(subject.SubjectID);

            entity.Title = subject.Title;
            entity.EnglishTitle = subject.EnglishTitle;
            entity.Color = subject.Color;
            entity.FontColor = subject.FontColor;
            entity.Image = subject.Image;
            entity.Icon = subject.Icon;
            entity.Description = subject.Description;

            db.SaveChanges();

            return Json(new[] { subject }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult subjects_Destroy([DataSourceRequest]DataSourceRequest request, Subject subject)
        {
            var entity = db.Subjects.Find(subject.SubjectID);
            db.Subjects.Attach(entity);
            db.Subjects.Remove(entity);
            db.SaveChanges();

            return Json(new[] { subject }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
