﻿using LawMingo.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;


namespace LawMingo.Areas.BlogPanel.Controllers
{
    //[Authorize(Roles = "Blogadmin")]
    [Area("BlogPanel")]
    public class RemoteDataController : Controller
    {
        private readonly ApplicationDbContext db;

        public RemoteDataController(ApplicationDbContext _db)
        {
            db = _db;
        }

        public  ActionResult GetTags()
        {
            var res = db.Tags.Select(t => new {
                title = t.Title,
                id = t.Title,
            });
            return Json(res);
        }

        public async Task<ActionResult> GetPosts()
        {
            var res = await db.Posts.Select(t => new {
                title = t.Title,
                id = t.PostID,
            }).ToListAsync();
            return Json(res);
        }

        public async Task<ActionResult> GetSubjects()
        {
            var res = await db.Subjects.Select(t => new {
                title = t.Title,
                id = t.SubjectID,
            }).ToListAsync();
            return Json(res);
        }

        public async Task<ActionResult> GetAuthors()
        {
            var res = await db.Authors.Select(t => new {
                title = t.Title,
                id = t.AuthorID,
            }).ToListAsync();
            return Json(res);
        }

        public async Task<ActionResult> GetRelatedPosts(int id)
        {
            var relateds = await db.RelatedPosts.Where(r => r.PostID == id).Select(s =>s.RelatedID).ToListAsync();
            var res = await db.Posts.Where(p => !relateds.Contains(p.PostID) && p.PostID != id).Select(t => new {
                title = t.Title,
                id = t.PostID,
            }).ToListAsync();
            return Json(res);
        }
    }
}