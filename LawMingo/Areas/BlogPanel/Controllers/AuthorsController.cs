﻿using System;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.BlogPanel.Controllers
{
    [Area("BlogPanel")]
    [Authorize(Roles = "Blogadmin")]

    public class AuthorsController : Controller
    {
        private readonly ApplicationDbContext db;

        public AuthorsController(ApplicationDbContext _db)
        {
            db = _db;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Authors_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Author> Authors = db.Authors;
            DataSourceResult result = Authors.ToDataSourceResult(request, Author => new {
                AuthorID = Author.AuthorID,
                Title = Author.Title,
                EnglishTitle = Author.EnglishTitle,
                Color = Author.Color,
                FontColor = Author.FontColor,
                Image = Author.Image,
                Icon = Author.Icon,
                Summery = Author.Summery,
                Description = Author.Description,
            });

            return Json(result);
        }

        [HttpPost]
        public ActionResult Authors_Create([DataSourceRequest]DataSourceRequest request, Author Author, int _PostID)
        {
            var entity = new Author
            {
                Title = Author.Title,
                EnglishTitle = Author.EnglishTitle,
                Color = Author.Color,
                FontColor = Author.FontColor,
                Image = Author.Image,
                Icon = Author.Icon,
                Summery = Author.Summery,
                Description = Author.Description,
            };

            db.Authors.Add(entity);
            db.SaveChanges();
            Author.AuthorID = entity.AuthorID;

            return Json(new[] { Author }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Authors_Update([DataSourceRequest]DataSourceRequest request, Author Author)
        {
            var entity = db.Authors.Find(Author.AuthorID);


            entity.Title = Author.Title;
            entity.EnglishTitle = Author.EnglishTitle;
            entity.Color = Author.Color;
            entity.FontColor = Author.FontColor;
            entity.Image = Author.Image;
            entity.Icon = Author.Icon;
            entity.Summery = Author.Summery;
            entity.Description = Author.Description;

            db.Authors.Add(entity);
            db.SaveChanges();
            Author.AuthorID = entity.AuthorID;

            return Json(new[] { Author }.ToDataSourceResult(request, ModelState));
        }



        [HttpPost]
        public ActionResult Authors_Destroy([DataSourceRequest]DataSourceRequest request, Author Author)
        {
            var entity = db.Authors.Find(Author.AuthorID);
            db.Authors.Attach(entity);
            db.Authors.Remove(entity);
            db.SaveChanges();

            return Json(new[] { Author }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
