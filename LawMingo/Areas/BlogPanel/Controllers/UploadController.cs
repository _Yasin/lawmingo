﻿using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;
using Microsoft.AspNetCore.Authorization;

namespace LawMingo.Areas.BlogPanel.Controllers
{
    [Area("BlogPanel")]
    [Authorize(Roles = "Blogadmin")]

    public class UploadController : Controller
    {

        private readonly IHostingEnvironment _hostingEnvironment;

        public UploadController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SubjectImage([Bind(Prefix = "Image.SubjectImage")]IFormFile image)
        {
            return UploadImage(image, 0, "Subjects");
        }

        public ActionResult PostImage([Bind(Prefix = "Image.PostImage")]IFormFile image)
        {
            return UploadImage(image, 0, "Posts");
        }

        public ActionResult PostCover([Bind(Prefix = "Cover.PostCover")]IFormFile image)
        {
            return UploadImage(image, 0, "Posts");
        }

        public JsonResult UploadImage(IFormFile file , int Width, string SubFolder = "", string folderName = "Blog")
        {
            var fileName = "";
            Guid GUID = Guid.NewGuid();
            try
            {
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName, SubFolder);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }


                string Extention = Path.GetExtension(file.FileName);
                fileName = file.FileName.Split(".")[0] + "__" + GUID + Extention;

                string fullPath = Path.Combine(newPath, fileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                //FileInfo snakewareLogo = new FileInfo(fullPath);
                //var vv = snakewareLogo.Length;

                //using (MagickImage image = new MagickImage(snakewareLogo))
                //{
                //    MagickGeometry size = new MagickGeometry(100);
                //    image.Resize(size);

                //    // Save the result
                //    image.Write(snakewareLogo);
                //}

                //ImageOptimizer optimizer = new ImageOptimizer();
                //optimizer.OptimalCompression = true;
                //optimizer.LosslessCompress(snakewareLogo);
                //snakewareLogo.Refresh();

                //var gg = snakewareLogo.Length;

            }
            catch (Exception)
            {

            }
            return Json(new { Image = fileName });
        }
    }
}
