﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Models.DomainModel;

namespace LawMingo.Models.ViewModel.Blog
{
    public class PostViewModel : Post
    {
        public int commentCount { get; set; }
        public string Url { get; set; }

    }
}
