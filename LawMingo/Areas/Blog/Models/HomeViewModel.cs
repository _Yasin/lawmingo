﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Models.DomainModel;

namespace LawMingo.Models.ViewModel.Blog
{
    public class HomeViewModel
    {
        public IEnumerable<PostItemViewModel> Slider { get; set; }
        public IEnumerable<PostItemViewModel> Trends { get; set; }
        public IEnumerable<PostItemViewModel> Latest { get; set; }

    }
    public class PostItemViewModel
    {
        public int PostID { get; set; }
        public string Title { get; set; }
        public string SeoTitle { get; set; }
        public string Author { get; set; }
        public string Subject { get; set; }
        public string SubjectSeo { get; set; }
        public string Cover { get; set; }
        public string Image { get; set; }
        public string Summary { get; set; }
        public string Color { get; set; }
        public string Font { get; set; }
        public string Time { get; set; }
        public PostViewType ViewType { get; set; }

    }
    public class ContractItemViewModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string summery { get; set; }
        public string english { get; set; }

    }
}
