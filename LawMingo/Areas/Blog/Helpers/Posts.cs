﻿namespace LawMingo.Helpers
{
    public static class Posts
    {
        public static string URL(string title , string subject = null)
        {
            var res = "";
            if (subject != null)
            {
                res = "/blog/" + subject.Replace(" " ,"-") + "/" + title.Replace(" ", "-");
            }
            else
            {
                res = "/blog/" + title.Replace(" ", "-");
            }
            return res;
        } 
    }
}
