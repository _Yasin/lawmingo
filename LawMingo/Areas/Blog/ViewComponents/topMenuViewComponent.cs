﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Blog
{
    public class topMenuViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext db;

        public topMenuViewComponent(ApplicationDbContext context)
        {
            db = context;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var res = await db.Subjects.Where(p => p.ParentID == null).ToListAsync();
            return View(res);
        }
    }
}
