﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel.Blog;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Blog.Controllers
{
    [Area("Blog")]
    public class SubjectsController : Controller
    {
        private static ApplicationDbContext _db { get; set; }

        public SubjectsController(ApplicationDbContext db)
        {
            _db = db;
        }

        //[Route("blog/{title}")]
        //public async Task<IActionResult> Index(string title)
        //{
        //    var name = title.Trim().Replace("-", "");
        //    var subject = await _db.Subjects.Where(p => p.Title.Trim().Replace(" " ,"") == name ).FirstOrDefaultAsync();
        //    if (subject == null)
        //    {
        //        return NotFound();
        //    }

        //    ViewBag.Posts = await _db.Posts.Where(p => p.SubjectID == subject.SubjectID).Select(r => new PostItemViewModel {  PostID = r.PostID, Title = r.Title, Summary = r.Summary, Author = r.Author.Title, Image = r.Image, Time = r.PublishTime.ToString() }).ToListAsync(); ;
        //    return View(subject);
        //}

        public async Task<List<Subject>> GetLastChilds(int id)
        {
            var res = new List<Subject>() { };
            var subjects = await _db.Subjects.ToListAsync();
            var end = false;

            while (end)
            {
                var item = subjects.Where(s => s.SubjectID == res.LastOrDefault().ParentID).FirstOrDefault();
                if (item != null)
                {
                    subjects.Add(item);
                }
                end = item == null;
            }
            return res;
        }

        public static Dictionary<string, string> GetParents(int? id)
        {
            var items = new List<string>() { };
            var subjects = new List<Subject>() { };

          
            var bb = _db.Subjects.ToList();
            var firstitem = bb.Where(s => s.SubjectID == id).FirstOrDefault();
            subjects.Add(firstitem);

            var end = false;
            while (end)
            {
                var item = bb.Where(s => s.SubjectID == subjects.LastOrDefault().ParentID).FirstOrDefault();
                if (item != null)
                {
                    subjects.Add(item);
                }
                end = item == null;
            }

            var Result = subjects.Select(s => new { s.Title, s.SubjectID }).ToDictionary(t => "/blog/subject/" + t.Title, t => t.Title);

            return Result;
        }
    }
}