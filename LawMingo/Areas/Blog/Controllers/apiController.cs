﻿
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using LawMingo.Models.ViewModel.Blog;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Areas.Blog.Controllers
{
    [Area("Blog")]
    public class apiController : Controller
    {

        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public apiController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
             db = _db;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<JsonResult> RelatedContracts(string id , string type)
        {
            List<ContractItemViewModel> result = new List<ContractItemViewModel>() { };
            IQueryable<RelatedContract> _result;
            var ID = 0;
            switch (type)
            {

                case "post":
                     ID = Convert.ToInt32(id);
                    _result = db.RelatedContracts.Where(r => r.PostID == ID);
                    break;
                case "subject":
                    ID = Convert.ToInt32(id);
                    _result = db.RelatedContracts.Where(r => r.Post.SubjectID == ID);
                    break;
                case "keyword":
                    _result = db.RelatedContracts;
                    foreach (var item in id.Split('-'))
                    {
                        _result = _result.Where(p => p.Post.Title.Contains(item) || p.Post.EnglishTitle.Contains(item) || p.Post.Tags.Contains(item));
                    }
                    break;
                case "tag":
                     ID = Convert.ToInt32(id);
                    var tag = db.Tags.Find(ID);
                    _result = db.RelatedContracts.Where(r => r.Post.Tags.Contains(tag.Title));
                    break;
                default:
                    //  _result = db.RelatedContracts.Where(r => r.PostID == null);
                    break;
            }
            result = await db.RelatedContracts.Select(r => new ContractItemViewModel { id = r.ContractID, title = r.Contract.Name, summery = r.Contract.Summery, english = r.Contract.EnglishName }).ToListAsync();
            return Json(result);
        }

        [HttpGet]
        public async Task<JsonResult> SliderRelatedPosts()
        {
            var coverPosts = await db.Posts.Where(p => p.Slider == true).Select(s => s.PostID).ToListAsync();
            var result = await db.RelatedPosts.Where(r => coverPosts.Any(i => i == r.PostID)).Select(r => new { parent = r.PostID, id = r.RelatedID, title = r.Related.Title, summery = r.Related.Summary, author = r.Related.Author.Title, image = r.Related.Image, time = r.Related.PublishTime }).ToListAsync();
            return Json(result);
        }

        [HttpGet]
        public async Task<JsonResult> Comments(int id , bool stream = false)
        {
            var result = await db.Comments
                .Where(p => p.PostID == id && (stream ? true : p.IsActive))
                .OrderByDescending(c => c.Time)
                .Select(r => new { active = r.IsActive ,  id = r.CommentID ,  author = r.Author, content = r.Content, time = r.Time, answers = r.Answers , read = r.IsRead }).ToListAsync();
            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> CreateComment(string content, string contact, string name, int id)
        {
            var comment = new Comment() { };
            comment.Time = DateTime.Now;
            comment.PostID = id;
            comment.Author = name;
            comment.Content = content;
            comment.AuthorEmail = contact;
            comment.IsActive = false;

            var result = await db.Comments.AddAsync(comment);
            await db.SaveChangesAsync();
            return Json(true);
        }

        [HttpGet]
        public async Task<JsonResult> LatestPosts(string id = null, string type = null, bool trends = false)
        {
            IQueryable<Post> _posts = db.Posts;
            switch (type)
            {
                case "tag":
                    var tag = await db.Tags.FindAsync(Convert.ToInt32(id));
                    _posts = _posts.Where(p => p.Tags.Contains(tag.Title)).OrderByDescending(p => p.PostID);
                    break;
                case "keyword":
                    foreach (var item in id.Split('-'))
                    {
                        _posts = _posts.Where(p => p.Title.Contains(item) || p.EnglishTitle.Contains(item) || p.Tags.Contains(item))
                            .OrderByDescending(p => p.PostID);
                    }
                    break;
                default:
                    _posts = _posts.Where(p => id != null ? p.SubjectID.ToString() == id : true).OrderByDescending(p => p.PostID);
                    break;
            }
            if (trends)
            {
                _posts = _posts.OrderByDescending(p => p.Visits);
            }
            var result = await _posts.Take(5).Select(r => new { parent = r.PostID, id = r.PostID, subject = r.Subject != null ? r.Subject.SEO_Title : null, seo = r.SEO_Title, view = r.ViewType, title = r.Title, summery = r.Summary, author = r.Author.Title, image = r.Image, time = r.PublishTime }).ToListAsync();
            return Json(result);
        }

        [HttpGet]
        public async Task<JsonResult> SimilarPosts(int id)
        {
            var result = await db.RelatedPosts.Where(r => r.PostID == id).Select(r => new { id = r.RelatedID, title = r.Related.Title, subject = r.Related.Subject != null ? r.Related.Subject.SEO_Title : null, seo = r.Related.SEO_Title, summery = r.Related.Summary, author = r.Related.Author.Title, image = r.Related.Image, time = r.Related.PublishTime }).ToListAsync();
            return Json(result);
        }
    }

}


