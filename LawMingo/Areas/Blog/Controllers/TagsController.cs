﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel.Blog;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Blog.Controllers
{
    [Area("Blog")]
    public class TagsController : Controller
    {
        private static ApplicationDbContext _db { get; set; }

        public TagsController(ApplicationDbContext db)
        {
            _db = db;
        }

        [Route("blog/tag/{title}")]
        public async Task<IActionResult> Index(string title)
        {
            var name = title.Trim().Replace("-", "");
            var tag = await _db.Tags.Where(p => p.Title.Trim().Replace(" " ,"") == name ).FirstOrDefaultAsync();

            if (tag == null)
            {
                Response.StatusCode = 404;
                return RedirectPermanent("https://lawmingo.com/mag");
             
            }

            ViewBag.Posts = await _db.Posts.Where(p => p.Tags.Contains(tag.Title)).Select(r => new PostItemViewModel {  PostID = r.PostID, Title = r.Title, Summary = r.Summary, Author = r.Author.Title, Image = r.Image, Time = r.PublishTime.ToString() }).ToListAsync(); ;
            return RedirectPermanent(tag.SEO_Keywords);
           
        }
    }
}