﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel.Blog;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Blog.Controllers
{
    [Area("Blog")]
    public class PostsController : Controller
    {
        private static ApplicationDbContext _db { get; set; }

        public PostsController(ApplicationDbContext db)
        {
            _db = db;
        }

        //[Route("blog/{id}/{title}")]
        //public async Task<IActionResult> Index(int id)
        //{
        //    var _posts = await _db.Posts.Where(p => !p.Disable && p.PublishTime < DateTime.Now && p.PostID == id).Include(p => p.Subject).Include(p => p.Author).FirstOrDefaultAsync();


        //    return View(_posts);
        //}

        [HttpPost]
        [Route("blog/search")]
        public IActionResult search(string q)
        {
            return RedirectToAction("keyword", new { title = q.Trim().Replace(" ", "-") });
        }

        [Route("blog/{title}")]
        public async Task<IActionResult> Index(string title)
        {
            var _title = title.Trim().Replace('-', ' ');
            var _post = await _db.Posts.Where(p => !p.Disable && p.PublishTime < DateTime.Now && p.SEO_Title == _title).Include(p => p.Subject).Include(p => p.Author).FirstOrDefaultAsync();


            if (_post == null)
            {
                var subject = await _db.Subjects.Where(s => s.SEO_Title == _title).FirstOrDefaultAsync();
                if (subject != null)
                {
                    return RedirectPermanent(subject.SEO_Keywords);
                    // return View("../Subjects/Index", subject);
                }
                return RedirectPermanent("https://lawmingo.com/mag");
                // return NotFound();
            }
            _post.Visits += 1;
            await _db.SaveChangesAsync();
            return RedirectPermanent(_post.SEO_Keywords);
            // return View(_post);
        }

        [Route("blog/{subject}/{title}")]
        public async Task<IActionResult> Index(string subject, string title)
        {
            var _title = title.Trim().Replace('-', ' ');
            var _subject = subject.Trim().Replace('-', ' ');

            var _post = await _db.Posts.Where(p => p.Subject.SEO_Title == _subject && !p.Disable && p.PublishTime < DateTime.Now && p.SEO_Title == _title).Include(p => p.Subject).Include(p => p.Author).FirstOrDefaultAsync();
            if (_post == null)
            {
                return RedirectPermanent("https://lawmingo.com/mag");
                // return NotFound();
            }
            _post.Visits += 1;
            await _db.SaveChangesAsync();
            return RedirectPermanent(_post.SEO_Keywords);
            // return View(_post);
        }

        [Route("blog/keyword/{title}")]
        public IActionResult keyword(string title)
        {
            ViewBag.title = title;
            return View();
        }
        public static Dictionary<string, string> GetParents(int? id)
        {
            var items = new List<string>() { };
            var subjects = new List<Subject>() { };

            // var res = "";
            var bb = _db.Subjects.ToList();
            var firstitem = bb.Where(s => s.SubjectID == id).FirstOrDefault();
            subjects.Add(firstitem);

            var end = false;
            while (end)
            {
                var item = bb.Where(s => s.SubjectID == subjects.LastOrDefault().ParentID).FirstOrDefault();
                if (item != null)
                {
                    subjects.Add(item);
                }
                end = item == null;
            }

            var Result = subjects.Select(s => new { s.Title, s.SubjectID }).ToDictionary(t => "/blog/subject/" + t.Title, t => t.Title);

            return Result;
        }
    }
}