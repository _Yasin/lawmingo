﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel.Blog;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Areas.Blog.Controllers
{
    [Area("Blog")]
    public class HomeController : Controller
    {
        private ApplicationDbContext _db { get; set; }

        public HomeController(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<IActionResult> Index(string culture = null)
        {
            var model = new HomeViewModel() { };
            var _posts = _db.Posts.Where(p => !p.Disable && p.PublishTime < DateTime.Now);
            model.Slider = await _posts.Where(p => p.Slider == true).OrderByDescending(p => p.PublishTime).Select(p => new PostItemViewModel()
            {
                Title = p.Title,
                SeoTitle = p.SEO_Title,
                PostID = p.PostID,
                Color = p.Color,
                Font = p.FontColor,
                Time = p.PublishTime.ElapsedTime(),
                Cover = p.Cover,
                Image = p.Image,
                Subject = p.Subject != null ? p.Subject.Title : null,
                SubjectSeo = p.Subject != null ? p.Subject.SEO_Title : null
            }).ToListAsync();
            ViewBag.cats = await _db.Subjects.Where(s => s.ParentID == null).ToListAsync();

            return RedirectPermanent("https://lawmingo.com/mag");
        
        }
    }
}