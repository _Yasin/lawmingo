﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace LawMingo.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ResetPasswordMobileModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private ApplicationDbContext _db { get; set; }
        public ResetPasswordMobileModel(UserManager<ApplicationUser> userManager, ApplicationDbContext db)
        {
            _userManager = userManager;
            _db = db;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {

            [Required(ErrorMessage = "وارد کردن شماره موبایل الزامی است .")]
            [RegularExpression(pattern: @"^0*?[9]\d{9}$", ErrorMessage = "شماره موبایل وارد شده صحیح نیست .")]
            [Display(Name = "شماره موبایل")]
            public string Mobile { get; set; }


            [Required(ErrorMessage = "رمز عبور الزامی است !")]
            [StringLength(100, ErrorMessage = "رمز عبور حداقل باید دارای 6 کاراکتر باشد .", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "رمز عبور")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "تکرار رمز عبور")]
            [Compare("Password", ErrorMessage = "مغایرت در رمزهای عبور !")]
            public string ConfirmPassword { get; set; }

            public string Code { get; set; }
        }

        public IActionResult OnGet()
        {
           
                //Input = new InputModel
                //{
                //    Code = code
                //};
                return Page();
            
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.FindByNameAsync(Input.Mobile);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToPage("./ResetPasswordConfirmation");
            }
            else
            {
                //var result = await _userManager.ResetPasswordAsync(user, Input.Code, Input.Password);
                //var getUser = await _userManager.FindByNameAsync(Input.Mobile);
                //if(getUser != null)
                //{                    
                //    var result =  await _userManager.AddPasswordAsync(getUser, Input.ConfirmPassword);
                //    if (result.Succeeded)
                //    {
                //        return RedirectToPage("./ResetPasswordConfirmation");
                //    }
                //    else
                //    {

                //    }

                //}
                //else
                //{
                //    ModelState.AddModelError(string.Empty, "شماره موبایل نامعتبر !");
                //}

                             
                var newPassword = _userManager.PasswordHasher.HashPassword(user, Input.Password);
                user.PasswordHash = newPassword;
                var res = await _userManager.UpdateAsync(user);

                if (res.Succeeded)
                {
                    return RedirectToPage("./ResetPasswordConfirmation");
                }
                else
                {
                 
                }
            }
                                                                              
            return Page();
        }
    }
}
