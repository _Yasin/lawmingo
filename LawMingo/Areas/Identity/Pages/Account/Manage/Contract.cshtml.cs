﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using static LawMingo.Data.ApplicationDbContext;

namespace LawMingo.Areas.Identity.Pages.Account.Manage
{
    public class ContractModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly IHostingEnvironment _env;

        public ContractModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IHostingEnvironment env,
            ApplicationDbContext db,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _env = env;
            _db = db;
        }
       
        public IEnumerable<InputModel> Contracts { get; set; }     
        public class InputModel
        {
            public int UserContractID { get; set; }
            public string OrderId { get; set; }         
            public int ContractID { get; set; }
            public string UserId { get; set; }
            public string ContractName { get; set; }
            public string ContractEnglishName { get; set; }         
            public string PDF { get; set; }
            public string Word { get; set; }
            public string Time { get; set; }
            public string EditTime { get; set; }
            public int? EditCount { get; set; }
            public decimal Price { get; set; }         
            public int? ParentID { get; set; }
            public UserContract.Status status { get; set; }
            public bool Payment { get; set; }
            public bool Subscrib { get; set; }

            public IEnumerable<PaymentLog> PaymentList { get; set; }

        }
   
        public IActionResult OnGetAsync()
        {        
            return Page();
        }

        public JsonResult OnPostSend()
        {

             string userId = _userManager.GetUserId(HttpContext.User);                                       
             var userContracts = _db.UserContracts.Where(q => q.ApplicationUserId == userId && q.ParentID == null);

            var checkSubscrib = Helpers.Subscription.check(_userManager, _db, HttpContext, null);

            var Result = userContracts.Select(q => new InputModel()
            {

                ContractID = q.ContractID,
                UserContractID = q.UserContractID,
                OrderId = q.OrderId,
                Price = q.Contract.Price.GetValueOrDefault(),
                UserId = q.ApplicationUserId,
                ContractName = q.Contract.Name,
                ContractEnglishName = q.Contract.EnglishName,
                EditCount = q.EditCount.GetValueOrDefault(),
                PDF = q.Content == null ? "false" : q.Content,
                EditTime = PersianDates.ToPersianString(q.EditTime),
                Time = PersianDates.ToPersianString(q.Time),
                ParentID = q.ParentID,
                status = q.status,
                Payment = q.PaymentLogs.Any(o => o.IsSuccessful),
                Subscrib = checkSubscrib.Message == "Yes" ? true : false

            }).ToList().OrderByDescending(q => q.UserContractID);

            return new JsonResult(new { Contracts = Result });
        }

        public JsonResult OnGetContracts(int userContractId)
        {

            string userId = _userManager.GetUserId(HttpContext.User);
            
            var userContracts = _db.UserContracts.Where(q => q.ApplicationUserId == userId && q.ParentID == userContractId);
            var payments = _db.PaymentLogs.Where(q => q.UserContractID == userContractId).ToList();

            foreach (var item in payments)
            {
              item.ResponseCode =  PersianDates.ToPersianString(item.Time);                
            }

        
            var Result = userContracts.Select(q => new InputModel()
            {
                ContractID = q.ContractID,
                UserContractID = q.UserContractID,
                OrderId = q.OrderId,
                Price = q.Contract.Price.GetValueOrDefault(),
                UserId = q.ApplicationUserId,
                ContractName = q.Contract.Name,
                ContractEnglishName = q.Contract.EnglishName,
                EditCount = q.EditCount != null ? q.EditCount : null,
                PDF = q.Content == null ? "false" : q.Content,
                Word = q.Word == null ? "false" : q.Word,
                EditTime = PersianDates.ToPersianString(q.EditTime),
                Time     = PersianDates.ToPersianString(q.Time),
                ParentID = q.ParentID,
                status = q.status,
                 
                Payment = q.PaymentLogs.Any(o => o.IsSuccessful),   
              

            }).ToList().OrderBy(q => q.UserContractID);

            return new JsonResult(new { Contracts = Result , Payments = payments });
        }
    }
}