﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using static LawMingo.Data.ApplicationDbContext;

namespace LawMingo.Areas.Identity.Pages.Account.Manage
{
    public class SubscriptionModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
   
      

        public SubscriptionModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,         
            ApplicationDbContext db
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;                 
            _db = db;
        }
       
        public IEnumerable<InputModel> Subscriptions { get; set; }
      
        public class InputModel
        {
            public int UserSubscriptionID { get; set; }
            public string OrderId { get; set; }
            public string Time { get; set; }
            public string SubscriptionTitle{ get; set; }               
            public int? UsageCount { get; set; }      
            public decimal Price { get; set; }                 
            public bool Payment { get; set; }
            public string ExpireDate { get; set; }       
            public IEnumerable<PaymentLog> PaymentList { get; set; }

        }
  
        public IActionResult OnGetAsync()
        {         
            return Page();
        }

        public JsonResult OnPostSend()
        {

             string userId = _userManager.GetUserId(HttpContext.User);                                       
             var userSubscription = _db.UserSubscriptions.Where(q => q.ApplicationUserId == userId && q.PaymentLogs.Any(r => r.IsSuccessful));
                              
            var Result = userSubscription.Select(q => new InputModel()
            {
                
                UserSubscriptionID = q.UserSubscriptionID,
                OrderId = q.OrderId,
                Price = q.Subscription.Price,
                UsageCount = q.UsageCount,
                SubscriptionTitle = q.Subscription.Title,
                PaymentList = q.PaymentLogs,                
                Time = PersianDates.ToPersianString(q.Time),       
                ExpireDate = PersianDates.ToPersianString(q.Time.AddMonths(q.Subscription.Month)),
                Payment = q.PaymentLogs.Any(o => o.IsSuccessful),
              
            }).ToList().OrderByDescending(q => q.UserSubscriptionID);

            return new JsonResult(new { userSubscriptions = Result });
        }
    
    }
}