﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using MimeKit;
using System.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Threading;
using static LawMingo.Models.ViewModel.SmsViewModel;

namespace LawMingo.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly IHostingEnvironment _env;
        private ApplicationDbContext _db { get; set; }

        private static readonly string Token = "525043434C35686441787436486F596E71467A56452B62776E6B504D453844716C34725469676A335758733D";
        private static readonly string BaseURL = "https://api.kavenegar.com/v1/" + Token;
        private static readonly string verifyURL = BaseURL + "/verify/lookup.json";


        public RegisterModel(
            ApplicationDbContext db,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<RegisterModel> logger,
            IHostingEnvironment env,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _env = env;
            _db = db;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {

            //[Required(ErrorMessage = "نام کاربری الزامی است .")]
            //[StringLength(100, ErrorMessage = "نام کاربر حداقل باید دارای 3 کاراکتر باشد .", MinimumLength = 3)]
            //[Display(Name = "نام کاربری")]
            //public string UserName { get; set; }

            //[Required(ErrorMessage = "ایمیل الزامی است .")]
            //[EmailAddress(ErrorMessage = "ایمیل نامعتبر !")]
            //[Display(Name = "ایمیل")]
            //public string Email { get; set; }

            [Required(ErrorMessage = "وارد کردن شماره موبایل الزامی است .")]
            //[RegularExpression(pattern: @"^0*?[9]\d{9}$", ErrorMessage = "شماره موبایل وارد شده صحیح نیست .")]
            [Display(Name = "شماره موبایل")]
            public string Mobile { get; set; }


            [Required(ErrorMessage = "رمز عبور الزامی است !")]
            [StringLength(100, ErrorMessage = "رمز عبور حداقل باید دارای 6 کاراکتر باشد .", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "رمز عبور")]
            public string Password { get; set; }

            //[DataType(DataType.Password)]
            //[Display(Name = "تکرار رمز عبور")]
            //[Compare("Password", ErrorMessage = "مغایرت در رمزهای عبور !")]
            //public string ConfirmPassword { get; set; }
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync()
        {
              
            if (ModelState.IsValid)
            {
                           
                var user = new ApplicationUser { UserName = Input.Mobile, PhoneNumber = Input.Mobile, Register = DateTime.Now };
                var result = await _userManager.CreateAsync(user, Input.Password);

                if (result.Succeeded)
                {

                     var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, Input.Mobile);
                     var Result = SMS(Input.Mobile, code, "VerifyToken" , "primary");

                    var smsLog = new SmsLog
                    {
                         cost = Result.entries.FirstOrDefault().cost,
                         date = Result.entries.FirstOrDefault().date,
                         EntriyMessage = Result.entries.FirstOrDefault().message,                      
                         messageid = Result.entries.FirstOrDefault().messageid,
                         Receptor = Result.entries.FirstOrDefault().receptor,
                         statustext = Result.entries.FirstOrDefault().statustext,
                         EntriyStatus = Result.entries.FirstOrDefault().status,
                         sender = Result.entries.FirstOrDefault().sender,
                         Template = "VerifyToken",
                         Time = DateTime.Now,
                         Usage = "primary",
                         ReturnMessage = Result.@return.message,
                         ReturnStatus =  Result.@return.status,                          
                    };

                    _db.SmsLogs.Add(smsLog);
                    _db.SaveChanges();

                    return RedirectToPage("./ConfirmCode", "UserId", new { UserId = user.Id });

                  
                    // register with Email confirm 
                    //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                    //var callbackUrl = Url.Page(
                    //    "/Account/ConfirmEmail",
                    //    pageHandler: null,
                    //    values: new { userId = user.Id, code = code },
                    //    protocol: Request.Scheme);

                    //var pathToFile = _env.WebRootPath
                    //           + Path.DirectorySeparatorChar.ToString()
                    //           + "Templates"
                    //           + Path.DirectorySeparatorChar.ToString()
                    //           + "EmailTemplate"
                    //           + Path.DirectorySeparatorChar.ToString()
                    //           + "Email_Confirmation.html";

                    //var subject = "تائید ایمیل ثبت نام در سامانه لامینگو";

                    //var builder = new BodyBuilder();
                    //using (StreamReader SourceReader = System.IO.File.OpenText(pathToFile))
                    //{
                    //    builder.HtmlBody = SourceReader.ReadToEnd();
                    //}

                    //string messageBody = string.Format(builder.HtmlBody,
                    //        subject,
                    //        Input.Email,
                    //        DateTime.Now.ToPersianDateTime().ToString().ToPersianNumber(),
                    //        callbackUrl,
                    //         Input.UserName
                    //        );

                    //await _emailSender.SendEmailAsync(Input.Email, subject, messageBody);

                    // await _signInManager.SignInAsync(user, isPersistent: false);
                    // return LocalRedirect(returnUrl);

                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

          
            return Page();
        }

        public async Task<ActionResult> OnPostResent(string Id)
        {
         
            var user = await _userManager.FindByNameAsync(Id);
            if (user == null)
            {
               
            }

            var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, Input.Mobile);

            var Result = SMS(user.PhoneNumber, code, "VerifyToken" , "Resend");

            var smsLog = new SmsLog
            {
                cost = Result.entries.FirstOrDefault().cost,
                date = Result.entries.FirstOrDefault().date,
                EntriyMessage = Result.entries.FirstOrDefault().message,
                messageid = Result.entries.FirstOrDefault().messageid,
                Receptor = Result.entries.FirstOrDefault().receptor,
                statustext = Result.entries.FirstOrDefault().statustext,
                EntriyStatus = Result.entries.FirstOrDefault().status,
                sender = Result.entries.FirstOrDefault().sender,
                Template = "VerifyToken",
                Time = DateTime.Now,
                Usage = "Resend",
                ReturnMessage = Result.@return.message,
                ReturnStatus = Result.@return.status,
            };

            _db.SmsLogs.Add(smsLog);
            _db.SaveChanges();

            return Content("OK");

        }


        public RootObject SMS(string mobile, string Code, string Template , string Type)
        {

            RootObject Result = null;
            string responseContent = null;

            var request = WebRequest.Create(verifyURL + String.Format("?receptor={0}&token={1}&template={2}", mobile, Code, Template)) as HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            try
            {
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
            
            catch (WebException ex)  
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            }

            if (responseContent != null)
            {
                Result = JsonConvert.DeserializeObject<RootObject>(responseContent);
            }

            Thread.Sleep(2000);
            return Result;
        }
    }
}
