﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MimeKit;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using System.Net.Mail;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using System.Threading;
using LawMingo.Data;
using static LawMingo.Models.ViewModel.SmsViewModel;

namespace LawMingo.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ForgotPasswordModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;
        private readonly IHostingEnvironment _env;
        private readonly ApplicationDbContext db;

        public ForgotPasswordModel(UserManager<ApplicationUser> userManager, IEmailSender emailSender , IHostingEnvironment env , ApplicationDbContext _db)
        {
            _userManager = userManager;
            _emailSender = emailSender;
            _env = env;
             db = _db;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage = "ایمیل / شماره موبایل الزامی است .")]         
            public string Email { get; set; }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
             
                if (IsValid(Input.Email))
                {

                    var user = await _userManager.FindByEmailAsync(Input.Email);
                    if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                    {
                        // Don't reveal that the user does not exist or is not confirmed
                        return RedirectToPage("./ForgotPasswordConfirmation");
                    }


                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);

                    var callbackUrl = Url.Page(
                        "/Account/ResetPassword",
                        pageHandler: null,
                        values: new { code },
                        protocol: Request.Scheme);

                    var webRoot = _env.WebRootPath;
                    var pathToFile = _env.WebRootPath
                               + Path.DirectorySeparatorChar.ToString()
                               + "Templates"
                               + Path.DirectorySeparatorChar.ToString()
                               + "EmailTemplate"
                               + Path.DirectorySeparatorChar.ToString()
                               + "Change_Password.html";

                    var subject = "بازیابی رمز عبور";

                    var builder = new BodyBuilder();
                    using (StreamReader SourceReader = System.IO.File.OpenText(pathToFile))
                    {
                        builder.HtmlBody = SourceReader.ReadToEnd();
                    }

                    string messageBody = string.Format(builder.HtmlBody,
                            subject,
                            Input.Email,
                            DateTime.Now.ToPersianDateTime().ToString().ToPersianNumber(),
                            callbackUrl
                            );

                    await _emailSender.SendEmailAsync(Input.Email, subject, messageBody);
                    return RedirectToPage("./ForgotPasswordConfirmation");
                }

                else if(Input.Email.All(char.IsDigit) && Input.Email.Length == 11)
                {

                    var user = db.Users.Where(w => w.UserName == Input.Email).FirstOrDefault();
                    if(user != null)
                    {
                        var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);
                        var Result = SMS(Input.Email, code, "VerifyToken", "ForgetPassword");

                        var smsLog = new SmsLog
                        {
                            cost = Result.entries.FirstOrDefault().cost,
                            date = Result.entries.FirstOrDefault().date,
                            EntriyMessage = Result.entries.FirstOrDefault().message,
                            messageid = Result.entries.FirstOrDefault().messageid,
                            Receptor = Result.entries.FirstOrDefault().receptor,
                            statustext = Result.entries.FirstOrDefault().statustext,
                            EntriyStatus = Result.entries.FirstOrDefault().status,
                            sender = Result.entries.FirstOrDefault().sender,
                            Template = "VerifyToken",
                            Time = DateTime.Now,
                            Usage = "ForgetPassword",
                            ReturnMessage = Result.@return.message,
                            ReturnStatus = Result.@return.status,
                        };

                         db.SmsLogs.Add(smsLog);
                         db.SaveChanges();

                        return RedirectToPage("./ConfirmCodeForgetPassword", "UserId", new { UserId = user.Id });
                    }
                    else
                    {

                        ModelState.AddModelError(string.Empty, "شماره موبایل وارد شده نامعتبر است .");
                        return Page();
                    }
                  

                }
              

            }

            return Page();
        }


        public bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public RootObject SMS(string mobile, string Code, string Template, string Type)
        {

         string Token = "525043434C35686441787436486F596E71467A56452B62776E6B504D453844716C34725469676A335758733D";
         string BaseURL = "https://api.kavenegar.com/v1/" + Token;
         string verifyURL = BaseURL + "/verify/lookup.json";


            RootObject Result = null;
            string responseContent = null;

            var request = WebRequest.Create(verifyURL + String.Format("?receptor={0}&token={1}&template={2}", mobile, Code, Template)) as HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            try
            {
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                    }
                }
            }

            catch (WebException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            }
            if (responseContent != null)
            {
                Result = JsonConvert.DeserializeObject<RootObject>(responseContent);
            }
            Thread.Sleep(2000);
            return Result;
        }



    }
}
