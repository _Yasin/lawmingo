﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace LawMingo.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ConfirmCodeModel : PageModel
    {

        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly IHostingEnvironment _env;
        private ApplicationDbContext _db { get; set; }

        public ConfirmCodeModel(
            ApplicationDbContext db,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<RegisterModel> logger,
            IHostingEnvironment env,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _env = env;
            _db = db;
        }
       
        [Required(ErrorMessage = "وارد کردن  کد تائید الزامی است .")]
        [Display(Name = "کد تائید")]
        [BindProperty]
        public string Code { get; set; }

        [TempData]
        public string userID { get; set; }

        [TempData]
        public string Mobile { get; set; }

        public void OnGetUserId(string UserId)
        {
            TempData["Mobile"] = _db.Users.Where(w => w.Id == UserId).FirstOrDefault().PhoneNumber;
            TempData["userID"] = UserId;
        }
     
        public async Task<IActionResult> OnPostAsync(string UserID)
        {
          
            if (ModelState.IsValid)
            {                        
                var user = _db.Users.Where(q => q.Id == UserID).FirstOrDefault();
                var Confirm = await _userManager.VerifyChangePhoneNumberTokenAsync(user, Code, user.PhoneNumber);
                if (Confirm)
                {
                     user.PhoneNumberConfirmed = true;
                    _db.SaveChanges();

                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("Code", "کد تائید وارد شده اشتباه است");               
            }        
            return Page();
        }
    }
}