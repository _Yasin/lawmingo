﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using static LawMingo.AffiliateHelper;

namespace LawMingo.Areas.Widget.Controllers
{
    public class initData
    {
        public string token { get; set; }
        public string mobile { get; set; }
        public string name { get; set; }
        public string family { get; set; }
    }
    [Area("Widget")]
    public class HomeController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private static ApplicationDbContext _db { get; set; }
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HomeController(ApplicationDbContext db, IHostingEnvironment hostingEnvironment, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager , IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _signInManager = signInManager;
            _hostingEnvironment = hostingEnvironment;
            _db = db;
        }

        [Route("Widget/payment/verify")]
        public ActionResult paymentverify(string Authority, string Status)
        {
            return RedirectToAction("verify", "payment", new { Authority, Status , layout = "widget" });
        }


        [Route("Widget/token/{guid}/{group}")]
        public ActionResult tokengenerate(string guid, string group)
        {
            var user = _db.Users.Find(guid);
            return Content(Token.Generate(group, user));
        }

        [Route("Widget/gettoken")]
        [HttpPost]
        [EnableCors("AllowMyOrigin")]

        public async Task<ActionResult> gettoken([FromBody]initData data)
        {
            var origin = HttpContext.Request.Headers["origin"];
            bool checkorigin = true;
            checkorigin = !_hostingEnvironment.IsDevelopment();

            var tokenData = Token.Decode(data.token);
            var affilatedata = await _db.Affiliates.Where(a => a.ApplicationUserId == tokenData._Id).FirstOrDefaultAsync();

            if (affilatedata == null || (checkorigin && affilatedata.URL.Split(',').Any(a => origin.Contains(a))))
            {
                Response.StatusCode = 403;
                if (affilatedata == null)
                {
                    return Content("forbidden");
                }
                else if (affilatedata.Disabled)
                {
                    return Content("disabled");
                }
                else if (affilatedata.ExpireDate < DateTime.Now)
                {
                    return Content("expired");
                }
                await AffiliateHelper.Log(_db, HttpContext.Request.Query.ToString(), HttpContext.Request.Path, false, true, tokenData._Id);
                return Content("forbidden");
            }
            var user = await _userManager.FindByNameAsync(data.mobile);
            if (user != null)
            {
                await _signInManager.SignInAsync(user, true);
            }
            else
            {
                user = new ApplicationUser { UserName = data.mobile, Email = "affiliate@lawmingo.com" };
                user.Register = DateTime.Now;
                user.AffiliateID = tokenData._Id;
                user.Name = data.name;
                user.Family = data.family;
                user.PhoneNumber = data.mobile;
                var Userresult = await _userManager.CreateAsync(user, data.mobile);
            }
            var usertoken = Token.Generate("widget", user);
            await AffiliateHelper.Log(_db, HttpContext.Request.Query.ToString(), HttpContext.Request.Path , true, false, tokenData._Id);

            return Json(new { msg = "success" , token = usertoken });
        }

        [Route("Widget/Script")]
        public async Task<JavaScriptResult> Index(string token, string id , string group , int width, int height, string mobile, string name, string family, string email)
        {
            var tokenData = Token.Decode(token);

            string webRootPath = _hostingEnvironment.WebRootPath;
            string result = System.IO.File.ReadAllText(webRootPath + "/widget.js");
            result = result.Replace("{0}", token);
            result = result.Replace("{s}", "true");
            result = result.Replace("{id}", id);
            result = result.Replace("{w}", width.ToString());
            result = result.Replace("{h}", height.ToString());

            var user = await _userManager.FindByNameAsync(mobile);
            if (user != null)
            {

            }
            else
            {
                user = new ApplicationUser { UserName = mobile, Email = "affiliate@lawmingo.com"  };

                user.Register = DateTime.Now;
                user.AffiliateID = tokenData._Id;
                user.Name = name;
                user.Family = family;
                user.PhoneNumber = mobile;

                var Userresult = await _userManager.CreateAsync(user, mobile);
                if (!Userresult.Succeeded)
                {
                    result = result.Replace("{s}", "true");
                }
            }
            var usertoken = Token.Generate("widget", user);

            result = result.Replace("{ut}", usertoken.ToString());
            result = result.Replace("{pt}", token.ToString());

            return new JavaScriptResult(result);
        }

        [Route("Widget/{usertoken?}/{partnertoken?}")]
        public async Task<ActionResult> Index(string usertoken = null, string partnertoken = null)
        {
            string _usertoken = Request.Cookies["usertoken"];
            string _partnertoken = Request.Cookies["partnertoken"];

            if (usertoken != null && partnertoken != null)
            {
                CookieOptions option = new CookieOptions();
                option.Expires = DateTime.Now.AddMinutes(100);
                option.IsEssential = true;
                Response.Cookies.Append("usertoken", usertoken, option);
                Response.Cookies.Append("partnertoken", partnertoken, option);
            }
            else
            {
                usertoken = _usertoken;
                partnertoken = _partnertoken;
            }

            ViewData["title"] = "لیست قراردادها";

            var tokenData = Token.Decode(usertoken);

            if (tokenData._time < DateTime.UtcNow.AddHours(24))
            {
                var user = await _db.Users.FindAsync(tokenData._Id);
                ViewData["name"] = user.UserName;
                await _signInManager.SignInAsync(user, true);
            }
            ViewData["usertoken"] = usertoken;
            ViewData["partnertoken"] = partnertoken;

            var id = Token.Decode(partnertoken)._Id;
            var affiliate = await _db.Affiliates.Where(a => a.ApplicationUserId == id).Select(s => new LawMingo.Models.DomainModel.Affiliate { Name = s.Name, Color = s.Color, FontColor = s.FontColor }).FirstOrDefaultAsync();
            
            ViewData["affiliate"] = affiliate;

            return View();
        }

        [Route("Widget/contract/{id}/{usertoken}/{partnertoken}")]
        public async Task<ActionResult> Index(string usertoken, string partnertoken , int id)
        {
            ViewData["title"] = "ایجاد قرارداد";

            var tokenData = Token.Decode(usertoken);

            if (tokenData._time < DateTime.UtcNow.AddHours(24))
            {
                var user = await _db.Users.FindAsync(tokenData._Id);
                ViewData["name"] = user.UserName;
                await _signInManager.SignInAsync(user, true);
            }

            ViewData["usertoken"] = usertoken;
            ViewData["partnertoken"] = partnertoken;

            return RedirectToAction("GetPage", "Contract", new { ContractID = id, token = partnertoken });
        }

        [Route("Widget/items/{usertoken}/{partnertoken}")]
        public async Task<ActionResult> Items(string usertoken, string partnertoken)
        {
            var categories = new List<CategoryItem>() { };
            var contractitems = await GetItems(_db , partnertoken);

            return Json(new BaseData { contracts = contractitems , categories =  categories});
        }
    }

    public class JavaScriptResult : ContentResult
    {
        public JavaScriptResult(string script)
        {
            this.Content = script;
            this.ContentType = "application/javascript";
        }
    }
}
