﻿using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LawMingo
{
    public class AffiliateHelper
    {
        public class ContractItem
        {
            public int id { get; set; }
            public string title { get; set; }
            public string english { get; set; }
            public string price { get; set; }
            public string discount { get; set; }
            public string category { get; set; }
        }
        public class ContractPrice
        {
            public decimal price { get; set; }
            public decimal discount { get; set; }
        }
        public class CategoryItem
        {
            public int id { get; set; }
            public string title { get; set; }
            public string english { get; set; }
        }

        public static async Task<List<ContractItem>> GetItems(ApplicationDbContext _db , string partnertoken, bool ExpandAll = false)
        {
            var tokenData = Token.Decode(partnertoken);
            var affiliate = await _db.Affiliates.Where(a => a.ApplicationUserId == tokenData._Id).FirstOrDefaultAsync();
            var group = await _db.AffiliateGroups.Where(g => g.AffiliateGroupID == Convert.ToInt32(tokenData._reason))
                .Include("Details.Contract.Category")
                .Include("Details.AffiliateGroup")
                .FirstOrDefaultAsync();
            var details = group.Details.ToList();

            var contracts = new List<Contract>() { };
            var contractitems = new List<ContractItem>() { };

            foreach (var item in details)
            {
                contracts = await GetContarcts( _db , item, contracts, ExpandAll);

                var Result = contracts.Select(s => new ContractItem()
                {
                    id = s.ContractID,
                    title = s.Name,
                    english = s.EnglishName,
                    //category = s.Category.Name,
                    price = string.Format("{0:n0}", CalculatePrice(_db, s, item).price),
                    discount = string.Format("{0:n0}", CalculatePrice(_db, s, item).discount),
                }).ToList();

                contractitems = Result;
            }

            return contractitems;
        }
        public static async Task<List<Contract>> GetContarcts(ApplicationDbContext _db, AffiliateDetail detail, List<Contract> res = null, bool ExpandAll = false)
        {
            res = res == null ? new List<Contract>() { } : res;

            if (detail.ContractID != null)
            {
                res.Add(detail.Contract);
            }

            else if (detail.CategoryID != null && !detail.Exclude && (ExpandAll || detail.ExpandView))
            {
                //var contracts = await _db.Contract
                //    .Where(c => c.CategoryID == detail.CategoryID)
                //    .Include("Category")
                //    .ToListAsync();
                //res.AddRange(contracts);
            }
            else if (detail.CategoryID != null && detail.Exclude)
            {
                //res.RemoveAll(c => c.CategoryID == detail.CategoryID);
            }

            return res;
        }
        public static ContractPrice CalculatePrice(ApplicationDbContext _db, Contract contract, AffiliateDetail detail)
        {
            var price = detail.Price == null ? contract.Price : detail.Price;
            var discount = contract.Discount;

            if (detail.Discount != null && detail.DiscountStart < DateTime.Now && detail.DiscountEnd > DateTime.Now)
            {
                discount = detail.Discount;
            }
            var finalprice = price - discount;

            if (detail.Wallet > 0 || detail.WalletPercent > 0)
            {
                var maxwallet = detail.AffiliateGroup.Wallet;
                var walletamount = detail.Wallet + detail.Wallet * finalprice;
                if (maxwallet >= walletamount)
                {
                    discount += walletamount;
                    discount = discount > price ? price : discount;
                }
            }
            return new ContractPrice() { price = price ?? 0, discount = discount ?? 0 };
        }
        public static decimal CalculateCommission(ApplicationDbContext _db, Contract contract, AffiliateDetail detail)
        {
            var calc = CalculatePrice(_db, contract, detail);
            var finalprice = calc.price - calc.discount;

            var commision = detail.CommissionStatic + ((decimal)detail.CommissionPercent * finalprice / 100);
            commision = commision < detail.MaxCommission ? commision : detail.MaxCommission;

            return commision;
        }

        public static async Task<AffiliateDetail> FindAffiliateDetailByContract(ApplicationDbContext _db, Contract contract, string partnertoken)
        {
            var Result = new AffiliateDetail() { };

            var tokenData = Token.Decode(partnertoken);
            var affiliate = await _db.Affiliates.Where(a => a.ApplicationUserId == tokenData._Id).FirstOrDefaultAsync();
            var group = await _db.AffiliateGroups.Where(g => g.AffiliateGroupID == Convert.ToInt32(tokenData._reason)).Include("Details").FirstOrDefaultAsync();
            var details = group.Details.ToList();

            foreach (var item in details)
            {
                if (item.ContractID == contract.ContractID && !item.Exclude)
                {
                    return item;
                }
                //if (item.CategoryID == contract.CategoryID && !item.Exclude)
                //{
                //    return item;
                //}
            }
            return Result;
        }

        public static async Task<int> Log(ApplicationDbContext _db, string request, string url, bool status, bool forbidden, string affilateid)
        {
            var entity = new AffiliateLog() { };
            entity.ForbiddenAccess = forbidden;
            entity.Success = status;
            entity.Time = DateTime.Now;
            entity.URL = url;
            entity.AffiliateID = Guid.Parse(affilateid);
            _db.AffiliateLogs.Add(entity);
            return await _db.SaveChangesAsync();
        }

        public static async Task<int> Transaction(ApplicationDbContext _db, Contract contract, AffiliateDetail detail)
        {
            var entity = new AffiliateTransaction() { };
            entity.Ip = "";
            entity.Time = DateTime.Now;
            entity.AffiliateDetailID = detail.AffiliateDetailID;
            entity.Amount = CalculateCommission(_db, contract, detail);

            _db.AffiliateTransactions.Add(entity);
            return await _db.SaveChangesAsync();
        }

    }

}
