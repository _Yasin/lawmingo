﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LawMingo.Models.DomainModel
{
    public class AffiliateLog
    {
        public AffiliateLog() { }
        public string AffiliateLogID { get; set; }
        public string ApplicationUserID { get; set; }
        public Guid AffiliateID { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string Request { get; set; }
        public string Ip { get; set; }
        public DateTime Time { get; set; }
        public bool Success { get; set; }
        public bool ForbiddenAccess { get; set; }

        [ForeignKey("ApplicationUserID")]
        public virtual ApplicationUser ApplicationUser { get; set; }
        [ForeignKey("AffiliateID")]
        public virtual Affiliate Affiliate { get; set; }
    }
}