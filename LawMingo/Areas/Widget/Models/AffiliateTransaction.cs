﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace LawMingo.Models.DomainModel
{
    public class AffiliateTransaction
    {
        public AffiliateTransaction() { }
        public int AffiliateTransactionID { get; set; }
        public int? UserContractID { get; set; }
        public int? AffiliateDetailID { get; set; }
        public string Description { get; set; }
        public string Ip { get; set; }
        public DateTime Time { get; set; }
        public decimal Amount { get; set; }
        public UserContract UserContract { get; set; }
        public AffiliateDetail AffiliateDetail { get; set; }

    }
}