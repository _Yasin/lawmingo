﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LawMingo.Models.DomainModel
{
    public class AffiliateGroup
    {
        public AffiliateGroup() { }
        public int AffiliateGroupID { get; set; }
        [ScaffoldColumn(false)]
        public Guid AffiliateID { get; set; }
        public string Title { get; set; }
        public DateTime CreateTime { get; set; }
        public bool Disabled { get; set; }
        public string Token { get; set; }
        public decimal? Wallet { get; set; }

        public virtual Affiliate Affiliate { get; set; }
        public ICollection<AffiliateDetail> Details { get; set; }
    }
}