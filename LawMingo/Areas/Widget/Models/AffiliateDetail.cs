﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;

namespace LawMingo.Models.DomainModel
{
    public class AffiliateDetail
    {
        public AffiliateDetail() { }
        public int AffiliateDetailID { get; set; }
        public int AffiliateGroupID { get; set; }

        public int? ContractID { get; set; }
        public int? CategoryID { get; set; }
        public bool Exclude { get; set; }

        public float CommissionPercent { get; set; }
        public decimal CommissionStatic { get; set; }
        public decimal MaxCommission { get; set; }

        public float ThirdPersonCommissionPercent { get; set; }
        public decimal ThirdPersonCommissionStatic { get; set; }
        public decimal ThirdPersonMaxCommission { get; set; }

        public decimal? Price { get; set; }
        public decimal? Discount { get; set; }

        public decimal Wallet { get; set; }
        public decimal WalletPercent { get; set; }

        public DateTime? DiscountStart { get; set; }
        public DateTime? DiscountEnd { get; set; }
        public bool ExpandView { get; set; }
        public virtual Contract Contract { get; set; }
        public virtual Category Category { get; set; }
        public virtual AffiliateGroup AffiliateGroup { get; set; }
    }
}