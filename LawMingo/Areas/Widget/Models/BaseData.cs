﻿using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using static LawMingo.AffiliateHelper;

namespace LawMingo.Models.ViewModel
{
    public class BaseData
    {
        public Affiliate affiliate { get; set; }
        public List<CategoryItem> categories { get; set; }
        public List<ContractItem> contracts { get; set; }
    }
}