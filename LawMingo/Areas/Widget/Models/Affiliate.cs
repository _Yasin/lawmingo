﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace LawMingo.Models.DomainModel
{
    public class Affiliate
    {
        public Affiliate() { }
        public string ApplicationUserId { get; set; }
        public Guid AffiliateID { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public string Ip { get; set; }
        public DateTime RegisterDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public string Color { get; set; }
        public string FontColor { get; set; }
        public bool Disabled { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public ICollection<AffiliateGroup> Groups { get; set; }

    }
}