﻿var _postcomment = new Vue({
    el: '.newqbox',
    data: {
        success: false,
        code: null,
        loading: true
    },
    methods: {
        msg: function () {
            $.validator.unobtrusive.parse("#form0");
            var name = $("#qtitle").val();
            var contact = $("#qnumber").val();
            var content = $("#qdesc").val();
            $('#form0').validate();

            if ($('#form0').valid()){
                _postcomment.$data.loading = true;
                var ret = $.ajax({
                    type: "POST",
                    url: "/api/CreateQuestion",
                    data: { content, name, contact },
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        _postcomment.$data.success = true;
                        _postcomment.$data.code = response;
                        setTimeout(function () {
                            lottie.loadAnimation({
                                container: document.getElementById('commentSuccess'), // the dom element that will contain the animation
                                renderer: 'svg',
                                width: 50,
                                height: 50,
                                loop: false,
                                autoplay: true,
                                path: '/widget/success.json' // the path to the animation json
                            });
                        }, 200)
                    }
                });

            }

        }
    }
});
