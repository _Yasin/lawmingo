﻿
//################## Address #######################

var Address;
if (window.location.href.includes("localhost")) Address = "https://localhost:5001/";
else Address = "https://lawmingo.com/";


//##################  show questions  #######################

function goToQuestions(userContractId , registerForm) {

     
    if(Address.includes("https://lawmingo.com")) ga('send', 'event', 'Internal', 'onclick', `startTO ${ContName}`, '1', '2');

    // session not clear in register Form
    if (!registerForm)
    {
        sessionStorage.clear();
        sessionStorage.clear();
    }

    //change Progress Bar
    NextLevel('#item1', '#item2', '#Wrapper-Introduction', registerForm ? '#InfoRegister' : '#questions');

    // check user recently contract
    if (userContractId !== undefined) {

        $("#check_recently").hide();

    // delete previuse user contract
        $.ajax({
                  url: '/Contract/Delete_UserContract',
                  type: 'POST',
                  dataType: 'json',
                  data: {
                      UserContractID: userContractId
                  },
                  success: function (Res) {
                      if (!Res) console.log("Error: Delete_UserContract !!!");
                  },
                  error: function () { alert('خطا : لطفا دقایقی دیگر مجددا اقدام نمائید'); }
        });
    }

    // show register form
    if (registerForm) GetFormData();

    // show question form
    else getQuestions(null, null, null); 
    
}

// NextLevel function for change progress bar Items
function NextLevel(Current, Next, hidden, show){

    $(`${Current} span:nth-child(2)`).addClass("changed");
    $(Next).addClass("active");
    $(hidden).hide();
    if (show) $(show).show();
   
}


//################## show help Modal   #######################
$(document).on('click', '#Allquestions>p>a', function () {

    $.ajax({
        url: '/Contract/GetHelp',
        type: 'POST',
        dataType: 'json',
        data: {
            QI: $(this).attr("id").split("-")[2]
        },
        success: function (Res) {

            //$("#Question-help-Modal .modal-content .modal-body.text-right h5").remove();
            $("#Question-help-Modal").modal('show');
            var template = $.templates("#Help-Temp");
            var htmlOutput = template.render(Res);
            $("#Question-help-Modal .modal-content .modal-body.text-right").html(htmlOutput);

        },
        error: function () { alert('خطا : لطفا دقایقی دیگر مجددا اقدام نمائید'); }
    });

});
//################## check user login and register user comment for help Modal #######################
$(document).on('click', '.UserComments>button', function () {

    if ($(".align-items-center .Account a:nth-child(1)").text().trim() === "ثبت نام") {

        window.location = Address + 'Identity/Account/Login';

    }

    else {

        if ($("#UserComments-message").css("display") === "none") {

            var comment = $(this).attr("id").split("-")[0];
            if (comment === "Like") $("#loader-Like").show();
            else $("#loader-DisLike").show();

            $.ajax({
                url: '/Contract/HelpScore',
                type: 'POST',
                dataType: 'json',
                data: {
                    Comment: $(this).attr("id")
                },
                success: function (Res) {

                    $("#UserComments-message").show();
                    if (Res.message === "success") {
                        $("#UserComments-message").html("<span class='confirm'>کاربر گرامی نظر شما با موفقیت ثبت شد .</span>");

                        $("#Like-" + Res.qID).find("span").text(Res.LikeCount);
                        $("#DisLike-" + Res.qID).find("span").text(Res.DisLikeCount);
                    }
                    else {
                        $("#UserComments-message").html("<span class='Noconfirm'>کاربر گرامی نظر شما در رابطه با این راهنما قبلا ثبت شده است .</span>");
                    }

                    $("#loader-DisLike , #loader-Like").hide();
                },
                error: function () { alert('خطا : لطفا دقایقی دیگر مجددا اقدام نمائید'); }
            });
        }

    }

});



//################## register user info before payment #######################
function RegisterFormData() {

    $(".Image-Holder").show();
    var Edit_UserContractId = null;
    var Answer_Id = [];
    var ArticleDetails_id = [];
    var Register_Data = [];

    for (var i = 0, len = sessionStorage.length; i < len; ++i) {

        if (sessionStorage.key(i).split('-')[0] === "ID") {
            var q = "q:" + sessionStorage.key(i).split('-')[2];
            var A = "A:" + sessionStorage.getItem(sessionStorage.key(i));
            Answer_Id.push(q + "," + A);
        }
        if (sessionStorage.key(i).split('-')[0] === "xx") {
            ArticleDetails_id.push(sessionStorage.getItem(sessionStorage.key(i)));
        }
    }

    $("#Wrapper-Contract-Info :input").each(function () {

        var PropertyId = $(this).attr("id").split("-")[2];
        var value = $(this).val();
        var item = PropertyId + ":" + value;

        if (value !== "" && PropertyId !== undefined) {
            Register_Data.push(item);
        }
    });

    //check Edit Mode
    if (user_Answer !== null) {
        var currentHref = window.location.href.split("/");
        Edit_UserContractId = currentHref[currentHref.length - 1];
    }

    $.ajax({

        url: '/Contract/Info',
        type: 'POST',
        dataType: 'json',
        data: {
            Data: Register_Data,
            ContractID: ContID,
            AnswerId: Answer_Id,
            ArticleDetails_Id: ArticleDetails_id,
            userContractId: Edit_UserContractId,
            partnertoken: _pt
        },
        success: function (Res) {

            if (Res === "false") {
                console.log("warning__register data");
            }
            else {

                sessionStorage.setItem('UserContract__ID', Res.UserContractID);
                $(".Image-Holder").hide();

                $("#item3 span:nth-child(2)").addClass("changed");
                //$(".Progress-bar li.progress-bar-Item").removeClass("active");
                $("#item4").addClass("active");
                $("#InfoRegister").hide();
                $("#Payment").show();

                //$("#Payment #Wrapper-Payment-Info").append(
                //    '<button  onclick="ContractDownload(' + Res.UserContractID + ')" id="contract-download" class="btn btn-success">ایجاد فایل قرارداد</button>'
                //);


                //console.log(user_Answer) check if contract NOT Free and user is Not in Edit Mode show Price and Payment

                if (Res.Price !== 0 && Res.Price !== null && Res.Price !== undefined && user_Answer.length === 0 && !Res.Subscrib) {


                    if (window.location.href.includes("token"))

                    {
                        $("#Payment #Wrapper-Payment-Info").append(

                            `
                            <p id='wrapper-Peyment'>  هزینه قابل پرداخت جهت دریافت فایل ${Res.Name}<span>${Res.Price.toLocaleString()} تومان </span>

                            <div class=".payment-container">
                                <button id="payment-contract" onclick="Payment(${Res.UserContractID} , 'zarinpal')" class="btn btn-success newpaybtn" >
                                    <img src="/Images/Logo/zarinpal.png" style="height: 32px;margin-left: 10px;">
                                    پرداخت از طریق زرین پال
                                </button>

                                <button id="payment-contract" onclick="Payment(${Res.UserContractID} , 'pod')" class="btn btn-success newpaybtn" style="border-color: #ff5001;">
                                    <img src="/Images/Logo/podoim.png" style="height: 32px;margin-left: 10px;">
                                    پرداخت از طریق پادیوم
                                </button>
                            </div>

                           <div class='container' style='direction:rtl;'><div class='row'><div class='col-md-6'><p class='text-left discount-code' style='margin-top: 6.5px;color: #666;'>لطفا کد تخفیف خود را وارد نمائید  : </p></div><div class='col-md-6'><input class='form-control' id='Discount-Code' placeholder='کد تخفیف ...' /></div></div></div>" +
                            <p><button id ='ApllayCoupon' class='btn btn-success'>اعمال کد تخفیف</button ><img src='/Images/Loader/ajax_loader.gif' id='Coupon-Loader' style='display:none;'/></p>" +
                           <input type='hidden' value='${Res.UserContractID}' />"
                             `
                          
                        );
                    }

                    else {
                        $("#Payment #Wrapper-Payment-Info").append(

                            `      
                  <div style='direction:rtl;'>                       
                      <div class="table-responsive">
                               <h5 id='invoice'>
                                  پیش فاکتور فروش
                               </h5>
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>ردیف</th>
                                <th>شرح</th>  
                                <th>مبلغ</th> 
                                <th>تخفیف</th>
                                <th>مبلغ پس از تخفیف</th>
                                <th>جمع مبلغ کل</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>1</td>        
                                <td>${Res.Name}</td>        
                                <td class='custom-letterSpace'>${Res.Price.toLocaleString()}</td>        
                                <td class='custom-letterSpace'>${Res.Discount !== 0 && Res.Discount !== null ? Res.Discount.toLocaleString() : 0}</td>        
                                <td class='custom-letterSpace'>${Res.Discount !== 0 && Res.Discount !== null ? (Res.Price - Res.Discount).toLocaleString() : Res.Price.toLocaleString()}</td>  
                                <td class='custom-letterSpace'>${Res.Discount !== 0 && Res.Discount !== null ? (Res.Price - Res.Discount).toLocaleString() : Res.Price.toLocaleString()}</td>
                              </tr>
                             <tr>
                                  <td class='custom-width' colspan='4'></td>
                                  <td id='coupun-name'>کوپن</td>
                                  <td class='coupun-amount'>0</td>
                             </tr>
                            <tr>
                                  <td class='custom-width' colspan='4'></td>
                                  <td id='price-style'>مبلغ قابل پرداخت</td>
                                  <td class='Amount-price-style'>
                                      ${Res.Discount !== 0 && Res.Discount !== null ? (Res.Price - Res.Discount).toLocaleString() : Res.Price.toLocaleString()} تومان                             
                                  </td>
                             </tr>
                            </tbody>
                          </table>
                      </div>
                       <div class='row'>
                           <div class='col-md-6 col-sm-12'>                            
                             <div id='wrapper-coupon'>
                                   <input class='form-control' id='Discount-Code' placeholder='کوپن تخفیف خود را وارد کنید . . .' /> 
                                   <button id='ApllayCoupon' onclick='Coupon(${Res.UserContractID} , this)' class='btn btn-primary'>ثبت</button> 
                                   <input type='hidden' value = ${Res.UserContractID} />
                                   <img id='Coupon-Loader' src="/Images/Loader/ajax-loader.gif"/>                                   
                             </div>
                           </div>
                           <div class='col-md-6 col-sm-12'>
                               <div class='container'>
                                <div class='row' style='justify-content: flex-end;margin-bottom:10px;'>
                                    <button style='font-size: 14px;' id="payment-contract" onclick="Payment(${Res.UserContractID} , 'zarinpal')" class="btn btn-success newpaybtn" >
                                    <img src="/Images/Logo/zarinpal.png" style="height: 32px;margin-left: 10px;">
                                    پرداخت از طریق زرین پال
                                    </button>
                                </div>
                               <div class='row' style='justify-content: flex-end;'>
                                    <button style='font-size: 14px;' id="payment-contract" onclick="Payment(${Res.UserContractID} , 'pod')" class="btn btn-success newpaybtn" style="border-color: #ff5001;">
                                    <img src="/Images/Logo/podoim.png" style="height: 32px;margin-left: 10px;">
                                    پرداخت از طریق پادیوم
                                </button>
                             </div>
                               </div>
                           </div>                           
                       </div>                   
                </div>
                   
                           `
                        );
                    }
                }

                else {

                    $("#Payment #Wrapper-Payment-Info").append(
                        `
                             <div class='row'>
                                <div class='col-md-6' style='margin-bottom: 10px;'>
                       <button  onclick="ContractDownload(${Res.UserContractID})" id="contract-download" class="btn btn-success"> </button>
                                  </div>
                             <div class='col-md-6' style='margin-bottom: 10px;'>
                       <button  onclick="DownloadWord(${Res.UserContractID})" id="contract-word"  class="btn btn-success"></button>

                                  </div>
                               </div>
                       `
                    );

                    //Word ایجاد فایل < i style = "vertical-align:middle;" class="fa fa-pencil-square-o" ></i >
                    //PDF ایجاد فایل < i style = "vertical-align:middle;" class="fa fa-pencil-square-o" ></i >

                    if (Res.Subscrib) {
                        $("#Payment #Wrapper-Payment-Info").append(
                            "<p id='usage-count-contract'> تعداد قرارداد  باقی مانده در این اشتراک  : " + Res.usageCount + " </p>"
                        );
                    }
                }

            }

        }

    });
}
//################## get all data for create contract form and send html contract to server for convert to pdf #######################
function ContractDownload(UserContractId, user_Answer , element) {

    console.log(element)
    //ga("send", "event", "CreateContract", "onclick", "ایجاد قرارداد", "4");

    $(".Image-Holder").show();

    $.ajax({

        url: '/Contract/ArticleDetails',
        type: 'POST',
        headers:
        {
            "RequestVerificationToken": $('#Wrapper-Payment-Info form input:hidden[name="__RequestVerificationToken"]').val()
        },
        dataType: 'json',
        data: {
            UserContractID: UserContractId
        },
        success: function (data) {

            console.log(data)

            //create Article Content with Data
            var content = "<form style='padding:0;margin:0;' id='contract'><div style='text-align:center;margin:0;margin-bottom:35px;'><p style='font-weight:bold;text-align:center;font-size:26px;margin:0 auto !important;'>" + data.ContractName + "</p></div>";

            //content += "<h6 style='font-weight:bold;margin-bottom:40px;text-align:center;>" + data.ContractName  + "<h6>";

            for (var i = 0, len = data.Art.length; i < len; ++i) {

                content += "<p style='font-weight:bold;margin-bottom:20px;color:#6e1529'> ماده " + Number(data.Art[i].Article.split('-')[0]) + " - " + data.Art[i].Article.split('-')[1] + "<p>";
                slicePluse = '';
                var counter = 0;
                for (var ii = 0; ii < data.Art[i].Content.length; ii++) {

                    if (data.Art[i].Content.length > 1) {

                        var con = data.Art[i].Content[ii];
                        con = con.replace('<p dir="RTL">', ' ').replace("</p>", " ").replace("placeholder", " ");

                        var count = (con.match(/===/g) || []).length;
                        var SliceCount = (slicePluse.match(/<p row>/g) || []).length;

                        //console.log("count : ", count)
                        //console.log("SliceCount : ", SliceCount)

                        if (count !== null && count >= 1) {

                            var slice = con.split('===');

                            for (var iii = 0; iii < slice.length; iii++) {

                                //if (isEmpty())
                                //slice[iii] !== "   " && slice[iii] !== "" && slice[iii] !== " " && slice[iii] !== "&nbsp;" && slice[iii] !== " &nbsp;" && slice[iii] !== "&nbsp; " && slice[iii] !== '<p dir="RTL" data-list="0" data-level="1">' && slice[iii] !== '<p>' && slice[iii] !== '<p dir="RTL" data-list="0" data-level="2">'
                                if (!isEmpty(slice[iii]) && !slice[iii].startsWith('<p dir="RTL"')) {

                                    SliceCount = (slicePluse.match(/<p row>/g) || []).length;

                                    counter++;

                                    var row = '';
                                    row += "<p row>";
                                    row += data.Art[i].Article.split('-')[0];
                                    row += " - ";
                                    row += ii < 1 ? counter : SliceCount + 1;
                                    row += " ";
                                    row += slice[iii];
                                    row += " </p> ";

                                    slicePluse += row;
                                }
                            }

                        }
                        else {

                            row = '';
                            row += "<p row>";
                            row += data.Art[i].Article.split('-')[0];
                            row += " - ";
                            row += ii < 1 ? 1 : SliceCount + 1;
                            row += " ";
                            row += con;
                            row += " </p> ";

                            slicePluse += row;
                        }

                    }
                    else {
                        con = data.Art[i].Content[ii];
                        con = con.replace('<p dir="RTL">', ' ').replace("</p>", " ");

                        count = (con.match(/===/g) || []).length;
                        SliceCount = (slicePluse.match(/<p row>/g) || []).length;

                        if (count !== null && count >= 1) {
                            slice = con.split('===');

                            for (iii = 0; iii < slice.length; iii++) {

                                if (!isEmpty(slice[iii]) && !slice[iii].startsWith('<p dir="RTL"')) {
                                    //if (slice[iii] !== "   " && slice[iii] !== "" && slice[iii] !== " " && slice[iii] !== "&nbsp;" && slice[iii] !== " &nbsp;" && slice[iii] !== "&nbsp; " && slice[iii] !== '<p dir="RTL" data-list="0" data-level="1">' && slice[iii] !== '<p>' && slice[iii] !== '<p dir="RTL" data-list="0" data-level="2">') {
                                    counter++;

                                    row = '';
                                    row += "<p row>";
                                    row += data.Art[i].Article.split('-')[0];
                                    row += " - ";
                                    row += counter;
                                    row += " ";
                                    row += slice[iii];
                                    row += " </p> ";

                                    slicePluse += row;
                                }
                            }

                        }
                        else {
                            slicePluse += "<p>" + con + "</p>";
                        }
                    }
                }

                content += slicePluse;
            }

            content += "</form>";

            content = content.replace(/ {1,}/g, " ");
            content = content.replace(/placeholder/g, " ");


            $("#temperory-wrapper").html(content);


            //bind properties to Artice Content
            data.Props.forEach(function (value, index) {

                var selector = "#temperory-wrapper input[id$='" + "-" + value.ArticlePropertyID + "']";
                $(selector).replaceWith("<span style='padding-left:10px;'>" + value.Value + "</span>");
            });

            //check if paragraph is empty deleted paragraph
            $('#temperory-wrapper p').each(function () {
                var $this = $(this);
                if ($this.html().replace(/\s|&nbsp;/g, '').length === 0)
                    $this.remove();
            });


            //check Edit Mode
            if (user_Answer !== null) {
                var currentHref = window.location.href.split("/");
                getPreviousUserContractId = currentHref[currentHref.length - 1];
            }

            $.ajax({
                url: '/ExportPdf/CreatePdfAsync',
                type: 'POST',
                data: {
                    Contract: $("#temperory-wrapper").html(),
                    userContractID: UserContractId,
                    // send Previous UserContractID if in edit mode
                    PreviousUserContractID: user_Answer !== null ? getPreviousUserContractId : 0
                },
                dataType: 'json',
                success: function (res) {

                }
            }).always(function (fileName) {

                if ($("#usage-count-contract").length) {
                    $("#usage-count-contract").text("تعداد قراردادهای  باقی مانده در این اشتراک  : " + fileName.responseText.split("*")[2]);
                }

                $("#temperory-wrapper").html(" ");
             
                $("#contract-download").replaceWith('<a class="btn btn-success" onclick = "download_PDF(event,' + "'" + fileName.responseText.split("*") + "'" + ')"> PDF دریافت فایل  <i style="vertical-align: middle;" class="fa fa-download"></i></a>');
                $("#Payment #Wrapper-Payment-Info .justify-content-center").remove();
                $("#Payment #Wrapper-Payment-Info").append(
                    `                   
                     <div class="row justify-content-center">   
                        <hr />
                        <div class="col-md-12 col-sm-12 order-1 order-md-12" id="DesForContract">
                                <div id='detail-cont'>                                  
                                        <ul>
                                            <li>  کاربر گرامی در صورت نیاز به ویرایش فایل PDF به بخش  <span> قراردادها </span>  در <span> پروفایل کاربری </span> رجوع کنید . </li>
                                            <li>ویرایش فایل PDF تا <span> 2 </span> بار برای شما امکان پذیر است .</li>
                                            <li> نسخه های  قرارداد در قسمت  <span>' قراردادها '</span>  برای شما موجود خواهد بود   .</li>
                                            <li>  در صورت داشتن هر گونه سوال ، از طریق <span>  چت آنلاین  </span> و یا <span>  شماره تماس  </span>  با ما در ارتباط باشید . </li>
                                        </ul>
                                </div>
                        </div>
                    </div>
                    `               
                );

                $(".Image-Holder").hide();

                //sessionStorage.clear();

            });
        }
    });

}

//################## Payment #######################

function Payment(UserContractId , ipg) {

    let discount = $("#payment-contract").attr('data-code');
    window.location = `${Address}Payment/Gateway/${ipg}?UserContractId=${UserContractId}-${discount}`;
}

//################## check if user login then register user info  #######################
  
function GoToInfoRegister() {

    if ($("#main-nav li:nth-child(1)").text().trim() === "ورود به لامینگو") 
       
           window.location = `${Address}Identity/Account/Login?returnUrl=/Contract/GetPage?ContractID=${ContID}-*`;

    else {

        NextLevel('#item2', '#item3', '#questions' );    
        $("#InfoRegister").css("display", "block");
        GetFormData();
    }
}

function check_RecentlyContracts(callback) {

    return $.ajax({
        url: '/Contract/checkRecentlyContracts',
        type: 'POST',
        cache: false,
        data: {
            ContractID: ContID
        },
        success: callback
    });
}



// create form for register user data 
function GetFormData() {
    
        // default  edit mode is false
        EditMode = false;

        // Replace '===' and another caracter with space when show to user
        var helper = {
            ReplaceDir: function (e) {
                return e = e.replace(/===/g, '')
                    .replace(/dir="RTL"/g, '')
                    .replace(/\u00a0/g, " ")
                    .replace(/ {2,}/g, ' ');
            }
        };
      
        $(".Image-Holder").show();
      
        // set all articleDetailId for send to server and create register form data
        var ArticleDetails_ID = [];
        for (var i = 0, len = sessionStorage.length; i < len; ++i) {
            if (sessionStorage.key(i).split('-')[0] === "xx") {
                ArticleDetails_ID.push(sessionStorage.getItem(sessionStorage.key(i)));
            }
        }

        // check update Mode
        if (user_Answer.length > 0) {

            var currentHref = window.location.href.split("/");
            UserContractId = currentHref[currentHref.length - 1];
        }

        $.ajax({

            // get all articleDetails with input 
            url: '/Contract/ArticleDetailsInput',
            type: 'POST',
            data: {
                ArticleDetailsId: ArticleDetails_ID,
                // if update mode send UserContractId
                userContractId: user_Answer.length > 0 ? UserContractId : null
            },
            dataType: 'json',
            success: function (res) {

                // Inject response data to html
                var template = $.templates("#RegisterForm");
                var htmlOutput = template.render(res.Art, helper);
                $("#Wrapper-Contract-Info").html(htmlOutput);

                // check if Edit Mode, bind user data (if exist) to register form
                if (res.Properties !== null && res.Properties.length > 0) {

                    res.Properties.forEach(function (value, index) {

                        var selector = "#Wrapper-Contract-Info input[id$='" + "-" + value.ArticlePropertyID + "']";
                        $(selector).val(value.Value);
                    });
                }

                // append button for register user data to server
              
                $("#Wrapper-Contract-Info").append('<button id="RegisterFormData" onclick="RegisterFormData()" class="btn btn-success"> ثبت اطلاعات </button>');
                $("#Register-UserData-Info p").after('<button id="RegisterFormData" onclick="RegisterFormData()" class="btn btn-success"> مرحله بعد</button>');

              
                $(".Image-Holder").hide();
            }

        });
  



    //check_RecentlyContracts(function (data) {
    //    console.log(data)
       
    //});

}

// get qustion data 
function getQuestions(direction) {

    // check user selected a answer AND disabled back Button if first question 
    if (Condition(direction)) {

        // save Previous Question AND ArticleID
        savePreviousQuestionANDArticleID();

        // get user answer
        var selectedAnswer = $('#Allquestions input:checked').val();

        // save QuestionID in session
        if (direction) setSession('ID-question-', $("#Temp-QuestionID-holder").val(), selectedAnswer);

        // show Loader
        Loader(true);

        // get Data 
        $.ajax({
            url: '/Contract/Questions',
            type: 'POST',
            headers:
            {
                "RequestVerificationToken": $('#Wrapper-Payment-Info form input:hidden[name="__RequestVerificationToken"]').val()
            },
            data: {

                ContractID: ContID,
                Number: getNumber(direction),
                Answer: getAnswer(direction, selectedAnswer),
                PreQuestion: !direction ? getPreviousQuestion(true) : null,
                Answers: getAllAnswers(),
                ArticleID: getArticleID(),
                nextQuestion: getnextQuestion()

            },
            dataType: 'json',
            success: function (res) {

                // Parse json file 
                var questions = JSON.parse(res.Questions);
                var xx = res.xx;
                var Number = JSON.parse(res.questionNumber);
                var Previous_Answer = res.PreviousAnswer;

                // Inject data to Template
                InjectHtml('#theTmpl-Questions', '#Allquestions', questions, null);

                // Set Contract Name 
                $("#ContractName").html(res.Contract);

                // Change Number of question
                $("#Nember-holder").val(Number).trigger('change');

                // disabled back Button if first question 
                if ($("#Nember-holder").val() === "0") $("#Prev").attr("aria-disabled", "true").addClass("disabled");

                //  *********  Update Mode (bind user answer to radio) *********************
                if (user_Answer.length > 0) BindAnswer(user_Answer, true);
                //  ********* END Update Mode (bind user answer to radio) *********************

                //console.log(Previous_Answer)
                // bind answer in back mode
                if (Previous_Answer !== null) {
                    BindAnswer(Previous_Answer, false);
                }

                // save QuestionID and Number of question in session
                setSession('q-', questions.QuestionID, Number);

                // save QuestionID and Number of question in session
                // when all question of one article finished, CA(current ArticleId) change and next questions of new Article start...
                if (res.CA !== 0) $("#Temp-ArticleID-holder").val(res.CA).trigger('change');

                // save all article detailsId AND Previous question (for deleted articleDetailID in back button) into the session storage
                if (xx !== null) {

                    // get Previous QuestionID for deleted articelDetailId in back button(USE WHEN => direction == false)
                    var PreviousQuestionID = getPreviousQuestion(false);

                    $.each(xx, function (key, value) {
                        sessionStorage.setItem('xx-' + value + "/" + PreviousQuestionID, value);
                    });
                }

                // console.log(sessionStorage)

                // Hide Loader
                Loader(false);
            }
        });
    }

}


//************ functions for  getQuestions() ************
function ShowModal(Type) {

    switch (Type) {

        case 'RequiredAnswer': $('#RequiredAnswer').modal('show');
            break;

    }
}
function Condition(direction) {

    //check user select Radio
    if ($('#Allquestions input:checked').val() === undefined && direction) {
        ShowModal('RequiredAnswer');
        return false;
    }

    //disabled back Button if first question 
    if (!direction && $("#Nember-holder").val() === '0') {

        $("#Prev").attr("aria-disabled", "true").addClass("disabled");
        return false;
    }
    else {
        return true;
    }
}
function savePreviousQuestionANDArticleID() {
    $("#Prev-holder").val($("#Temp-QuestionID-holder").val()).trigger('change');
    $("#ArticleID-holder").val($("#Temp-ArticleID-holder").val()).trigger('change');
}
function getPreviousQuestion(delete_ArticleDetailId) {

    var PREV;
    // get Previous QuestionID for send to server
    $.each(sessionStorage, function (key, value) {

        if (value === (Number($("#Nember-holder").val()) - 1).toString()) {
            PREV = Number(key.split("-")[1]);
        }
    });

    if (delete_ArticleDetailId && PREV !== undefined) {
        for (var i = sessionStorage.length - 1; i >= 0; i--) {

            if (sessionStorage.key(i) && sessionStorage.key(i).split('-')[0] === "xx" && sessionStorage.key(i).split('/')[1] === PREV.toString()) {
                //console.log(sessionStorage.key(i))
                sessionStorage.removeItem(sessionStorage.key(i));
            }
        }
    }

    return PREV;
}
function setSession(prefix, key, value) {

    if (key !== undefined) sessionStorage.setItem(prefix + key, value);

}
function getAllAnswers() {

    var answers = [];
    for (var i = 0, len = sessionStorage.length; i < len; ++i) {
        if (sessionStorage.key(i).split('-')[0] === "ID") {
            answers.push(sessionStorage.getItem(sessionStorage.key(i)));
        }
    }

    return answers;
}
function getArticleID() {

    if ($("#Temp-END-holder").val() === 'true') return $("#Temp-ArticleID-holder").val();
    else return null;

}
function getnextQuestion() {

    if ($("#Temp-CheckBox-holder").val() === "true") return $("#Temp-QuestionID-holder").val() + "-" + $("#Temp-ArticleID-holder").val();
    else return null;
}
function Loader(state) {

    if (state) $("#Question-Loader").show();
    else $("#Question-Loader").hide();

}
function InjectHtml(TemplateId, InjectTo, json, Helper) {

    var template = $.templates(TemplateId);
    var htmlOutput = template.render(json, Helper !== null ? Helper : '');
    $(InjectTo).html(htmlOutput);

}
function BindAnswer(user_Answers, EditMode) {

    var answers = $("#Allquestions input[type='radio']");
    var finish = false;

    for (i = 0; i < answers.length; i++) {

        if (finish) break;

        for (ii = 0; ii < user_Answers.length; ii++) {
            if (answers[i].defaultValue === user_Answers[ii]) {

                $(answers[i]).prop('checked', true);

                if (EditMode) {
                    $(answers[i]).parent().css({
                        'color': '#4CAF50'
                    });
                }

                finish = true;
            }
        }
    }
}

//************ Initialized Data ************
function getNumber(direction) {

    var number = direction === null ||
        Number($("#Nember-holder").val()) === 0 && !direction ? 0 :
        direction ? Number($("#Nember-holder").val()) + 1 : Number($("#Nember-holder").val()) - 1;
    return number;
}
function getAnswer(direction, selectedAnswer) {
    return direction === null || !direction || $('#Allquestions input:checked').val() === undefined ? null : selectedAnswer;
}
function isEmpty(str) {
    return (!str || 0 === str.length || /^\s*$/.test(str) || str.trim() === "");
}

//************   ************

function download_PDF(event, ele) {

    event.preventDefault();
    ele = ele.split(",");
    window.location = Address + "Uploads/PdfFiles/" + ele[1] + "/" + ele[0];

    //ga('send', 'event', 'CreateContract', 'onclick', 'دریافت فایل قرارداد', '4');
}
function download_Word(event, ele) {

    event.preventDefault();
    ele = ele.split(",");
    window.location = Address + "Uploads/WordFiles/" + ele[1] + "/" + ele[0];
    //ga('send', 'event', 'CreateContract', 'onclick', 'دریافت فایل قرارداد', '4');

}



function DownloadWord(UserContractId) {
 
        $(".Image-Holder").show();
    
    $.ajax({

        url: '/Contract/ArticleDetails',
        type: 'POST',
        dataType: 'json',
        headers:
        {
            "RequestVerificationToken": $('#Wrapper-Payment-Info form input:hidden[name="__RequestVerificationToken"]').val()
        },
        data: {
            UserContractID: UserContractId
        },
        success: function (data) {
         
            //create Article Content with Data
            var content = "<form style='padding:15px;' id='contract'><div style='text-align:center;margin-bottom:80px;'><p style='font-weight:bold;text-align:center;font-size:26px;margin:0 auto !important;'>" + data.ContractName + "</p></div>";

            //content += "<h6 style='font-weight:bold;margin-bottom:40px;text-align:center;>" + data.ContractName  + "<h6>";

            for (var i = 0, len = data.Art.length; i < len; ++i) {

                content += "<p style='font-weight:bold;margin-bottom:20px;color:#6e1529'> ماده " + Number(data.Art[i].Article.split('-')[0]) + " - " + data.Art[i].Article.split('-')[1] + "<p>";
                slicePluse = '';
                var counter = 0;
                for (var ii = 0; ii < data.Art[i].Content.length; ii++) {

                    if (data.Art[i].Content.length > 1) {

                        var con = data.Art[i].Content[ii];
                        con = con.replace('<p dir="RTL">', ' ').replace("</p>", " ").replace("placeholder", " ");

                        var count = (con.match(/===/g) || []).length;
                        var SliceCount = (slicePluse.match(/<p row>/g) || []).length;

                        //console.log("count : ", count)
                        //console.log("SliceCount : ", SliceCount)

                        if (count !== null && count >= 1) {

                            var slice = con.split('===');

                            for (var iii = 0; iii < slice.length; iii++) {

                                //if (isEmpty())
                                //slice[iii] !== "   " && slice[iii] !== "" && slice[iii] !== " " && slice[iii] !== "&nbsp;" && slice[iii] !== " &nbsp;" && slice[iii] !== "&nbsp; " && slice[iii] !== '<p dir="RTL" data-list="0" data-level="1">' && slice[iii] !== '<p>' && slice[iii] !== '<p dir="RTL" data-list="0" data-level="2">'
                                if (!isEmpty(slice[iii]) && !slice[iii].startsWith('<p dir="RTL"')) {

                                    SliceCount = (slicePluse.match(/<p row>/g) || []).length;

                                    counter++;

                                    var row = '';
                                    row += "<p row>";
                                    row += data.Art[i].Article.split('-')[0];
                                    row += " - ";
                                    row += ii < 1 ? counter : SliceCount + 1;
                                    row += " ";
                                    row += slice[iii];
                                    row += " </p> ";

                                    slicePluse += row;
                                }
                            }

                        }
                        else {

                            row = '';
                            row += "<p row>";
                            row += data.Art[i].Article.split('-')[0];
                            row += " - ";
                            row += ii < 1 ? 1 : SliceCount + 1;
                            row += " ";
                            row += con;
                            row += " </p> ";

                            slicePluse += row;
                        }

                    }
                    else {
                        con = data.Art[i].Content[ii];
                        con = con.replace('<p dir="RTL">', ' ').replace("</p>", " ");

                        count = (con.match(/===/g) || []).length;
                        SliceCount = (slicePluse.match(/<p row>/g) || []).length;

                        if (count !== null && count >= 1) {
                            slice = con.split('===');

                            for (iii = 0; iii < slice.length; iii++) {

                                if (!isEmpty(slice[iii]) && !slice[iii].startsWith('<p dir="RTL"')) {
                                    //if (slice[iii] !== "   " && slice[iii] !== "" && slice[iii] !== " " && slice[iii] !== "&nbsp;" && slice[iii] !== " &nbsp;" && slice[iii] !== "&nbsp; " && slice[iii] !== '<p dir="RTL" data-list="0" data-level="1">' && slice[iii] !== '<p>' && slice[iii] !== '<p dir="RTL" data-list="0" data-level="2">') {
                                    counter++;

                                    row = '';
                                    row += "<p row>";
                                    row += data.Art[i].Article.split('-')[0];
                                    row += " - ";
                                    row += counter;
                                    row += " ";
                                    row += slice[iii];
                                    row += " </p> ";

                                    slicePluse += row;
                                }
                            }

                        }
                        else {
                            slicePluse += "<p>" + con + "</p>";
                        }
                    }
                }

                content += slicePluse;
            }

            content += "</form>";

            content = content.replace(/ {1,}/g, " ");
            content = content.replace(/placeholder/g, " ");


            $("#temperory-wrapper").html(content);


            //bind properties to Artice Content
            data.Props.forEach(function (value, index) {

                var selector = "#temperory-wrapper input[id$='" + "-" + value.ArticlePropertyID + "']";
                $(selector).replaceWith("<span style='padding-left:10px;'>" + value.Value + "</span>");
            });

            //check if paragraph is empty deleted paragraph
            $('#temperory-wrapper p').each(function () {
                var $this = $(this);
                if ($this.html().replace(/\s|&nbsp;/g, '').length === 0)
                    $this.remove();
            });
          
            $.ajax({
                url: '/ExportWord/CreateWordAsync',
                type: 'POST',
                data: {
                    Contract: $("#temperory-wrapper").html(),
                    userContractID: UserContractId                 
                },
                dataType: 'json',
                success: function (res) {

                }
            }).always(function (fileName) {

                if ($("#usage-count-contract").length) {
                    $("#usage-count-contract").text("تعداد قراردادهای  باقی مانده در این اشتراک  : " + fileName.responseText.split("*")[2]);
                }

                $("#temperory-wrapper").html(" ");
                                      
                $("#contract-word").replaceWith('<a class="btn btn-success" onclick = "download_Word(event,' + "'" + fileName.responseText.split("*") + "'" + ')"> Word دریافت فایل  <i style="vertical-align: middle;" class="fa fa-download"></i></a>');
                $("#Payment #Wrapper-Payment-Info .justify-content-center").remove();
                $("#Payment #Wrapper-Payment-Info").append(
                    `                   
                     <div class="row justify-content-center">   
                        <hr />
                        <div class="col-md-12 col-sm-12 order-1 order-md-12" id="DesForContract">
                                <div id='detail-cont'>                                
                                        <ul>
                                            <li>  کاربر گرامی در صورت نیاز به ویرایش فایل PDF به بخش  <span> قراردادها </span>  در <span> پروفایل کاربری </span> رجوع کنید . </li>
                                            <li>ویرایش فایل PDF تا <span> 2 </span> بار برای شما امکان پذیر است .</li>
                                            <li> نسخه های  قرارداد در قسمت  <span>' قراردادها '</span>  برای شما موجود خواهد بود   .</li>
                                            <li>  در صورت داشتن هر گونه سوال ، از طریق <span>  چت آنلاین  </span> و یا <span>  شماره تماس  </span>  با ما در ارتباط باشید . </li>
                                        </ul>
                                </div>
                        </div>
                    </div>
                    `
                );


                $(".Image-Holder").hide();

                //sessionStorage.clear();

            });
        }
    });
}

