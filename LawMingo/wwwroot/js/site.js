﻿$(document).ready(function () {

    // Get Contract Data
    GetHomeData();

    // Init animation
    AOS.init();

     // show home page Carousel
    Carousel();

    // check user Subscrib for Profile menu
    checkSubscrib();
  
    // show and hide menu in mobile
    $(".mob-menu-toggle2").click(function () {
        $(window).scrollTop(0);
        $(this).toggleClass("change_Bg");
        $(".mobile-header").toggleClass("Mobile_Menu");
    });

     // show Password
    $("#PasswordShow").click(function () {
        if ($("#Input_Password").attr("type") === "password") $("#Input_Password").attr('type', 'text');           
        else $("#Input_Password").prop('type', 'password');                        
        $(this).toggleClass("showPass");  

    });

     // Animation for Top Menu
     window.onscroll = function () {

         if (window.innerWidth > 992) {
             if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
                 $("header").removeClass("topMargin").addClass("topFixed");
             } else {
                 $("#header2").show();
                 $("header").removeClass("topFixed").addClass("topMargin");
             }
         }         
     };
      
     // touch event in mobile set red color
    $(function () {
        $(".mob-menu li a span").on('mousedown touchstart', function () {
            jQuery(this).css("color", "black");
        }).on('mouseup touchend', function () {
            jQuery(this).css("color", "red");
        });
    });

});

    // set z-Index for top Fixed header
$("#main-nav li:nth-child(1)").hover(function () {       
    $("#header2").css("z-index", "9999");
         }, function () {   
        $("#header2").css("z-index", "999");
    }); 


 // check user subscription
$(document).on('click ', '.Subscription-Content a.Buy-Subscription', function () {   
    checkSubscrib($(this).attr("data-subscription"));
});


function Carousel()
{
    $('.blogloop').owlCarousel({
        loop: true,
        margin: 20,
        responsive: {
            1400: {
                items: 4
            },
            1000: {
                items: 3
            },
            500: {
                items: 2
            },
            0: {
                items: 1
            }
        }
    });
    $('.brandloop').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayHoverPause: true,
        margin: 20,
        autoplayTimeout: 2000,
        responsive: {
            1400: {
                items: 6
            },
            1000: {
                items: 4
            },
            500: {
                items: 3
            },
            0: {
                items: 2
            }
        }
    });
    $('.testimonialloop').owlCarousel({
        center: true,
        loop: true,
        autoplayHoverPause: true,
        autoplay: true,
        autoplayTimeout: 4000,
        margin: 5,
        responsive: {
            1400: {
                items: 4
            },
            1000: {
                items: 3
            },
            500: {
                items: 2
            },
            0: {
                items: 1
            }
        }
    });
}

function GetHomeData(){

    var helper = {
        HostName: function () {
            var hostName;
            if (window.location.href.includes("localhost")) hostName = "https://localhost:5001/";
            else hostName = "https://lawmingo.com/";
            return hostName;
        },
        UrlFriendly: function (e) {
            return e = e.replace(/\s+/g, '-');
        },       
         PriceFormat: function (e) {
             return e = e.toLocaleString();
         }               
    };

    $.ajax({

             url: '/Home/HomeData',
             type: 'Get',
             data: null,
             dataType: 'json',
             success: function (data) {     
             console.log(data)

            // Subscription
            var Template = $.templates("#Template-Subscription");
            var Html = Template.render(data.Subscription, helper);
            $("#Subscription .row.justify-content-around").html(Html);
                      
            // contract carousel
            var Template_carousel = $.templates("#Template-carousel");
            var Html_carousel = Template_carousel.render(data.Contracts, helper);
            $(".slider1.owl-theme").html(Html_carousel);

            $('.slider1').owlCarousel({
                    
                       rtl: true,
                       loop: true,
                       autoplay: false,                   
                       autoplayHoverPause: true,
                       margin: 30,
                       stagePadding: 50,
                       nav: false,                    
                       responsiveClass: true,
               
                      responsive: {
                          0: {
                              items: 1,
                              dots: false,
                             
                          },
                          600: {
                              items: 3
                            
                          },
                          1000: {
                              items: 4,                          
                              loop: false
                          }
                      }                   
                 });                
        }
    });
}

function checkSubscrib(id , ipg) {

     $.ajax({

         url: '/Subscription/check',
         type: 'POST',
         dataType: 'json',
         data: {
             SubscriptionId: id !== null ? id : null
         },
         success: function (Res) {
         
             if (Res.Message !== "Unauthorized") {
                
                 if (Res.Message === "Yes" && Res.SubScriptionId === null) {
                     $("#subscrib-user , #subscrib-mobile").text(Res.Title);                    
                 }

                 if (Res.Message === "NO" && Res.SubScriptionId === null) {
                     $("#subscrib-user , #subscrib-mobile").text("فاقد اشتراک !");
                 }

                 if (Res.Message === "NO" && Res.SubScriptionId !== null) {

                     var subid = Number(Res.SubScriptionId) + 1128;
                     window.location = Address + 'Payment/gateway/'+ipg+'?SubscriptionID=' + subid;                                          
                 }

                 if (Res.Message === "Yes" && Res.SubScriptionId !== null) {

                     $("#subscrib-modal .modal-content .modal-body div").remove();
                     $("#subscrib-modal .modal-content .modal-body").append(

                        "<div id='subscrib-dis'><p>کاربر گرامی <strong>اعتبار اشتراک</strong> شما به <strong>اتمام نرسیده</strong> است . در صورت نیاز به تولید قرارداد می توانید از همین اشتراک استفاده نمائید و پس از اتمام سقف تعداد قراردادهای تولید شده در این اشتراک ، اقدام به خرید اشتراک جدید نمائید .<p>" +
                         "<div id='conditions-subscrib'><p>اشتراک فعلی شما  : <span>" + Res.Title + "</span></p>" +
                         "<p>تعداد قرارداد تولید شده  : <span>" + Res.usegeCount + "</span></p>" +
                         "<p> تاریخ خرید اشتراک : <span>" + Res.Time + "</span></p>" +
                         "<p> تاریخ انقضا : <span>" + Res.Expire + "</span></p></div>" +
                        "<p>شرایط اتمام اعتبار : </p>" +
                        "<ul>" +
                        "<li>اتمام سقف تعداد قراردادهای آنلاین ایجاد  شده در پلن خریداری شده .</li>" +
                        "<li>اتمام مدت زمان اشتراک .</li></ul></div>"
                    );

                     $("#subscrib-modal").modal('show');
                 }
             }
             else
             {
                 if (Res.SubScriptionId !== null)
                 {
                     $("#subscrib-modal .modal-content .modal-body div").remove();
                     $("#subscrib-modal .modal-content .modal-body").append(

                         "<div id='subscrib-dis'><p>کاربر گرامی ابتدا وارد سایت شوید ، سپس اقدام به خرید اشتراک ماهانه نمائید .<p></div>"
                     );

                     $("#subscrib-modal").modal('show');
                 }
             }           
         },
         error: function () { alert('خطا : لطفا دقایقی دیگر مجددا اقدام نمائید'); }
     });
                
}

//Coupon
function Coupon(userContractId, element) {

    var DiscountCode = $(element).prev().val();

    if (DiscountCode.trim() === "") return Message('<p>وارد کردن کد تخفیف الزامی است .</p>');

    $("#Coupon-Loader").show();

    $.ajax({

        url: '/Coupon/Check',
        type: 'POST',
        dataType: 'json',
        data: {

            DiscountCode: DiscountCode,
            userContractId: userContractId
        },
        success: function (Res) {

            if (Res.FinalPrice === null) return Message(`<p>کد تخفیف وارد شده اشتباه و یا فاقد اعتبار است .</p>`);

            if (!Res.matchCategory) return Message(`<p>کد تخفیف وارد شده قابل استفاده در گروه  <span style='color:#ffbf00'> ${Res.Category} </span> می باشد  .</p>`);


            // styles in applay discount
            $(".Amount-price-style").text(`${Res.FinalPrice === 0 ? 'رایگان' : Res.FinalPrice.toLocaleString() + '  تومان '} `);
            $(".coupun-amount").show().text(`${Res.coupon.Amount} %`);
            $("#coupun-name").text(`کد تخفیف (${Res.coupon.Name})`);
            $("#payment-contract").attr("data-code", Res.discountCode);
            $("#coupun-name , .coupun-amount").css({

                'background-color': '#4CAF50',
                'color': 'white',
                'border-radius': '4px'
            });
            $("#Coupon-Loader").hide();


            // %100 Discount
            if (Res.FinalPrice === 0) {

                $("#payment-contract").replaceWith(

                    `<div class='row' id='wrapper-discount-free'>
                         <div class='col-md-6 text-left' style='margin-bottom: 10px;padding-left:0;'>
                             <button  onclick="ContractDownload(${userContractId} , null , this)" id="contract-download" class="btn btn-success">                                                           
                             </button>
                         </div>
                         <div class='col-md-6 text-right' style='margin-bottom: 10px;'>
                             <button  onclick="DownloadWord(${userContractId})" id="contract-word"  class="btn btn-success">                                                 
                             </button>
                         </div>
                      </div>                        
                     `
                );
            }
        },
        error: function () { alert('خطا : لطفا دقایقی دیگر مجددا اقدام نمائید'); }
    });


    function Message(Msg) {

        $("#Discount-Code-Modal .modal-body p").remove();
        $("#Discount-Code-Modal .modal-body").html(Msg);
        $('#Discount-Code-Modal').modal('show');
        $("#Coupon-Loader").hide();
        return false;
    }
}

//payment modal 
function PaymentModal(element) {

    if (Address.includes("lawmingo.com")) ga("send", "event", "Profile", "onclick", "تکمیل فرآیند", "3");

    var info = $(element).attr("data-info").split(",");

    $("#PaymentComplete #wrapper-payment-profile").remove();
    $("#PaymentComplete .modal-body").append(

                 `
                      <div id ='wrapper-payment-profile'>
                           <p> هزینه قابل پرداخت جهت دریافت فایل <span>${info[2]}</span></p>
                           <p  class='Amount-price-style'>${Number(info[1]).toLocaleString()} تومان</p>
                           <p class='coupun-amount' style='display:none;'></p>
                            <div id='wrapper-discount'>
                                <input class="form-control" id="Discount-Code" placeholder="افزودن کد تخفیف ...">
                                <button id="ApllayCoupon" onclick='Coupon(${info[0]} , this , true)' class="btn btn-primary">ثبت</button>
                                <img src="/Images/Loader/ajax-loader.gif" id="Coupon-Loader">
                           </div>                             
                                                      
                            <div class="payment-container">
                                <button id="payment-contract" onclick="Payment(${info[0]} , 'zarinpal')" class="btn btn-success newpaybtn" >
                                    <img src="/Images/Logo/zarinpal.png" style="height: 32px;margin-left: 10px;">
                                    پرداخت از طریق زرین پال
                                </button>

                                <button id="payment-contract" onclick="Payment(${info[0]} , 'pod')" class="btn btn-success newpaybtn" style="border-color: #ff5001;">
                                    <img src="/Images/Logo/podoim.png" style="height: 32px;margin-left: 10px;">
                                    پرداخت از طریق پادیوم
                                </button>
                            </div>
                           <img class='Image-Holder' src="/Images/Logo/podoim.png" width='30'>
                      </div>
                 `
    );
    $('#PaymentComplete').modal('show');
}

//function DownloadWord(UserContractId) {

//    $(".Image-Holder").show();

//    $.ajax({

//        url: '/Contract/ArticleDetails',
//        type: 'POST',
//        dataType: 'json',
//        headers:
//        {
//            "RequestVerificationToken": $('#Wrapper-Payment-Info form input:hidden[name="__RequestVerificationToken"]').val()
//        },
//        data: {
//            UserContractID: UserContractId
//        },
//        success: function (data) {

//            //create Article Content with Data
//            var content = "<form style='padding:15px;' id='contract'><div style='text-align:center;margin-bottom:80px;'><p style='font-weight:bold;text-align:center;font-size:26px;margin:0 auto !important;'>" + data.ContractName + "</p></div>";

//            //content += "<h6 style='font-weight:bold;margin-bottom:40px;text-align:center;>" + data.ContractName  + "<h6>";

//            for (var i = 0, len = data.Art.length; i < len; ++i) {

//                content += "<p style='font-weight:bold;margin-bottom:20px;color:#6e1529'> ماده " + Number(data.Art[i].Article.split('-')[0]) + " - " + data.Art[i].Article.split('-')[1] + "<p>";
//                slicePluse = '';
//                var counter = 0;
//                for (var ii = 0; ii < data.Art[i].Content.length; ii++) {

//                    if (data.Art[i].Content.length > 1) {

//                        var con = data.Art[i].Content[ii];
//                        con = con.replace('<p dir="RTL">', ' ').replace("</p>", " ").replace("placeholder", " ");

//                        var count = (con.match(/===/g) || []).length;
//                        var SliceCount = (slicePluse.match(/<p row>/g) || []).length;

//                        //console.log("count : ", count)
//                        //console.log("SliceCount : ", SliceCount)

//                        if (count !== null && count >= 1) {

//                            var slice = con.split('===');

//                            for (var iii = 0; iii < slice.length; iii++) {

//                                //if (isEmpty())
//                                //slice[iii] !== "   " && slice[iii] !== "" && slice[iii] !== " " && slice[iii] !== "&nbsp;" && slice[iii] !== " &nbsp;" && slice[iii] !== "&nbsp; " && slice[iii] !== '<p dir="RTL" data-list="0" data-level="1">' && slice[iii] !== '<p>' && slice[iii] !== '<p dir="RTL" data-list="0" data-level="2">'
//                                if (!isEmpty(slice[iii]) && !slice[iii].startsWith('<p dir="RTL"')) {

//                                    SliceCount = (slicePluse.match(/<p row>/g) || []).length;

//                                    counter++;

//                                    var row = '';
//                                    row += "<p row>";
//                                    row += data.Art[i].Article.split('-')[0];
//                                    row += " - ";
//                                    row += ii < 1 ? counter : SliceCount + 1;
//                                    row += " ";
//                                    row += slice[iii];
//                                    row += " </p> ";

//                                    slicePluse += row;
//                                }
//                            }

//                        }
//                        else {

//                            row = '';
//                            row += "<p row>";
//                            row += data.Art[i].Article.split('-')[0];
//                            row += " - ";
//                            row += ii < 1 ? 1 : SliceCount + 1;
//                            row += " ";
//                            row += con;
//                            row += " </p> ";

//                            slicePluse += row;
//                        }

//                    }
//                    else {
//                        con = data.Art[i].Content[ii];
//                        con = con.replace('<p dir="RTL">', ' ').replace("</p>", " ");

//                        count = (con.match(/===/g) || []).length;
//                        SliceCount = (slicePluse.match(/<p row>/g) || []).length;

//                        if (count !== null && count >= 1) {
//                            slice = con.split('===');

//                            for (iii = 0; iii < slice.length; iii++) {

//                                if (!isEmpty(slice[iii]) && !slice[iii].startsWith('<p dir="RTL"')) {
//                                    //if (slice[iii] !== "   " && slice[iii] !== "" && slice[iii] !== " " && slice[iii] !== "&nbsp;" && slice[iii] !== " &nbsp;" && slice[iii] !== "&nbsp; " && slice[iii] !== '<p dir="RTL" data-list="0" data-level="1">' && slice[iii] !== '<p>' && slice[iii] !== '<p dir="RTL" data-list="0" data-level="2">') {
//                                    counter++;

//                                    row = '';
//                                    row += "<p row>";
//                                    row += data.Art[i].Article.split('-')[0];
//                                    row += " - ";
//                                    row += counter;
//                                    row += " ";
//                                    row += slice[iii];
//                                    row += " </p> ";

//                                    slicePluse += row;
//                                }
//                            }

//                        }
//                        else {
//                            slicePluse += "<p>" + con + "</p>";
//                        }
//                    }
//                }

//                content += slicePluse;
//            }

//            content += "</form>";

//            content = content.replace(/ {1,}/g, " ");
//            content = content.replace(/placeholder/g, " ");


//            $("#temperory-wrapper").html(content);


//            //bind properties to Artice Content
//            data.Props.forEach(function (value, index) {

//                var selector = "#temperory-wrapper input[id$='" + "-" + value.ArticlePropertyID + "']";
//                $(selector).replaceWith("<span style='padding-left:10px;'>" + value.Value + "</span>");
//            });

//            //check if paragraph is empty deleted paragraph
//            $('#temperory-wrapper p').each(function () {
//                var $this = $(this);
//                if ($this.html().replace(/\s|&nbsp;/g, '').length === 0)
//                    $this.remove();
//            });

//            $.ajax({
//                url: '/ExportWord/CreateWordAsync',
//                type: 'POST',
//                data: {
//                    Contract: $("#temperory-wrapper").html(),
//                    userContractID: UserContractId
//                },
//                dataType: 'json',
//                success: function (res) {

//                }
//            }).always(function (fileName) {

//                if ($("#usage-count-contract").length) {
//                    $("#usage-count-contract").text("تعداد قراردادهای  باقی مانده در این اشتراک  : " + fileName.responseText.split("*")[2]);
//                }

//                $("#temperory-wrapper").html(" ");

//                $("#contract-word").replaceWith('<a class="btn btn-success" onclick = "download_Word(event,' + "'" + fileName.responseText.split("*") + "'" + ')"> Word دریافت فایل  <i style="vertical-align: middle;" class="fa fa-download"></i></a>');
//                $("#Payment #Wrapper-Payment-Info .justify-content-center").remove();
//                $("#Payment #Wrapper-Payment-Info").append(
//                    `                   
//                     <div class="row justify-content-center">   
//                        <hr />
//                        <div class="col-md-12 col-sm-12 order-1 order-md-12" id="DesForContract">
//                                <div id='detail-cont'>                                
//                                        <ul>
//                                            <li>  کاربر گرامی در صورت نیاز به ویرایش فایل PDF به بخش  <span> قراردادها </span>  در <span> پروفایل کاربری </span> رجوع کنید . </li>
//                                            <li>ویرایش فایل PDF تا <span> 2 </span> بار برای شما امکان پذیر است .</li>
//                                            <li> نسخه های  قرارداد در قسمت  <span>' قراردادها '</span>  برای شما موجود خواهد بود   .</li>
//                                            <li>  در صورت داشتن هر گونه سوال ، از طریق <span>  چت آنلاین  </span> و یا <span>  شماره تماس  </span>  با ما در ارتباط باشید . </li>
//                                        </ul>
//                                </div>
//                        </div>
//                    </div>
//                    `
//                );


//                $(".Image-Holder").hide();

//                //sessionStorage.clear();

//            });
//        }
//    });
//}
