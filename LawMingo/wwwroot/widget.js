﻿
(function() {


    this.LawmingoWidget = function () {


        this.closeButton = null;
        this.modal = null;
        this.overlay = null;

  
        var defaults = {
            token: 'fade-and-drop',
            id: "",
            width: 600,
            height: 280,
        };

   
        if (arguments[0] && typeof arguments[0] === "object") {
            this.options = extendDefaults(defaults, arguments[0]);
        }

    };

    LawmingoWidget.prototype.init = function () {
        let user = {
            token: this.options.token,
            mobile: this.options.mobile,
            name: this.options.mobile,
            family: this.options.mobile
        };
        console.log(user);

        //var url = 'https://lawmingo.com/widget/gettoken';
        var url = 'https://localhost:5001/widget/gettoken';

     
        
        var xhttp = new XMLHttpRequest();
        var self = this;
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                console.log(self.options.id);
                var response = JSON.parse(this.responseText);
                console.log(response);

                var local = true;
                var host = local ? "https://localhost:5001/" : "https://lawmingo.com/";
                document.getElementById(self.options.id).innerHTML = `<iframe src="${host}Widget/${response.token}/${self.options.token}" style="width:${self.options.width};height:${self.options.height}" id="lawmingowidget" frameborder="0"></iframe>`;

            }
        };
        xhttp.open("POST", url , true);
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.send(JSON.stringify(user));
    };

  
  function extendDefaults(source, properties) {
    var property;
    for (property in properties) {
      if (properties.hasOwnProperty(property)) {
        source[property] = properties[property];
      }
    }
    return source;
  }

}());

