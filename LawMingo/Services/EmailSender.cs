﻿using LawMingo.Controllers;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using System.Net.Mail;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;
using MailKit.Security;
using System.IO;

namespace LawMingo.Services
{
    public class EmailSender : IEmailSender
    {

        public EmailSender(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public AuthMessageSenderOptions Options { get; }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Execute(Options.Password, subject, message, email);
        }
    
        public async Task Execute(string Password, string subject, string message, string email)
        {

            string From = subject == "بازیابی رمز عبور" ? "security-noreply@lawmingo.com" : "info-noreplay@lawmingo.com";
            string Pass = subject == "بازیابی رمز عبور" ? "25twsB6&" : "u3C4m_5p";

            try
            {

                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress("لامینگو", From));
                mimeMessage.To.Add(new MailboxAddress(email, email));
                mimeMessage.Subject = subject;
                mimeMessage.Body = new TextPart("Html")
                {
                    Text = message
                };

                //using (var client = new MailKit.Net.Smtp.SmtpClient())
                //{
                //    client.ServerCertificateValidationCallback = (s, c, ch, e) => true;

                //    await client.ConnectAsync(
                //        "mail.lawmingo.com",
                //         587,
                //         false
                //     )
                //    .ConfigureAwait(false);
                //    client.AuthenticationMechanisms.Remove("XOAUTH2");

                //    await client.AuthenticateAsync(From, Pass)
                //   .ConfigureAwait(false);

                //    await client.SendAsync(mimeMessage).ConfigureAwait(false);
                //    await client.DisconnectAsync(true).ConfigureAwait(false);
                //}


                MailKit.Net.Smtp.SmtpClient client = new MailKit.Net.Smtp.SmtpClient();
                client.Connect("mail.lawmingo.com", 25, false);
                client.Authenticate(From, Pass);

                client.Send(mimeMessage);
                client.Disconnect(true);
                client.Dispose();

            }
            catch (System.Exception)
            {
                throw;
            }
            await Task.FromResult(0);
        }
    }    
}






