﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace LawMingo
{
    public class Program
    {
        public static void Main(string[] args)
        {                     
            CreateWebHostBuilder(args).Build().Run();        
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                //.UseSentry("https://9b3f128fb05543c091a5553eb46dd979@sentry.io/2354974")
                .UseStartup<Startup>();
    }
}
