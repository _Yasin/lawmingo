﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class sdygfhjda : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "PaymentLogs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Refrence",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "PaymentLogs");

            migrationBuilder.DropColumn(
                name: "Refrence",
                table: "AspNetUsers");
        }
    }
}
