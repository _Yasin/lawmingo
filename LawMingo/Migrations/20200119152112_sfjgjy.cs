﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class sfjgjy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Thumbnail",
                table: "Counselors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ConsultationRequests",
                nullable: true,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Thumbnail",
                table: "Counselors");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "ConsultationRequests");
        }
    }
}
