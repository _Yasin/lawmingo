﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class fjdyghjfg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Categories_CategoryID",
                table: "Contract");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Counselors_CounselorCategories_CounselorCategoryID",
            //    table: "Counselors");

            //migrationBuilder.DropIndex(
            //    name: "IX_Counselors_CounselorCategoryID",
            //    table: "Counselors");

            migrationBuilder.DropIndex(
                name: "IX_Contract_CategoryID",
                table: "Contract");

            //migrationBuilder.DropColumn(
            //    name: "CounselorCategoryID",
            //    table: "Counselors");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AddColumn<int>(
            //    name: "CounselorCategoryID",
            //    table: "Counselors",
            //    nullable: false,
            //    defaultValue: 0);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Counselors_CounselorCategoryID",
            //    table: "Counselors",
            //    column: "CounselorCategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_CategoryID",
                table: "Contract",
                column: "CategoryID");

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Categories_CategoryID",
                table: "Contract",
                column: "CategoryID",
                principalTable: "Categories",
                principalColumn: "CategoryID",
                onDelete: ReferentialAction.Restrict);

        //    migrationBuilder.AddForeignKey(
        //        name: "FK_Counselors_CounselorCategories_CounselorCategoryID",
        //        table: "Counselors",
        //        column: "CounselorCategoryID",
        //        principalTable: "CounselorCategories",
        //        principalColumn: "CounselorCategoryID",
        //        onDelete: ReferentialAction.Restrict);
        }
    }
}
