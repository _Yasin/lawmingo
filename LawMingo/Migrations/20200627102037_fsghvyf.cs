﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class fsghvyf : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PhoneNumberConfirmedSender",
                table: "signatureShares",
                newName: "PhoneNumberConfirmed");

            migrationBuilder.RenameColumn(
                name: "ConfirmMobile",
                table: "signatureShares",
                newName: "NationalCodeConfirmed");

            migrationBuilder.RenameColumn(
                name: "Confirm",
                table: "signatureShares",
                newName: "ConfirmSendContractLink");

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "signatureShares",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EditTime",
                table: "signatureShares",
                nullable: false,
                defaultValue: DateTime.Now);

            migrationBuilder.AddColumn<DateTime>(
                name: "RegisterTime",
                table: "signatureShares",
                nullable: false,
                defaultValue: DateTime.Now);

            migrationBuilder.AddColumn<string>(
                name: "ReseverIPAddress",
                table: "signatureShares",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SenderIPAddress",
                table: "signatureShares",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "signatureShares");

            migrationBuilder.DropColumn(
                name: "EditTime",
                table: "signatureShares");

            migrationBuilder.DropColumn(
                name: "RegisterTime",
                table: "signatureShares");

            migrationBuilder.DropColumn(
                name: "ReseverIPAddress",
                table: "signatureShares");

            migrationBuilder.DropColumn(
                name: "SenderIPAddress",
                table: "signatureShares");

            migrationBuilder.RenameColumn(
                name: "PhoneNumberConfirmed",
                table: "signatureShares",
                newName: "PhoneNumberConfirmedSender");

            migrationBuilder.RenameColumn(
                name: "NationalCodeConfirmed",
                table: "signatureShares",
                newName: "ConfirmMobile");

            migrationBuilder.RenameColumn(
                name: "ConfirmSendContractLink",
                table: "signatureShares",
                newName: "Confirm");
        }
    }
}
