﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class sjgydfjddkhue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CatType",
                table: "Categories");

            migrationBuilder.CreateTable(
                name: "CategoryAttachment",
                columns: table => new
                {
                    CategoryAttachID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CatType = table.Column<int>(nullable: false),
                    CategoryID = table.Column<int>(nullable: false),
                    ContractID = table.Column<int>(nullable: true),
                    CounselorID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryAttachment", x => x.CategoryAttachID);
                    table.ForeignKey(
                        name: "FK_CategoryAttachment_Categories_CategoryID",
                        column: x => x.CategoryID,
                        principalTable: "Categories",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CategoryAttachment_Contract_ContractID",
                        column: x => x.ContractID,
                        principalTable: "Contract",
                        principalColumn: "ContractID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CategoryAttachment_Counselors_CounselorID",
                        column: x => x.CounselorID,
                        principalTable: "Counselors",
                        principalColumn: "CounselorID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CategoryAttachment_CategoryID",
                table: "CategoryAttachment",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryAttachment_ContractID",
                table: "CategoryAttachment",
                column: "ContractID");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryAttachment_CounselorID",
                table: "CategoryAttachment",
                column: "CounselorID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CategoryAttachment");

            migrationBuilder.AddColumn<int>(
                name: "CatType",
                table: "Categories",
                nullable: false,
                defaultValue: 0);
        }
    }
}
