﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class hfsg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "CounselorCategories",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "EnglishName",
                table: "CounselorCategories",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CounselorCategories_ParentID",
                table: "CounselorCategories",
                column: "ParentID");

            migrationBuilder.AddForeignKey(
                name: "FK_CounselorCategories_CounselorCategories_ParentID",
                table: "CounselorCategories",
                column: "ParentID",
                principalTable: "CounselorCategories",
                principalColumn: "CounselorCategoryID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CounselorCategories_CounselorCategories_ParentID",
                table: "CounselorCategories");

            migrationBuilder.DropIndex(
                name: "IX_CounselorCategories_ParentID",
                table: "CounselorCategories");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "CounselorCategories",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "EnglishName",
                table: "CounselorCategories",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
