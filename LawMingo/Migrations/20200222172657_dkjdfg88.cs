﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class dkjdfg88 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentID",
                table: "CounselorCategories",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ParentID",
                table: "CounselorCategories");
        }
    }
}
