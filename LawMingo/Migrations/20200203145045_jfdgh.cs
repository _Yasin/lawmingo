﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class jfdgh : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Contract_EnglishName",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_Name",
                table: "Contract");

            migrationBuilder.CreateTable(
                name: "ArtsDetails",
                columns: table => new
                {
                    ArtsDetailID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnswerID = table.Column<int>(nullable: false),
                    Content = table.Column<string>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArtsDetails", x => x.ArtsDetailID);
                    table.ForeignKey(
                        name: "FK_ArtsDetails_Answers_AnswerID",
                        column: x => x.AnswerID,
                        principalTable: "Answers",
                        principalColumn: "AnswerID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArtsDetails_AnswerID",
                table: "ArtsDetails",
                column: "AnswerID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArtsDetails");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_EnglishName",
                table: "Contract",
                column: "EnglishName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contract_Name",
                table: "Contract",
                column: "Name",
                unique: true);
        }
    }
}
