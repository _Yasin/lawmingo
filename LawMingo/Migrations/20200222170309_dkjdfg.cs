﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class dkjdfg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "Comments",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Author",
                table: "Comments",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "AnswerCount",
                table: "Comments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CategoryID",
                table: "Comments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Comments",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CommentCount",
                table: "Comments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CounselorID",
                table: "Comments",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsPublic",
                table: "Comments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Subject",
                table: "Comments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tags",
                table: "Comments",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Comments_CategoryID",
                table: "Comments",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_CounselorID",
                table: "Comments",
                column: "CounselorID");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_CounselorCategories_CategoryID",
                table: "Comments",
                column: "CategoryID",
                principalTable: "CounselorCategories",
                principalColumn: "CounselorCategoryID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Counselors_CounselorID",
                table: "Comments",
                column: "CounselorID",
                principalTable: "Counselors",
                principalColumn: "CounselorID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_CounselorCategories_CategoryID",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Counselors_CounselorID",
                table: "Comments");

            migrationBuilder.DropIndex(
                name: "IX_Comments_CategoryID",
                table: "Comments");

            migrationBuilder.DropIndex(
                name: "IX_Comments_CounselorID",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "AnswerCount",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "CategoryID",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "CommentCount",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "CounselorID",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "IsPublic",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "Subject",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "Tags",
                table: "Comments");

            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "Comments",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Author",
                table: "Comments",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
