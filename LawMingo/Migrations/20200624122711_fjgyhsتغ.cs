﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class fjgyhsتغ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ConfirmMobile",
                table: "signatureShares",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConfirmMobile",
                table: "signatureShares");
        }
    }
}
