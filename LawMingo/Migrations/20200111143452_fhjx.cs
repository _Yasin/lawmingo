﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LawMingo.Migrations
{
    public partial class fhjx : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "File",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "Contract",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "File",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Contract");
        }
    }
}
