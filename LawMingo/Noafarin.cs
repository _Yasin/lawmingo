﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace LawMingo
{
    public static class Noafarin
    {
        private static string client_id = "rhZQ1Yz5k9jBF6bVm9qQ5DYLS8gbXc0f";
        private static string client_secret = "ASW5HEh9BmNQpaSWUxM72Pc9zf3jrDYg";

        private static string BaseURL = " https://irannoafarin.ir/api/v1/central-authentication/";
        private static string GenerateURL = BaseURL + "generate";
        private static string VerifyURL = BaseURL + "verify";

        public static HttpWebRequest AuthorzeRequest(HttpWebRequest request , object obj)
        {

            var jObj = (JObject)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(obj));
            var query = String.Join("&",
                jObj.Children().Cast<JProperty>()
                .OrderBy(o => o.Name, StringComparer.Ordinal)
                .Select(jp => jp.Name + "=" + HttpUtility.UrlEncode(jp.Value.ToString())));

            var HMAC = Token.GetHash(query, client_secret);

            request.Headers.Add("authorization", $"HMAC {client_id}:{HMAC}");
            return request;

        }
        public static string Generate(string noafarin_code)
        {
            string responseContent = null;
            var request = WebRequest.Create(GenerateURL) as HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "Post";
            request.ContentType = "application/json; charset=utf-8";
            var obj = new
            {
                innovative_code = noafarin_code
            };
            byte[] byteArray = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj));

            request = AuthorzeRequest(request, obj);

            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                        var resobj = (JObject)JsonConvert.DeserializeObject(responseContent);
                        var objj = resobj.Children().Cast<JProperty>()
                            .Where(o => o.Name == "data").FirstOrDefault()
                            .First.Children().Cast<JProperty>().FirstOrDefault().First;
                        return objj.ToString();
                    }
                }
            }
            catch (WebException ex)
            {
                return ex.Message;
            }

        }
        public static JToken Verify(string token , string code , string noafarin_code)
        {
            string responseContent = null;
            var request = WebRequest.Create(VerifyURL) as HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "Post";
            request.ContentType = "application/json";

            var obj = new
            {
                innovative_code = noafarin_code,
                token,
                code,
                Name = true,
                CellPhoneNumber = true,
                City = true,
                FamilyName = true
            };
            byte[] byteArray = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj));

            request = AuthorzeRequest(request, obj);

            using (var writer = request.GetRequestStream())
            {
                writer.Write(byteArray, 0, byteArray.Length);
            }
            using (var response = request.GetResponse() as HttpWebResponse)
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    responseContent = reader.ReadToEnd();
                    var resobj = (JObject)JsonConvert.DeserializeObject(responseContent);
                    var objj = resobj.Children().Cast<JProperty>()
                        .Where(o => o.Name == "data").FirstOrDefault().First;
                    return objj;
                }
            }
        }

    }
}