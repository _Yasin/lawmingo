﻿using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Helpers
{
    public static class RecentlyContract
    {
    
        public static bool check(

           this UserManager<ApplicationUser> _userManager,
           ApplicationDbContext _db,
           HttpContext currentUser,
           int ContractID

            )
        {

            string userId = _userManager.GetUserId(currentUser.User);
            var recently_UserContracts = _db.UserContracts.Where(

             w => w.ApplicationUserId == userId &&
             w.ContractID == ContractID &&
             w.ParentID == null &&
             w.status != UserContract.Status.Complete).ToList();

            return recently_UserContracts.Count > 0;

        }

        public static int check(

           this UserManager<ApplicationUser> _userManager,
           ApplicationDbContext _db,
           HttpContext currentUser,
           int ContractID,
           bool returnData

            )
        {

            string userId = _userManager.GetUserId(currentUser.User);
            var recently_UserContracts = _db.UserContracts.Where(

             w => w.ApplicationUserId == userId &&
             w.ContractID == ContractID &&
             w.ParentID == null &&
             w.status != UserContract.Status.Complete).ToList();

            if (recently_UserContracts.Count > 0) return recently_UserContracts[0].UserContractID;
            else return 0;
        }
     
    }
}      

