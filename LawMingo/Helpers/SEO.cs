﻿//using Schema.NET;
using LawMingo.Models.DomainModel;
using Schema.NET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LawMingo.Helpers
{
    public class SEO
    {
        public static string BreadcrumbList( Dictionary<string,string> data)
        {
            var items = new List<string>() { };
            var url = "https://lawmingo.com/blog/";
            data.Add("/", "بلاگ");
            foreach (var item in data)
            {
                var _item = 
                     "{"+
                     "    \"@type\":\"ListItem\","+
                     "    \"position\":\""+ (items.Count() + 1).ToString() + "\","+
                     "   \"item\":"+
                     "   {"+
                     "       \"@id\":\""+ url + item.Key + "\","+
                     "       \"name\":\""+ item.Value + "\""+
                     "   }"+
                    "}";
                items.Add(_item);
            }

            var Items = string.Join(",", items);

            string res =
                    "{" +
                    "    \"@context\":\"http://schema.org\"," +
                    "    \"@type\":\"BreadcrumbList\"," +
                    "    \"itemListElement\":" +
                    "    [" +
                             Items +
                    "    ]" +
                    "}";
            return res;
        }


        public static string BreadcrumbList2(Dictionary<string, string> data)
        {
            var items = new List<string>() { };
            var url = "https://lawmingo.com/";
            data.Add("/", "لامینگو");
            foreach (var item in data)
            {
                var _item =
                     "{" +
                     "    \"@type\":\"ListItem\"," +
                     "    \"position\":\"" + (items.Count() + 1).ToString() + "\"," +
                     "   \"item\":" +
                     "   {" +
                     "       \"@id\":\"" + url + item.Key + "\"," +
                     "       \"name\":\"" + item.Value + "\"" +
                     "   }" +
                    "}";
                items.Add(_item);
            }

            var Items = string.Join(",", items);

            string res =
                    "{" +
                    "    \"@context\":\"http://schema.org\"," +
                    "    \"@type\":\"BreadcrumbList\"," +
                    "    \"itemListElement\":" +
                    "    [" +
                             Items +
                    "    ]" +
                    "}";
            return res;
        }


        public static int WordCount(string text)
        {
            char[] delimiters = new char[] { ' ', '\r', '\n' };
            if (text == null)
            {
                return 0;
            }
            return text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries).Length;
        }
        //public static string _FAQ(List<Models.DomainModel.Comment> data)
        //{
        //    var _questions =new List<Question>() { };

        //    foreach (var item in data)
        //    {
        //        if (item.Answers != null)
        //        {
        //            var newq = new Question()
        //            {
        //                Name = item.Content,
        //                AcceptedAnswer = new Answer()
        //                {
        //                    Text = item.Answers.FirstOrDefault().Content
        //                }
        //            };
        //            _questions.Add(newq);
        //        }

        //    }

        //    // Multiple PostalAddress addresses
        //    var organization = new Organization()
        //    {
        //        Address = new List<PostalAddress>()
        //        {
        //            new PostalAddress()
        //            {
        //                StreetAddress = "123 Old Kent Road",
        //                PostalCode = "E10 6RL"
        //            },
        //            new PostalAddress()
        //            {
        //                StreetAddress = "456 Finsbury Park Road",
        //                PostalCode = "SW1 2JS"
        //            }
        //        }
        //    };
        //    var website = new FAQPage()
        //    {
        //        MainEntity= new OneOrMany<IThing>((IThing)_questions)
        //    };
        //    return website.ToString();
        //}
        public static string FAQ(List<Models.DomainModel.Comment> data)
        {
            if(data.Where(d => d.Answers != null).Count() == 0) return "";

            var items = new List<string>() { };
            foreach (var item in data)
            {
                if (item.Answers != null)
                {
                    var _item =
                     "{" +
                     "    \"@type\":\"Question\"," +
                     $"    \"name\":\"{item.Content}\"," +
                     "   \"acceptedAnswer\":" +
                     "   {" +
                     "       \"@type\":\"Answer\"," +
                     $"       \"text\":\"{item.Answers.FirstOrDefault().Content}\"" +
                     "   }" +
                    "}";
                    items.Add(_item);
                }

            }

            var Items = string.Join(",", items);

            string res =
                    "{" +
                    "    \"@context\":\"http://schema.org\"," +
                    "    \"@type\":\"FAQPage\"," +
                    "    \"mainEntity\":" +
                    "    [" +
                             Items +
                    "    ]" +
                    "}";
            return res;
        }
        public static string QAPage(Models.DomainModel.Comment item)
        {
            if (item.Answers.Where(a => a.IsActive).Count() == 0) return "";

            var items = new List<string>() { };
            if (item.Answers != null)
            {
                var _item =
                 "{" +
                 "    \"@type\":\"Question\"," +
                 $"    \"name\":\"{item.Subject}\"," +
                 $"    \"text\":\"{item.Content}\"," +
                 $"    \"dateCreated\":\"{item.Time}\"," +
                 $"    \"answerCount\":\"{item.Answers.Where(a => a.IsActive).Count()}\"," +
                    "   \"author\":" +
                    "   {" +
                    "       \"@type\":\"Person\"," +
                    $"       \"name\":\"کاربر لامینگو\"" +
                    "   }," +
                 "   \"acceptedAnswer\":" +
                 "   {" +
                     "   \"@type\":\"Answer\"," +
                    $"  \"text\":\"{item.Answers.FirstOrDefault().Content}\"," +
                    $"    \"dateCreated\":\"{item.Time}\"," +
                    $"    \"url\":\"https://lawmingo.com/question/{item.Code}/{item.Subject.URLFriendly()}#acceptedAnswer \"," +
                    $"    \"upvoteCount\":\"0\"," +

                     (item.Answers.FirstOrDefault().Counselor != null ? "   \"author\":" +
                     "   {" +
                     "       \"@type\":\"Person\"," +
                     $"       \"name\":\"{item.Answers.FirstOrDefault().Counselor.Name}\"" +
                     "   }" : "") +

                 "   }" +
                "}";
                items.Add(_item);
            }

            var Items = string.Join(",", items);

            string res =
                    "{" +
                    "    \"@context\":\"http://schema.org\"," +
                    "    \"@type\":\"QAPage\"," +
                    "    \"mainEntity\":" +
                    "    [" +
                             Items +
                    "    ]" +
                    "}";
            return res;
        }
        public static string Product(Contract contract)
        {
            var product = new Schema.NET.Product() { 
                Name = contract.Name,
                Image = new string[] { contract.Image },
                Description = contract.Description,
                Sku = $"LMNG{contract.ContractID}",
                Brand = new Brand()
                {
                    Name = "لامینگو",
                    Logo = new string[] { "/logo.png" },
                    Slogan = "پلتفرم هوشمند خدمات حقوقی"
                },
                AggregateRating = new AggregateRating()
                {
                    RatingValue = 4,
                    RatingCount = 10,
                },
                Offers = new Offer()
                {
                    Url = new Uri("https://lawmingo.com/"),
                    PriceCurrency = "ریال",
                    Price = contract.Price,
                    PriceValidUntil = DateTime.Now.AddMonths(1),
                    ItemCondition = OfferItemCondition.NewCondition,
                    Availability= ItemAvailability.InStock,
                }
            };
            return product.ToString();
        }
    }
}
