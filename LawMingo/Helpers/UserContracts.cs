﻿using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Helpers
{
    public static class UserContracts
    {
        public static IEnumerable<UserContract> GetParents(

            this UserManager<ApplicationUser> _userManager,
            ApplicationDbContext _db,
            HttpContext currentUser,
            int id
            
            )
        {
            List<UserContract> Result = new List<UserContract>();
            string userId = _userManager.GetUserId(currentUser.User);

            var AllUserContracts = _db.UserContracts.Where(e => e.ApplicationUserId == userId).ToList();
            var userContract = AllUserContracts.Where(q => q.UserContractID == id).FirstOrDefault();
         
            Result.Add(userContract);

            var counter = 0;
            while (id != 0)
            {
                var newrow = AllUserContracts.Where(c => c.UserContractID == userContract.ParentID).FirstOrDefault();
                if (newrow != null)
                {
                    Result.Add(newrow);
                    counter++;
                    userContract = newrow;
                }
                else
                {
                    id = 0;
                }
            }
            Result.Reverse();

            return Result;
        }
    }
}
