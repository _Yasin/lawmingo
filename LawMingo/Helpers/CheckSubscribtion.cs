﻿using LawMingo.Data;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Helpers
{
    public static class Subscription
    {
        public static SubscribViewModel check(

            this UserManager<ApplicationUser> _userManager,
            ApplicationDbContext _db,
            HttpContext currentUser, 
            int? SubscriptionId
        )

        {

            var entry = new SubscribViewModel();
            var User_Subscrib = new UserSubscription();

            string userId = _userManager.GetUserId(currentUser.User);


            if (!string.IsNullOrEmpty(userId))
            {
                var userSubscriptions = _db.UserSubscriptions.Include("Subscription").Include("PaymentLogs")
                .Where(q => q.ApplicationUserId == userId);



                if (userSubscriptions != null &&  userSubscriptions.ToList().Count > 0)
                {
                     User_Subscrib = userSubscriptions.Where(

                         t => t.PaymentLogs != null && t.PaymentLogs.Any(u => u.IsSuccessful) &&
                         t.Time.AddMonths(t.Subscription.Month) >= DateTime.Now &&
                         (t.UsageCount == null || t.UsageCount < t.Subscription.Count)

                    ).FirstOrDefault();

                    if (User_Subscrib != null)
                    {
                    
                        entry = new SubscribViewModel
                        {
                            Expire = PersianDates.ToPersianString(User_Subscrib.Time.AddMonths(User_Subscrib.Subscription.Month)),
                            Time = PersianDates.ToPersianString(User_Subscrib.Time),
                            Message = "Yes",
                            Title = User_Subscrib.Subscription.Title,
                            usegeCount = User_Subscrib.UsageCount == null ? 0 : User_Subscrib.UsageCount,
                            SubScriptionId = SubscriptionId != null ? SubscriptionId : null,
                            UserSubscriptionId = User_Subscrib.UserSubscriptionID,
                            RemainingCount = User_Subscrib.Subscription.Count - (User_Subscrib.UsageCount == null ? 0 : User_Subscrib.UsageCount)
                        };

                        return entry;
                    }

                    entry.Message = "NO";
                    entry.SubScriptionId = SubscriptionId != null ? SubscriptionId : null;
                    return entry;
                }
                else
                {
                    entry.Message = "NO";
                    entry.SubScriptionId = SubscriptionId != null ? SubscriptionId : null;
                    return entry;
                }
            }
            else
            {
                entry.Message = "Unauthorized";
                entry.SubScriptionId = SubscriptionId != null ? SubscriptionId : null;
                return entry;
            }
            
        }

    }
   
}
