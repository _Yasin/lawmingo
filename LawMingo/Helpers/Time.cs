﻿using MD.PersianDateTime.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace LawMingo.Helpers
{
    public static class Times
    {

        public static string ElapsedTime(this DateTime dateTime)
        {
            var res = "";
            var diff = DateTime.Now - dateTime;
            var hours = diff.TotalHours;

            if (hours < 1)
            {
                res = Math.Round(diff.TotalMinutes) + " دقیقه پیش";
            }
            else if (hours < 24)
            {
                res = Math.Round(hours)  + " ساعت پیش";
            }
            else if (hours < 48)
            {
                res = "دیروز";
            }
            else if (hours < 96)
            {
                res = "2 روز پیش";
            }
            else
            {
                res = dateTime.ToPersianDateTime().ToString("dd MMMM yyyy ساعت HH:mm");
            }
            return res;
        }

    }
}