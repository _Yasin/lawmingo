﻿using LawMingo.Data;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LawMingo.Helpers
{
    public static class Categories
    {
        public static List<CounselorCategory> GetParents(
            ApplicationDbContext _db,
            int? id,
            string name = null
            )
        {
            var cats = _db.CounselorCategories.ToList();
            var list = new List<CounselorCategory> { };
            if (name != null)
            {
                id = cats.Where(c => c.Name == name.Replace('_', ' ')).FirstOrDefault().CounselorCategoryID;
            }
            while (id != null)
            {
                var item = cats.Where(c => c.CounselorCategoryID == id).FirstOrDefault();
                id = item.ParentID;
                list.Add(item);
            }
            list.Reverse();
            return list;
        }
        public static List<int> GetChildren(
            ApplicationDbContext _db,
            int? id , 
            string name = null , 
            bool self = false
            )
        {
            var cats = _db.CounselorCategories.ToList();
            if (name != null)
            {
                id = cats.Where(c => c.Name == name.Replace('_', ' ')).FirstOrDefault().CounselorCategoryID;
            }
            var list = new List<int> { };
            
            if (id == null && name == null)
            {
                return list;
            }
            
            foreach (var item in cats.Where(c => c.ParentID == id))
            {
                var _cats = cats.Where(c => c.ParentID == item.CounselorCategoryID);
                foreach (var _item in _cats)
                {
                    list.Add(_item.CounselorCategoryID);
                }
                if (_cats.Count() == 0)
                {
                    list.Add(item.CounselorCategoryID);
                }
            }

            if (self) list.Add(id ?? 0);

            return list;
        }
    }
}
