﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using static LawMingo.Models.ViewModel.NationalCodeVerifyViewModel;

namespace LawMingo.Helpers
{
    public static class NationalCode
    {
     
        private readonly static string     ClientId = "Lawmingo:63ca2133060212d97a9f";
        private readonly static string     TokenURL = "https://apibeta.finnotech.ir/dev/v2/oauth2/token";
        private readonly static string     BaseURL  = "https://apibeta.finnotech.ir/mpg/v2/clients/Lawmingo/shahkar/verify";                    
        public static bool  Verify(string Mobile , string nationalCode)
        {
            try
            {                             
                HttpWebRequest httpWebRequest = WebRequest.CreateHttp(TokenURL);
                string Token = Base64Encode(ClientId);              
                httpWebRequest.Headers.Add("Authorization", "Basic " + Token);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{ \"grant_type\" : \"client_credentials\", \"nid\" : \"0011206780\" , \"scopes\" : \"card:shahkar:get\" }";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {                   
                    var responseText = streamReader.ReadToEnd();
                    var responseObject = JsonConvert.DeserializeObject<JsonResponse>(responseText);
                    return CallverifyNationalCodeApi(responseObject , Mobile, nationalCode);
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return true;
        }

        private static bool CallverifyNationalCodeApi(JsonResponse Response , string Mobile , string nationalCode)
        {
            try
            {
                Random generator = new Random();
                string Random_trackId = generator.Next(0, 999999).ToString("D9");
                HttpWebRequest httpWebRequest =
                    WebRequest.CreateHttp($"{BaseURL}?trackId={Random_trackId}&mobile={Mobile}&nationalCode={nationalCode}");
                httpWebRequest.PreAuthenticate = true;
                httpWebRequest.Headers.Add("Authorization", "Bearer " + Response.result.value);              
                httpWebRequest.Method = "GET";              
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    var responseObject = JsonConvert.DeserializeObject<JsonRes>(responseText);
                    //log in verifyNationalCodeLog
                    return responseObject.result.isValid;
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }     
    }
}
