﻿using MD.PersianDateTime.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace LawMingo.Helpers
{
    public static class PersianDates
    {
        private static readonly string[] pn = { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };
        private static readonly string[] en = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        public static string ToPersianNumber(this string strNum)
        {
            if (!string.IsNullOrEmpty(strNum))
            {
                string chash = strNum;
                for (int i = 0; i < 10; i++)
                    chash = chash.Replace(en[i], pn[i]);
                return chash;
            }
            return "";
        }

        public static string ToEnglishNumber(this string strNum)
        {
            if (!string.IsNullOrEmpty(strNum))
            {
                string chash = strNum;
                for (int i = 0; i < 10; i++)
                    chash = chash.Replace(pn[i], en[i]);
                return chash;
            }
            return "";
        }


        public static string ToPersianNumber(this int intNum)
        {
            string chash = intNum.ToString();
            for (int i = 0; i < 10; i++)
                chash = chash.Replace(en[i], pn[i]);
            return chash;
        }
        public static DateTime ToPersian(this DateTime dateTime)
        {
            PersianCalendar PC = new PersianCalendar();
            int intYear = PC.GetYear(dateTime);
            int intMonth = PC.GetMonth(dateTime);
            int intDay = PC.GetDayOfMonth(dateTime);
            int intHour = PC.GetHour(dateTime);
            int intMinute = PC.GetMinute(dateTime);

            return new DateTime(intYear, intMonth, intDay, intHour, intMinute, 0);
        }

        public static string ToPersianString(this DateTime dateTime)
        {
            PersianCalendar p = new PersianCalendar();
            var d = string.Format("{0:0000}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}",
                   p.GetYear(dateTime),
                   p.GetMonth(dateTime),
                   p.GetDayOfMonth(dateTime),
                   p.GetHour(dateTime),
                   p.GetMinute(dateTime),
                   p.GetSecond(dateTime)
                   );
            return d;

           
        }

        public static DateTime ToMiladi(this DateTime dateTime)
        {
            PersianCalendar PC = new PersianCalendar();
            return PC.ToDateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, 0, 0);
        }

        public static PersianDateTime ToPersianDateTime(this DateTime DT)
        {
            return new PersianDateTime(DT);
        }

        public static PersianDateTime ToPersianDateTime(this string dateTime)
        {
            return new PersianDateTime(Convert.ToDateTime(dateTime));
        }

        //Convert DateTime to Total Second from 1970/1/1 00:00:00 to now
        public static int calculateSeconds()
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0);//from 1970/1/1 00:00:00 to now
            DateTime dtNow = DateTime.Now;
            TimeSpan result = dtNow.Subtract(dt);
            int seconds = Convert.ToInt32(result.TotalSeconds);
            return seconds;
        }

        //Convert DateTime to Total Second from 1970/1/1 00:00:00 
        public static int calculateSeconds(DateTime DT)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0);//from 1970/1/1 00:00:00 to now         
            TimeSpan result = DT.Subtract(dt);
            int seconds = Convert.ToInt32(result.TotalSeconds);
            return seconds;
        }

        //public static string ToStringDateTime12(this PersianDateTime DT)
        //{
        //    return DT.ToString("yyyy/MM/dd hh:mm tt");
        //}

        //TimeSpan MySpan = new TimeSpan(60,0, 0, 0);
        //var s =  MySpan.TotalSeconds;
        public static string GetDayOfWeekName(this DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Saturday: return "شنبه";
                case DayOfWeek.Sunday: return "يکشنبه";
                case DayOfWeek.Monday: return "دوشنبه";
                case DayOfWeek.Tuesday: return "سه‏ شنبه";
                case DayOfWeek.Wednesday: return "چهارشنبه";
                case DayOfWeek.Thursday: return "پنجشنبه";
                case DayOfWeek.Friday: return "جمعه";
                default: return "";
            }
        }
    }
}