﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace LawMingo.TagHelpers
{
    public class PanelMenuItemTagHelper : TagHelper
    {
        public string CurrentURL { get; set; }
        public string Action { get; set; }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var classNames = "";
            output.TagName = "li";                                 // Replaces <email> with <a> tag
            //var content = await output.GetChildContentAsync();

            //TagHelperAttribute href;
            //output.Attributes.TryGetAttribute("href" , out href);
            //output.Attributes.SetAttribute(href);

            if (CurrentURL.ToLower().Contains(Action.ToLower()))
            {
                classNames = " active";
            }
            output.Attributes.SetAttribute("class", classNames);
        }
    }
}
