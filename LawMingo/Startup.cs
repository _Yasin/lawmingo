﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LawMingo.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity.UI.Services;
using LawMingo.Services;
using LawMingo.Models.ViewModel;
using LawMingo.Validation;
using Newtonsoft.Json.Serialization;
using DinkToPdf;
using DinkToPdf.Contracts;
using System.IO;
using LawMingo.Utility;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Net.Http.Headers;

using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Globalization;

namespace LawMingo
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            StaticConfig = configuration;

        }

        public IConfiguration Configuration { get; }
        public static IConfiguration StaticConfig { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowMyOrigin",
                _builder => _builder.AllowAnyOrigin().WithHeaders(HeaderNames.ContentType, "application/json;charset=UTF-8"));
            });

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(5000);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.None;
            });
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.HttpOnly = true;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>(config =>
            {
                config.SignIn.RequireConfirmedEmail = false;
                config.User.RequireUniqueEmail = false;
            })
                .AddErrorDescriber<CustomizeIdentityErrorDescriber>()
                .AddDefaultUI(UIFramework.Bootstrap4)
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
                services.AddAuthentication()



            .AddCookie()
            //.AddGoogle(googleOptions =>
            //{
            //    googleOptions.ClientId = Configuration.GetSection("SecretData")["AuthenticationGoogleClientId"];
            //    googleOptions.ClientSecret = Configuration.GetSection("SecretData")["AuthenticationGoogleClientSecret"];
            //})
            .AddOAuth("Podium", options =>
            {
                options.ClientId = Configuration["Podium:ClientId"];
                options.ClientSecret = Configuration["Podium:ClientSecret"];
                options.CallbackPath = new PathString("/Account/pod/verify");
                options.Scope.Add("profile");
                options.Scope.Add("email");
                //options.Scope.Add("legal");
                //options.Scope.Add("address");
                options.SaveTokens = true;
                options.AuthorizationEndpoint = "https://accounts.pod.ir/oauth2/authorize";
                options.TokenEndpoint = "https://accounts.pod.ir/oauth2/token/";
                options.UserInformationEndpoint = "https://api.pod.ir/srv/core/nzh/getUserProfile/";

                options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "userId");
                options.ClaimActions.MapJsonKey(ClaimTypes.Name, "firstName");
                options.ClaimActions.MapJsonKey(ClaimTypes.GivenName, "lastName");
                options.ClaimActions.MapJsonKey(ClaimTypes.Email, "email");
                //options.ClaimActions.MapJsonKey(ClaimTypes.MobilePhone, "cellphoneNumber");
                //options.ClaimActions.MapJsonKey(ClaimTypes.StreetAddress, "address");
                //options.ClaimActions.MapJsonKey("urn:github:login", "login");
                //options.ClaimActions.MapJsonKey("urn:github:url", "html_url");
                //options.ClaimActions.MapJsonKey("urn:github:avatar", "avatar_url");

                options.Events = new OAuthEvents
                {
                    OnCreatingTicket = async context =>
                    {
                        var request = new HttpRequestMessage(HttpMethod.Get, context.Options.UserInformationEndpoint);
                        request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", context.AccessToken);
                        request.Headers.Add("_token_", context.AccessToken);
                        request.Headers.Add("_token_issuer_", "1");

                        var parameters = new Dictionary<string, string> {
                             { "client_id", Configuration["Podium:ClientId"] },
                             { "client_secret", Configuration["Podium:ClientSecret"] }
                         };

                        var encodedContent = new FormUrlEncodedContent(parameters);

                        request.Content = encodedContent;

                        var response = await context.Backchannel.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, context.HttpContext.RequestAborted);
                        response.EnsureSuccessStatusCode();

                        dynamic user = JObject.Parse(await response.Content.ReadAsStringAsync());

                        context.RunClaimActions(user.result);
                    }
                };
            });
            // .AddFacebook(facebookOptions =>
            // {
            //     facebookOptions.AppId        =      Configuration.GetSection("SecretData")["AuthenticationFacebookAppId"];
            //     facebookOptions.AppSecret    =      Configuration.GetSection("SecretData")["AuthenticationFacebookAppSecret"];
            // })

            //.AddMicrosoftAccount(microsoftOptions =>
            //{
            //    microsoftOptions.ClientId     =     Configuration.GetSection("SecretData")["AuthenticationMicrosoftApplicationId"];
            //    microsoftOptions.ClientSecret =     Configuration.GetSection("SecretData")["AuthenticationMicrosoftPassword"];
            //});

            services.AddMvc((mopts) => {
                // We need to roll our own support for AdditionalMetadataAttribute 
                mopts.ModelMetadataDetailsProviders.Add(new AdditionalMetadataProvider());
            }).AddRazorPagesOptions(o =>
            {
                o.Conventions.ConfigureFilter(new IgnoreAntiforgeryTokenAttribute());
            })

                   .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)

                .AddJsonOptions(
            options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
        )

                    .AddJsonOptions(options =>
                 options.SerializerSettings.ContractResolver = new DefaultContractResolver())

              .AddRazorPagesOptions(options =>
              {
                  options.AllowAreas = true;
                  options.Conventions.AuthorizeAreaFolder("Identity", "/Account/Manage");
                  options.Conventions.AuthorizeAreaPage("Identity", "/Account/Logout");
              });

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Identity/Account/Login";
                options.LogoutPath = $"/Identity/Account/Logout";
                options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
                options.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.None;

            });
            services.AddHttpContextAccessor();
            services.AddKendo();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IEmailSender, EmailSender>();
            services.AddSingleton<ITempDataProvider, CookieTempDataProvider>();
            services.Configure<AuthMessageSenderOptions>(Configuration);

            var Context = new CustomAssemblyLoadContext();
            Context.LoadUnmanagedLibrary(Path.Combine(Directory.GetCurrentDirectory(), "libwkhtmltox.dll"));

            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
            services.AddSingleton<HtmlEncoder>(
            HtmlEncoder.Create(allowedRanges: new[] { UnicodeRanges.BasicLatin,
                                                        UnicodeRanges.Arabic }));

            var builder = services.AddIdentityCore<ApplicationUser>(p =>
            {

                p.Password.RequireDigit = false;
                p.Password.RequireLowercase = false;
                p.Password.RequireUppercase = false;
                p.Password.RequireNonAlphanumeric = false;
                p.Password.RequiredLength = 6;

            });

          


        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {

                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

            }
            else
            {

                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                //app.UseExceptionHandler("/Home/Error");
                //app.UseHsts();

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            }

            app.UseDefaultFiles();
            app.UseHttpsRedirection();
         
            app.UseSession();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseAuthentication();

            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    // Cache static files for 30 days
                    ctx.Context.Response.Headers.Append("Cache-Control", "public,max-age=2592000");
                    ctx.Context.Response.Headers.Append("Expires", DateTime.UtcNow.AddDays(30).ToString("R", CultureInfo.InvariantCulture));
                }
            });




            //var options = new RewriteOptions()
            //    .AddRedirect("blog/events/\u062e\u0648\u0634\u0627\u06cc\u0646\u062f(.*)", "https://www.digikala.com");
            ///(.*)
            //app.UseRewriter(options);

            app.UseMvc(routes =>
            {
                routes.MapRoute("areaRoute", "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute("Contract", "Contract-{ContractID}/{Name}/{EnglishName}/{Id?}",
                defaults: new { controller = "Contract", action = "GetPage" });

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
