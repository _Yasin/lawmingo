﻿using System;
using LawMingo.Data;
using Microsoft.AspNetCore.Mvc;
using LawMingo.Models.ViewModel;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace LawMingo.Controllers
{
   
    public class ConsultationController : Controller
    {

        private ApplicationDbContext _db { get; set; }
        private readonly UserManager<ApplicationUser> _userManager;

        public ConsultationController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

       
        public IActionResult Index()
        {
            var a = 1;
            var data = new CounselorViewModel
            {
                Counselors = _db.Counselors.Include("CategoryAttachs").Where(e => e.IsActive),
                CounselorCategories = _db.CategoryAttachment.Include("Category").Where(q => q.CounselorID != null).Select(r => r.Category).Distinct().ToList()
            };
          
            return View(data);
        }


        //[HttpPost]     
        //public IActionResult Create(ConsultationRequest consultation)

        //{

        //    Random generator = new Random();
        //    string Random_OrderId = generator.Next(0, 999999).ToString("D6");

        //    //if (consultation.Hour < 1)
        //    //{
        //    //    return Json(new { message= "HourNotValid" });
        //    //}
        //    if (
        //        consultation.AdviceTime == null ||
        //        consultation.AdviceTime.ToString() == "1/1/0001 12:00:00 AM" || 
        //        consultation.CounselorID == 0 ||
        //        string.IsNullOrEmpty(consultation.Mobile)                 
             
        //        )
        //    {
        //        return Json(new { message = "NotValid" });              
        //    }
                 
        //    var entry = new ConsultationRequest
        //    {
        //        AdviceTime = consultation.AdviceTime,
        //         TimeRange = consultation.TimeRange,
        //        Time = DateTime.Now,
        //        OrderID = Random_OrderId,
        //        status = ConsultationRequest.Status.Waiting,
        //        ApplicationUserId = _userManager.GetUserId(HttpContext.User),
        //        CounselorID = consultation.CounselorID,          
        //        Mobile = consultation.Mobile
        //    };

        //    _db.ConsultationRequests.Add(entry);
        //    _db.SaveChanges();
        //    int ConsultationRequestID = entry.ConsultationRequestID;
         
        //    return Json(new { message = "success" , id = ConsultationRequestID });

        //}

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> request(string[] req , string Name , string Mobile , string Description)
        {

            var user = await _userManager.GetUserAsync(HttpContext.User);
            if(user != null)
            {
                if (req.Count() > 0 && !string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(Description))
                {

                    Random generator = new Random();
                    string Random_OrderId = generator.Next(0, 999999).ToString("D6");

                    var counselorId = (Convert.ToInt32(req[0].Split("_")[2])) - 1161;
                    var counselor = await _db.Counselors.Where(w => w.CounselorID == counselorId).FirstOrDefaultAsync();

                    if (counselor != null)
                    {

                        foreach (var item in req)
                        {

                            var month = req[0].Split("_")[0].Split(" ")[1];
                            var day = req[0].Split("_")[0].Split(" ")[2];
                            var year = req[0].Split("_")[0].Split(" ")[3];

                            var DateTime = Convert.ToDateTime(year + "-" + month + "-" + day);
                            var Time = item.Split("_")[1];

                            var entry = new ConsultationRequest
                            {
                                AdviceTime = DateTime,
                                TimeRange = Time,
                                Time = DateTime.Now,
                                OrderID = Random_OrderId,
                                status = ConsultationRequest.Status.Waiting,
                                ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                                CounselorID = counselorId,
                                Mobile = Mobile,
                                FullName = Name,
                                Description = Description
                            };

                             await _db.ConsultationRequests.AddAsync(entry);
                             await _db.SaveChangesAsync();
                        }
                        return Json(new { orderId = Random_OrderId });
                    }

                }
                else
                {
                    return Json(false);
                }
            }
            else
            {               
                return Json("Noauthenticate");
            }
                           
            return Json("");
        }

        public JsonResult Schedule(int ID)
        {
            var t = 5;
            var Counselor = _db.Counselors.Include("CategoryAttachs").Include("Schedules").Where(q => q.CounselorID == ID);        
            return Json(Counselor);
        }
    }
}