﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Xml;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.EntityFrameworkCore;
using X.Web.Sitemap;

namespace LawMingo.Controllers
{
    public class LandingController : Controller
    {

        private ApplicationDbContext _db { get; set; }
        private readonly UserManager<ApplicationUser> _userManager;
        public LandingController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        [Route("landing/{name}")]
        public IActionResult Index(string name = null)
        {
            IViewEngine viewEngine = HttpContext.RequestServices.GetService(typeof(ICompositeViewEngine)) as ICompositeViewEngine;
            ViewEngineResult viewResult = viewEngine.FindView(ControllerContext, name + "/Index", true);

            if (viewResult.View != null)
            {
                return View(viewResult.View.Path);
            }
            return RedirectPermanent("http://lawmingo.com");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}