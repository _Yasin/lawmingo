﻿
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static LawMingo.AffiliateHelper;

namespace LawMingo.Controllers
{

    public class QuestionsController : Controller
    {

        private readonly ApplicationDbContext db;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<ApplicationUser> _userManager;

        public QuestionsController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager , IHttpContextAccessor httpContextAccessor)
        {
             db = _db;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        [Route("question/{id}/{title?}")]
        [HttpGet]
        public async Task<ActionResult> Index(int id, string title = null)
        {
            var question = await db.Comments
                .Where(c => c.Code == id.ToString())
                .Include("Answers.Counselor").FirstOrDefaultAsync();

            if (question == null)
            {
                return NotFound();
            }

            var cats = LawMingo.Helpers.Categories.GetParents(db, question.CategoryID);
            ViewData["cats"] = cats;

            ++question.Visits;
            await db.SaveChangesAsync();

            return View(question);

        }

        [Route("questions/{title?}")]
        [HttpGet]
        public async Task<ActionResult> List(string title = null)
        {
            title = title != null ? title.Replace('-', ' ') : null;
            var cats = LawMingo.Helpers.Categories.GetChildren(db, null, title , true);
            if (title != null)
            {
                var _cats = LawMingo.Helpers.Categories.GetParents(db, null, title);
                ViewData["cats"] = _cats;
            }

            var questions = db.Comments
                .Where(c => c.IsActive && c.Subject != null && c.CategoryID != null);
            questions = cats.Count() > 0 ? questions.Where(c => cats.Contains(c.CategoryID ?? 0)) : questions;

            SEOTags(title);
            var Result = await questions.OrderByDescending(o => o.Time).ToListAsync();
            return View(questions);
        }

        [Route("questions/tag/{title?}")]
        [HttpGet]
        public async Task<ActionResult> TagList(string title = null)
        {
            title = title.Replace('-', ' ');
            var questions = db.Comments
                .Where(c => c.IsActive && c.Subject != null && c.CategoryID != null && c.Tags.Contains(title));

            var Result = await questions.OrderByDescending(o => o.Time).ToListAsync();

            SEOTags(title);

            return View("List" , questions);
        }

        public void SEOTags(string title)
        {
            var _title = "مشاوره حقوقی آنلاین رایگان | بهترین وکیل آنلاین | لامینگو";
            var _desc = "خدمت مشاوره حقوقی آنلاین رایگان خدمتی است که توسط لامینگو با هدف امکان دسترسی عموم به مشاوره حقوقی تخصصی و کمک به حل مسائل حقوقی ارائه شده است. لامینگو سعی دارد مشاوره حقوقی آنلاین رایگان را در زمینه‌های مختلفی از جمله  کسبوکار، کیفری، خانواده،  املاک و.غیره با بالاترین کیفیت ارائه نماید. جهت دریافت مشاوره حقوقی آنلاین رایگان می‌توانید پرسش‌های حقوقی خود را مطرح نمایید. مشاوران حقوقی و وکلای پایه یک دادگستری در اسرع وقت به این پرسش‌ها به صورت آنلاین و کاملا رایگان پاسخ خواهند داد. بهترین وکیل آنلاین را با لامینگو تجربه کنید";

            if (title != null)
            {
                _title = $"مشاوره حقوقی {title} رایگان | وکیل {title} آنلاین | لامینگو";
                _desc = $"مسائل حقوقی {title} به علت تنوع بالا نیازمند رسیدگی توسط مشاوران حقوقی {title} مجرب میباشد. در این بخش پاسخ سوالات حقوقی {title} خود را می‌توانید از طریق مشاوره حقوقی {title} رایگان دریافت نمایید. لامینگو به عنوان وکیل {title} رایگان شما، در هر زمان و هرجا در دسترس شما خواهد بود. به منظور دریافت مشاوره حقوقی {title} مساله خود را در بین پرسش و پاسخ حقوقی {title} جستجو کنید. در صورتی عدم یافتن پرسش خود، کافیست آن را مطرح نمایید. در اسرع وقت مشاوران حقوقی لامینگو در بخش مشاوره حقوقی {title} پاسخ شما را خواهند داد.";
            }

            ViewData["Title"] = _title;
            ViewData["Description"] = _desc;
        }
    }

}


