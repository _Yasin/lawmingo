﻿
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using Spire.Doc;
using Spire.Doc.Documents;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static LawMingo.AffiliateHelper;

namespace LawMingo.Controllers
{

    public class ContractController : Controller
    {

        private readonly ApplicationDbContext db;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _env;

        public ContractController(IHostingEnvironment env, ApplicationDbContext _db, UserManager<ApplicationUser> userManager , IHttpContextAccessor httpContextAccessor)
        {
             db = _db;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _env = env;
        }

        [HttpGet]
        public async Task<ActionResult>  GetPage(int? ContractID, int? Id, string token = null, string Name = null, string EnglishName = null)
        {
          
            Contract contract = null;
            string[] UserAnswers = null;



            if (ContractID != null) {

                contract = db.Contract.Find(ContractID);
            } 

            if (contract != null && (contract.Name.URLFriendly() != Name || contract.EnglishName.URLFriendly() != EnglishName))
            {
                return RedirectToRoutePermanent("Contract",
                    new
                    {
                        contract.ContractID,
                        Name = contract.Name.URLFriendly(),
                        EnglishName = contract.EnglishName.URLFriendly()
                    }
                );
            }


            else
            {
                if(ContractID == null)
                {
                    int contract_Id = Convert.ToInt32(Request.GetDisplayUrl().Split("=")[1].Split("-")[0]);
                    contract = db.Contract.Find(contract_Id);
                }              
            }

            contract.Description = System.Net.WebUtility.HtmlDecode(contract.Description);
            contract.EnglishName = contract.EnglishName.Trim();
            int CommentCount = contract.Comments != null ? contract.Comments.Count() : 0;

            if (Id != null)
            {
                var UserContract = db.UserContracts.Find(Id);
                if(UserContract.SelectedAnswer != null)
                {
                    UserAnswers = UserContract.SelectedAnswer.Split("_");
                    for (int i = 0; i < UserAnswers.Count(); i++)
                    {
                        UserAnswers[i] = UserAnswers[i].Split(",")[1].Split(":")[1];
                    }
                }
                else
                {
                    return Redirect("/");
                }
                          
            }

            if (token != null)
            {
                ViewData["layout"] = "~/Areas/Widget/Views/Shared/_Layout.cshtml";
                ViewData["partnertoken"] = token;

                var det = await AffiliateHelper.FindAffiliateDetailByContract(db, contract, token);
                var priceitem = AffiliateHelper.CalculatePrice(db, contract, det);

                contract.Price = priceitem.price;
                contract.Discount = priceitem.discount;
            }

            var faq = await db.Comments.Include(c => c.Answers).Where(c => c.ContractID == ContractID && c.Answers.Count > 0).ToListAsync();

            var ContractData = new ContractDataViewModel
            {
                Contract = contract,
                CommentCount = CommentCount,
                GoToInfo = ContractID != null ? false : true,
                userAnswers = UserAnswers,
                UserId = _userManager.GetUserId(HttpContext.User),
                FAQ = faq

            };
            return View(ContractData);

        }


        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult Questions(int? PreQuestion, int? ArticleID, string nextQuestion, string checkBox, int? Answer, string[] Answers, int ContractID, int Number = 0)
        {

            var question = new Question();
            var Article = new Article();
            IQueryable<RelatedArticle> RelatedArticles = null;
            IList<ArticleDetail> ArticleDetails = null;
            IList<ArticleDetail> noQuestion_AticleDetail = null;
            bool Last = false;
            List<int> ArticleDetails_Deleted = null;
            List<int> ArticleDetails_ID = null;
            int currentArticle = 0;

            if (ArticleID == null && PreQuestion == null && Answer == null)
            {
                Article = db.Articles.Include("Questions.Answers").Include("Questions.Helps").Where(q => q.ContractID == ContractID).First();
            }

            if (ArticleID != null && PreQuestion == null)
            {

                int order = db.Articles.Find(ArticleID).Order;
                Article = db.Articles.Include("Questions.Answers").Include("Questions.Helps").Where(q => q.ContractID == ContractID).OrderBy(q => q.Order).ToList()  
               .SkipWhile(i => i.Order != order).OrderBy(q => q.Order).Skip(1).FirstOrDefault();

                if (Article == null)
                {
                    Last = true;
                    Article = db.Articles.Include("Questions.Answers").Include("Questions.Helps").Where(q => q.ContractID == ContractID).OrderByDescending(q => q.ArticleID).First();
                }

                RelatedArticles = db.RelatedArticles.Where(q => q.ArticleID == ArticleID);
            }

            //if (!string.IsNullOrEmpty(nextQuestion) && string.IsNullOrEmpty(checkBox))
            //{
            //    int qId = Convert.ToInt32(nextQuestion.Split('-')[0]);
            //    int ArId = Convert.ToInt32(nextQuestion.Split('-')[1]);
            //    question = db.Questions.Include("Answers").Where(q => q.QuestionID > qId && q.ArticleID == ArId).ToList()
            //   .SkipWhile(i => i.AnsID != null).FirstOrDefault();
            //}

            if (Answer == null && PreQuestion == null && string.IsNullOrEmpty(checkBox) && string.IsNullOrEmpty(nextQuestion))
            {
                question = Article.Questions.OrderBy(q => q.QuestionID).Skip(ArticleID == null ? Number : 0).Take(1).First();
            }
            else
            {
                if (Answer != null || !string.IsNullOrEmpty(checkBox))
                {
                    var index = Answer != null ? Answer : (checkBox.StartsWith("unChecked-") ? Convert.ToInt32(checkBox.Split('-')[1]) : 0);
                    if (index != 0)
                    {

                        ArticleDetails_ID = db.ArticleDetails.Where(q => q.AnswerID == index).Select(q => q.ArticleDetailID).ToList();

                        if (ArticleID == null)
                            question = db.Questions.Include("Answers").Include("Helps").Where(q => q.AnsID == index).FirstOrDefault();

                        if (ArticleID != null)
                            question = Article.Questions.OrderBy(q => q.QuestionID).Take(1).First();

                        if (!Last)
                        {
                            bool countinu = true;
                            while (countinu)
                            {

                                if (question.MyQuestion == "؟" || question.MyQuestion == "?")
                                {

                                    ArticleDetails_ID.Add(db.ArticleDetails.Where(q => q.Answer.QuestionID == question.QuestionID).Select(q => q.ArticleDetailID).First());

                                    if (question.End)
                                    {

                                        int order = db.Articles.Find(question.ArticleID).Order;
                                        Article = db.Articles.Include("Questions.Answers").Include("Questions.Helps").Where(q => q.ContractID == ContractID).OrderBy(q => q.Order).ToList()
                                       .SkipWhile(i => i.Order != order).OrderBy(q => q.Order).Skip(1).FirstOrDefault();

                                        if (Article != null)
                                        {
                                            question = Article.Questions.OrderBy(q => q.QuestionID).Take(1).First();
                                        }
                                        else
                                        { 
                                            Last = true;
                                            Article = db.Articles.Include("Questions.Answers").Include("Questions.Helps").Where(q => q.ContractID == ContractID).OrderByDescending(q => q.ArticleID).First();
                                            question.MyQuestion = "<div class='Create-Contract'><div class='row'><div class='col-md-6 col-sm-12' style='text-align:right;'><button id='register-data-info' onclick='GoToInfoRegister()' class='btn btn-success'><i class='fa fa-arrow-circle-left'></i>   ثبت اطلاعات تکمیلی   </button></div><div class='col-md-6 col-sm-12' style='text-align:left;'><button  id='questionBack' onclick='getQuestions(false , null , null)' class='btn btn-success'>بازگشت به سوالات  <i class='fa fa-arrow-circle-right'></i></button></div></div></div>";
                                            question.QuestionID = 0;
                                        }

                                        currentArticle = question.ArticleID;
                                    }
                                    else
                                    {
                                        question = db.Questions.Include("Answers").Include("Helps").Where(q => q.QuestionID > question.QuestionID && q.ArticleID == question.ArticleID).ToList().FirstOrDefault();
                                        if (question != null && question.End)
                                        {
                                            currentArticle = question.ArticleID;
                                        }
                                    }

                                }
                                else
                                {
                                    countinu = false;
                                }

                            }

                        }
                        else
                        {
                            Last = true;
                            Article = db.Articles.Include("Questions.Answers").Include("Questions.Helps").Where(q => q.ContractID == ContractID).OrderByDescending(q => q.ArticleID).First();
                            question.MyQuestion = "<div class='Create-Contract'><div class='row'><div class='col-md-6 col-sm-12' style='text-align:right;'><button id='register-data-info' onclick='GoToInfoRegister()' class='btn btn-success'><i class='fa fa-arrow-circle-left'></i>   ثبت اطلاعات تکمیلی   </button></div><div class='col-md-6 col-sm-12' style='text-align:left;'><button  id='questionBack' onclick='getQuestions(false , null , null)' class='btn btn-success'>بازگشت به سوالات  <i class='fa fa-arrow-circle-right'></i></button></div></div></div>";
                            question.QuestionID = 0;
                        }

                    }
                    else Tracker(null, Convert.ToInt32(checkBox), Article);

                    if (RelatedArticles != null && RelatedArticles.Count() > 0)
                    {

                        List<int> UserAnswers = new List<int>();
                        foreach (var A in Answers)
                        {
                            if (A.ToString().Split(',').Count() > 1)
                            {
                                for (int i = 0; i < A.ToString().Split(',').Count(); i++)
                                {
                                    if (A.ToString().Split(',')[i] != "") UserAnswers.Add(Convert.ToInt32(A.ToString().Split(',')[i]));
                                }
                            }
                            else if (A != "") UserAnswers.Add(Convert.ToInt32(A));
                        }

                        RelatedArticles = RelatedArticles.Where(q => UserAnswers.Any() ? UserAnswers.Any(e => e == q.Answer.AnswerID) : true);

                        if (RelatedArticles != null && RelatedArticles.Count() > 0)
                        {
                            foreach (var item in RelatedArticles)
                            {
                                switch (item.Act)
                                {
                                    case RelatedArticle.Actions.Add:

                                        var art_D = new ArticleDetail
                                        {
                                            AnswerID = item.AnswerID,
                                            ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                                            ArticleContent = item.NewContent,
                                            Time = DateTime.Now
                                        };

                                        ArticleDetails.Add(art_D);
                                        break;


                                    case RelatedArticle.Actions.Update:

                                        var entity = ArticleDetails.FirstOrDefault(q => q.ArticleDetailID == item.ArticleDetailID);
                                        entity.ArticleContent = item.NewContent;

                                        break;
                                    case RelatedArticle.Actions.Delete:

                                        var Object = ArticleDetails.SingleOrDefault(x => x.ArticleDetailID == item.ArticleDetailID);
                                        if (Object != null)
                                            ArticleDetails.Remove(Object);

                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                    }
                }



                if (PreQuestion != null && string.IsNullOrEmpty(checkBox))
                {
                    question = db.Questions.Include("Answers.ArticleDetails").FirstOrDefault(q => q.QuestionID == PreQuestion);
                    //ArticleDetails_Deleted = db.ArticleDetails.Where(q => q.Answer.QuestionID == PreQuestion).Select(q => q.ArticleDetailID).ToList();
                }
            }

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
            };

            var json_Questions = JsonConvert.SerializeObject(question, settings);

            return Json(
                        new
                        {

                            Questions = json_Questions,
                            Contract = db.Contract.Find(ContractID).Name,
                            questionNumber = Number,
                            LastQuestion = Last,
                            ckeckBOX = checkBox,
                            xx = ArticleDetails_ID,
                            Deleted_A = ArticleDetails_Deleted,
                            noQuestion = noQuestion_AticleDetail != null ? noQuestion_AticleDetail : null,
                            CA = currentArticle != 0 ? currentArticle : 0,
                            PreviousAnswer = PreQuestion != null ? Answers : null
                        }
                          );
        }


        public void Tracker(int? ArticleId, int id, Article article, bool Finish = true, int counter = 0)
        {

            //if (ArticleId != null)
            //{
            //    var currentArticle = db.Articles.Where(q => q.ArticleID == ArticleId).Include("Questions.Answers").FirstOrDefault();
            //    article = currentArticle;
            //}

            //if (counter == 0)
            //{
            //    for (int i = 0; i < HttpContext.Session; i++)
            //    {
            //        if (Convert.ToInt32(Session.Keys[i].Split('-')[1]) == id) Session.Remove(Session.Keys[i]);
            //    }
            //}

            //while (Finish)
            //{
            //    var answers = db.Answers.Where(q => q.DefaultAnswerID == id).FirstOrDefault();
            //    var question = article.Questions.Where(q => q.AnsID == answers.AnswerID).FirstOrDefault();

            //    if (question != null)
            //    {
            //        var defaultNumber = question.Answers.Select(q => q.DefaultAnswerID).ToList();
            //        if (defaultNumber.Count() > 0)
            //        {
            //            foreach (var item in defaultNumber)
            //            {
            //                for (int i = 0; i < Session.Keys.Count; i++)
            //                {
            //                    if (Convert.ToInt32(Session.Keys[i].Split('-')[1]) == item)
            //                    {
            //                        counter++;
            //                        Session.Remove(Session.Keys[i]);
            //                        Tracker(ArticleId != null ? ArticleId : null, item, article, true, counter);
            //                    }
            //                }
            //            }

            //        }
            //    }
            //    Finish = false;
            //}
        }

        [HttpPost]
        public async Task<ActionResult> ArticleDetailsInput(int[] ArticleDetailsId , int? userContractId)
        {
            dynamic properties = null;

            if (userContractId != null)
            {
                  var userContract = db.UserContracts.Find(userContractId);
                  properties = db.UserContractProperties.Where(q => q.UserContractID == userContract.UserContractID).Select(s => new
                  {
                      s.ArticlePropertyID,
                      s.Value
                  });
            }

            var Art = await db.ArticleDetails.Where(p => ArticleDetailsId.Any(r => r == p.ArticleDetailID && p.ArticleContent.Contains("input"))).GroupBy(
                 p => p.Answer.Question.Article,
                 (key, g) => new ArticledetailViewModel
                 {
                     Order = key.Order,
                     Article = key.Order + "-" + key.Name,
                     ArticleDetail = g.Select(r => new ArticleDetailsViewModel
                     { Content = r.ArticleContent })                    
                 }).ToListAsync();

            var result = Art.OrderBy(w => w.Order);

            
            return Json(new {Art = result , Properties = properties });
        }


        //[HttpPost]
        //public JsonResult SendWord(int UserContractID)
        //{

        //    var UserContract = db.UserContracts.Find(UserContractID);         
        //    UserContract.Word = "OK";          
        //    db.Entry(UserContract).State = EntityState.Modified;
        //    db.SaveChanges();

        //    return Json("seccess");
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public JsonResult ArticleDetails(int UserContractID)
        {

             
            var userContract = db.UserContracts.Find(UserContractID);

            var currentId = _userManager.GetUserId(HttpContext.User);
            if (userContract.ApplicationUserId == currentId)
            {
                var Properties = db.UserContractProperties.Where(q => q.UserContractID == userContract.UserContractID).Select(s => new
                {
                    s.ArticlePropertyID,
                    s.Value
                });

                string[] ArtsId = userContract.ArtsID.Split("_");
                List<int> Arts_ID = new List<int>();

                foreach (var id in ArtsId)
                {
                    Arts_ID.Add(Convert.ToInt32(id));
                }

                var ArticleDetails_Data = db.ArticleDetails.Where(p => Arts_ID.Any(r => r == p.ArticleDetailID)).GroupBy(
                     p => p.Answer.Question.Article,
                     (key, g) => new ArticleDetailDataViewModel

                     {
                         Article = key.Order + "-" + key.Name,
                         Content = g.Select(q => q.ArticleContent),
                         Order = key.Order
                     }).OrderBy(e => e.Order);

                return Json(
                       new
                       {
                           Art = ArticleDetails_Data,
                           Props = Properties,
                           ContractName = db.Contract.Find(userContract.ContractID).Name,
                       });

            }
            else
            {
                return Json("");                      
            }     
        }


        [Authorize]
        public async Task<JsonResult>  Info(string[] Data, int ContractID, string[] AnswerId, string[] ArticleDetails_Id , int? userContractId, string partnertoken = null)
        {

            string selected_Answer = "";
            string ArticleDetails = "";
            UserContract parent = null;
          

            var recently_UserContracts = db.UserContracts.Where(
                
                w => w.ApplicationUserId == _userManager.GetUserId(HttpContext.User) && 
                w.ContractID == ContractID &&
                w.ParentID == null &&
                w.status  != UserContract.Status.Complete).ToList();

            if(recently_UserContracts.Count() == 0)
            {

                foreach (string AnsId in AnswerId)
                {
                    selected_Answer += AnsId + "_";
                }

                foreach (string ArtID in ArticleDetails_Id)
                {
                    ArticleDetails += ArtID + "_";
                }

                selected_Answer = selected_Answer.Remove(selected_Answer.Length - 1);
                ArticleDetails = ArticleDetails.Remove(ArticleDetails.Length - 1);

                var contract = db.Contract.Find(ContractID);

                Random generator = new Random();
                string orderID = generator.Next(0, 999999).ToString("D6");


                if(userContractId != null)
                {
                    var FirstParent = UserContracts.GetParents(_userManager, db, HttpContext, userContractId.GetValueOrDefault()).FirstOrDefault();
                    if(FirstParent.EditCount <= 2)
                    {
                       parent =  db.UserContracts.Find(FirstParent.UserContractID);
                       parent.EditCount += 1;
                    }
                }

                //Affiliate
                var det = new AffiliateDetail() { };
                var priceitem = new ContractPrice() { };
                int? AffiliateDetailID = null;

                if (partnertoken != null)
                {
                    det = await AffiliateHelper.FindAffiliateDetailByContract(db, contract, partnertoken);
                    priceitem = AffiliateHelper.CalculatePrice(db, contract, det);
                    AffiliateDetailID = det.AffiliateDetailID;
                }
                //Affiliate

                var entity = new UserContract
                {
                    OrderId = parent == null ? orderID : parent.EditCount + " / " + parent.OrderId,
                    Time = parent == null ? DateTime.Now : parent.Time,
                    ContractID = ContractID,
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                    SelectedAnswer = selected_Answer,
                    ArtsID = ArticleDetails,
                    EditTime = DateTime.Now,
                    ParentID = parent?.UserContractID ?? null,
                    EditCount = parent == null ? null : parent.EditCount,
                    status = parent == null ? 0 : parent.status,                 
                    AffiliateDetailID = AffiliateDetailID
                };
                        
                db.UserContracts.Add(entity);
                db.SaveChanges();

                int UserContract_ID = entity.UserContractID;

                for (int i = 0; i < Data.Count(); i++)
                {
                    var Properties = new UserContractProperty
                    {
                        UserContractID = UserContract_ID,
                        ArticlePropertyID = Convert.ToInt32(Data[i].Split(':')[0]),
                        Value = Data[i].Split(':')[1]
                    };

                    db.UserContractProperties.Add(Properties);
                    db.SaveChanges();
                }

                //Affiliate
                if (partnertoken != null)
                {
                    contract.Price = priceitem.price;
                    contract.Discount = priceitem.discount;
                }
                //Affiliate

                SubscribViewModel Result = (SubscribViewModel)Helpers.Subscription.check(_userManager, db , HttpContext , null);
             
                return Json(new
                {
                    contract.Price,
                    contract.Discount,
                    UserContractID = UserContract_ID,
                    contract.Name,
                    Subscrib = Result.Message == "Yes"  ? true : false ,
                    usageCount = Result.Message == "Yes" ? Result.RemainingCount : null
                }
                );
            }

            else
            {
                return Json("false");
            }
           
        }


        [HttpPost]
        public JsonResult Delete_UserContract(int UserContractID)
        {

            try
            {
                var usercontractProperty = db.UserContractProperties.Where(w => w.UserContractID == UserContractID).ToList();
                if (usercontractProperty.Count > 0)
                {
                    foreach (var item in usercontractProperty)
                    {
                        db.UserContractProperties.Remove(item);
                        db.SaveChanges();
                    }
                }

                var usercontractPayment = db.PaymentLogs.Where(w => w.UserContractID == UserContractID).ToList();
                if (usercontractPayment.Count > 0)
                {
                    foreach (var item in usercontractPayment)
                    {
                        db.PaymentLogs.Remove(item);
                        db.SaveChanges();
                    }
                }

                var Coupons = db.Coupons.Where(w => w.UserContractID == UserContractID).ToList();
                if (Coupons.Count > 0)
                {
                    foreach (var item in Coupons)
                    {
                        db.Coupons.Remove(item);
                        db.SaveChanges();
                    }
                }

                var usercontract = db.UserContracts.Find(UserContractID);
                        db.UserContracts.Remove(usercontract);
                        db.SaveChanges();
            }
            catch (Exception)
            {
               return Json(false);
            }
                                                                                                               
            return Json(true);
        }


        [HttpPost]
        public JsonResult GetHelp(int QI)
        {
            var helps = db.Helps.Where(q => q.QuestionID == QI);

            var LikeCount    = helps.FirstOrDefault().Like;
            var DisLikeCount = helps.FirstOrDefault().DisLike;

            return Json(new { Helpers = helps , Like =  LikeCount , DisLike =  DisLikeCount });
        }


        [HttpPost]
        public JsonResult HelpScore(string Comment)
        {

            var  currentUser =  _userManager.GetUserId(HttpContext.User);
            int QI = Convert.ToInt32(Comment.Split("-")[1]);
            string comment = Comment.Split("-")[0];

            var helps = db.Helps.Where(q => q.QuestionID == QI).ToList();
            bool checkUser =  helps.Any(q => q.UserAppId == currentUser);

            if (checkUser)
            {            
                return Json(new {message = "Faild" });
            }
            else
            {
                switch (comment)
                {
                    case "Like":

                        foreach (var item in helps)
                        {
                            item.UserAppId = currentUser;
                            item.Like += 1;
                        }
                        db.SaveChanges();
                        break;


                    case "DisLike":
                        foreach (var item in helps)
                        {
                            item.UserAppId = currentUser;
                            item.DisLike += 1;
                        }
                        db.SaveChanges();
                        break;

                    default:
                        break;
                }
            }
           
            return Json(new {message ="success" , LikeCount = helps.FirstOrDefault().Like , DisLikeCount = helps.FirstOrDefault().DisLike , qID = QI });
        }
     
        public JsonResult GetArticleDetail(int ArticleId)
        {
            var ArticleDetails = db.ArticleDetails.Where(e => e.Answer.Question.ArticleID == ArticleId);            
            return Json(ArticleDetails);
        }

        public JsonResult checkRecentlyContracts(int ContractID)
        {
            var result =  RecentlyContract.check(_userManager, db, HttpContext , ContractID);
            return Json(result);
        }

     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ContractTemplate(int ContractId)
        {

            if (User.Identity.IsAuthenticated)
            {
                int id = ContractId - 2124;
                var entity = new UserContract();
                var contract = db.Contract.Where(q => q.ContractID == id).FirstOrDefault();

                if (contract != null)
                {

                    Random generator = new Random();
                    string orderID = generator.Next(0, 999999).ToString("D6");

                    entity = new UserContract
                    {
                        OrderId = orderID,
                        Time = DateTime.Now,
                        ContractID = contract.ContractID,
                        ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                        SelectedAnswer = "",
                        ArtsID = "",
                        EditTime = DateTime.Now,
                        status = UserContract.Status.Pending,
                    };

                    db.UserContracts.Add(entity);
                    db.SaveChanges();
                }

                return Json(new { user_ContId = entity.UserContractID , Contract = contract });
            }
            else
            {
                return Json(new { user_ContId = 0 });
            }           
        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> WordFile(int UserContractID)
        {
           
                var Usercontract = db.UserContracts.Include("Contract").Include("PaymentLogs")
               .Where(q => q.UserContractID == (UserContractID - 1584)).FirstOrDefault();

                if (Usercontract != null)
                {
                    if (
                         Usercontract.Contract.Type == Contract.ContractType.Template &&
                        (Usercontract.PaymentLogs != null &&
                         Usercontract.PaymentLogs.Count > 0 &&
                         Usercontract.PaymentLogs.Any(q => q.IsSuccessful) &&
                         Usercontract.ApplicationUserId == _userManager.GetUserId(HttpContext.User))
                        )
                    {

                        string folderName = "Uploads";
                        string SubFolder = "ContractTemplate";
                        string webRootPath = _env.WebRootPath;

                        try
                        {
                            string newPath = Path.Combine(webRootPath, folderName, SubFolder);
                            string fullPath = Path.Combine(newPath, Usercontract.Contract.File);

                            Usercontract.status = UserContract.Status.Complete;
                            Usercontract.EditTime = DateTime.Now;
                            db.UserContracts.Update(Usercontract);
                            db.SaveChanges();


                            var memory = new MemoryStream();
                            using (var stream = new FileStream(fullPath, FileMode.Open))
                            {
                                await stream.CopyToAsync(memory);
                            }
                            memory.Position = 0;

                             Response.Clear();                           
                             Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                             Response.Headers.Add("Content-Disposition", "attachment; filename=\"" + Usercontract.Contract.File + "\"");
                            
                          

                        return File(memory, "application/vnd.ms-word", Usercontract.Contract.File);
                        }
                        catch (Exception e)
                        {
                            throw;
                        }

                    }
                    else
                    {
                        return Content("");
                    }
                }
                else
                {
                    return Content("");
                }            
           
        }
    }

}


