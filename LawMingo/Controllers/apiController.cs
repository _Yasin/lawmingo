﻿using LawMingo.Data;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel.Blog;

using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Helpers;

namespace LawMingo.Controllers
{
    public class apiController : Controller
    {
        public enum CommentSort
        {
            Latest,
            MostVisited
        }
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public apiController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager , SignInManager<ApplicationUser> signInManager)
        {
             db = _db;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        public async Task<JsonResult> fAQ(int id)
        {
            var result = await db.Comments
                .Where(p => (p.ContractID == id || p.IsPublic) && p.IsActive && p.ParentID == null && p.Code == null)
                .OrderBy(c => c.CommentID)
                .Select(r => new { active = r.IsActive, id = r.CommentID, author = r.Author, content = r.Content, time = r.Time, answers = r.Answers, read = r.IsRead }).ToListAsync();
            return Json(result);
        }

        [HttpGet]
        public async Task<JsonResult> ContractQuestions(int id)
        {
            var result = await db.Comments
                .Where(p => (p.ContractID == id || p.IsPublic) && p.IsActive && p.ParentID == null && p.Code != null)
                .Include(p => p.Answers)
                .OrderBy(c => c.CommentID)
                .Select(r => new { active = r.IsActive, id = r.CommentID, author = r.Author, content = r.Content, time = r.Time, answers = r.Answers, read = r.IsRead }).ToListAsync();
            return Json(result);
        }

        [HttpGet]
        public async Task<JsonResult> Questions(int? id , int row)
        {
            var cats = new List<int> { };
            if (id != 0)
            {
                cats = LawMingo.Helpers.Categories.GetChildren(db, id, null, true);
            }

            var questions = db.Comments
                .Where(c => c.IsActive && c.Subject != null && c.CategoryID != null);
            questions = cats.Count() > 0 ? questions.Where(c => cats.Contains(c.CategoryID ?? 0)) : questions;

            var Result = await questions.OrderByDescending(o => o.Time)
                        .Select(r => new 
                        { 
                            code = r.Code,
                            content = r.Content.Substring(0, r.Content.Length > 260 ? 260 : r.Content.Length),
                            subject = r.Subject,
                            time = LawMingo.Helpers.Times.ElapsedTime(r.Time),
                            tags = r.Tags,
                            url = $"question/{r.Code}/{r.Subject.URLFriendly().Replace(' ', '_')}"
                        }).Skip(row).Take(10).ToListAsync();
            return Json(Result);
        }

        [HttpGet]
        public async Task<JsonResult> CampaignComments(string name, CommentSort sort)
        {
            var _result = db.Comments
                .Where(p => p.Subject != null);
            //.Where(p =>  p.CampaignName == name && p.Answers.Count > 0 && p.IsActive && p.ParentID == null && p.PostID == null)
            switch (sort)
            {
                case CommentSort.Latest:
                    _result = _result.OrderByDescending(c => c.CommentID);
                    break;
                case CommentSort.MostVisited:
                    _result = _result.OrderByDescending(c => c.Visits);
                    break;
                default:
                    break;
            };

            var result = await _result.Select(r => new { id = r.Code, subject = r.Subject , url = $"/question/{r.Code}/{r.Subject.URLFriendly()}"})
                .Take(5).ToListAsync();
            return Json(result);
        }
        
        [HttpPost]
        public async Task<JsonResult> LawyerRequest(string name, string post , string education , string summery , string mobile , string email ,bool direct , bool online , bool tell)
        {
            var user = await _userManager.FindByNameAsync(mobile);
            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = PersianDates.ToEnglishNumber(mobile),
                    PhoneNumber = PersianDates.ToEnglishNumber(mobile),
                    Email = email,
                    Name = name,
                    Register = DateTime.Now
                };
                var result = await _userManager.CreateAsync(user, mobile);
            }


            var uid = await _userManager.GetUserIdAsync(user);
            if (uid != null)
            {
                var counselor = new Counselor() { };
                counselor.Time = DateTime.Now;
                counselor.Name = name;
                counselor.ApplicationUserId = uid;
                counselor.Education = education;
                counselor.Post = post;
                counselor.Summery = summery;
                counselor.IsActive = false;
                counselor.Online = online;
                counselor.Direct = direct;
                counselor.Phone = tell;

                var _result = await db.Counselors.AddAsync(counselor);
                await db.SaveChangesAsync();
                Pushe.Send("درخواست همکاری جدید", counselor.Name , "Counselors");
                return Json(new { msg = "درخواست شما با موفقیت ثبت شد ، به زودی از لامینگو با شما تماس خواهیم گرفت." , res = true });

            }
            return Json(new { msg = "بروز خطا", res = false });
        }


        [HttpPost]
        public async Task<JsonResult> CreateComment(string content, string contact, string name, int id)
        {
            var comment = new Comment() { };
            comment.Time = DateTime.Now;
            comment.ContractID = id;
            comment.Author = name;
            comment.Content = content;
            comment.AuthorEmail = contact;
            comment.IsActive = false;

            Random r = new Random();
            var codenotfined = true;
            var newcode = 0;
            while (codenotfined)
            {
                newcode = r.Next(111111, 999999);
                codenotfined = db.Comments.Where(h => h.Code == newcode.ToString()).Count() > 0;
            }
            comment.Code = newcode.ToString();

            var result = await db.Comments.AddAsync(comment);
            var contract = db.Contract.Find(id);

            await db.SaveChangesAsync();
            Pushe.Send("پرسش جدید", contract.Name, "/Questions/list?" + comment.CommentID);

            return Json(true);
        }

        [HttpPost]
        public async Task<JsonResult> CreateQuestion(string content, string contact, string name, int id)
        {
            var comment = new Comment() { };
            comment.Time = DateTime.Now;
            comment.Subject = name;
            comment.Author = contact;
            comment.Content = content;
            comment.AuthorEmail = contact;
            comment.IsActive = false;

            Random r = new Random();
            var codenotfined = true;
            var newcode = 0;
            while (codenotfined)
            {
                newcode = r.Next(111111, 999999);
                codenotfined = db.Comments.Where(h => h.Code == newcode.ToString()).Count() > 0;
            }
            comment.Code = newcode.ToString();

            var result = await db.Comments.AddAsync(comment);
            await db.SaveChangesAsync();

            SMS.Verify(contact, newcode.ToString(), "NewQuestion");
            Pushe.Send("پرسش جدید", comment.Subject, "/Questions/list?" + comment.CommentID);

            return Json(newcode);
        }
        [HttpPost]
        public async Task<JsonResult> Noafaringenerate(string noafarin_code)
        {
            return Json(new { token = Noafarin.Generate(noafarin_code) });
        }

        [HttpPost]
        public async Task<JsonResult> Noafarinverify(string token, string code, string noafarin_code)
        {
            return Json(new { data = Noafarin.Verify(token, code, noafarin_code) });
        }

        [HttpPost]
        public async Task<JsonResult> saveuser(string token, string code, string noafarin_code, string name, string city, string mobile, string password , string company ,string email)
        {
            var user = await _userManager.FindByNameAsync(mobile);
            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = PersianDates.ToEnglishNumber(mobile),
                    PhoneNumber = PersianDates.ToEnglishNumber(mobile),
                    Email = email,
                    Name = name,
                    Company = company,
                    City = city,
                    Register = DateTime.Now,
                    Refrence = "Noafarin",
                    AffiliateID = noafarin_code
                };
                var result = await _userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);

                    return Json(new { result = true  , message ="ثبت نام شما با موفقیت انجام شد"});
                }
                else
                {
                    return Json(new { result = false , message = "بروز خطا ، لطفا مجددا نسبت به ثبت نام از طریق نوآفرین اقدام نمایید" }); ;
                }
            }
            return Json(new { result = false, message = "شما قبلا با این شماره تلفن در لامینگو ثبت نام کرده اید ، لطفا از طریق گزینه ورود به لامینگو اقدام نمایید" }); ;

        }
    }

    }


