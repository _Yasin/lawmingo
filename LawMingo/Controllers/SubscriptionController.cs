﻿
using Microsoft.AspNetCore.Mvc;
using LawMingo.Helpers;
using Microsoft.AspNetCore.Identity;
using LawMingo.Data;
using Microsoft.AspNetCore.Http;
using LawMingo.Models.DomainModel;

namespace LawMingo.Controllers
{
  
    public class SubscriptionController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext db;      
        public SubscriptionController(UserManager<ApplicationUser> userManager, ApplicationDbContext _db)
        {
            _userManager = userManager;                     
             db = _db;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult check(int? SubscriptionId)

        {
            var Result = Helpers.Subscription.check(_userManager, db, HttpContext , SubscriptionId != null ? SubscriptionId : null);
            return Json(Result);
        }   

    }

  
}