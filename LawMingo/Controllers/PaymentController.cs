﻿using Dto.Payment;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using ZarinPal.Class;

namespace LawMingo.Controllers
{

    public class GatewayResult
    {
        public string Status { get; set; }
        public string TrackingCode { get; set; }
        public string Link { get; set; }
        public bool Success { get; set; }
        public string IPG { get; set; }
    }
    public class VerificationResult
    {
        public string Status { get; set; }
        public bool Success { get; set; }
        public bool Wallet { get; set; }
    }

    public class PaymentController : Controller
    {
        private readonly Payment _payment;
        private readonly Authority _authority;
        private readonly Transactions _transactions;


        private readonly string Merchant = "a8df972a-e18f-11e8-9276-005056a205be";

        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;

        public PaymentController(ApplicationDbContext _db, UserManager<ApplicationUser> userManager)
        {
            db = _db;
            _userManager = userManager;

            var expose = new Expose();
            _payment = expose.CreatePayment();
            _authority = expose.CreateAuthority();
            _transactions = expose.CreateTransactions();
        }

        [Route("Payment/Gateway/{ipg}")]
        public async Task<ActionResult> RequestAsync(
            string UserContractId,
            int? SubscriptionID,
            string ConsultationOrderID,
            [FromRoute] string ipg)
        {

            string Message = "";
            string CallBackUrl = "";
            int Price = 0;
            int UserContractID = 0;
            ConsultationRequest Consultation = null;


            string currentUrl = Request.GetDisplayUrl();
            if (currentUrl.Contains("localhost")) CallBackUrl = "https://localhost:5001/Payment/Verify/" + ipg;
            else CallBackUrl = "https://lawmingo.com/Payment/Verify/" + ipg;

            if (string.IsNullOrEmpty(ConsultationOrderID))
            {
                if (SubscriptionID == null)
                {
                    UserContractID = Convert.ToInt32(UserContractId.Split("-")[0]);

                    //var splitInfo = UserContractId.Split("-");
                    //if (splitInfo.Length > 2 && UserContractId.Split("-")[2] == "Profile")
                    //{
                    //    if (currentUrl.Contains("localhost")) CallBackUrl = "https://localhost:5001/Identity/Account/Manage/Contract/";
                    //    else CallBackUrl = "https://lawmingo.com/Identity/Account/Manage/Contract/";
                    //}

                    var entity = db.UserContracts.Include("Contract").Include(q => q.AffiliateDetail).Where(q => q.UserContractID == UserContractID).FirstOrDefault();
                    Price = (int)entity.Contract.Price.GetValueOrDefault();

                    //Affiliate
                    if (entity.AffiliateDetailID != null)
                    {
                        var _price = AffiliateHelper.CalculatePrice(db, entity.Contract, entity.AffiliateDetail);
                        Price = (int)(_price.price - _price.discount);

                        if (currentUrl.Contains("localhost")) CallBackUrl = "https://localhost:5001/widget/Payment/Verify/" + ipg;
                        else CallBackUrl = "https://lawmingo.com/widget/Payment/Verify/" + ipg;
                    }
                    //Affiliate


                    string code = "undefined";
                    if (UserContractId.Split("-").Count() > 1)
                    {
                        code = UserContractId.Split("-")[1];
                    }

                    if (code != "undefined")
                    {

                        var Usaged_Coupon = db.Coupons.Where(q => q.Name.ToLower().Trim() == code.ToLower().Trim() && q.ParentID == null).FirstOrDefault();

                        if (Usaged_Coupon != null && Usaged_Coupon.End > DateTime.Now && Usaged_Coupon.IsActive && Usaged_Coupon.Count > 0)
                        {
                            switch (Usaged_Coupon.discountType)
                            {
                                case Coupon.DiscountType.Percent:

                                    var Discount = (Price * Usaged_Coupon.Amount) / 100;
                                    Price = Convert.ToInt32(Price - Discount);
                                    break;

                                case Coupon.DiscountType.Static:
                                    Price = Convert.ToInt32(Price - Usaged_Coupon.Amount);
                                    break;

                                default:
                                    break;
                            }

                            Usaged_Coupon.Count = Usaged_Coupon.Count - 1;
                            Usaged_Coupon.IsActive = Usaged_Coupon.Count == 1 ? false : true;


                            var Coupon_Used = new Coupon()
                            {
                                ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                                CategoryID = Usaged_Coupon.CategoryID,
                                discountType = Usaged_Coupon.discountType,
                                Name = Usaged_Coupon.Name,
                                Amount = Usaged_Coupon.Amount,
                                Start = Usaged_Coupon.Start,
                                End = Usaged_Coupon.End,
                                Time = DateTime.Now,
                                ParentID = Usaged_Coupon.CouponID,
                                UserContractID = entity.UserContractID,
                                Count = 1
                            };

                            db.Coupons.Add(Coupon_Used);
                            db.SaveChanges();
                        }
                    }

                    Message = " پرداخت هزینه  " + entity.Contract.Name;
                }
                else
                {
                    var subId = SubscriptionID - 1128;
                    var Subscription = db.Subscriptions.Find(subId);
                    Price = Convert.ToInt32(Subscription.Price);
                    Message = "پرداخت هزینه  " + Subscription.Title + " خریداری شده از پلتفرم لامینگو  ";
                }
            }
            else
            {

                var reqs = db.ConsultationRequests.Include("Counselor").Where(q => q.OrderID == ConsultationOrderID).ToList();
                Price = Convert.ToInt32(reqs.Count() * reqs[0].Counselor.Price);
                Message = "پرداخت هزینه مشاوره خریداری شده از پلتفرم لامینگو";
            }

            var _Gateway = new GatewayResult() { };

            switch (ipg)
            {
                case "zarinpal":

                    var Status = await Zarin.Gateway.Request(Price, Message, CallBackUrl);

                    //var Status = await _payment.Request(new DtoRequest()
                    //{
                    //    CallbackUrl = CallBackUrl,
                    //    Description = Message,
                    //    Amount = Price,
                    //    MerchantId = Merchant
                    //}, Payment.Mode.zarinpal);

                    //var payment = new ZarinPal.Payment(Merchant, Price);

                    //var Status = await payment.PaymentRequest(Message, CallBackUrl);

                    _Gateway.Status = Status.Status.ToString();
                    _Gateway.TrackingCode = Status.Authority;
                    _Gateway.Link = $"https://zarinpal.com/pg/StartPay/{Status.Authority}";
                    _Gateway.Success = Status.Status == 100;
                    _Gateway.IPG = "ZarinPal";

                    break;

                case "pod":

                    try
                    {
                        var userId = _userManager.GetUserId(HttpContext.User);
                        var poduid = db.UserLogins.Where(u => u.UserId == userId).FirstOrDefault().ProviderKey;
                        try
                        {
                            var id = Podium.Payment.GenerateInvoice(Convert.ToInt64(poduid), Price, Message);
                            var url = Podium.Payment.GenerateURL(id, CallBackUrl);

                            _Gateway.Status = "100";
                            _Gateway.TrackingCode = id.ToString();
                            _Gateway.Link = url;
                            _Gateway.Success = true;
                            _Gateway.IPG = "POD";
                            //hi
                            //salam
                        }
                        catch (Exception e)
                        {
                            _Gateway.Success = false;
                            _Gateway.Status = "امکان اتصال به درگاه پرداخت وجود ندارد";
                        }
                    }
                    catch (Exception)
                    {
                        return Redirect("/Identity/Account/Login");
                    }
                    
                    break;

                default:
                    break;
            }

            Random generator = new Random();
            string Random_PaymentID = generator.Next(0, 999999).ToString("D6");
            string Random_userSubscription = generator.Next(0, 999999).ToString("D7");

            int ID = 0;
            if (SubscriptionID != null)
            {
                var subId = SubscriptionID - 1128;
                var userSubscription = new UserSubscription()
                {
                    ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                    SubscriptionID = subId.GetValueOrDefault(),
                    Time = DateTime.Now,
                    OrderId = Random_userSubscription
                };

                db.UserSubscriptions.Add(userSubscription);
                db.SaveChanges();
                ID = userSubscription.UserSubscriptionID;
            }

            var Log = new PaymentLog()
            {
                ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                UserContractID = (UserContractId == null ? null : (int?)UserContractID),
                UserSubscriptionID = ID != 0 ? (int?)ID : null,
                ConsultationOrderID = !string.IsNullOrEmpty(ConsultationOrderID) ? ConsultationOrderID : null,
                Time = DateTime.Now,
                RequestStatus = _Gateway.Status,
                TrackingCode = _Gateway.TrackingCode,
                IsSuccessful = false,
                ReferenceBank = _Gateway.IPG,
                Amount = Price,
                PaymentID = Random_PaymentID
            };

            db.PaymentLogs.Add(Log);
            db.SaveChanges();

            if (_Gateway.Success) return Redirect(_Gateway.Link);
            else return Content("ErrorStatus: " + _Gateway.Status);

        }

        [Route("Payment/Verify/{ipg}")]
        public async Task<ActionResult> Verify(
            string Authority,
            string Status,
            [FromRoute]string ipg,
            long? invoiceId,
            string layout = null)
        {
            var _VerificationResult = new VerificationResult() { };
            var trackingcode = Authority ?? invoiceId.ToString();
            var userid = _userManager.GetUserId(HttpContext.User);

            var payment = db.PaymentLogs
                .Include("UserContract.AffiliateDetail")
                .Include("UserContract.Contract")
                .Include("UserContract")
                .Where(q => q.TrackingCode == trackingcode && q.ApplicationUserId == userid).FirstOrDefault();

            PaymentViewModel PV = new PaymentViewModel();

            if (payment != null)
            {
                PV.Time = PersianDates.ToPersianString(payment.Time);
                PV.Amount = payment.Amount;
                PV.IsSuccessful = payment.IsSuccessful;
                PV.TrackingCode = payment.TrackingCode;
                PV.UserContractID = payment.UserContractID.GetValueOrDefault();
                PV.UserSubscriptionID = payment.UserSubscriptionID.GetValueOrDefault();
                PV.ConsultationOrderID = payment.ConsultationOrderID;
                PV.PaymentID = payment.PaymentID;
                PV.Title = payment.UserContract != null ? payment.UserContract.Contract.Name : "";
                PV.Bank = payment.ReferenceBank;
                PV.Type = payment.UserContract != null ? payment.UserContract.Contract.Type : 0;
                PV.Subscription = payment.UserSubscriptionID != null ? db.UserSubscriptions.Include("Subscription").Where(q => q.UserSubscriptionID == payment.UserSubscriptionID).FirstOrDefault().Subscription.Title : null;

                switch (ipg)
                {
                    case "zarinpal":
                        if (
                                HttpContext.Request.Query["Authority"] != "" &&
                                HttpContext.Request.Query["Status"] != "" &&
                                (HttpContext.Request.Query["Status"] == "OK" || HttpContext.Request.Query["Status"] == "NOK")
                            )
                        {
                            //var pay = new Zarinpal.Payment(Merchant, Convert.ToInt32(payment.Amount));
                            //var zarres = pay.Verification(Authority);

                            //var verification = await _payment.Verification(new DtoVerification
                            //{
                            //    Amount = Convert.ToInt32(payment.Amount),
                            //    MerchantId = Merchant,
                            //    Authority = Authority
                            //}, Payment.Mode.zarinpal);

                            var verification = await Zarin.Gateway.Verify(Convert.ToInt32(payment.Amount), Authority);

                            _VerificationResult.Success = (verification.Status == 100 || verification.Status == 101) ? true : false;
                            _VerificationResult.Status = verification.Status.ToString();
                        }
                        break;

                    case "pod":
                        try
                        {
                            var _InquiryResult = Podium.Payment.Inquiry(invoiceId ?? 0);
                            var _res = _InquiryResult.Result[0];
                            if (payment.Amount == _res.PayableAmount / 10 && !_res.Closed)
                            {
                                var res = Podium.Payment.VerifyAndCloseInvoice(invoiceId ?? 0);
                                _res = res.Result;
                            }
                            _VerificationResult.Success = _res.Payed && payment.Amount == _res.PayableAmount / 10;
                            _VerificationResult.Status = _res.ReferenceNumber;
                            _VerificationResult.Wallet = _res.Wallet != null;
                        }
                        catch (Exception e)
                        {
                            ViewData["error"] = e;
                            return View(PV);
                        }

                        break;

                    default:
                        return View(PV);
                }

                PV.IsSuccessful = payment.IsSuccessful = _VerificationResult.Success;
                PV.TrackingCode = payment.ResponseCode = _VerificationResult.Status;

                //if (payment.UserContract != null)
                //{
                //    payment.UserContract.PaymentLogs
                //}
                db.SaveChanges();

                if (!string.IsNullOrEmpty(payment.ConsultationOrderID) && _VerificationResult.Success)
                {
                    var reqs = db.ConsultationRequests.Where(w => w.OrderID == payment.ConsultationOrderID).ToList();
                    foreach (var item in reqs)
                    {
                        item.status = ConsultationRequest.Status.Confirm;
                    }

                    db.SaveChanges();
                }

                //Affiliate
                if (payment.UserContract != null && payment.UserContract.AffiliateDetailID != null && _VerificationResult.Success)
                {
                    await AffiliateHelper.Transaction(db, payment.UserContract.Contract, payment.UserContract.AffiliateDetail);
                }
                //Affiliate

                //Affiliate
                if (layout != null)
                {
                    ViewData["title"] = "پرداخت هزینه و دریافت فایل";
                    return View("NewVerify", PV);
                }
                //Affiliate         

            }
            ViewData["error"] = new Exception() { };

            return View(PV);
        }

    }
}