﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DinkToPdf;
using DinkToPdf.Contracts;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Controllers
{


    public class ExportPdfController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _db;  
        private IConverter _converter;

        public ExportPdfController(IConverter converter, IHostingEnvironment hostingEnvironment, UserManager<ApplicationUser> userManager, ApplicationDbContext db)
        {
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
            _db = db;      
            _converter = converter;
        }

        [HttpPost]
        public async Task<string> CreatePdfAsync(string Contract, int userContractID, int PreviousUserContractID)
        {

            SubscribViewModel Result_SubscriptionCheck = Helpers.Subscription.check(_userManager, _db, HttpContext, null);
            string ExternalLinkAddress = Path.Combine(_hostingEnvironment.WebRootPath, "css", "PDF.css?" + DateTime.Now.Ticks);
            var sb = new StringBuilder();

            sb.Append(
                        "<!DOCTYPE html><html>" +
                            "<head>" +
                                "<link rel='stylesheet' href='" + ExternalLinkAddress + "' />" +
                            "</head>" +
                            "<body id='WrapperPDF'>");
            sb.Append(Contract);
            sb.Append(
                      "</body>" +
                      " </html>"
               );

            string folderName = "Uploads";
            string SubFolder = "PdfFiles";
            string SubFolderId = _userManager.GetUserId(HttpContext.User);
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName, SubFolder, SubFolderId);

            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            string FileName = "Contract" + "__" + DateTime.Now.ToString("MM-dd-yyyy") + "__" + DateTime.Now.ToString("HH-mm") + ".pdf";
            string UploadPath = Path.Combine(newPath, FileName);

            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Right = 5, Left = 5, Top = 16, Bottom = 10 },
                DocumentTitle = "PDF Report",
                Out = UploadPath
            };

            //UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css")
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = sb.ToString(),
                WebSettings = { DefaultEncoding = "utf-8", },
                HeaderSettings = { FontName = "IRANSansWeb", FontSize = 9, Line = false, Spacing = 1.8, HtmUrl = Path.Combine(webRootPath, "Templates", "HeaderPDF", "header.html") },
                FooterSettings = { FontName = "IRANSansWeb", FontSize = 9, Left = "صفحه [page] از [toPage]" },
                IncludeInOutline = false
            };


            var UserContract = _db.UserContracts.Include("Contract").Include("PaymentLogs").Where(r => r.UserContractID == userContractID).FirstOrDefault();
            var currentId = _userManager.GetUserId(HttpContext.User);

            if (currentId == UserContract.ApplicationUserId && UserContract != null && UserContract.Contract.Price != null && UserContract.Contract.Price != 0 && (UserContract.PaymentLogs.Count() != 0 && UserContract.PaymentLogs.Any(e => e.IsSuccessful)))
            {
                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);

            }

            else if ((currentId == UserContract.ApplicationUserId && UserContract != null && UserContract.Contract.Price == null || UserContract.Contract.Price == 0)  ||  (currentId == UserContract.ApplicationUserId &&  Result_SubscriptionCheck.Message == "Yes"))
            {

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
            }
           
            UserContract entity = null;
            if (PreviousUserContractID != 0) entity = await _db.UserContracts.FindAsync(PreviousUserContractID);
            else entity = await _db.UserContracts.FindAsync(userContractID);

            switch (entity.status)
            {
                case UserContract.Status.Pending:

                    entity.status = UserContract.Status.Complete;
                    entity.EditCount = 0;

                    var usercontract = new UserContract
                    {
                        OrderId = "0 " + "/ " + entity.OrderId,
                        Time = DateTime.Now,
                        ContractID = entity.ContractID,
                        ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                        SelectedAnswer = entity.SelectedAnswer,
                        ArtsID = entity.ArtsID,
                        EditTime = DateTime.Now,
                        ParentID = entity.UserContractID,
                        EditCount = 0,
                        status = UserContract.Status.Complete,
                        Content = FileName,                    
                        UserContractProperties = entity.UserContractProperties,
                        Word = entity.Word,
                    };

                    _db.UserContracts.Add(usercontract);
                    _db.SaveChanges();

                    break;

                case UserContract.Status.Complete:

                    // check word file 
                    var child = _db.UserContracts.Where(q => q.ParentID == entity.UserContractID).FirstOrDefault();
                    if (child == null)
                    {
                        var FirstParent = UserContracts.GetParents(_userManager, _db, HttpContext, PreviousUserContractID).FirstOrDefault();
                        if (FirstParent.EditCount <= 2)
                        {
                            var newUserContract = _db.UserContracts.Where(w => w.ParentID == FirstParent.UserContractID).LastOrDefault();
                            _db.UserContracts.Find(PreviousUserContractID).EditCount = null;
                            if (newUserContract != null)
                            {
                                newUserContract.Content = FileName;
                                _db.Update(entity);
                            }
                            _db.SaveChanges();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(child.Word))
                        {
                            goto case UserContract.Status.Pending;
                        }
                       
                    }
                   

                    break;
                default:
                    break;
            }

            _db.Entry(entity).State = EntityState.Modified;
            _db.SaveChanges();

            int? usage = null;
            int RemainingCount = 0;
            if (_db.Contract.Find(entity.ContractID).Price != 0 && _db.Contract.Find(entity.ContractID).Price != null)
            {              
                if (Result_SubscriptionCheck.Message == "Yes")
                {
                    var UserSubscription = _db.UserSubscriptions.Find(Result_SubscriptionCheck.UserSubscriptionId);

                    if (UserSubscription.UsageCount != null)
                    {
                        UserSubscription.UsageCount += 1;
                    }
                    else
                    {
                        UserSubscription.UsageCount = 1;
                    }

                    usage = UserSubscription.UsageCount;
                    RemainingCount = UserSubscription.Subscription.Count - usage.GetValueOrDefault();
                    _db.SaveChanges();
                }
            }

            var result = FileName + "*" + _userManager.GetUserId(HttpContext.User) + "*" + (usage != null ? RemainingCount.ToString() : null);
            return result;
        }

    }
}