﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LawMingo.Controllers
{
    public class CouponController : Controller
    {

        private ApplicationDbContext _db { get; set; }

        public CouponController(ApplicationDbContext db)
        {
            _db = db;
        }

        public JsonResult Check(string DiscountCode , int userContractId)
        {

            decimal? PriceWithDiscount = null;        
            bool checkCategory = false;
            bool Confirm = false;

            var Price = _db.UserContracts.Include("Contract").Where(q => q.UserContractID == userContractId).FirstOrDefault().Contract.Price;

            var result = _db.Coupons.Where(q => q.Name.ToLower().Trim() == DiscountCode.ToLower().Trim() && q.ParentID == null).FirstOrDefault();
            if(result != null && result.End > DateTime.Now && result.IsActive && result.Count > 0)
            {

                var Discount = (Price * result.Amount) / 100;
                  
                switch (result.discountType)
                {
                 
                    case Models.DomainModel.Coupon.DiscountType.Percent:   PriceWithDiscount = Price - Discount;                                                                
                        break;

                    case Models.DomainModel.Coupon.DiscountType.Static:    PriceWithDiscount = Price - result.Amount;                        
                        break;

                    default:
                        break;
                }

                if (result.CategoryID != null && result.CategoryID != 0)
                {
                    checkCategory = true;
                    var contId = _db.UserContracts.Find(userContractId).ContractID;
                    Confirm    = _db.Contract.Where(w => w.CategoryAttachs.Any(t => t.CategoryID == result.CategoryID)).Any(w => w.ContractID == contId);                      
                }
            }
          
            return Json(
                new
                  {
                       coupon = result,
                       FinalPrice = PriceWithDiscount ,
                       discountCode = result != null ?  result.Name.ToLower() : null ,
                       matchCategory = !checkCategory  || checkCategory && Confirm ? true : (checkCategory && !Confirm ? false : false) ,
                       Category = result != null && result.CategoryID != null ? _db.Categories.Find(result.CategoryID).Name : ""
                });
                  }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

    }
}