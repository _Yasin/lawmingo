﻿
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Spire.Doc;
using Spire.Doc.Documents;

namespace LawMingo.Controllers
{
    public class ExportWordController : Controller
    {

        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _db;

        public ExportWordController(IHostingEnvironment hostingEnvironment, UserManager<ApplicationUser> userManager, ApplicationDbContext db)
        {
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
            _db = db;                  
        }

        public async Task<string> CreateWordAsync(string Contract, int userContractID)
        {

           
            string LData = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8TGljZW5zZSBLZXk9ImNyOERaN1hKMkR5MUs2UUJBTkRPSVRLdlpjTzZkelVod2lsSHBnVlluQ3k0cXlHV2V6TFZubFJGeFAxNU1mSWZnUmdNWm1XaEdOQWRFNFRqZWZnQ1ovbFR2b1BkSXRIbDZXdDVBNWk1TVhFbnFkQnVPMUthRnovRFFzYUdWTGhzdjlySG1ybnRxSElFRGxJeGRxYUpNcGtLb0Frd1A3d1N6T01KMVkrbUNmVTVVRmV6REwvTjd1enJ4M0Y0d2I1SGErd0E2VFQ5VFJ3SzAzejlFS01aRmwzU1lSL3o0YVU3TE0wZFNYWTlqU0ZKZ2dqZlZzRFVLaUJyVm5td1ljaXVyOUVrYmw5Q3RaWTAzdG1yZm01QlplKzZnaHRFTm4wb2gzMzh0WlJleWpjcjc0QWs3MWhnWWtuTE9CQzE1VllmalhzcXVBVW13MlI2TWNWMlBPT2JyY1RSYlhBZ3pvUWJPeWQ4U2JFWmN3aE43NktQd1dzUVFTMUowdGlZSFVLeE9tMnQ0ZkJWMGhQVmhhOUI4Y0swNHFKUVp0MDBaMWNKRGEwd2I4VWx6RWs5QkhVVzJlbk9mVDE0UnlIQ2krWUdlbVBLY2RDUXJoMXpyWVRGN0ltb0x4N3h1NGV2RFRZc2xzV0JrbFFJb3g4NnJWckVVa1N0dXErQUNTWS9xVTM5L1Zhd3Y5S0FmUjVUZUVicGt3RGhTYjBOQkFqVDhBeXRsRFZkR2ZpZzBxS0czVllpVHBYRnc1cHRMVmgrYmtkK2RnN3Z4dHZyNDVaVVdKZXlyekdOR0g3YUZZZDZwLzJNRy9YSlRsR3ovU05RbzJDUExraU83SlhuOU5HZXhaN3BIbTBkZ3pNWmJHRVhxVmR2bG04MTJhL1hMMVNxeEdVWStvNVpsVUM3WTV4Z2dhRCtGZVA5enpoeUpxSUVwcDk3My9ScTRteG1wQWZMcVNzTzJSeHlTcStpdjFDc3AwQ3JvMDc4OEhybDFteWt4dVQweWRSWVpDNkRTeDhNMi9MWTNkOXNud3U3NkFmYjVDOVF1ZE9Zc0wzREh2aGZncmNVSWUvcUhmVFo5QWF6Y3pUanlyM2RPQkFjczBLZk12Y0xVUzRSeHZDdW1NNDVyNDJnMXJ3UGluN2JBcmYvZnNMTzZtS3g0WWRoSURNWlF6V3RjbkhFSTF5TXJ6aU9pdXhMdE8xalRBV25uU2VLVDJ0cXI3Tm42Qmg5TURHNjZZK2lJaW4xV05TUCtMdDFYdXRkajNKTyt4b1FNUVB5ZFpoZkJYZXpVMEhRMnd0eEdwdzRNczRTMTVJbFg1TEdXR3dXeUdYTWNjVWd3b1RDeFRGYmgyZFo0Vkg3OVZHTEVFR1JRWEZrNTRBdlFLdFBpdUcxY0w4RFo3WEoyRHkxSzZUUWVORE9YeFl2NFNveitCMHNBS0VwTVRrNCtTYWpYNksrSjlUOFhZVXRTOE8wWWZGUFZqZkhIYTZORWQyODdVcUlqMnJnQlF1bjVDV3hCczFHUm5BYmd1Z3MyL2ZQakcwZmdQemdSYzR5Q3ZObFg4V2pKUnloc3U5VFRKTjd1R3NOdnprU2IyZWlyQmhEaG1vQ0Jqa0wyYnMzT3I2d2pnNnBUNVpmNGhEdDF0STBJNXo1aytxQXVSZnRhd1lmamhXYmpMS0xKOTlUVk1kRDZaTCtTenNtQkNWN05lYm96V0RUTWgrRnJPT292R09ZbUk1bWp4Smd1MVRXNnI1V0JUK2oxSjBFNmJIb2tEMWo0Wm1DWUQreVBPUW1PMm1yUTNGdC9jVmZwQWlJdzliRkgwZ1FIbXQ4QnNuZnQ2MVV3c1h6cSs2akNvY1hOOUMvRXZPblhTczZuVlNGSkVBL3l1QmNIazZxOWdqanBnRG1NTEcrNlpxR1VjRWMzZEp2THpuK3pNT0p3TDI4WUQxN3BLSXBUNnd6WFBFVFJwWS9qNHhoMkQvaFhJRVNHcTk1eTVmZE9MNmx1QT09IiBWZXJzaW9uPSI5LjkiPgogICAgPFR5cGU+UnVudGltZTwvVHlwZT4KICAgIDxVc2VybmFtZT5Vc2VyTmFtZTwvVXNlcm5hbWU+CiAgICA8RW1haWw+ZU1haWxAaG9zdC5jb208L0VtYWlsPgogICAgPE9yZ2FuaXphdGlvbj5Pcmdhbml6YXRpb248L09yZ2FuaXphdGlvbj4KICAgIDxMaWNlbnNlZERhdGU+MjAxNi0wMS0wMVQxMjowMDowMFo8L0xpY2Vuc2VkRGF0ZT4KICAgIDxFeHBpcmVkRGF0ZT4yMDk5LTEyLTMxVDEyOjAwOjAwWjwvRXhwaXJlZERhdGU+CiAgICA8UHJvZHVjdHM+CiAgICAgICAgPFByb2R1Y3Q+CiAgICAgICAgICAgIDxOYW1lPlNwaXJlLk9mZmljZSBQbGF0aW51bTwvTmFtZT4KICAgICAgICAgICAgPFZlcnNpb24+OS45OTwvVmVyc2lvbj4KICAgICAgICAgICAgPFN1YnNjcmlwdGlvbj4KICAgICAgICAgICAgICAgIDxOdW1iZXJPZlBlcm1pdHRlZERldmVsb3Blcj45OTk5OTwvTnVtYmVyT2ZQZXJtaXR0ZWREZXZlbG9wZXI+CiAgICAgICAgICAgICAgICA8TnVtYmVyT2ZQZXJtaXR0ZWRTaXRlPjk5OTk5PC9OdW1iZXJPZlBlcm1pdHRlZFNpdGU+CiAgICAgICAgICAgIDwvU3Vic2NyaXB0aW9uPgogICAgICAgIDwvUHJvZHVjdD4KICAgIDwvUHJvZHVjdHM+CiAgICA8SXNzdWVyPgogICAgICAgIDxOYW1lPklzc3VlcjwvTmFtZT4KICAgICAgICA8RW1haWw+aXNzdWVyQGlzc3Vlci5jb208L0VtYWlsPgogICAgICAgIDxVcmw+aHR0cDovL3d3dy5pc3N1ZXIuY29tPC9Vcmw+CiAgICA8L0lzc3Vlcj4KPC9MaWNlbnNlPg==";
            Spire.License.LicenseProvider.SetLicenseKey(LData);
            SubscribViewModel Result_SubscriptionCheck = Helpers.Subscription.check(_userManager, _db, HttpContext, null);

            string ExternalLinkAddress = Path.Combine(_hostingEnvironment.WebRootPath, "css", "Word.css?" + DateTime.Now.Ticks);
            var sb = new StringBuilder();

            sb.Append(
                        "<!DOCTYPE html><html>" +
                            "<head>" +
                                "<link rel='stylesheet' href='" + ExternalLinkAddress + "' />" +
                            "</head>" +
                            "<body id='WrapperPDF' dir='rtl' style='direction:rtl !important;'>");
            sb.Append(Contract);
            sb.Append(
                      "</body>" +
                      " </html>"
               );


            string folderName = "Uploads";
            string SubFolder = "wordFiles";
            string SubFolderId = _userManager.GetUserId(HttpContext.User);
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName, SubFolder, SubFolderId);

            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            string FileName = "Contract" + "__" + DateTime.Now.ToString("MM-dd-yyyy") + "__" + DateTime.Now.ToString("HH-mm") + ".docx";
            string UploadPath = Path.Combine(newPath, FileName);


            var UserContract = _db.UserContracts.Include("Contract").Include("PaymentLogs").Where(r => r.UserContractID == userContractID).FirstOrDefault();
            var currentId = _userManager.GetUserId(HttpContext.User);

            if (
                
                currentId == UserContract.ApplicationUserId &&
                UserContract != null &&
                UserContract.Contract.Price != null && 
                UserContract.Contract.Price != 0 && 
                (UserContract.PaymentLogs.Count() != 0 &&              
                UserContract.PaymentLogs.Any(e => e.IsSuccessful))
              
                )
            {
                Document doc = new Document();
                Spire.Doc.Section section = doc.AddSection();
                Spire.Doc.Documents.Paragraph Para = section.AddParagraph();
                Para.AppendHTML(sb.ToString());
                Para.Format.PageBreakBefore = true;
                Para.Format.IsBidi = true;
                doc.SaveToFile(UploadPath, FileFormat.Docx);
            }
            else if((currentId == UserContract.ApplicationUserId && UserContract != null && UserContract.Contract.Price == null || UserContract.Contract.Price == 0) || (currentId == UserContract.ApplicationUserId && Result_SubscriptionCheck.Message == "Yes"))
            {
                try
                {
                    Document doc = new Document();
                    Spire.Doc.Section section = doc.AddSection();
                    Spire.Doc.Documents.Paragraph Para = section.AddParagraph();
                    Para.AppendHTML(sb.ToString());
                    Para.Format.PageBreakBefore = true;
                    Para.Format.IsBidi = true;
                    doc.SaveToFile(UploadPath, FileFormat.Docx);
                }
                catch (Exception )
                {

                    throw;
                }
                    
            }

        
            var entity = await _db.UserContracts.FindAsync(userContractID);
            entity.status = UserContract.Status.Complete;

            var usercontract = new UserContract
            {
                OrderId = "0 " + "/ " + entity.OrderId,
                Time = DateTime.Now,
                ContractID = entity.ContractID,
                ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                SelectedAnswer = entity.SelectedAnswer,
                ArtsID = entity.ArtsID,
                EditTime = DateTime.Now,
                ParentID = entity.UserContractID,
                EditCount = 0,
                status = UserContract.Status.Complete,             
                UserContractProperties = entity.UserContractProperties,
                Word = FileName,
            };

            _db.UserContracts.Add(usercontract);
            _db.SaveChanges();


            int? usage = null;
            int RemainingCount = 0;
            if (_db.Contract.Find(entity.ContractID).Price != 0 && _db.Contract.Find(entity.ContractID).Price != null)
            {
               
                if (Result_SubscriptionCheck.Message == "Yes")
                {
                    var UserSubscription = _db.UserSubscriptions.Find(Result_SubscriptionCheck.UserSubscriptionId);

                    if (UserSubscription.UsageCount != null)
                    {
                        UserSubscription.UsageCount += 1;
                    }
                    else
                    {
                        UserSubscription.UsageCount = 1;
                    }

                    usage = UserSubscription.UsageCount;
                    RemainingCount = UserSubscription.Subscription.Count - usage.GetValueOrDefault();
                    _db.SaveChanges();
                }
            }

            var result = FileName + "*" + _userManager.GetUserId(HttpContext.User) + "*" + (usage != null ? RemainingCount.ToString() : null);
            return result;

          
        }
    }
}