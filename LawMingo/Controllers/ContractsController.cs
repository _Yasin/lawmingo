﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using LawMingo.Data;
using LawMingo.Models.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace LawMingo.Controllers
{
    public class ContractsController : Controller
    {

        private ApplicationDbContext _db { get; set; }
        private readonly UserManager<ApplicationUser> _userManager;

        public ContractsController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        public IActionResult List()
        {
            var entry = new ContractsViewModel
            {
                Categories = _db.CategoryAttachment.Include("Category").Where(q => q.ContractID != null).Select(r => r.Category).Distinct().ToList(),
                Contracts  = _db.Contract.Include("CategoryAttachs").Where(w => w.IsVisible).ToList().OrderBy(e => e.Order)               
            };    
            
            return View(entry);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult customRequest(ContractRequest request)
        {
            if (ModelState.IsValid)
            {               
                try
                {
                    request.Time = DateTime.Now;
                     _db.ContractRequests.AddAsync(request);
                     _db.SaveChangesAsync();
                    return Json("درخواست مورد نظر با موفقیت ثبت شد.");
                }
                catch (Exception)
                {
                    return Json("خطایی در هنگام پردازش اطلاعات رخ داده است لطفا دقایقی دیگر مجددا اقدام نمائید .");
                    throw;
                }
            }
            else
            {
                return  Json("عدم ثبت صحیح اطلاعات !");
            }          
        }

    }
}