﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models;
using LawMingo.Models.DomainModel;
using LawMingo.Models.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using X.Web.Sitemap;

namespace LawMingo.Controllers
{
    public class HomeController : Controller
    {
   
        private ApplicationDbContext _db { get; set; }
        private readonly UserManager<ApplicationUser> _userManager;
        public HomeController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)  
        {
            _db = db;
            _userManager = userManager;
        }

        [Route("/sitemap.xml")]
        public async Task<IActionResult> SitemapXml()
        {
            Response.ContentType = "application/xml";
            var contracts = await _db.Contract.ToListAsync();
            var sitemap = new Sitemap();

            foreach (var item in contracts)
            {
                sitemap.Add(new Url
                {
                    ChangeFrequency = ChangeFrequency.Monthly,
                    Location = $"https://lawmingo.com/Contract-{item.ContractID}/{item.Name.URLFriendly()}/{item.EnglishName.URLFriendly()}",
                    Priority = 0.5,
                    TimeStamp = DateTime.Now
                });
            }
            sitemap.Add(CreateUrl("https://lawmingo.com/Consultation"));
            sitemap.Add(CreateUrl("https://lawmingo.com/Subscription"));
            sitemap.Add(CreateUrl("https://lawmingo.com/Mag"));
            sitemap.Add(CreateUrl("https://lawmingo.com/Contracts/List"));
            sitemap.Add(CreateUrl("https://lawmingo.com/Home/About"));
            sitemap.Add(CreateUrl("https://lawmingo.com/Home/Terms"));
            sitemap.Add(CreateUrl("https://lawmingo.com/Home/ContactUs"));

            return Content(sitemap.ToXml());

        }

        private static Url CreateUrl(string url)
        {
            return new Url
            {
                ChangeFrequency = ChangeFrequency.Monthly,
                Location = url,
                Priority = 0.5,
                TimeStamp = DateTime.Now
            };
        }
        public IActionResult Index()
        {

            //var www = _db.PaymentLogs.Where(w => w.IsSuccessful && w.UserSubscriptionID == null && w.ConsultationRequestID == null && w.UserContractID != null && w.UserContract.ApplicationUserId != "77455567-e014-4379-9fd0-6b313ed922bc").ToList();

            //var result =  www.Sum(e => e.Amount);
            //var ttt = _db.UserContracts.Where(q => q.ApplicationUserId == _userManager.GetUserId(HttpContext.User)).Include("PaymentLogs").ToList();

            //foreach (var item in ttt)
            //{
            //    if(item.PaymentLogs.Count > 0)
            //    {
            //        var eee = _db.PaymentLogs.Where(q => q.UserContractID == item.UserContractID).ToList();

            //        foreach (var item1 in eee)
            //        {
            //            _db.Remove(item1);
            //            _db.SaveChanges();
            //        }

            //    }

            //}
            //var eee = _db.UserContracts.Where(r => r.PaymentLogs.Any(t => t.IsSuccessful)).ToList();
            ////var www = _db.PaymentLogs.(w => w.UserContractID).Sum(i => i.Amount).ToString();
            //var qqq = _db.PaymentLogs.Where(w => w.IsSuccessful && w.UserContractID != null);

            //var cats = _db.PaymentLogs.Where(w => w.IsSuccessful && w.ConsultationRequestID != null)
            // .Select(i => new { i.Amount })


            //      .ToList().Sum(y => y.Amount);


            //var ttt = _db.Contract
            //      //.Include(r => r.Articles)
            //      .Select(s => new Contract {
            //          Name = s.Name,
            //          Price = s.Price,
            //          Articles = s.Articles.Select(b => new Article {
            //              Name = b.Name,
            //              ArticleID = b.ArticleID,
            //              Order = b.Order,
            //              Questions = b.Questions.Select(n => new Question {
            //                  MyQuestion = n.MyQuestion,
            //                  Time = n.Time,
            //                  Answers = n.Answers.Select(q => new Answer {
            //                      MyAnswer = q.MyAnswer,
            //                      ArticleDetails = q.ArticleDetails.Select(o => new ArticleDetail {
            //                          ArticleContent = o.ArticleContent,
            //                          ArticleDetailID = o.ArticleDetailID
            //                      })
            //                  })
            //              })
            //          })
            //      }).ToList();

            //var arts =   _db.ArticleDetails.ToList();

            //foreach (var item in arts)
            //{
            //    var ee = item.ArticleContent.Split("===");
            //    foreach (var item2 in ee)
            //    {
            //        if(item2 != "<p dir=\"RTL\">" && item2 != "" && item2 != "<p>")
            //        {
            //            var entity = new ArtsDetail
            //            {
            //                AnswerID = item.AnswerID,
            //                Content = item.ArticleContent,
            //                Time = DateTime.Now
            //            };
            //            _db.ArtsDetails.Add(entity);
            //            _db.SaveChanges();
            //        }
                      
            //    }
            //}
          
           
          

            return View();
        }

        public async Task<ActionResult> HomeData()
        {
             var _contracts =  _db.Contract.Where(q => q.IsVisible);
             //var contracts = await _contracts.GroupBy(
             //    p => p.Category,
             //    (key, g) => new ContractViewModel
             //    {
             //        CategoryName = key.Name,
             //        contract = g.Select(r => new ContractSelectedViewModel
             //        { ContractID = r.ContractID, EnglishName = r.EnglishName, Name = r.Name , Price = r.Price.GetValueOrDefault()})
             //    }).ToListAsync();

            var Contracts = _contracts.OrderBy(q => q.Order).Select(q => new
            {
                 q.Price,
                 q.Type,
                 q.ContractID,
                 q.EnglishName,                              
                 q.Name                
            });

            var Subscription = _db.Subscriptions.OrderByDescending(q => q.SubscriptionID).ToList();

            return Json(new { Contracts, Subscription });
        }

        //public async Task<ActionResult> FreeContract()
        //{
        //    var _contracts = _db.Contract.Where(q => q.Price  == 0);
        //    var contracts = await _contracts.GroupBy(
        //        p => p.Category,
        //        (key, g) => new ContractViewModel
        //        {
        //            CategoryName = key.Name,
        //            contract = g.Select(r => new ContractSelectedViewModel
        //            { ContractID = r.ContractID, EnglishName = r.EnglishName, Name = r.Name, Price = r.Price.GetValueOrDefault() })
        //        }).ToListAsync();
        //    return Json(new { contracts });
        //}


        [HttpPost]
        public async Task<ActionResult> ComplaintRegister(Complaint complaint)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    complaint.Time = DateTime.Now;

                    await _db.Complaints.AddAsync(complaint);
                    await _db.SaveChangesAsync();
                    TempData["success"] = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
           
            return View("Complaint");
        }


        [HttpPost]
        public async Task<ActionResult> ContactRegister(ContactUs Contact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Contact.Time = DateTime.Now;

                    await _db.Contacts.AddAsync(Contact);
                    await _db.SaveChangesAsync();
                    TempData["success"] = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }

            return View("Complaint");
        }

        public IActionResult Terms()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Guide()
        {
            return View();
        }

        public IActionResult Complaint()
        {
            return View();
        }

        public IActionResult ContactUs()
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}