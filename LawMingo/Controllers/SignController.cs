﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DinkToPdf.Contracts;
using LawMingo.Data;
using LawMingo.Helpers;
using LawMingo.Models.DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using MimeKit;
using Microsoft.AspNetCore.Identity.UI.Services;
using static LawMingo.Models.ViewModel.SmsViewModel;
using Microsoft.EntityFrameworkCore;
using LawMingo.Models.ViewModel;
using System.Web;

namespace LawMingo.Controllers
{
    public class SignController : Controller
    {

        private static readonly string Token = "525043434C35686441787436486F596E71467A56452B62776E6B504D453844716C34725469676A335758733D";
        private static readonly string BaseURL = "https://api.kavenegar.com/v1/" + Token;
        private static readonly string verifyURL = BaseURL + "/verify/lookup.json";


        private readonly IHostingEnvironment _env;
        private readonly ApplicationDbContext _db;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConverter _converter;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;

        public SignController(

            SignInManager<ApplicationUser> signInManager,
            IConverter converter,
            IHostingEnvironment env,
            ApplicationDbContext db,
            UserManager<ApplicationUser> userManager,
            IHttpContextAccessor httpContextAccessor,
            IEmailSender emailSender
            )
        {

            _db = db;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _env = env;
            _converter = converter;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }

        public IActionResult Pdf()
        {            
            signatureViewModel sign = new signatureViewModel();
            return View(sign);
        }


        [HttpPost]    
        [AjaxOnly]
        //[Authorize]
        [ValidateAntiForgeryToken]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Upload(IFormFile file, string Type, string base64PDF , string SignFile , string UserFile)
        {

            string FileName = "";
            string filename = base64PDF == null ? ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"') : "";
            filename = base64PDF == null ? EnsureCorrectFilename(filename) : "";
            string userId = _userManager.GetUserId(HttpContext.User);
       

            try
            {
                FileName = GetPathAndFilename(filename, Type, userId);
            
                if (base64PDF != null)
                {
                    var base64_PDF = base64PDF.Replace("data:application/pdf;base64,", "");
                    var Base64String = Convert.FromBase64String(base64_PDF);

                    using (var fs = new FileStream(FileName, FileMode.Create))
                    using (var writer = new BinaryWriter(fs))
                    {
                        writer.Write(Base64String, 0, Base64String.Length);
                        writer.Close();
                    }

                    var Entity = new Signature
                    {
                        ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                        PDF = UserFile,
                        Sign = SignFile,
                        FinalFile = FileName.Split("\\").Last(),
                        RegisterTime = DateTime.Now
                    };

                    _db.Signatures.Add(Entity);
                    _db.SaveChanges();                  
                }
                else
                {
                    
                    using (FileStream output = System.IO.File.Create(FileName))
                        await file.CopyToAsync(output);
                                
                }
            }
            catch (Exception)
            {
                return Json(new { error = true, file = FileName, Type });
            }

            return Json(new { error = false, file = FileName.Split("\\").Last(), Type , id = userId});
        }


        [ValidateAntiForgeryToken]
        [HttpPost]
        public  async Task<JsonResult> SendConfirmCode(string mobile)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    var user = await _userManager.GetUserAsync(HttpContext.User);
                    var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, mobile);
                    var Result = SMS(mobile, code, "VerifySignCode", "primary");

                    var smsLog = new SmsLog
                    {
                        cost = Result.entries.FirstOrDefault().cost,
                        date = Result.entries.FirstOrDefault().date,
                        EntriyMessage = Result.entries.FirstOrDefault().message,
                        messageid = Result.entries.FirstOrDefault().messageid,
                        Receptor = Result.entries.FirstOrDefault().receptor,
                        statustext = Result.entries.FirstOrDefault().statustext,
                        EntriyStatus = Result.entries.FirstOrDefault().status,
                        sender = Result.entries.FirstOrDefault().sender,
                        Template = "VerifySignCode",
                        Time = DateTime.Now,
                        Usage = "primary",
                        ReturnMessage = Result.@return.message,
                        ReturnStatus = Result.@return.status,
                    };

                    _db.SmsLogs.Add(smsLog);
                    _db.SaveChanges();
                    return Json("OK");                
                }
                catch (Exception)
                {
                    return Json("NOK");
                }
            }
            else
            {
                return Json("NoAuth");
            }

          
        }



        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<JsonResult> VerifyNationalCode(string NationalCode)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {

                    //NationalCode.Verify(NationalCode , );
                    return Json("OK");
                }
                catch (Exception)
                {
                    return Json("NOK");
                }
            }
            else
            {
                return Json("NoAuth");
            }


        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> VerifyPhoneNumber(string Code , string mobile)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {
                    var user    = await _userManager.GetUserAsync(HttpContext.User);
                    var Confirm = await _userManager.VerifyChangePhoneNumberTokenAsync(user, Code, mobile);
                    if (Confirm)
                    {
                        var entity = new signatureShare
                        {
                            ApplicationUserId = _userManager.GetUserId(HttpContext.User),
                            ConfirmSendContractLink = false,
                            EditTime = DateTime.Now,
                            EmailResever = "",
                            FullNameRecever = "",
                            FullNameSender = "",
                            MobilResever = "",
                            MobilSender = mobile,
                            NationalCode = "",
                            NationalCodeConfirmed = false,
                            PhoneNumberConfirmed = true,
                            RegisterTime = DateTime.Now,
                            EmailSender = ""
                        };

                        _db.signatureShares.Add(entity);
                        _db.SaveChanges();
                        return Json("OK");
                    }
                    return Json("FaildVerifyPhoneNumber");                  
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                return Json("NoAuth");
            }           
        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  async Task<JsonResult>  SendContractLink(signatureViewModel Data)
        {

                var signatureShare = _db.signatureShares.AsNoTracking()
                           .Where(q => q.ApplicationUserId == _userManager.GetUserId(HttpContext.User) &&
                           q.MobilSender.Trim() == Data.MobilSender.Trim()).FirstOrDefault();

                 var Result =  NationalCode.Verify(Data.MobilSender.Trim(), Data.NationalCode);

            if (
                     signatureShare == null || 
                     //signatureShare.PhoneNumberConfirmed == false ||
                     signatureShare.MobilSender.Trim() != Data.MobilSender.Trim()
                  )
                  {
                      return Json("PhoneNumberConfirmed");
                  }
               
                if (ModelState.IsValid)
                {
                    
                     signatureShare.ConfirmSendContractLink = Data.ConfirmSendContractLink;
                     signatureShare.EditTime = DateTime.Now;
                     signatureShare.RegisterTime = DateTime.Now;
                     signatureShare.EmailResever = Data.EmailResever;
                     signatureShare.EmailSender  = Data.EmailSender;
                     signatureShare.FullNameRecever = Data.FullNameRecever;
                     signatureShare.FullNameSender = Data.FullNameSender;
                     signatureShare.MobilResever = Data.MobilResever;
                     signatureShare.MobilSender = Data.MobilSender;
                     signatureShare.NationalCode = Data.NationalCode;
                     signatureShare.NationalCodeConfirmed = true;
                     signatureShare.PhoneNumberConfirmed = true;  
                
                     _db.signatureShares.Attach(signatureShare);
                     _db.Entry(signatureShare).State = EntityState.Modified;
                     _db.SaveChanges();


                var ReseverUser = new ApplicationUser { 
                    UserName = signatureShare.MobilResever,
                    PhoneNumber = signatureShare.MobilResever,
                    Email = Data.EmailResever,
                    Register = DateTime.Now,                    
                };

                var result = await _userManager.CreateAsync(ReseverUser, "123456");

                if (result.Succeeded)
                {
                    //var user = await _userManager.GetUserAsync(HttpContext.User);
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(ReseverUser);
                    string codeHtmlVersion = HttpUtility.UrlEncode(code);
                 
                    string callbackUrl = "https://localhost:5001/Sign/Confirm?userId=" +
                                          ReseverUser.Id + "&code=" + codeHtmlVersion;

                    var pathToFile = _env.WebRootPath
                               + Path.DirectorySeparatorChar.ToString()
                               + "Templates"
                               + Path.DirectorySeparatorChar.ToString()
                               + "SginTemplate"
                               + Path.DirectorySeparatorChar.ToString()
                               + "ContractLink.html";

                    var subject = "لینک قرارداد امضاء شده در پلتفروم لامینگو";

                    var builder = new BodyBuilder();
                    using (StreamReader SourceReader = System.IO.File.OpenText(pathToFile))
                    {
                        builder.HtmlBody = SourceReader.ReadToEnd();
                    }

                    string messageBody = string.Format(builder.HtmlBody,
                            subject,
                            Data.FullNameRecever,
                            Data.FullNameSender,
                            Data.NationalCode,
                            Data.MobilSender,
                            DateTime.Now.ToPersianDateTime().ToString().ToPersianNumber(),
                            callbackUrl
                            );

                    await _emailSender.SendEmailAsync(Data.EmailResever, subject, messageBody);
                } 
            }
            return Json("");
        }
  
        public async Task<IActionResult> Confirm(string userId, string code)
        {
            //if (userId == null || code == null)
            //{
            //    return Redirect("Error");
            //}

            //var user = await _userManager.FindByIdAsync(userId);
            //if (user == null)
            //{
            //    return NotFound($"Unable to load user with ID '{userId}'.");
            //}

            //var result = await _userManager.ConfirmEmailAsync(user, code);
            //if(result.Succeeded)
            //{
            //    return View();
            //}
            //else
            //{
            //    // throw new InvalidOperationException($"Error confirming email for user with ID '{userId}':");
            //    foreach (var error in result.Errors)
            //    {
            //        ModelState.AddModelError(string.Empty, error.Description);
            //    }
            //}

            var Result = NationalCode.Verify("09191765110", "0078760984");

            return View();

        }


        public async Task<IActionResult> UserConfirm(ConfirmUserViewModel info)
        {

            if(ModelState.IsValid)
            {

            }
            return Json("");

        }




        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetPDF()
        {

                   string webRootPath = _env.WebRootPath;
                   string folderName = "Uploads";
                   string SubFolder = "Sign";
                   string userId = _userManager.GetUserId(HttpContext.User);
                   string Destination = "Final";
                   
                 try
                     {
                        var Signatures = _db.Signatures.Where(q => q.ApplicationUserId == userId).Last();
                        string newPath = Path.Combine(webRootPath, folderName, SubFolder, userId, Destination , Signatures.FinalFile);
                        var memory = new MemoryStream();
                        using (var stream = new FileStream(newPath, FileMode.Open))
                        {
                            await stream.CopyToAsync(memory);
                        }
                        memory.Position = 0;

                        Response.Clear();
                        Response.ContentType = "application/pdf";
                        //Response.Headers.Add("Content-Disposition", "attachment; filename=\"" + "filnam" + "\"");
                        return File(memory, "application/pdf");
                    }
                    catch(Exception e)
                    {
                        throw;
                    }          
        }

        private RootObject SMS(string mobile, string Code, string Template, string Type)
        {

            RootObject Result = null;
            string responseContent = null;

            var request = WebRequest.Create(verifyURL + String.Format("?receptor={0}&token={1}&template={2}", mobile, Code, Template)) as HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            try
            {
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }

            catch (WebException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            }

            if (responseContent != null)
            {
                Result = JsonConvert.DeserializeObject<RootObject>(responseContent);
            }
          
            return Result;
        }
        private string EnsureCorrectFilename(string filename)
        {
            if (filename.Contains("\\"))
                filename = filename.Substring(filename.LastIndexOf("\\") + 1);
            return filename;
        }
        private string GetPathAndFilename(string filename, string Type , string userId)
        {
            string webRootPath = _env.WebRootPath;
            string folderName  = "Uploads";
            string SubFolder   = "Sign";
            string Destination = "";
            switch (Type)
            {
                case "PDF":
                    Destination = "PDF";                 
                    break;
                case "SIGN":
                    Destination = "Signature";
                    break;
                case "":
                    Destination = "Share";
                    break;
                case "Final":
                    Destination = "Final";
                    break;
                default:                   
                    break;
            }          
            string newPath = Path.Combine(webRootPath, folderName, SubFolder, userId, Destination);
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            Random generator = new Random();
            string RandomId = generator.Next(0, 999999).ToString("D8");

            var fileNameWithoutExt = Path.GetFileNameWithoutExtension(filename);
            var extention = Path.GetExtension(filename);

            var FileName = fileNameWithoutExt + "_" + RandomId + (Destination == "Share" || Destination == "Final" ? ".pdf" :  extention);
            return Path.Combine(newPath, FileName);
        }
    }
}