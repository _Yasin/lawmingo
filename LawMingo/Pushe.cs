﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
namespace LawMingo
{
    public static class Pushe
    {
        public static void Send(string title, string desc, string url)
        {

            string responseContent = null;
            var request = WebRequest.Create("https://api.pushe.co/v2/messaging/notifications/") as HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "Post";
            request.Headers.Add("X-Requested-With", "XMLHttpRequest");
            request.ContentType = "application/json; charset=utf-8";
            request.Headers.Add("authorization", "Token 56a144af8cfa3ccca3fd8bde1a0f35d6ff8f45c3");
            var obj = new
            {
                app_ids = new object[] { "6g03y5wk9j4jworg" },
                data = new
                {
                    title = title,
                    content = desc,
                    buttons = new object[] {
                        new {
                                btn_order= 1,
                                btn_content = "ورود به پنل",
                                btn_action = new {
                                    url = $"https://lawmingo.com/Admin/{url}",
                                    action_type= "U"
                                }
                            }
                    }
                },
                platform = 2
            };
            byte[] byteArray = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj));

            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            }
        }
    }
}