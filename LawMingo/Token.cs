﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Text;
using System.Security.Cryptography;

namespace LawMingo
{
    public static class Token
    {
        static readonly char[] padding = { '=' };

        public static String GetHash(String text, String key)
        {
            // change according to your needs, an UTF8Encoding
            // could be more suitable in certain situations
            ASCIIEncoding encoding = new ASCIIEncoding();

            Byte[] textBytes = encoding.GetBytes(text);
            Byte[] keyBytes = encoding.GetBytes(key);

            Byte[] hashBytes;

            using (HMACSHA256 hash = new HMACSHA256(keyBytes))
                hashBytes = hash.ComputeHash(textBytes);

            return BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
        }

        public static string Generate(string reason, IdentityUser user)
        {
            byte[] _time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] _key = Encoding.UTF8.GetBytes(user.SecurityStamp.ToString());
            byte[] _Id = Encoding.UTF8.GetBytes(user.Id.ToString());
            byte[] _reason = Encoding.UTF8.GetBytes(reason);
            byte[] data = new byte[_time.Length + _key.Length + _reason.Length + _Id.Length];

            System.Buffer.BlockCopy(_time, 0, data, 0, _time.Length);
            System.Buffer.BlockCopy(_key, 0, data, _time.Length, _key.Length);
            System.Buffer.BlockCopy(_Id, 0, data, _time.Length + _key.Length , _Id.Length);
            System.Buffer.BlockCopy(_reason, 0, data, _time.Length + _key.Length + _Id.Length, _reason.Length);
            string res = Convert.ToBase64String(data.ToArray())
                .TrimEnd(padding).Replace('+', '-').Replace('/', '_');

            return res ;
        }
        public static TokenValidation Validate(string reason, IdentityUser user, string token)
        {
            var result = new TokenValidation();
            byte[] data = Convert.FromBase64String(token.Replace("_" , "+"));
            byte[] _time = data.Take(8).ToArray();
            byte[] _key = data.Skip(8).Take(32).ToArray();
            byte[] _Id = data.Skip(40).Take(36).ToArray();
            byte[] _reason = data.Skip(76).ToArray();

            Encoding.UTF8.GetString(data);

            DateTime when = DateTime.FromBinary(BitConverter.ToInt64(_time, 0));
            if (when < DateTime.UtcNow.AddHours(-24))
            {
                result.Errors.Add(TokenValidationStatus.Expired);
            }

            Guid gKey = new Guid(_key);
            if (gKey.ToString() != user.SecurityStamp)
            {
                result.Errors.Add(TokenValidationStatus.WrongGuid);
            }

            if (reason != Encoding.UTF8.GetString(_reason))
            {
                result.Errors.Add(TokenValidationStatus.WrongPurpose);
            }

            if (user.Id.ToString() != Encoding.UTF8.GetString(_Id))
            {
                result.Errors.Add(TokenValidationStatus.WrongUser);
            }

            return result;
        }
        public static TokenData Decode(string token)
        {
            var result = new TokenValidation();
            string incoming = token.Replace('_', '/').Replace('-', '+');
            switch (token.Length % 4)
            {
                case 2: incoming += "=="; break;
                case 3: incoming += "="; break;
            }

            byte[] data = Convert.FromBase64String(incoming);
            byte[] _time = data.Take(8).ToArray();
            byte[] _key = data.Skip(8).Take(32).ToArray();
            byte[] _Id = data.Skip(40).Take(36).ToArray();
            byte[] _reason = data.Skip(76).ToArray();
            var vv = Encoding.UTF8.GetString(data);

            DateTime when = DateTime.FromBinary(BitConverter.ToInt64(_time, 0));
            if (when < DateTime.UtcNow.AddHours(-24))
            {
                result.Errors.Add(TokenValidationStatus.Expired);
            }

            var tokenData = new TokenData
            {
                _key = Encoding.UTF8.GetString(_key),
                _reason = Encoding.UTF8.GetString(_reason),
                _Id = Encoding.UTF8.GetString(_Id),
                _time = when
            };

            return tokenData;
        }
        public class TokenValidation
        {
            public bool Validated { get { return Errors.Count == 0; } }
            public readonly List<TokenValidationStatus> Errors = new List<TokenValidationStatus>();
        }

        public enum TokenValidationStatus
        {
            Expired,
            WrongUser,
            WrongPurpose,
            WrongGuid
        }
        public class TokenData
        {
            public DateTime _time { get; set; }
            public string _key { get; set; }
            public string _Id { get; set; }
            public string _reason { get; set; }
        }
    }
}
